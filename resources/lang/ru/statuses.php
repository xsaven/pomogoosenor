<?php

return [
    'draft' => [
        'title' => 'Черновик', //редактировать
        'color' => 'gray'
    ],
    'opened' => [
        'title' => 'Открытый', //редактировать
        'color' => '#a2cf3a'
    ],
    'in-progress' => [
        'title' => 'В процессе', //похожее
        'color' => '#fcb53a'
    ],
    'finished' => [
        'title' => 'Выполнено', //похожее
        'color' => '#c60537'
    ],
    'canceled' => [
        'title' => 'Отмененный', //похожее
        'color' => 'red'
    ],
    'in-arbitration' => [
        'title' => 'В арбитраже', //похожее
        'color' => '#ff0000'
    ],
    'not-finished' => [
        'title' => 'Не выполнено', //похожее
        'color' => 'red'
    ],
    'not-moderated' => [
        'title' => 'Ожидает проверки модератором', //похожее
        'color' => 'red'
    ]
];