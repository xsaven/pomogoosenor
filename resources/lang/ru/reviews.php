<?php

return [
    'rate' => [
        'politeness' => 'Вежливость',
        'punctuality' => 'Пунктуальность',
        'adequacy' => 'Адекватность',
        'price' => 'Стоимость услуг',
        'quality' => 'Качество',
    ]
];