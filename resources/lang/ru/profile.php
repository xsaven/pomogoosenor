<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Языковые ресурсы аутентификации
    |--------------------------------------------------------------------------
    |
    | Следующие языковые ресурсы используются во время аутентификации для
    | различных сообщений которые мы должны вывести пользователю на экран.
    | Вы можете свободно изменять эти языковые ресурсы в соответствии
    | с требованиями вашего приложения.
    |
    */

    'tasks' => [
        'created_label' => '{1}Создала|{0}Создал',
        'executed_label' => '{1}Выполнила|{0}Выполнил',
        'was' => '{1}Была|{0}Был',
        'tasks' => '{1}:count задание|[2,4]:count задания|[5,*]:count заданий',
    ],
    'reviews' => [
        'count' => ':count отзыв|:count отзыва|:count отзывов'
    ],
    'age' => ':age год|:age года|:age лет',
];