<?php

return [
    'draft' => [
        'title' => 'Draft',
        'color' => 'gray'
    ],
    'opened' => [
        'title' => 'Opened',
        'color' => '#FFB53B'
    ],
    'in-progress' => [
        'title' => 'In progress',
        'color' => 'blue'
    ],
    'finished' => [
        'title' => 'Finished',
        'color' => '#a2cf3a'
    ],
    'canceled' => [
        'title' => 'Canceled',
        'color' => 'red'
    ],
    'in-arbitration' => [
        'title' => 'In arbitration',
        'color' => 'yellow'
    ]
];