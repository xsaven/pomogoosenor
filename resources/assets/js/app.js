
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
// import VueResource from "vue-resource"
window.Vue = require('vue');

import Echo from "laravel-echo"

require('lightgallery.js');



/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

const moment = require('moment-timezone');

moment().tz("Europe/Moscow").format();

require('moment/locale/ru');

window.moment = moment;

window.Pusher = require('pusher-js')
//window.Pusher.logToConsole = true;

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: window.PusherKey, //Add your pusher key here
    cluster: 'eu',
    encrypted: true,
    //forceTLS: true
    //client: require('pusher-js')
});



// EventBus Object

const EventBus = new Vue();

Object.defineProperties(Vue.prototype, {
    $bus: {
        get: function () {
            return EventBus
        }
    }
})


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/*
 *  Chat Component
 */
Vue.component('chat-layout', require('./components/chat/Layout.vue'));  //Main layout
Vue.component('chat-list', require('./components/chat/ChatList.vue'));   //User list
Vue.component('chat-message', require('./components/chat/ChatMessage.vue'));  //Message list
Vue.component('simple-chat', require('./components/chat/SimpleChatForm.vue'));   //Form for chat with user (Message list + Send message form)

/*
 *  Header Event Counter
 */
Vue.component('event-counter', require('./components/notification/EventCounter.vue'));
Vue.component('notification', require('./components/notification/Notification.vue'));  //Message list



const app = new Vue({
    el: '#app'
});

let vue_chat_wrapper = new Vue({
    el: '#vue-chat-wrapper'
});

window.VueChat = vue_chat_wrapper.$children[0];

