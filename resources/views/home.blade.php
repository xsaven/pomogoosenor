@extends('layouts.basic')

@section('title')Добро пожаловать @endsection
@section('body')
    <div class="l-page-wrapper main-page-wrapper">
        <div class="main-page__top">
            @widget('MainMenu',['classes' => 'c-header_transparent'])
            <div class="l-main-slider__wrp">
                @widget('Slider')
                <div class="l-main-slider__content">
                    <div class="container">
                        <div class="l-main-slider__cont-wrp">
                            <h1>{{$homeInfo['title']}}</h1>
                            <div class="l-mn-slider__sub-head">{{$homeInfo['sub-title']}}</div>
                            <form class="l-main-slider__form" method="post" action="{{route('task.search')}}">
                                {{csrf_field()}}
                                <input type="search" name="q" placeholder="{{$homeInfo['search-placeholder']}}"
                                       class="form-control"/>
                                <button type="submit" class="but but_bondi"><span>{{$homeInfo['search-button']}}</span>
                                </button>
                            </form>
                            <div class="l-main-slider__annotation">{{$homeInfo['search-example']}}</div>
                            @if(!(\Auth::user() && \Auth::user()->profile && \Auth::user()->profile->moderated_worker))
                                <a style="cursor: pointer"
                                        @if(\Auth::user())
                                        href="{{url('testing/start')}}"
                                        @else
                                        data-toggle="modal" data-target="#fastregmail"
                                        @endif
                                        class="but but_y-orange">
                                    <span>Cтать исполнителем и начать зарабатывать</span></a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="l-propose">
            <div class="container">
                <h4>{{$homeInfo['categories-title']}}</h4>
                <div class="row row_main">
                    @foreach($catTree as $key => $parent)

                        @if($key < 4)
                            <div class="col-md-3 col-sm-3">
                                <div class="l-propose__item">
                                    <div class="l-propose__item-ico">
                                        <svg width="1em" height="1em" class="icon {{$parent->icon}} ">
                                            <use xlink:href="{{ symbolSvg($parent->icon) }}"></use>
                                        </svg>
                                    </div>
                                    <h6 style="min-height: 90px; position: relative;">{{$parent->name}}</h6>
                                    @if($parent->subcategories)
                                        <ul class="l-propose__item-links h-list minimized">
                                            @foreach($parent->subcategories as $child)
                                                <li><a href="{{route('task.new', ['category' => $parent->slug,'subcategory' => $child->slug, 'select' => true,])}}">{{$child->name}}</a></li>
                                            @endforeach
                                        </ul>
                                        @if(count($parent->subcategories) > 2)
                                            <button type="button"><span>Показать все категории</span></button>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        @endif
                    @endforeach

                </div>
                <div class="l-propose__grid">
                    <div class="row">
                        @foreach($catTree as $key => $parent)
                            @if ($key >= 4)
                                <div class="col-md-3 col-sm-3">
                                    <a href="{{route('task.new', [
                                                    'category' => $parent->slug,
                                                    'subcategory' => false
                                                    ])}}" class="l-propose__grid-item">
                                        <div class="l-propose__grid-item-ico">
                                            <svg style="width: 30px; height: 28px;" class="icon {{$parent->icon}} ">
                                                <use xlink:href="{{ symbolSvg($parent->icon) }}"></use>
                                            </svg>
                                        </div>
                                        <div class="l-propose__grid-item-name">{{$parent->name}}</div>
                                    </a>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
                <a href="{{route('categories_list')}}" class="l-propose__button but but_bondi">
                    <span>{{$homeInfo['all-categories']}}</span>
                </a>
            </div>
        </div>
        {!! $homeInfo['main-info-block'] !!}

        <div class="l-publications">
            <div class="container">
                <h4>{{$homeInfo['blog-title']}}</h4>
                <div class="row">
                    @if(!$lastBlog->isEmpty())
                        @foreach($lastBlog as $post)
                            <div class="col-md-6 col-sm-6">
                                <a href="#" class="l-publications__item">
                                    <div class="l-publications__item-img h-object-fit">
                                        <img src="{{url(''.$post->preview)}}" alt=""/>
                                    </div>
                                    <div class="l-publications__item-txt">
                                        <div class="l-publications__item-ttl">{{ $post->title }}</div>
                                        <div class="l-publications__item-about">{{ $post->subtitle }}</div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="l-publications__question">
                    <div class="l-publications__question-ttl">{{$homeInfo['story-block-title']}}</div>
                    <div class="l-publications__question-btns"><a href="{{url('task-categories')}}"
                                                                  class="but but_white"><span>{{$homeInfo['add-task-text']}}</span></a><span
                                class="or">или</span><a @if(\Auth::user()) href="{{url('testing/start')}}"
                                                        @else data-toggle="modal" data-target="#fastregmail"
                                                        @endif class="but but_white"><span>{{$homeInfo['executor-text']}}</span></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="l-to-order">
            <div class="container">
                <h4>{{$homeInfo['bottom-block-title']}}</h4>
                <div class="l-to-order__content">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="l-to-order__articles">
                                @foreach($tasks as $task)
                                    <a href="{{url('task/preview', ['userTasks' => $task])}}"
                                       class="l-to-order__articles-item">
                                        <div class="l-to-order__articles-ico">
                                            <svg width="1em" height="1em" class="icon icon-comp ">
                                                <use xlink:href="{{ symbolSvg($task->subcategory->category->icon) }}"></use>
                                            </svg>
                                        </div>
                                        <div class="l-to-order__articles-about">
                                            <div class="l-to-order__articles-ttl">{{$task->name}}</div>
                                            <div class="l-to-order__articles-txt">{{$task->description}}</div>
                                        </div>
                                    </a>
                                @endforeach
                            </div>
                            <div class="l-to-order__button"><a href="{{url('task/all')}}"
                                                               class="but but_bondi"><span>{{$homeInfo['all-tasks-button']}}</span></a>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="l-to-order__cards-wrp">
                                <div class="l-to-order__cards-links">
                                    <div class="l-to-order__cards-links"><a href="{{url('how-performer')}}"
                                                                            class="l-to-order__cards-item">
                                            <div class="l-to-order__cards-item-img h-object-fit"><img
                                                        src="{{ url('require/dist/s/images/tmp_file/card1.jpg') }}"
                                                        alt=""/>
                                            </div>
                                            <span>Как стать исполнителем</span></a><a href="{{url('security')}}"
                                                                                      class="l-to-order__cards-item">
                                            <div class="l-to-order__cards-item-img h-object-fit"><img
                                                        src="{{ url('require/dist/s/images/tmp_file/card2.jpg') }}"
                                                        alt=""/>
                                            </div>
                                            <span>Безопасность и гарантии</span></a><a href="{{url('task/all')}}"
                                                                                       class="l-to-order__cards-item">
                                            <div class="l-to-order__cards-item-img h-object-fit"><img
                                                        src="{{ url('require/dist/s/images/tmp_file/card3.jpg') }}"
                                                        alt=""/>
                                            </div>
                                            <span>Самые необычные задания</span></a>
                                    </div>
                                </div>
                                <div class="l-to-order__widgets">
                                    <div class="l-to-order__widget-facebook">
                                        <div data-href="https://www.facebook.com/facebook" data-tabs=""
                                             data-height="252" data-small-header="true"
                                             data-adapt-container-width="true" data-hide-cover="false"
                                             data-show-facepile="true" class="fb-page">
                                            <blockquote cite="https://www.facebook.com/facebook"
                                                        class="fb-xfbml-parse-ignore"><a
                                                        href="https://www.facebook.com/facebook">Facebook</a>
                                            </blockquote>
                                        </div>
                                    </div>
                                    <div class="l-to-order__widget-twitter"><a href="https://twitter.com/TwitterDev"
                                                                               data-size="large"
                                                                               class="twitter-follow-button">Follow
                                            @TwitterDev</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @widget('Footer')
    </div>

@endsection

@section('script')
    <script>
        window.onload = function () {
            var xhr;
            new autoComplete({
                selector: 'input[name="q"]',
                source: function (term, response) {
                    try {
                        xhr.abort();
                    } catch (e) {
                    }
                    xhr = $.getJSON('{{route('find_subcategory')}}', {query: term}, function (data) {
                        response(data);
                    });
                },
                renderItem: function (item, search) {
                    search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                    var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
                    return '<div class="home-search-item"><a href="' + item.url + '">' + item.name + '</a></div>';
                }
            });
        };

    </script>
@endsection