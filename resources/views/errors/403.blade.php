<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Pomogoo | 404</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;amp;subset=cyrillic"
          rel="stylesheet"/>
    <link href="s/css/all.css" rel="stylesheet"/>
</head>
<body>
<div id="fb-root"></div>
<div class="l-page-wrapper">
    <header class="c-header js-header">
        <div class="container">
            <div class="c-header__wrp clearfix">
                <div class="c-header__left">
                    <button type="button" class="c-header__menu_small-trigger"><i></i><i></i><i></i></button>
                    <a href="#" class="c-header__logo"><span>Pomogoo</span></a>
                    <ul class="h-list c-header__menu_small">
                        <li><a href="#">Как это работает</a></li>
                        <li><a href="#">Как стать исполнителем</a></li>
                        <li><a href="#">Наш блог</a></li>
                        <li><a href="#">Отзывы заказчиков</a></li>
                        <li><a href="#">Правила сервиса</a></li>
                        <li><a href="#">Частые вопросы</a></li>
                        <li><a href="#">Контакты</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <main>
        <div class="container">
            <div class="error-block">
                <div class="error-block__txt"><span>403</span>
                    <svg width="1em" height="1em" class="icon icon-fourofour ">
                        <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-fourofour"></use>
                    </svg>
                </div>
                <div class="error-block__content"><span>Нет доступа</span><span><a
                                href="{{route('home')}}" class="link_bondi">На главную</a></span></div>
            </div>
        </div>
    </main>
</div>
<script data-main="s/js/main" src="s/js/require.js"></script>
</body>
</html>