<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Pomogoo | 404</title>
    @include('_partials._head')
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="{{asset('/js/app.js')}}"></script>
</head>
<body>
<div id="fb-root"></div>
<div class="l-page-wrapper">
    @widget('MainMenu')
    <main>
        <div class="container">
            <div class="error-block">
                <div class="error-block__txt"><span>404</span>
                    <svg width="1em" height="1em" class="icon icon-fourofour ">
                        <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-fourofour')}}"></use>
                    </svg>
                </div>
                <div class="error-block__content"><span>Такой страницы у нас нет</span><span>Зато у нас есть <a
                                href="{{route('task.all')}}"
                                class="link_bondi">страница с заданиями</a></span>
                </div>
            </div>
        </div>
    </main>
</div>
@include('_partials._footer')
</body>
</html>