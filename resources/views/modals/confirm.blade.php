<div id="confirm" class="modal fade modal-mob-numb">
    <div class="modal-dialog modal-dialog__fastreg">
        <div class="modal-dialog">
            <div class="modal-body">
                <button type="button" data-dismiss="modal" class="but-modal-close"></button>
                <div class="modal-welcome__txt modal-welcome__txt-left">
                    <h2>Подтвердите адрес вашей почты</h2>
                    <p>На ваш электронный адрес <span>{{auth()->user()->email}}</span> было отправлено письмо со
                        ссылкой
                        для
                        подтверждения вашей почты на Pomogoo.</p>
                    <p>Перейдите по ссилке и активируйте вашу электронную почту.</p>
                </div>
                <form method="POST" action="{{ route('send_email') }}" id="sendRegisterEmail">
                    {{csrf_field()}}
                    <div class="modal-welcome__bottom modal-welcome__bottom-left">
                        <button type="submit"
                                class="link_bondi">Отправить
                            новое письмо для подтверждения почты
                        </button>
                        <a href="{{url('/profile/settings#email')}}" class="underline">Указать другую почту</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="email-sent-modal" class="modal fade modal-mob-numb">
    <div class="modal-dialog">
        <div class="modal-body">
            <button type="button" data-dismiss="modal" class="but-modal-close"></button>
            <div class="modal-reg_success__txt">
                <h5 id="email-sent-modal-message" style="margin-top: 20px;"></h5>
            </div>
        </div>
    </div>
</div>