<div id="modal-mob-numb" class="modal fade modal-mob-numb">
    <div class="modal-dialog">
        <form class="modal-content l-bid-write__form" id="sendPhoneVerification" method="POST"
              action="{{ route('send_phone_verification') }}">
            <button type="button" data-dismiss="modal" class="but-modal-close"></button>
            <div class="modal-mob-numb__ttl">Подтвердите номер телефона</div>
            <div class="l-bid-write__contacts clearfix">
                <div class="tel-wrap">
                    <select class="selectpicker form-data" name="phone_code">
                        @foreach($def_phones = ['+7'] as $phone_code)
                            <option value="{{$phone_code}}" {{ in_array($user->profiles->phone_code, $def_phones)?'selected':'' }}>{{$phone_code}}</option>
                        @endforeach
                    </select>
                    <label>
                        <input type="tel" placeholder="Ваш телефон" class="form-control form-data tel-input"
                               value="{{ $user->profiles->phone }}" name="phone_number">
                    </label>
                </div>
                <button type="submit" class="but but_bondi"><span>Подтвердить</span></button>
            </div>
            <p class="allert" id="modal-mob-error"></p>
        </form>
    </div>
</div>
<div id="modal-sms" class="modal fade modal-mob-numb modal-sms">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" data-dismiss="modal" class="but-modal-close"></button>
            <div class="modal-mob-numb__ttl">Подтверджения телефона</div>
            <p>На указанный номер {{ user()->profile->full_phone }}</p>
            <p>мы выслали код подтверждения</p>
            <form class="modal-sms__form l-bid-write__form" id="checkPhoneVerification" method="GET"
                  action="{{ route('check_phone_verification') }}">
                <p>Введите полученный код, чтобы подтвердить свой номер телефона.</p>
                <div class="modal-sms__form-enter clearfix">
                    <label>
                        <input type="number" class="form-control tel-input form-data" name="code">
                    </label>
                    <button type="submit" class="but but_bondi"><span>ОК</span></button>
                </div>
                <p class="allert" id="modal-sms-error"></p>
            </form>
            <a id="call-modal-mob-numb" class="link_bondi">Попробовать еще раз</a>
        </div>
    </div>
</div>
<div id="modal-verify" class="modal fade modal-mob-numb modal-verify">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" data-dismiss="modal" class="but-modal-close"></button>
            <div class="modal-mob-numb__ttl">Ваш телефон успешно <br>верифицирован</div>
            <button class="but but_bondi" type="button" data-dismiss="modal">
                ОК
            </button>
        </div>
    </div>
</div>