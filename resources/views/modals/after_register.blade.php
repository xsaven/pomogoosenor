@if(auth()->check() && session()->has('first_login'))
    <div id="after_register" class="modal fade modal-mob-numb">
        <div class="modal-dialog">
            <div class="modal-body">
                <button type="button" data-dismiss="modal" class="but-modal-close"></button>
                <div class="modal-welcome__txt">
                    <h2>Добро пожаловать!</h2><span>Вы успешно зарегестрированы на сервисе Pomogoo. Регистрационные данные отправлены на вашу почту и указаны под этим сообщением.</span>
                </div>
                <div class="modal-welcome__info">
                    <p>Обязательно сохраните ваш доступ к сайту:</p>
                    <div class="modal-welcome__info-wrap">
                        <span>Логин: {{auth()->user()->email}}</span>
                        <span>Пароль: {{session()->get('first_login')}}</span>
                    </div>
                </div>
                <div class="modal-welcome__bottom">
                    <a href="{{url('/how-performer')}}" class="link_bondi">
                        Хотите стать исполнителем Pomogoo?
                    </a>
                    <button type="button" data-dismiss="modal" class="but but_bondi">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
@endif