<div class="modal_fast-reg__socials">
    <a href="{{route('social_mailru_auth')}}" class="registration-content__socials">
        <svg width="1em" height="1em" class="icon icon-mailrubig ">
            <use xlink:href="{{ symbolSvg('icon-mailrubig') }}"></use>
        </svg>
    </a>
    <a href="{{route('social_vk_auth')}}" class="registration-content__socials">
        <svg width="1em" height="1em" class="icon icon-vkbig ">
            <use xlink:href="{{ symbolSvg('icon-vkbig') }}"></use>
        </svg>
    </a>
    <a href="{{ route('social_facebook_auth') }}" class="registration-content__socials">
        <svg width="1em" height="1em" class="icon icon-fbbig ">
            <use xlink:href="{{ symbolSvg('icon-fbbig') }}"></use>
        </svg>
    </a>
    <a href="{{ route('social_google_auth') }}" class="registration-content__socials registration-content__socials-google">
        <svg width="1em" height="1em" class="icon icon-googlebig ">
            <use xlink:href="{{ symbolSvg('icon-googlebig') }}"></use>
        </svg>
    </a>
</div>