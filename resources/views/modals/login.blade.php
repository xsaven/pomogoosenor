<div id="loginModal" class="modal fade modal-mob-numb">
    <div class="modal-dialog modal-dialog__fastreg">
        <div class="modal-body">
            <button type="button" data-dismiss="modal" class="but-modal-close"></button>
            <div class="modal-welcome__txt">
                <p class="big_p">Быстрая регистрация через</p>
            </div>

            @include('modals.parts.social-button')

            <div class="modal-welcome__txt">
                <p class="big_p">Вход по паролю</p>
            </div>
            <form method="POST" id="loginForm" action="{{ route('login') }}">

                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="steps-content__form-input">
                    <label>
                        <input data-rule-required="true" data-msg-required="Email - обязательное поле"
                               data-rule-email="true" data-msg-email="Неверный Email"
                               name="email" type="email" placeholder="Email" class="form-control form-data">
                        <span class="errorMsg"></span>
                    </label>
                </div>
                <div class="steps-content__form-input">
                    <label>
                        <input data-rule-required="true" data-msg-required="Пароль - обязательное поле"
                               name="password" type="password" placeholder="Password" class="form-control form-data">
                        <span class="errorMsg"></span>
                    </label>
                </div>
                <div class="modal-welcome__bottom-check modal-welcome__bottom-check__fastreg">
                    <button type="submit" class="but but_bondi submit-btn">Войти</button>
                </div>
            </form>

            <div class="modal_mypass"><a href="{{route('password.request')}}" class="link_bondi_grey">Забыли пароль?</a>
            </div>
            <div class="modal_mypass modal_mypass__between"><a href="#" id="register-button-alt">Еще не с нами</a><a
                        id="register-button" href="#" class="link_bondi">Зарегистрируйтесь</a></div>
        </div>
    </div>
</div>

@if (session('login') === true)
    @push('scripts')
        <script>
            requirejs(['plugins/bs/modal'], function () {
                jQuery("#fastregmail").modal();
            });

        </script>
    @endpush
@endif
