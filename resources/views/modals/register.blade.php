<div id="fastregmail" class="modal fade modal-mob-numb">
    <div class="modal-dialog modal-dialog__fastreg">
        <div class="modal-body">
            <button type="button" data-dismiss="modal" class="but-modal-close"></button>
            <div class="modal-welcome__txt">
                <p class="big_p">Быстрая регистрация через</p>
            </div>
            @include('modals.parts.social-button')
            <div class="modal-welcome__txt">
                <p class="big_p">или по e-mail</p>
            </div>

            <form method="POST" id="fastRegForm" action="{{ route('get_email') }}">
                {{csrf_field()}}

                <div class="steps-content__form-input">
                    <label>
                        <input name="email" type="email" placeholder="Email" class="form-control form-data">
                    </label>
                </div>
                <div class="modal-welcome__bottom-check modal-welcome__bottom-check__fastreg">
                    <button type="submit" class="but but_bondi submit-btn">Зарегистрироваться</button>
                </div>

                <div class="modal_mypass modal_mypass__between">Уже зарегистрированы?
                    <a href="#" id="login-button" class="link_bondi">Войдите</a>
                </div>
                <div class="step_access step_access__link step_access step_access__link-modal">
                    <label class="check-label">
                        <input name="confirm_rules" class="form-data" value="1" type="checkbox"><i class="checkbox"></i>
                    </label><span>Я согласен с <a target="_blank" href="{{url('rules')}}"
                                                  class="link_bondi">правилами</a></span>
                </div>
            </form>
        </div>
    </div>
</div>
