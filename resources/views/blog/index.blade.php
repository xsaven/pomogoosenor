@extends('layouts.basic')
@section('title') Блог @stop
@section('body')
    <div class="l-page-wrapper">
        <div class="blog-main__page-wrap">
            <div class="blog-main__banner">
                <div class="blog-insane__banner-img h-object-fit">
                    <img src="{{url('require/dist/s/images/tmp_file/blog_banner.jpg')}}" alt="Blog">
                </div>
                <div class="container">
                    <h1>Блог Pomogoo</h1>
                    <div class="blog-main__banner-subheader">Делимся интересными историями наших пользователей, необычными заданиями и полезными бытовыми советами</div>
                    <form method="POST" action="{{url('email-subscribe')}}" class="blog-main__banner-form subscribe_form">
                        <label>
                            <input name="email" class="form-data" type="email" placeholder="Укажите ваш e-mail">
                        </label>
                        <button type="sumbit"><span>Подписаться</span></button>
                    </form>
                </div>
            </div>
            <div class="blog-main__content">
                <div class="container">
                    @if($posts->isEmpty())
                        <h3>Записи не найдены</h3>
                    @else
                        @foreach($posts as $post)
                            @include('blog.parts.preview',compact('post'))
                        @endforeach
                    @endif
                    {{ $posts->links() }}
                </div>
            </div>
            @include('blog.parts.footer')
        </div>
    </div>
@stop