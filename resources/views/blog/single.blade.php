@extends('layouts.basic')
@section('title') {{$post->title}} @stop
@section('body')
    <div class="l-page-wrapper">
        <div class="blog-insane__page-wrap">
            <div class="blog-insane__banner">
                <div class="container">
                    <div class="blog-insane__banner-img h-object-fit">
                        @if($post->preview)
                            <img src="{{url($post->preview)}}" alt="{{$post->title}}">
                        @endif
                    </div>
                    <div class="blog-insane__banner-txt">
                        <div class="blog-insane__banner-top">Блог Pomogoo</div>
                        <div class="blog-insane__banner-middle">{{$post->title}}</div>
                        <div class="blog-insane__banner-bottom">{{$post->subtitle}}</div>
                    </div>
                </div>
            </div>
            <div class="blog-insane__content">
                <div class="container">
                    @if($post->user && $post->user->profile)
                        @include('blog.parts.author',array(
                            'user' => $post->user
                        ))
                    @endif
                    <div class="blog-insane__user-text">
                        @if(!is_null($post->content))
                            @php($content = json_decode($post->content))
                            @if(is_array($content) && !empty($content))
                                @foreach($content as $section)
                                    @include('blog.parts.sections.'.$section->type,array(
                                        'section' => $section
                                    ))
                                @endforeach
                            @endif
                        @endif
                    </div>
                    @if($post->titres)
                        <div class="blog-insane__user-titles">
                            <strong>Титры</strong>
                            {!! $post->titres !!}
                        </div>
                    @endif

                        <br/>
                        <a href="{{url('blog/posts')}}" style="font-size: 18px;">Вернуться назад</a>

                    @if($related)
                        @include('blog.parts.related',compact('related'))
                    @endif

                </div>
            </div>
            @include('blog.parts.footer')
        </div>
    </div>
@stop