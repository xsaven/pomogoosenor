<div class="blog-insane__subscribe">
    <div class="container">
        <h6>Подпишитесь на нашу расылку</h6><span>И получайте уведомления о новых материалах раньше других</span>
        <form method="POST" action="{{url('email-subscribe')}}" class="subscribe_form">
            <label>
                <input name="email" class="form-data" type="email" placeholder="Укажите ваш e-mail">
            </label>
            <button type="submit"><span>Получить уведомления</span></button>
        </form>
    </div>
</div>
<div class="blog-insane__footer">
    <a href="#" class="blog-insane__footer-btn">
        <span>Наверх</span>
        <svg width="1em" height="1em" class="icon icon-top ">
            <use xlink:href="{{ symbolSvg('icon-top') }}"></use>
        </svg>
    </a>
    <div class="container">
        <ul>
            <li><a href="{{ url('how-performer') }}">Как стать исполнителем  Pomogoo</a></li>
            <li><a target="_blank" rel="nofollow" href="{{ get_setting('fb_url') }}">Мы в Facebook</a></li>
            <li><a target="_blank" rel="nofollow" href="{{ get_setting('vk_url') }}">Мы в ВКонтакте</a></li>
            <li><a target="_blank" rel="nofollow" href="{{ get_setting('tw_url') }}">Мы в Twitter</a></li>
            <li><a target="_blank" rel="nofollow" href="{{ get_setting('yt_url') }}">Мы в Youtube</a></li>
        </ul>
    </div>
</div>