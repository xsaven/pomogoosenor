<a href="{{ url('blog/post/'.$post->slug) }}" class="blog-main__item clearfix">
    @if($post->preview)
        <div class="blog-main__item-img h-object-fit">
            <img src="{{ url($post->preview) }}" alt="">
        </div>
    @endif
    <div class="blog-main__item-txt">
        {{--<span>Интервью с исполнителем</span>--}}
        <b>{{ $post->title }}</b>
        @if($post->subtitle)
            <span>{{ $post->subtitle }}</span>
        @endif
    </div>
</a>