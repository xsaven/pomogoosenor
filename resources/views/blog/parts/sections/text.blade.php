@if($section->title && mb_strlen($section->title) > 0)
    <b>- {{$section->title}}</b>
@endif
{!! $section->content !!}
