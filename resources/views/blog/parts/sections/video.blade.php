<div class="blog-insane__user-text-img">
    {!! $section->embed !!}
    @if($section->caption && mb_strlen($section->caption) > 0)
        <span>{{$section->caption}}</span>
    @endif
</div>