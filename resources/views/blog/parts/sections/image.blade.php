<div class="blog-insane__user-text-img">
    <div class="h-object-fit">
        <img src="{{url($section->path)}}" alt="{{$section->caption}}">
    </div>
    @if($section->caption && mb_strlen($section->caption) > 0)
        <span>{{$section->caption}}</span>
    @endif
</div>