<div class="blog-insane__user-arts">
    <h4>Другие материалы</h4>
    <div class="row">
        @foreach($related as $post)
            <div class="col-md-4 col-sm-4">
                <a href="{{url('blog/post/'.$post->slug)}}" class="blog-insane__user-arts-item">
                    @if($post->preview)
                        <div class="h-object-fit">
                            <img src="{{url($post->preview)}}" alt="{{$post->title}}">
                        </div>
                    @endif
                    <span>{{$post->title}}</span>
                </a>
            </div>
        @endforeach
    </div>
</div>