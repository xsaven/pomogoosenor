<div class="blog-insane__user-preview">
    <div class="blog-insane__user-img h-object-fit">
        <img src="{{$user->profile->avatar()}}" alt="">
    </div>
    <div class="blog-insane__user-name">{{$user->name}}</div>
    <div class="blog-insane__user-reg-date">На Pomogoo с {{$user->profile->registeredFrom()}}</div>
</div>