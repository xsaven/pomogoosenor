<?php
/** @var \App\UserTasks $task */
?>
@foreach($tasks as $task)
    <div class="my-tasks__item">
        <div class="my-tasks__item-icon">
            <svg width="1em" height="1em" class="icon {{$task->subcategory->category->icon}}">
                <use xmlns:xlink="http://www.w3.org/1999/xlink"
                     xlink:href="{{asset('/s/images/useful/svg/theme/symbol-defs.svg')}}#{{$task->subcategory->category->icon}}">
                </use>
            </svg>
        </div>
        <div class="my-tasks__item-info">
            <div class="all-tasks__list-item-left">
                <div class="all-tasks__list-item-title"><a
                            href="{{route('task.review', ['userTasks' => $task->id])}}">{{$task->name}}</a></div>
                <div class="my-tasks__item-status" style="color: {{$task->status_color()}};">
                    <div class="my-tasks__item-status{{($type != 'executor'&&$task->status != 'canceled')?'-txt':''}}"
                         style="position: relative">
                        <svg width="1em" height="1em" class="icon icon-mark ">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                 xlink:href="{{asset('/s/images/useful/svg/theme/symbol-defs.svg')}}#icon-mark"></use>
                        </svg>
                        {!! $task->status() !!}
                    </div>
                    @if($type != 'executor')
                        <div class="my-tasks__item-buttons">
                            @if($task->status == 'draft')
                                <button type="button" class="but but_green but_xs">Опубликовать</button>
                            @endif
                            @if($task->status == 'opened')
                                <a href="#"
                                   data-toggle="modal"
                                   data-target="#cancel-task-modal"
                                   data-task-id="{{$task->id}}"
                                   class="but but_t-orange but_link {{$task->status == 'draft'?:'pull-left'}}">Удалить</a>
                            @endif
                        </div>
                    @endif
                </div>
            </div>
            <div class="all-tasks__list-item-right">
                <div class="all-tasks__list-item-price">
                    <span>
                        {{($task->exact_cost>0)?$task->exact_cost:$task->cost   }} руб.
                    </span>
                </div>
            </div>
        </div>
    </div>
@endforeach
{{ $tasks->appends(['type'=>$type])->links() }}