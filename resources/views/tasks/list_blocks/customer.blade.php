<div class="all-tasks__list">
    <?php
    /** @var \Illuminate\Pagination\LengthAwarePaginator $userTasks */
    ?>
    @if($tasks->isEmpty())
        <div class="all-tasks__list-empty">
            <div class="all-tasks__list-empty-icon">
                <svg width="1em" height="1em" class="icon icon-big-pointer icon-1">
                    <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg')}}#icon-big-pointer"></use>
                </svg>
                <svg width="1em" height="1em" class="icon icon-big-pointer icon-2">
                    <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg')}}#icon-big-pointer"></use>
                </svg>
            </div>
            <div class="all-tasks__list-empty-info">
                <div class="all-tasks__list-empty-ttl"><span>Нет заданий</span></div>
                <div class="all-tasks__list-empty-txt"><span><a href="{{url('/how-it-work')}}">Посмотрите как это работает</a></span></div>
            </div>
        </div>
    @endif
    <div data-type="my-tasks-creator">
        @include('tasks.list_blocks._task_items',['type'=>'creator'])
    </div>
</div>