<?php /** @var \App\TaskOffers[] $offers */ ?>

@foreach($offers as $offer)
    @php $r_data = $offer->user->getChatRelatedData(user()) @endphp
    <div class="l-task-card__bid-propose clearfix">
        <div class="l-task-card__bid-propose-wrp clearfix">
            <a href="#" class="bid-propose__img h-object-fit">
                <img src="{{$offer->user->avatar()}}"
                     alt="Аватар пользователя"/>
            </a>
            <div class="bid-propose__txt">
                <div class="bid-propose__head">
                    <a href="{{route('profile_show',$offer->user)}}"
                       class="bid-propose__head-name">
                        {{ $offer->user->name }}
                    </a>
                    <div class="bid-propose__head-icons">
                        @include('blocks.user.badges_small', ['executor' => $offer->user])
                    </div>
                    @if($offer->user->positive_reviews_count || $offer->user->negative_reviews_count)
                        <div class="bid-propose__head-revs">
                            <div class="reviews__body-user-about">
                                <span class="about-left">Отзывы</span>
                                <div class="about-right">
                                    @if($offer->user->positive_reviews_count)
                                        <svg width="1em" height="1em"
                                             class="icon icon-like-2 ">
                                            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-like-2')}}"></use>
                                        </svg>
                                        <span>{{$offer->user->positive_reviews_count}}</span>
                                    @endif
                                    @if($offer->user->negative_reviews_count)
                                        <svg width="1em" height="1em"
                                             class="icon icon-dislike ">
                                            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-like-2')}}"></use>
                                        </svg>
                                        <span>{{$task->creator->negative_reviews_count}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="bid-propose__rating">
                    @if($rating = $offer->user->getPositionByPositiveReviewsByCategory($task->subcategory->id))
                        {{$rating}} место в категории «{{$task->subcategory->name}}»
                    @endif
                </div>
                <div class="bid-propose__message">
                    <div class="bid-propose__message-head">
                        <b>Примерная стоимость
                            @if($task->is_creator)
                                {{$offer->cost}}
                            @else
                                {{$offer->cost}}
                            @endif
                            Р</b>
                    </div>
                    <div class="bid-propose__message-body">
                        <p>{{$offer->message}}</p>
                    </div>
                    @if($r_data['show_phone'])
                    <div class="bid-propose__message-footer">
                        <p>Телефон
                            исполнителя: {{$offer->user->profile->full_phone??''}}</p>
                    </div>
                    @endif
                    <div class="bid-propose__message-btns">
                        <?php
                        /**
                         * @var \App\UserTasks $task
                         */
                        ?>
                        @if($task->is_creator)
                            @if($task->getOriginal('status') == \App\UserTasks::STATUS_OPENED && !$task->locked)
                                <a href="#" data-toggle="modal"
                                   data-target="#select-executor-modal"
                                   data-user-id="{{$offer->user->id}}"
                                   data-user-name="{{$offer->user->name}}"
                                   data-cost="{{$offer->cost}}"
                                   class="but but_green">
                                    Выбрать исполнителем
                                </a>
                            @endif
                            <a href="javascript:;" onclick="VueChat.getChatByUserId({{$offer->user->id}})">Написать в
                                Чат</a>
                        @elseif($task->getOriginal('status') == \App\UserTasks::STATUS_OPENED)
                            @if($offer->user->id == user()->id)
                                {{ Form::open(['method' => 'DELETE', 'route' => ['task.offers.remove', $offer],'class'=>"pull-left"]) }}
                                <a href="javascript:;" onclick="parentNode.submit();">Удалить</a>
                                {{ Form::close() }}
                                <a href data-toggle="modal"
                                   data-target="#task-offer-modal">Редактировать</a>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="l-task-card__bid-propose-time">{{ \Date\DateFormat::dateOnly($offer->created_at) }}</div>
    </div>
@endforeach