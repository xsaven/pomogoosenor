<div class="all-tasks__sidebar-collapses">
    <form id="categories-filter">
        <div class="l-prof-set__subs-item">
            <div class="l-prof-set__subs-item-but collapsed">
                <label class="check-label main-label">
                    <input type="checkbox" value="all" class="category-checkbox"/>
                    <i class="checkbox"></i>
                    <span>Все категории </span>
                </label>
            </div>
        </div>
        @foreach($categories as $key => $category)
            <div class="l-prof-set__subs-item">
                <div data-toggle="collapse" data-target="#serv_{{$key}}" class="l-prof-set__subs-item-but">
                    <label class="check-label main-label">
                        <input type="checkbox"/>
                        <i class="checkbox"></i>
                        <span>{{$category->name}}</span>
                    </label>
                </div>
                <div id="serv_{{$key}}" class="l-prof-set__subs-item-cont collapse">
                    <div class="l-prof-set__subs-item-cont-wrp">
                        @foreach($category->subcategories as $subcategory)
                            <label class="check-label">

                                <input type="checkbox"
                                       {{in_array($subcategory->id, $sort_data['subcategories']??[])?'checked':''}} value="{{$subcategory->id}}"
                                       class="category-checkbox"/><i
                                        class="checkbox"></i><span>{{$subcategory->name}} </span>
                            </label>
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach
    </form>
</div>