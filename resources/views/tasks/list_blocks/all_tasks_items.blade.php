<?php
/** @var \App\UserTasks $task */
?>

@foreach($tasks as $task)
    @if(!$task->creator) @continue @endif
    <div class="all-tasks__list-item">
        <div class="all-tasks__list-item-inner">
            <div class="all-tasks__list-item-left">
                <div class="all-tasks__list-item-title"><a
                            href="{{route('task.review', ['userTasks' => $task->id])}}">{{$task->name}}</a>
                </div>
                <div class="all-tasks__list-item-user">
                    <div class="all-tasks__list-item-user-image">
                        <div class="h-object-fit">
                            <img src="{{$task->creator->avatar()}}" alt="Аватар пользователя"/>
                        </div>
                    </div>
                    <div class="all-tasks__list-item-user-info">
                        <div class="all-tasks__list-item-user-name"><a
                                    href="{{route('profile_show',$task->creator)}}">{{$task->creator->name}}</a>
                        </div>
                        <div class="all-tasks__list-item-reviews">
                            @if($task->creator->positive_reviews_count || $task->creator->negative_reviews_count)
                                <span>Отзывы</span>
                                @if($task->creator->positive_reviews_count)
                                    <svg width="1em" height="1em" class="icon icon-like-2 ">
                                        <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-like-2')}}"></use>
                                    </svg>
                                    <span>{{$task->creator->positive_reviews_count}}</span>
                                @endif
                                @if($task->creator->negative_reviews_count)
                                    <svg width="1em" height="1em" class="icon icon-dislike ">
                                        <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-like-2')}}"></use>
                                    </svg>
                                    <span>{{$task->creator->negative_reviews_count}}</span>
                                @endif

                            @else
                                <span>Нет отзывов</span>
                            @endif
                            <div class="all-tasks__list-item-reviews">
                                <span>{{ trans_choice('tasks.offers.count_small',$task->offers_count) }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="all-tasks__list-item-right">
                <div class="all-tasks__list-item-price">
                    <span>{{ $task->base_cost  }} руб.</span>
                </div>
                @if(now()->subDay(1) < \Carbon\Carbon::parse($task->opened_to)??now())
                    <div class="all-tasks__list-item-date low_time">
                        <svg width="1em" height="1em" class="icon icon-clock-2 ">
                            <use xlink:href="{{asset('/s/images/useful/svg/theme/symbol-defs.svg#icon-clock-2')}}"></use>
                        </svg>
                        <span class="task-time">{{\Carbon\Carbon::parse($task->opened_to)->toTimeString()}}</span>
                    </div>
                @else
                    <div class="all-tasks__list-item-date">
                        <svg width="1em" height="1em" class="icon icon-clock-2 ">
                            <use xlink:href="{{asset('/s/images/useful/svg/theme/symbol-defs.svg#icon-clock-2')}}"></use>
                        </svg>
                        <span>До {{\Date\DateFormat::dateOnly($task->opened_to)}}</span>

                    </div>
                @endif
                <div class="all-tasks__list-item-type"><span>{{$task->subcategory->category->name}}</span></div>
                @if(isset($task->data['address'][0]['name']))
                    <div class="all-tasks__list-item-address" title="{{$task->data['address'][0]['name']}}">
                        <svg width="1em" height="1em" class="icon icon-mark ">
                            <use xlink:href="{{asset('/s/images/useful/svg/theme/symbol-defs.svg#icon-mark')}}"></use>
                        </svg>
                        <span>
                          {{ str_limit($task->data['address'][0]['name'],40)}}
                       </span>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endforeach
{{ $tasks->links() }}

@if(!isset($sort_data['map']))
    <script>
        let locations ={!! $tasksMapData !!};
        if (typeof initMap !== 'undefined') {
            initMap(locations);
        }

    </script>
@endif
