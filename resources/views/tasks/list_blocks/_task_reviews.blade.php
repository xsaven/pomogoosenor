<?php
/** @var \App\UserTasks $task */
$average_mark = $task->reviews->avg('average_mark');
//dump($task->reviews->where("type", 0));
?>
@if($task->reviews->where("type", 0)->isNotEmpty())
    <div class="l-task-card__reviews">
        <div class="l-task-card__reviews-head"><b>Отзывы</b>
            <div class="card__reviews-head-stars">
                @for($i = 1; $i <= 5;$i++)
                    @if(round($average_mark) >= $i)
                        <svg width="1em" height="1em" class="icon icon-star">
                            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star')}}"></use>
                        </svg>
                    @else
                        <svg width="1em" height="1em" class="icon icon-star2">
                            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star2')}}"></use>
                        </svg>
                    @endif
                @endfor
            </div>
            <span>Рейтинг: {{ $average_mark }} - {{ trans_choice('tasks.reviews.count',$task->reviews->count()) }}</span>
        </div>
        @foreach($task->reviews->where("type", 0) as $review)
            @include('blocks.review')
        @endforeach
    </div>
@endif