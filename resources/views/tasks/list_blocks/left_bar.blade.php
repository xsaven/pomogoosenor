<div class="col-md-4">
    <div class="all-tasks__sidebar">
        <div class="all-tasks__map-wrap">
            <div class="all-tasks__map-inner">
                <div id="all-tasks__map" class="all-tasks__map js-all-tasks-map"></div>
            </div>
            <button type="button" class="all-tasks__map-trig">
                <svg width="1em" height="1em" class="icon icon-expand ">
                    <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg')}}#icon-expand"></use>
                </svg><span class="hide-text">Cкрыть карту</span><span class="show-text">Раскрыть карту</span>
            </button>
        </div>

        @include('tasks.list_blocks.filter')

        <div class="l-profile__sidebar-posts">
            @include('profile._partials._part._baner')
            @widget('Blog')
        </div>
    </div>
</div>