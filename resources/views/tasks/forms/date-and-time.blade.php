@php
    $old = old('data', $task->data ?? null);
    $d1 = isset($old[$fields[$fId]->slug]['work']) ? $old[$fields[$fId]->slug]['work'] : null;
    $d2 = isset($old[$fields[$fId]->slug]['from_date']) ? $old[$fields[$fId]->slug]['from_date'] : null;
    $d3 = isset($old[$fields[$fId]->slug]['from_time']) ? $old[$fields[$fId]->slug]['from_time'] : null;
    $d4 = isset($old[$fields[$fId]->slug]['to_date']) ? $old[$fields[$fId]->slug]['to_date'] : null;
    $d5 = isset($old[$fields[$fId]->slug]['to_time']) ? $old[$fields[$fId]->slug]['to_time'] : null;
@endphp

<div class="l-bid-write__date" style="max-width: 100%;">
    <div class="label-txt">{{$title}}</div>
    <div class="row">
        <div class="col-md-4 col-sm-4">
            <select class="selectpicker" name="data[{{$fields[$fId]->slug}}][work]">
                <option {{$d1=='Начать работу' ? "selected" : ''}} value="Начать работу">Начать работу</option>
                <option {{$d1=='Завершить работу' ? "selected" : ''}} value="Завершить работу">Завершить работу</option>
                <option {{$d1=='Указать период' ? "selected" : ''}} value="Указать период">Указать период</option>
            </select>
        </div>
        <div class="col-md-4 col-sm-4 messContainer" style="padding-left:0;padding-right:0;">
            <div class="label-date-time">
                <label class="label-date" style="width: 135px;">
                    <svg width="1em" height="1em" class="icon icon-date ">
                        <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg')}}#icon-date"></use>
                    </svg>
                    <input type="text"
                           data-rule-required="true" data-msg-required="Необходимо ввести дату"
                           name="data[{{$fields[$fId]->slug}}][from_date]" value="{{ $d2 }}" placeholder="Дата" class="form-control datepicker-here"/>
                </label>
                <label class="label-time">
                    <input type="text"
                           data-rule-required="true" data-msg-required="Необходимо указать время"
                           name="data[{{$fields[$fId]->slug}}][from_time]" value="{{ $d3 }}" placeholder="Время" class="form-control mask"/>
                </label>
            </div>
            <div class="errorMsg" style="clear: both"></div>
            <div class="label-time__btns">
                <button type="button" class="today" onclick="$('[name=\'data[{{$fields[$fId]->slug}}][from_date]\']').val('{{date('d.m.Y')}}');">Сегодня,</button>
                <button type="button" class="tommorow" onclick="$('[name=\'data[{{$fields[$fId]->slug}}][from_date]\']').val('{{date('d.m.Y', time()+86400)}}');">завтра</button>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 messContainer" style="padding-left:0;padding-right:0;">
            <div class="label-date-time">
                <label class="label-date" style="width: 135px;">
                    <svg width="1em" height="1em" class="icon icon-date ">
                        <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg')}}#icon-date"></use>
                    </svg>
                    <input type="text"
                           data-rule-required="true" data-msg-required="Необходимо ввести дату"
                           name="data[{{$fields[$fId]->slug}}][to_date]" value="{{ $d4 }}" placeholder="Дата" class="form-control datepicker-here"/>
                </label>
                <label class="label-time">
                    <input type="text"
                           data-rule-required="true" data-msg-required="Необходимо указать время"
                           name="data[{{$fields[$fId]->slug}}][to_time]" value="{{ $d5 }}" placeholder="Время" class="form-control mask"/>
                </label>
            </div>
            <div class="errorMsg" style="clear: both"></div>
            <div class="label-time__btns">
                <button type="button" class="today" onclick="$('[name=\'data[{{$fields[$fId]->slug}}][to_date]\']').val('{{date('d.m.Y')}}');">Сегодня,</button>
                <button type="button" class="tommorow" onclick="$('[name=\'data[{{$fields[$fId]->slug}}][to_date]\']').val('{{date('d.m.Y', time()+86400)}}');">завтра</button>
            </div>
        </div>
    </div>
</div>

@push('scripts')

<script>
    setInterval(function(){
        var to_date_ = $('[name="data[{{$fields[$fId]->slug}}][to_date]"]').parents('.col-sm-4');
        var date_selector_ = $('[name="data[{{$fields[$fId]->slug}}][work]"]');
        if(date_selector_.val()!='Указать период') {
            to_date_.hide()
            to_date_.find('[name="data[{{$fields[$fId]->slug}}][to_date]"]').val('');
            to_date_.find('[name="data[{{$fields[$fId]->slug}}][to_time]"]').val('');
        }
        else to_date_.show();
    }, 100);

</script>
@endpush

















