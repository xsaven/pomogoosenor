@php
    $old = old('data', $task->data ?? null);
    $d1 = isset($old[$fields[$fId]->slug]['weight']) ? $old[$fields[$fId]->slug]['weight'] : null;
    $d2 = isset($old[$fields[$fId]->slug]['length']) ? $old[$fields[$fId]->slug]['length'] : null;
    $d3 = isset($old[$fields[$fId]->slug]['width']) ? $old[$fields[$fId]->slug]['width'] : null;
    $d4 = isset($old[$fields[$fId]->slug]['height']) ? $old[$fields[$fId]->slug]['height'] : null;
    $d5 = isset($old[$fields[$fId]->slug]['required_lu']) ? $old[$fields[$fId]->slug]['required_lu'] : null;
@endphp

<div class="l-bid-write__cargo messContainer">
    <div class="label-txt">{{$title}}</div>
    <div class="l-bid-write__cargo-line clearfix">
        <div class="l-bid-write__cargo-item">
            <input name="data[{{$fields[$fId]->slug}}][weight]" required
                   data-rule-required="true" data-rule-number="true" data-msg-number="Неправильный вес груза"
                   data-msg-required="Введите Вес груза, кг"
                   value="{{ $d1 }}" type="number" placeholder="Вес груза, кг" class="form-control"/>
            <span class="errorMsg"></span>
        </div>
        <div class="l-bid-write__cargo-item">
            <input name="data[{{$fields[$fId]->slug}}][length]" required
                   data-rule-required="true" data-rule-number="true" data-msg-number="Неправильная длина груза"
                   data-msg-required="Введите Длина, м"
                   value="{{ $d2 }}" type="number" placeholder="Длина, м" class="form-control"/>
            <span class="errorMsg"></span>
        </div>
        <div class="l-bid-write__cargo-item">
            <input name="data[{{$fields[$fId]->slug}}][width]" required
                   data-rule-required="true" data-rule-number="true" data-msg-number="Неправильная ширина груза"
                   data-msg-required="Введите Ширина, м"
                   value="{{ $d3 }}" type="number" placeholder="Ширина, м" class="form-control"/>
            <span class="errorMsg"></span>
        </div>
        <div class="l-bid-write__cargo-item">
            <input name="data[{{$fields[$fId]->slug}}][height]" required
                   data-rule-required="true" data-rule-number="true" data-msg-number="Неправильная высота груза"
                   data-msg-required="Введите Высота, м"
                   value="{{ $d4 }}" type="number" placeholder="Высота, м" class="form-control"/>
            <span class="errorMsg"></span>
        </div>

    </div>

    <div class="l-bid-write__cargo-check">
        <label class="check-label">
            <input name="data[{{$fields[$fId]->slug}}][required_lu]" value="1"
                   {{ !is_null($d5) ? "checked":"" }} type="checkbox"/><i class="checkbox"></i><span>Требуется погрузка / разгрузка</span>
        </label>
    </div>
</div>
