@php
    if(isset(Auth::user()->profiles->cities->name))
        $defCity = ['name'=>Auth::user()->profiles->cities->name];
    else
        $defCity = ['name'=>'Москва'];

    $old = old('data', $task->data ?? null);

    if(!isset($old[$fields[$fId]->slug])) {
        $old[$fields[$fId]->slug]=[];
    }
    if (is_array($old[$fields[$fId]->slug])){
        $old[$fields[$fId]->slug] = $old[$fields[$fId]->slug];
    }else{
        $old[$fields[$fId]->slug] = json_decode_try($old[$fields[$fId]->slug]);
    }
    $d1 = isset($old[$fields[$fId]->slug][0]) ? $old[$fields[$fId]->slug][0] : $defCity;
    $d2 = isset($old[$fields[$fId]->slug][1]) ? $old[$fields[$fId]->slug][1] : $defCity;
    unset($old[$fields[$fId]->slug][0], $old[$fields[$fId]->slug][1]);
    $allFields = [];
    if(isset($old[$fields[$fId]->slug])){
        foreach ($old[$fields[$fId]->slug] as $val) $allFields[] = $val;
    }

@endphp
<script>
    var saveFields_ =  {!! json_encode($allFields) !!};
            @auth
    var city = '{{isset(Auth::user()->profiles->cities->name) && !is_null(Auth::user()->profiles->cities->name) ? Auth::user()->profiles->cities->name : ''}}';
            @else
    var city = '';
    @endauth
</script>
<div class="l-bid-write__adress">
    <div class="label-txt">{{$title}}</div>
    <div class="l-bid-write__adress-line"><i class="adress-line__ico"><span>А</span></i>
        <label>
            <input value="{{$d1['name']}}" type="text" onchange="reBuildPoints()" placeholder="Улица, дом, строение, корпус" required
                   class="form-control pac-input"/>
        </label>
    </div>
    <div class="l-bid-write__adress-line with-delete" copy="true">
        <svg width="1em" height="1em" class="icon icon-arrow ">
            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg')}}#icon-arrow"></use>
        </svg>
        <i class="adress-line__ico"><span>Б</span></i>
        <label>
            <input value="{{$d2['name']}}" type="text" onchange="reBuildPoints()" placeholder="Улица, дом, строение, корпус" required
                   class="form-control pac-input"/>
        </label><span class="but-del" onclick="dropPoint($(this))"></span>
    </div>
    <button type="button" class="l-bid-write__add" onclick="newAddressPoint_($(this))" id="addrAdd_"><span>Добавить ещё один адрес</span>
    </button>
</div>
<input type="hidden" value="[]" name="data[address]">
<div class="l-bid-write__map">
    <div id="bid-map"></div>
    <div id="total_km"></div>
</div>
<div class="messContainer">
    <input type="hidden"
           data-rule-required="true" data-msg-required="Необходимо указать правильные адреа"
           name="validAddress" id="validAddress" value="valid">
    <div class="errorMsg"></div>
</div>

@push('scripts')
    <script>
        var first_rebuild = true;
        /*var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer({
            map: map
        });*/

        var reBuildPoints = function () {
            var locations = [];
            var addresses = [];
            $('.l-bid-write__adress input').each(function (index) {
                    locations[index] = $(this).val();
                    addresses[index] = $(this).val();
            });
            console.log(addresses);
            if (locations.length >= 2) {
                var pointA = locations[0];
                delete locations[0];
                var pointEnd = locations[locations.length - 1];
                delete locations[locations.length - 1];
                var waypoints = [];
                $.each(locations, function (key, val) {
                    if (val !== undefined)
                        waypoints[waypoints.length] = {location: val};
                })
                displayRoute(pointA, pointEnd, waypoints, addresses);
            }
        };
        window.reBuildPoints = reBuildPoints;

        $(function () {
            setTimeout(function () {
                reBuildPoints();
            }, 1000);
        });

        var addrCount_ = 0;
        var addrTotalCount_ = 0;
        var literals = ['А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У'];

        function newAddressPoint_(obj, newVal) {
            var block = obj.parents('.l-bid-write__adress');
            var copyBlock = block.find('[copy="true"]');
            if ($('[copy="true"]').is(':hidden')) {
                $('[copy="true"]').show();
                return;
            }
            var pastedBlock = copyBlock.clone().appendTo(block);
            obj.appendTo(block);
            pastedBlock.removeAttr('copy');
            pastedBlock.find('input').val(newVal !== undefined ? newVal : city);
            addrTotalCount_ = 0;
            block.find('.l-bid-write__adress-line').each(function () {
                $(this).find('.adress-line__ico span').html(literals[addrCount_]);
                addrCount_++;
                addrTotalCount_++;
            });
            addrCount_ = 0;
            var input = pastedBlock.find('input')[0];
            var autocomplete = new google.maps.places.Autocomplete(input);

            autocomplete.addListener('place_changed', function() {
                reBuildPoints();
            });
            if (addrTotalCount_ == literals.length) obj.hide();
        }

        function dropPoint(obj) {
            var block = obj.parents('.l-bid-write__adress');

            if (obj.parents('.l-bid-write__adress-line').attr('copy') !== undefined) {
                if (obj.parents('.l-bid-write__adress-line').next().hasClass('with-delete')) {
                    obj.parents('.l-bid-write__adress-line').find('input').val(obj.parents('.l-bid-write__adress-line').next().find('input').val());
                    obj.parents('.l-bid-write__adress-line').next().remove();
                } else {
                    obj.parents('.l-bid-write__adress-line').hide();
                    obj.parents('.l-bid-write__adress-line').find('input').val(city);
                }
            } else {
                obj.parents('.l-bid-write__adress-line').remove();
            }

            addrTotalCount_ = 0;
            block.find('.l-bid-write__adress-line').each(function () {
                $(this).find('.adress-line__ico span').html(literals[addrCount_]);
                addrCount_++;
                addrTotalCount_++;
            });
            addrCount_ = 0;
            if (addrTotalCount_ == literals.length) $('.l-bid-write__add').hide();
            else $('.l-bid-write__add').show();
            reBuildPoints();
        }

        if (saveFields_.length) {
            $.each(saveFields_, function (key, val) {
                newAddressPoint_($('#addrAdd_'), val.name);
            });
        }
    </script>
@endpush
