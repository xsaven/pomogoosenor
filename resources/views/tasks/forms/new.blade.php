<style>
    .errorMsg label {
        margin-bottom: 0;
    }
</style>
<form class="l-bid-write__form" method="post"
      id="new-task-form"
      action="{{!isset($task) ? route('task.create', ['category' => $selectCategory->slug, 'subcategory' => $selectSubcategory->slug]) : route('task.edit', ['category' => $selectCategory->slug, 'subcategory' => $selectSubcategory->slug, 'userTasks' => $task->id])}}"
      enctype="multipart/form-data" data-toggle="validator">
    {{csrf_field()}}
    @if (count($errors) > 0)
        <div class="alert alert-error">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <label class="l-bid-write__need">
        <div class="label-txt">Мне нужно</div>
        <div class="errorMsg"></div>
        <input name="name" value="{{old('name', $task->name ?? $sessionName)}}" type="text" required
               data-rule-required="true" data-msg-required="Вы должны сообщить нам что вам нужно"
               placeholder="{{$selectSubcategory->example_title}}" data-toggle="popover"
               data-content="{{$selectSubcategory->question}}" data-placement="right" class="form-control"/>
    </label>
    <label class="l-bid-write__need">
        <div class="label-txt">Искать заказчика в</div>
        <div class="errorMsg"></div>
        @php($city = $user ? $user->profiles->cities : null)
        <input name="city" type="text" placeholder="Город"
               data-rule-required="true" data-msg-required="Вы должны указать город"
               class="form-control form-data" value="{{old('city') ?? ($city ? $city->name : 'Москва')}}">
    </label>

    <div class="l-bid-write__selects" {!! !is_null(request()->select)  ? "style='display:none;'" : "" !!}>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <select class="selectpicker" onchange="selectCategory($(this))">
                    @foreach($categories as $category)
                        @if(isset($category->default_subcategory['slug']))
                            <option {{$category->slug==$selectCategory->slug ? 'selected' : ''}} value="{{!isset($task) ? route('task.new', ['category' => $category->slug, 'subcategory' => $category->default_subcategory['slug'], 'query_search' => $sessionName]) : route('task.edit', ['category' => $category->slug, 'subcategory' => $category->default_subcategory['slug'], 'userTasks' => $task->id])}}">{{$category->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="col-md-6 col-sm-6">
                <select class="selectpicker" onchange="selectCategory($(this))">
                    @foreach($subcategories as $subcategory)
                        <option {{$subcategory->slug==$selectSubcategory->slug ? 'selected' : ''}} value="{{!isset($task) ? route('task.new' , ['category' => $selectCategory->slug, 'subcategory' => $subcategory->slug, 'query_search' => $sessionName]) : route('task.edit' , ['category' => $selectCategory->slug, 'subcategory' => $subcategory->slug, 'userTasks' => $task->id])}}">{{$subcategory->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="l-bid-write__sub-cat label-txt" {!! is_null(request()->select) ? "style='display:none;'" : "" !!}>
        Подкатегория <a href="#">«{{$selectSubcategory->name}}»</a></div>
    @if(!is_null(request()->select))
        @push('scripts')
            <script>
                $('.l-bid-write__sub-cat a').on('click', function () {
                    window.history.pushState("", "", '{{!isset($task) ? route('task.new' , ['category' => $selectCategory->slug, 'subcategory' => $selectSubcategory->slug]) : route('task.edit' , ['category' => $selectCategory->slug, 'subcategory' => $selectSubcategory->slug, 'userTasks' => $task->id])}}');
                    $('.l-bid-write__selects').show();
                    $('.l-bid-write__sub-cat').hide();
                    return false;
                });
            </script>
        @endpush
    @endif
    <div class="l-bid-write__desc valid">
        <label>
            <div class="label-txt">Опишите пожелания и детали, чтобы исполнители лучше оценили вашу задачу</div>
            <div class="errorMsg"></div>
            <textarea name="description" required
                      data-rule-required="true"
                      data-msg-required="Вы должны сообщить нам детали вашего пожелания, чтобы исполнители лучше оценили вашу задачу"
                      placeholder="{{$selectSubcategory->description}}" data-toggle="popover"
                      data-content="{{$selectSubcategory->hint_description}}" data-placement="right"
                      class="form-control">{{old('description', ($task->description ?? ''))}}</textarea>
        </label>
        <div class="label-private-wrp info">
            <div class="label-private">
                <svg width="1em" height="1em" class="icon icon-lock ">
                    <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg')}}#icon-lock"></use>
                </svg>
                <span>Приватная информация</span>
            </div>
            <div class="label-private__popup">
                <span>Эта информация будет доступна только выбранному исполнителю задания</span></div>
        </div>
        <div class="label-private-wrp photo">
            <div class="label-private">
                <svg width="1em" height="1em" class="icon icon-camera ">
                    <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg')}}#icon-camera"></use>
                </svg>
                <span>Добавить фото</span>
            </div>
        </div>
    </div>
    <div class="l-bid-write__addit">
        <div class="l-bid-write__addit-head clearfix">
            <div class="label-txt">Добавить приватную информацию</div>
            <button type="button">Скрыть</button>
        </div>
        <label>
            <textarea name="private_description"
                      placeholder="Например: квартира 17, код домофона 777, ключ под ковриком :)"
                      class="form-control">{{old('private_description', ($task->private_description ?? ''))}}</textarea>
        </label>
    </div>
    <div class="l-bid-write__file clearfix">
        <label>
            <img src="#" id="photo_preview" style="padding: 10px; display: none" alt="photo"/>
            <input id="photo" type="file" name="photo"/>
            <div class="l-bid-write__file-txt">Наличие фото помогает исполнителям лучше оценить вашу задачу и сформулировать
                свое предложение.
            </div>
            <br/>
            <div class="l-bid-write__file-but"><span>Выберите файл</span></div>
        </label>
        <label>
            <div class="l-bid-write__file-but" style="cursor: pointer; display: none" id="delete_photo">Удалить файл
            </div>
        </label>

    </div>

    {{--included forms--}}
    @if(!empty($selectSubcategory->form_fields))
        @foreach($selectSubcategory->form_fields as $key => $field)
            @foreach($field as $fId => $title)
                @include('tasks.forms.'.$fields[$fId]->shape)
            @endforeach
        @endforeach
    @endif


    <div class="l-bid-write__budget">
        <div class="l-bid-write__budget-head"><b>На какой бюджет вы расчитиваете?</b>
            <div class="budget-quest">
                <svg width="1em" height="1em" class="icon icon-quest ">
                    <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg')}}#icon-quest"></use>
                </svg>
                <div class="budget-popup"><span>Это поможет найти исполнителей, которые будут готовы выполнить ваше задание.</span>
                </div>
            </div>
        </div>
        <div class="l-bid-write__budget-body">
            <div class="l-bid-write__budget-cols">
                <div class="row">
                    <div class="cols-item"><span></span></div>
                    <div class="cols-item"><span></span></div>
                    <div class="cols-item"><span></span></div>
                    <div class="cols-item"><span></span></div>
                    <div class="cols-item"><span></span></div>
                </div>
            </div>
            <div class="l-bid-write__budget-slider">
                @php
                    $cost_range = \App\PriceRangersMask::find($selectSubcategory->range_id);
                    if(isset($task)){
                        $cost = explode(' ', $task->getAttributes()['cost']);
                        $coste = isset($cost[1]) ? $cost[1] : $cost[0];
                        $step = 0;
                        foreach ($cost_range['formatted_data'] as $key => $val){ if($val['value']==$coste) $step = $key; }
                        if(isset($cost[1]) && $cost[0]=='От') $step++;
                        $step*=5000;
                    }
                @endphp
{{--                {{dd($cost_range->toArray())}}--}}
                <script>
                    var firstLoad = '';
                    var ranges = {!! json_encode($cost_range) !!};
                </script>
                <input type="text" id="budget-slider" class="price-slider"/>
                <input type="hidden" name="cost" value="" id="price_from_range"/>
                <input type="hidden" name="cost_save" value="{{old('cost_save', $step ?? 'false')}}"/>
            </div>
            <label class="l-bid-write__budget-indicate"><span
                        class="indicate-txt"> Укажите точную стоимость задания</span>
                <input
                        data-rule-required="true" data-msg-required="Укажите стоимость"
                        data-rule-number="true" data-msg-number="Неправильная стоимость"
                        required type="number" value="{{old('exact_cost', !isset($cost[1]) ? $coste ?? '' : '' )}}"
                        name="exact_cost" class="form-control indicate-input" id="myPriceInput"/><span
                        class="indicate-rub">₽</span>
                <div class="errorMsg"></div>
            </label>
        </div>

    </div>
    <div class="l-bid-write__must">
        <label class="check-label">
            <input type="checkbox" value="1" name="need_documents"/>
            <i class="checkbox"></i>
            <span>Исполнитель должен иметь документы для оформления расписки.</span>
        </label>
        <a target="_blank" href="{{url('receipt')}}">
            <span>Посмотреть пример расписки</span>
        </a>
        <svg width="1em" height="1em" class="icon icon-quest ">
            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-quest')}}"></use>
        </svg>
        <div class="l-bid-write__must-popup">
            <span>Мы настоятельно советуем оформлять расписку при передаче перевозимого исполнителем груза, денег</span>
        </div>
    </div>
    @guest()
        <div class="l-bid-write__contacts">
            <div class="label-txt">Ваши контакты</div>
            <div class="row">
                <div class="col-md-6 col-sm-6 messContainer">
                    <label>
                        <input type="text"
                               data-rule-required="true" data-msg-required="Введите ваше имя и фамилию"
                               placeholder="Имя Фамилия" value="{{old('user_name')}}" name="user_name"
                               class="form-control"/>
                    </label>
                    <div class="errorMsg"></div>
                </div>
                <div class="col-md-6 col-sm-6 messContainer">
                    <label>
                        <input type="email"
                               data-msg-email="Неправильный Email"
                               data-rule-required="true" data-msg-required="Введите вашу почту"
                               placeholder="E-mail" name="user_email" value="{{old('user_email')}}"
                               class="form-control"/>
                    </label>
                    <div class="errorMsg"></div>
                </div>
                <div class="col-md-6 col-sm-6 messContainer">
                    <div class="tel-wrap">
                        <select class="selectpicker" name="user_phone_code">
                            <option {{old('user_phone_code')=='+7' ? 'selected' : ''}}>+7</option>
                            {{--<option {{old('user_phone_code')=='+38' ? 'selected' : ''}}>+38</option>--}}
                        </select>
                        <label>
                            <input type="tel"
                                   data-rule-minlength="15" data-msg-minlength="Неправильный номер телефона"
                                   data-rule-required="true" data-msg-required="Введите ваш номер телефона"
                                   name="user_phone" value="{{old('user_phone')}}" placeholder="Ваш телефон"
                                   class="form-control tel-input"/>
                        </label>
                    </div>
                    <div class="errorMsg"></div>
                </div>
            </div>
        </div>
    @else
        <div class="l-bid-write__your-cont">
            @php $profile = Auth::user()->profiles; @endphp
            <div class="label-txt">Ваши контакты</div>
            <b>{{$profile->phone_code}} {{$profile->phone}}</b>
        </div>
    @endguest
    @guest()
        <div class="l-bid-write__enter">
                    <span>Уже зарегистрированы?
                        <a href="#" data-toggle="modal" data-target="#loginModal">Войдите!</a>
                    </span>
        </div>
    @endguest
    <div class="l-bid-write__addit-cont">
        <button type="button" data-toggle="collapse" data-target="#addit-cont">
            <span>Дополнительные условия</span></button>
        <div id="addit-cont" class="collapse addit-cont-checks">
            <div class="addit-cont-checks__wrp">
                <div class="addit-cont-checks__item">
                    <label class="check-label">
                        <input name="email_report" value="1" type="checkbox"/><i class="checkbox"></i><span>Получать email-уведомления о новых предложениях</span>
                    </label>
                    <label class="check-label">
                        <input name="executor_only" value="1" type="checkbox"/><i class="checkbox"></i><span>Показывать мое задание только исполнителям  </span>
                    </label>
                    {{--<label class="check-label">--}}
                    {{--<input name="commentaries" value="1" type="checkbox"/><i class="checkbox"></i><span>Отключить комментарии у задания</span>--}}
                    {{--</label>--}}
                </div>
            </div>
        </div>
    </div>
    <div class="l-bid-write__agree">
        <button type="submit" class="but but_bondi but_bondi_lg">
            <span>{{isset($task) ? 'Редактировать' : 'Опубликовать'}}</span></button>
        @if(Auth::check())
            <input name="agree_with_rules" type="hidden" value="1"/>
        @else
            <label class="check-label">
                <input name="agree_with_rules"
                       required
                       data-rule-required="true"
                       data-msg-required="Вы должны принять правила использования даного сервиса"
                       {{old('agree_with_rules', isset($task) ? '1' : false) ? "checked":""}} type="checkbox"/><i
                        class="checkbox"></i><span>Я согласен с <a target="_blank" href="{{url('rules')}}">Правилами сайта</a></span>
                <div class="errorMsg"></div>
            </label>

        @endif
    </div>
</form>

<div id="photo-loaded-modal" class="modal fade modal-mob-numb">
    <div class="modal-dialog">
        <div class="modal-body">
            <button type="button" data-dismiss="modal" class="but-modal-close"></button>
            <div class="modal-reg_success__txt">
                <span id="photo-loaded-modal-message" style="color: black; margin-top: 20px;"></span>
            </div>
        </div>
    </div>
</div>

@push('scripts')

@endpush


@section('script')
    <script>
        window.onload = function () {

            $('#new-task').submit(function (event) {
                if (!event.target.validate()) {
                    event.preventDefault();
                    return false;
                }
            });

            var xhr;
            new autoComplete({
                selector: 'input[name="city"]',
                source: function (term, response) {
                    try {
                        xhr.abort();
                    } catch (e) {
                    }
                    xhr = $.getJSON('{{route('find_city')}}', {query: term}, function (data) {
                        response(data);
                    });
                },
                renderItem: function (item, search) {
                    search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                    var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
                    return '<div class="autocomplete-suggestion" value="' + item.id + '" data-val="' + item.name + '">' + item.name + '</div>';
                }
            });


            $('#photo').on('change', function () {
                var th = this;
                var block = $('#photo_preview');

                block.closest('.steps-content__input-file').blockForm();
                if (th.files && th.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        block.attr('src', e.target.result);
                        block.show();
                        $('#delete_photo').show();
                    };
                    reader.readAsDataURL(th.files[0]);
                }
            });

            $('#delete_photo').on('click', function () {
                let block = $('#photo_preview');
                block.attr('src', '');
                block.hide();
                $('#photo').val(null);
                $(this).hide();
            });
        };

    </script>
@endsection