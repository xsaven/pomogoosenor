<div class="col-md-3">
    <div class="c-card-sidebar">
        <div class="c-card-sidebar__head"><span>Задание № {{$task->id}}</span><span>Заказчик этого задания</span></div>
        <a href="{{route('profile_show',['user'=>$task->creator->id])}}" class="c-card-sidebar__user clearfix">
            <div class="c-card-sidebar__user-img h-object-fit"><img src="{{$task->creator->avatar()}}"
                                                                    alt="{{$task->creator->name}}"/></div>
            <div class="c-card-sidebar__user-txt">
                <div class="c-card-sidebar__user-name"><span>{{$task->creator->name}}</span><i></i></div>


                    <div class="c-card-sidebar__user-city">
                        <span>@if($task->creator->profile) {{$task->creator->profile->age}} лет @endif</span>
                        <span>{{$task->creator->get_city()->name}}</span>
                    </div>


                <div class="c-card-sidebar__user-revs"><span>Отзывы</span>
                    <div class="revs-count">
                        <svg width="1em" height="1em" class="icon icon-like ">
                            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg')}}#icon-like"></use>
                        </svg>
                        <div class="revs-count__numb">{{$task->creator->positiveReviewsCount}}</div>
                    </div>
                </div>
            </div>
        </a>
        <div class="c-card-sidebar__faq">
            <h5>Частые вопросы</h5>
            <ul>
                @foreach($faq as $row)
                    <li><a href="{{url('faq#'.$row->slug)}}">{{$row->question}}</a></li>
                @endforeach
            </ul>
        </div>
        @widget('SocialShare')
    </div>
</div>