<?php
/** @var \App\UserTasks $task */
?>
<div id="rev-send" class="modal fade rev-send">
    <div class="modal-dialog">
        <form class="modal-content" method="POST" action="{{route('task.review.store',$task)}}">
            {{csrf_field()}}
            <button type="button" data-dismiss="modal" class="but-modal-close"></button>
            <div class="rev-send__head">
                <h5>Задание было выполнено?</h5>
                <label class="yes">
                    <input type="radio" name="positive" value="1"><span>ДА</span>
                    <svg width="1em" height="1em" class="icon icon-yes ">
                        <use xlink:href="{{asset('/s/images/useful/svg/theme/symbol-defs.svg#icon-yes')}}"></use>
                    </svg>
                </label>
                <label class="no">
                    <input type="radio" name="positive" value="0">
                    <svg width="1em" height="1em" class="icon icon-no ">
                        <use xlink:href="{{asset('/s/images/useful/svg/theme/symbol-defs.svg#icon-no')}}"></use>
                    </svg><span>НЕТ</span>
                </label>
            </div>
            <div class="rev-send__estimate">
                <div class="rev-send__estimate-ttl">Хотите оценить заказчика?</div>
                <div class="rev-send__estimate-item clearfix">
                    <div class="rev-send__estimate-key">Вежливость</div>
                    <div class="rev-send__estimate-val">
                        <div class="estimate__stars" data-type="politeness"></div>
                        <input type="hidden" name="politeness" value="0">
                    </div>
                </div>
                <div class="rev-send__estimate-item clearfix">
                    <div class="rev-send__estimate-key">Пунктуальность</div>
                    <div class="rev-send__estimate-val">
                        <div class="estimate__stars" data-type="punctuality"></div>
                        <input type="hidden" name="punctuality" value="0">
                    </div>
                </div>
                <div class="rev-send__estimate-item clearfix">
                    <div class="rev-send__estimate-key">Адекватность</div>
                    <div class="rev-send__estimate-val">
                        <div class="estimate__stars" data-type="adequacy"></div>
                        <input type="hidden" name="adequacy" value="0">
                    </div>
                </div>
            </div>
            <div class="rev-send__message">
                <div class="rev-send__message-head">
                    <h5>Оставьте свой отзыв</h5>
                    <div class="rev-send__message-sub">Он будет опубликован в профиле заказчика и поможет <br>другим исполнителям сделать правильный выбор</div>
                    <textarea required placeholder="Понравилось сотрудничать с {{$task->creator->name}} Проект согласовали быстро, оплата была произведена в срок.  Рекомендую!"
                              class="form-control" name="text" ></textarea>
                </div>
                <input type="hidden" class="form-control" name="final_task_price" value="0">

                {{--<div class="rev-send__message-footer clearfix">--}}
                    {{--<div class="rev-send__message-footer-left">Укажите финальную стоимость задания <br>(сколько заказчик заплатил вам)</div>--}}
                    {{--<div class="rev-send__message-footer-right">--}}
                        {{--<input type="number" class="form-control" name="final_task_price"><span>руб.</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
            <button type="submit" class="but but_bondi">Сохранить</button>
        </form>
    </div>
</div>