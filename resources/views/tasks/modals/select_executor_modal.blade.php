
<div id="select-executor-modal" class="modal fade modal-mob-numb">
    <form id="offer-form"  method="POST" action="{{ route('payment.sbr.pay',$task->id) }}">
        <div class="modal-dialog modal-dialog__572">
            <div class="modal-body">
                <button type="button" data-dismiss="modal" class="but-modal-close"></button>
                <div class="modal-welcome__txt">
                    <h2>Выбрать пользователя<br>
                        <a href="#" class="link_bondi_nound" id="user_link"></a>
                        <input type="hidden" value="" name="user_id">
                        {{csrf_field()}}
                        <br>исполнителем вашего задания?</h2>
                </div>
                <div class="modal-grey">
                    <p>Стоимость задания
                        <input type="text" readonly="readonly" value="" name="cost" id="cost_input"><span>руб.</span>
                        <a href="#">
                            <svg width="1em" height="1em" class="icon icon-pensil ">
                                <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-pensil')}}"></use>
                            </svg>ред.</a>
                    </p>
                    <span>
                    комиссию оплачивает исполнитель <br>- 15% от стоимости задания
                </span>
                </div>
                <div class="modal-welcome__txt modal-welcome__txt-left">
                    <p>Оплата исполнителю поступит после выполнения задания<br>
                        В случае проблем, <a href="#" class="link_bondi">Pomogoo вернет вам деньги</a>
                    </p>
                </div>
                <div class="modal_two-button">
                    <button type="submit" class="but but_bondi submit-btn" id="form-submit-btn">Да</button>
                    <button type="button" data-dismiss="modal" class="but but_y-orange submit-btn">Нет</button>
                </div>
                <div class="modal_footer_info">
                <span>Нажимая «Да» вы соглашаетесь  с правилами платежного сервиса <br>
                    <a href="#" class="link_bondi">«Единая каса - безопасная сделка»</a>
                    и сервиса <a href="#" class="link_bondi">«Сделка без риска».</a>
                    <br>Ваш банк может взимать дополнительную коммисию.
                </span>
                </div>
            </div>
        </div>
    </form>
</div>
