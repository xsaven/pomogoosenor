<div id="block-task-modal" class="modal fade cancel-task-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" data-dismiss="modal" class="but-modal-close"></button>
            <form method="post" action="{{route('task.report', ['id' => $task->id])}}"
                  class="cancel-task-modal__content">
                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                <h5>Причина жалобы</h5>
                @foreach($reportReasons as $reason)
                    <label class="radio-label">
                        <input required type="radio" value="{{$reason->id}}" name="reason"><i
                                class="radio"></i><span>{{$reason->name}}</span>
                    </label>
                @endforeach
                <textarea class="form-control" required rows="5" name="description" placeholder="Описание"></textarea>
                <div class="cancel-task-modal__footer">
                    <button type="submit" class="but but_bondi">Пожаловаться</button>
                </div>
            </form>
        </div>
    </div>
</div>