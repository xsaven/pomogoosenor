<?php
/** @var \App\TaskOffers $offer */
$offer = $task->offers()->whereUserId(user()->id)->first();
?>
<div id="task-offer-modal" class="modal fade modal-mob-numb">
    <div class="modal-dialog">
        <div class="modal-body modal-body__width">
            <button type="button" data-dismiss="modal" class="but-modal-close"></button>
            <form id="offer-form" method="POST" action="
            @if($offer)
            {{route('task.offers.update',$offer)}}
            @else
            {{route('task.offers.store',$task)}}
            @endif
                    ">
                {{csrf_field()}}
                <div class="modal-welcome__txt modal-welcome__txt-left">
                    <h2>Добавить предложение к заданию </h2>
                    <p><b>Внимание! </b><br>Вы сможете обменяться контактами с заказчиком только после того, <br>как
                        заказчик выберет вас исполнителем и оплатит услугу.</p>
                    <div id="template-collapse">
                        <a role="button" class="steps-content__collapse">Использовать шаблон</a>
                    </div>
                    <div id="template-list" class="panel-heading hide" role="tabpanel" style="padding-bottom: 20px">
                        <select id="template-list-select" title="Выбрать шаблон" class="selectpicker">
                            <option class="add_new_option" value="add_new_template">Новый шаблон</option>
                        </select>
                    </div>
                    <div>
                        <div class="scroll_block">
                            <textarea name="message" id="template-textarea" required>{{$offer->message??''}}</textarea>
                        </div>
                    </div>
                    <div class="check_list">
                        <div class="check_list-check">
                            <label class="check-label">
                                <input type="checkbox" name="other_notification" value="1" {{($offer && $offer->other_notification)?'checked':''}}>
                                <i class="checkbox"></i><span>Уведомить меня, если исполнителем задания выберут другого или заказчик<br>отменит задание</span>
                            </label>
                        </div>
                        <div class="check_list-check">
                            <label class="check-label">
                                <input type="checkbox" checked="checked"><i class="checkbox"></i><span>Указать время актуальности предложения</span>
                            </label>
                        </div>
                        <div class="check_list-check mart-20">
                            <span class="grey">Ваше предложения будет удалено, если за указанное время вас не выберут исполнителем задания.</span>
                        </div>
                        <div class="check_list-check marl-40">
                            <select name="deactivate_hours" class="selectpicker">
                                <option value="7">7 часов</option>
                                <option value="5">5 часов</option>
                                <option value="3">3 часов</option>
                            </select>
                        </div>
                        <div class="check_list-check mart-20">
                            <span>Выберите карту для получения оплаты</span>
                        </div>
                        <div class="check_list-check marl-40">
                            <select id="card-list-select" data-type="executor" class="selectpicker" name="card_id">
                                @foreach(user()->beneficiary_cards as $card)
                                    <option value="{{$card->id}}" {{($offer && $offer->card_id == $card->id)?'selected':''}}
                                            data-content='<img style="height: 1.5em" src="https://www.echeep.com/bundles/virtualcertificate/img/payment/mastercard.svg"> {{$card->mask}}'></option>
                                @endforeach
                                <option class="add_new_option" value="add_new_card" data-content='Добавить новую карту'></option>
                            </select>
                        </div>
                        <div class="stoimost-block"><span>Стоимость работ</span>
                            <label>
                                <input type="number" name="cost" class="form-control" value="{{$offer->cost??0}}"><span>руб.</span>
                            </label>
                        </div>
                        <div class="modal-welcome__bottom-check">
                            <button type="submit" class="but but_bondi submit-btn disabled" disabled="true" id="form-submit-btn">Добавить
                                предложение
                            </button>
                            <span>Комиссию оплачивает исполнитель <br>15% от стоимости задания<br>
                    <label class="check-label">
                      <input type="checkbox" id="agree_checkbox"><i class="checkbox" id="agree_icon"></i><span>Я согласен</span>
                        @push("scripts")
                            <script>
                                $(function () {

                                    $("#agree_checkbox").on("click", function () {

                                        if ($(this).is(":checked")) {

                                            $("#form-submit-btn").removeAttr("disabled").removeClass("disabled");

                                        } else {

                                            $("#form-submit-btn").attr("disabled", "true").addClass("disabled");

                                        }
                                    });
                                });
                            </script>
                        @endpush
                    </label></span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="template-modal" class="modal fade rev-send">
    <div class="modal-dialog">
        <form class="modal-content" id="template-form">
            <div class="rev-send__message">
                <div class="rev-send__message-head">
                    <h5>Название шаблона</h5>
                    <div class="steps-content__form-input">
                        <label>
                            <input name="title">
                        </label>
                    </div>
                </div>
                <div class="rev-send__message-footer clearfix">
                    <h5>Текст шаблона</h5>
                    <textarea class="form-control" name="text"></textarea>
                </div>
            </div>
            <button type="submit" class="but but_bondi">Сохранить</button>
            <button data-toggle="modal" data-target="#template-modal" class="but but_bondi"
                    onclick="event.preventDefault();">Отмена
            </button>
        </form>
    </div>
</div>
