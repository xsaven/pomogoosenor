<div id="cancel-task-modal" class="modal fade cancel-task-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" data-dismiss="modal"
                    class="but-modal-close"></button>
            <form class="cancel-task-modal__content" id="cancel-task-form"
                  @if(isset($task))
                  action="{{route('task.edit.field', ['userTasks' => $task->id, 'field' => 'status'])}}"
                  @endif
                  method="post">
                {{csrf_field()}}
                <h5>Причина отмены задания</h5>
                <label class="radio-label">
                    <input type="radio" name="cancel"
                           value="no_matching_suggestions"><i
                            class="radio"></i><span>Нет подходящих предложений</span>
                </label>
                <label class="radio-label">
                    <input type="radio" name="cancel"
                           value="the_task_is_no_longer_relevant"><i
                            class="radio"></i><span>Задание уже не актуально</span>
                </label>
                <label class="radio-label">
                    <input type="radio" name="cancel"
                           value="artist_found_on_another_resource"><i
                            class="radio"></i><span>Исполнитель найден на другом ресурсе</span>
                </label>
                <label class="radio-label">
                    <input type="radio" name="cancel"
                           value="solved_the_problem_myself"><i
                            class="radio"></i><span>Решил проблему самостоятельно</span>
                </label>
                <label class="radio-label">
                    <input type="radio" name="cancel" value="expensive"><i
                            class="radio"></i><span>Дорого</span>
                </label>
                <label class="radio-label">
                    <input type="radio" name="cancel" id="diff-cancel"
                           value="another_reason"><i
                            class="radio"></i><span>Другая причина</span>
                </label>
                <textarea id="diff_reason"
                          style="display: none; width: 100%; height: 100px;"
                          name="another_reason_description"></textarea>
                @push('scripts')
                    <script>
                        $(function () {
                            $('.cancel-task-modal__content .radio-label').each(function () {
                                $(this).on('click', function () {
                                    if ($('#diff-cancel').is(':checked')) $('#diff_reason').show('fast');
                                    else $('#diff_reason').hide('fast');
                                });
                            });
                        });
                    </script>
                @endpush
                <div class="cancel-task-modal__footer">
                    <button type="submit" class="but but_bondi">Отправить
                        запрос
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>