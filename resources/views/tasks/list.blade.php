@extends('layouts.main')

@section('title', 'Мои заказы')

@section('content')

    <div class="l-all-tasks-wrapper">
        <div class="container">
            <div class="row">
                @include('tasks.list_blocks.left_bar')
                <div class="col-md-8">
                    <div class="all-tasks__filter">
                        <div class="all-tasks__filter-nav-tabs">
                            <ul role="tablist" class="nav nav-tabs">
                                <li role="presentation" class="active"><a href="#i_am_executor"
                                                                          aria-controls="i_am_executor" role="tab"
                                                                          data-toggle="tab">Я исполнитель</a></li>
                                <li role="presentation"><a href="#i_am_customer" aria-controls="i_am_customer"
                                                           role="tab" data-toggle="tab">Я заказчик</a></li>
                            </ul>
                        </div>
                        @include('tasks.blocks.my_tasks_filter')
                    </div>
                    <div class="tab-content">
                        <div id="i_am_executor" class="tab-pane fade active in">
                            @include('tasks.list_blocks.executor',['tasks'=>$executor_tasks])
                        </div>
                        <div id="i_am_customer" class="tab-pane fade">
                            @include('tasks.list_blocks.customer',['tasks'=>$creator_tasks])
                        </div>
                    </div>
                    @include('tasks.modals.cancel_task')
                </div>
            </div>
        </div>
    </div>
    @widget('Footer')
    @include('tasks.list_blocks.modals')

@endsection

@include('tasks.blocks.js')

@if(!isset($sort_data['map']))
    <script>
        let locationsCreators ={!! $tasksMapDataCreators !!};

        let locations ={!! $tasksMapDataExecutors !!};

        if (typeof initMap !== 'undefined') {
            initMap(locations);
        }




    </script>
@endif

