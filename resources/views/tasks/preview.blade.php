<?php /** @var \App\UserTasks $task */ ?>
@extends('layouts.main')

@section('title', $task->name)

@section('content')
    <div class="l-task-card-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="l-task-card">
                        <div class="l-task-card__content">
                            <div class="l-task-card__header">
                                <h1 class="h3">{{$task->name}}</h1><span class="e-tag e-tag_aqua-island"><span>{{$task->base_cost}}
                                        руб.</span></span>
                                <div class="c-task-card__tools">
                                    <span class="c-task-card__tool">{!! $task->status() !!}</span>
                                    <span class="c-task-card__tool">{{$task->views_count}} просмотров</span>
                                    <span class="c-task-card__tool">Создано {{$task->created_at}}</span>
                                    <span class="c-task-card__tool">{{$task->subcategory->name}}</span>
                                </div>
                            </div>
                            <div class="l-task-card__table @if(isset($task->data['address'])) map @endif">
                                @if(isset($task->data['address']))
                                    <div id="card-task-map" class="l-task-card__table-item clearfix map">
                                        {{--<div id="card-task-map" class="map_single"></div>--}}
                                        <div id="bid-map"></div>
                                    </div>

                                    @push('scripts')
                                        <script>
                                            var reBuildPoints = function () {
                                                var locations = {!! json_encode($task->data['address']) !!};
                                                var addresses = locations;
                                                if (locations.length >= 2) {
                                                    var pointA = locations[0]['name'];
                                                    delete locations[0];
                                                    var pointEnd = locations[locations.length - 1]['name'];
                                                    delete locations[locations.length - 1];
                                                    var waypoints = [];
                                                    $.each(locations, function (key, val) {
                                                        if (val !== undefined)
                                                            waypoints[waypoints.length] = {location: val['name']};
                                                    });
                                                    displayRoute(pointA, pointEnd, waypoints, addresses);
                                                }
                                            };

                                            $(function () {
                                                setTimeout(function () {
                                                    reBuildPoints();
                                                }, 1000);
                                            });
                                        </script>
                                    @endpush

                                    <div class="l-task-card__table-item clearfix">
                                        <div class="table-item__key">Адрес</div>
                                        @if(count($task->data['address'])==1)
                                            <div class="table-item__val">{{$task->data['address'][0]['name']}}</div>
                                        @else
                                            <div class="table-item__val">

                                                @foreach($task->data['address'] as $key => $address)
                                                    @if($key == 0)
                                                        <div class="table-item__val-adr"><span
                                                                    class="orange">От:</span> {{$address['name']}}
                                                        </div>
                                                    @else
                                                        <div class="table-item__val-adr"><span
                                                                    class="green">До:</span> {{$address['name']}}
                                                        </div>
                                                    @endif
                                                @endforeach
                                                <div class="table-item__val-distance">Примерное расстояние: <span
                                                            id="total_km"></span></div>
                                            </div>
                                        @endif
                                    </div>
                                @else
                                    <div class="l-task-card__table-item clearfix">
                                        <div class="table-item__key">Адрес</div>
                                        <div class="table-item__val">Виртуальное задание</div>
                                    </div>
                                @endif
                                @if(isset($task->data['date-and-time']))
                                    <div class="l-task-card__table-item clearfix">
                                        <div class="table-item__key">Начать</div>
                                        <div class="table-item__val">{{$task->data['date-and-time']['from_date']}}
                                            , {{$task->data['date-and-time']['from_time']}}</div>
                                    </div>
                                    @if(isset($task->data['date-and-time']['to_date']) && !empty($task->data['date-and-time']['to_date']))
                                        <div class="l-task-card__table-item clearfix">
                                            <div class="table-item__key">Законичить</div>
                                            <div class="table-item__val">{{$task->data['date-and-time']['to_date']}}
                                                , {{$task->data['date-and-time']['to_time']}}</div>
                                        </div>
                                    @endif
                                @endif

                                <div class="l-task-card__table-item clearfix">
                                    <div class="table-item__key">Стоимость</div>
                                    <div class="table-item__val">{{$task->base_cost}} руб.</div>
                                </div>
                                {{--<div class="l-task-card__table-item clearfix">
                                    <div class="table-item__key">Оплата</div>
                                    <div class="table-item__val">Онлайн</div>
                                </div>--}}
                                <div class="l-task-card__table-item need clearfix">
                                    <div class="table-item__key">Нужно</div>
                                    <div class="table-item__val">
                                        <p>{!! $task->description !!}</p>
                                        @if($task->photo)
                                            <img src="{{url('uploads/' . $task->photo)}}">
                                        @endif
                                    </div>
                                </div>
                                @if(($task->is_creator || $task->is_executor)&&$task->private_description)
                                    <div class="l-task-card__table-item need clearfix">
                                        <div class="table-item__key">Приватное описание</div>
                                        <div class="table-item__val">
                                            <p>{!! $task->private_description !!}</p>
                                        </div>
                                    </div>
                                @endif
                                @if($task->need_documents)
                                    <div class="l-task-card__table-item need clearfix">
                                        <label>
                                            <span>Исполнитель должен иметь документы для оформления расписки.</span>
                                        </label>
                                        <a href="{{url('receipt')}}" class="link_bondi">
                                            <span>Посмотреть пример расписки.</span>
                                        </a>
                                        <div class="l-bid-write__must-popup">
                                            <span>Мы настоятельно советуем оформлять расписку при передаче перевозимого исполнителем груза, денег</span>
                                        </div>
                                    </div>
                                @endif

                                <div class="l-task-card__table-edit">
                                    @if($task->status=='draft' || $task->status=='opened')
                                        <a href="#" data-toggle="modal" data-target="#edit-task-modal"
                                           class="but but_edit">
                                            <svg width="1em" height="1em" class="icon icon-pensil ">
                                                <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg')}}#icon-pensil"></use>
                                            </svg>
                                            <span>Редактировать  задание</span>
                                        </a>
                                        @include('tasks.blocks.share_tooltip')
                                        @push('scripts')
                                            <div id="edit-task-modal" class="modal fade edit-task-modal">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <button type="button" data-dismiss="modal"
                                                                class="but-modal-close"></button>
                                                        <div class="edit-task-modal__content">
                                                            <div class="edit-task-modal__item">
                                                                <div class="edit-task-modal__item-top"><a
                                                                            href="{{route('task.edit', ['category' => $task->subcategory->category->slug, 'subcategory' => $task->subcategory->slug,'userTasks' => $task->id])}}"
                                                                            class="link_bondi edit-task-modal__item-link">Изменить
                                                                        условия задания</a>
                                                                    <p class="edit-task-modal__item-about">Изменить
                                                                        время, адрес или другие условия задания</p>
                                                                </div>
                                                            </div>
                                                            <div class="edit-task-modal__item">
                                                                @php
                                                                    $cost_range = \App\PriceRangersMask::find($task->subcategory->range_id);
                                                                    $step = $cost_range->getStepByTaskCost($task->cost);
                                                                @endphp
                                                                <div class="edit-task-modal__item-top">
                                                                    <a href="#edit_1" data-toggle="collapse"
                                                                       class="link_bondi edit-task-modal__item-link">
                                                                        Изменить цену задания
                                                                    </a>
                                                                    <p class="edit-task-modal__item-about">
                                                                        Измененить цену задания или сделать цену
                                                                        открытой
                                                                    </p>
                                                                </div>
                                                                <div id="edit_1"
                                                                     class="edit-task-modal__item-content collapse">
                                                                    <form class="edit-task-modal__price" method="post"
                                                                          action="{{route('task.edit.field', ['userTasks' => $task->id, 'field' => 'cost'])}}">
                                                                        {{csrf_field()}}

                                                                        <div class="edit-task-modal__price-input">
                                                                            <input type="number"
                                                                                   value="{{old('exact_cost', $task->exact_cost != 0?$task->exact_cost:'' )}}"
                                                                                   name="exact_cost"
                                                                                   id="myPriceInput">
                                                                            <span>руб.</span>
                                                                        </div>
                                                                        <div class="l-bid-write__budget-cols">
                                                                            <div class="row">
                                                                                <div class="cols-item"><span></span>
                                                                                </div>
                                                                                <div class="cols-item"><span></span>
                                                                                </div>
                                                                                <div class="cols-item"><span></span>
                                                                                </div>
                                                                                <div class="cols-item"><span></span>
                                                                                </div>
                                                                                <div class="cols-item"><span></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="edit-task-modal__slider">
                                                                            <div class="l-bid-write__budget-slider">
                                                                                <script>
                                                                                    var firstLoad = '{{old('exact_cost', $task->exact_cost != 0?$task->exact_cost:'' )}}';
                                                                                    var ranges = {!! json_encode($cost_range) !!};
                                                                                </script>
                                                                                <input type="text" id="budget-slider"
                                                                                       class="price-slider">
                                                                                <input type="hidden" name="cost"
                                                                                       value="" id="price_from_range"/>
                                                                                <input type="hidden" name="cost_save"
                                                                                       value="{{$step ?? 'false'}}"/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="edit-task-modal__btn">
                                                                            <button type="submit" class="but but_green">
                                                                                Подтвердить
                                                                            </button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                            <div class="edit-task-modal__item">
                                                                <div class="edit-task-modal__item-top"><a href="#edit_2"
                                                                                                          data-toggle="collapse"
                                                                                                          class="link_bondi edit-task-modal__item-link">Задать
                                                                        вопрос службе поддержки</a>
                                                                    <p class="edit-task-modal__item-about">Служба
                                                                        поддержки поможет вам и ответит на любые ваши
                                                                        вопросы</p>
                                                                </div>
                                                                <div id="edit_2"
                                                                     class="edit-task-modal__item-content collapse">
                                                                    <div class="edit-task-modal__quest">
                                                                        <div class="edit-task-modal__quest-top">Наши
                                                                            менеджеры ответят на любые вопросы,
                                                                            связанные с вашим заданием, помогут сделать
                                                                            его более привлекательным для исполнителей,
                                                                            определиться с ценой и т.д.
                                                                        </div>
                                                                        <div class="edit-task-modal__quest-bot clearfix">
                                                                            <span>Контакты поддержки: {{get_static_info('phone-moscov')}} </span><a
                                                                                    href="mailto:{{get_static_info('email.main')}}">{{get_static_info('email.main')}}</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="edit-task-modal__item">
                                                                <div class="edit-task-modal__item-top"><a href="#"
                                                                                                          data-toggle="modal"
                                                                                                          data-target="#cancel-task-modal"
                                                                                                          class="link_bondi edit-task-modal__item-link">Отменить
                                                                        задание</a>
                                                                    <p class="edit-task-modal__item-about">Отправить
                                                                        запрос на удаление задания</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            @include('tasks.modals.cancel_task')

                                        @endpush
                                    @elseif($task->status=='in-progress' && !$task->creator_review)
                                        <div class="l-task-card__table-two-btns">
                                            <div class="table-two-btns__head">
                                                <p>Пожалуйста, укажите статус задания после того, как оно будет
                                                    выполнено или не выполнено.</p>
                                                <p>После этого вы сможете оставить отзыв о заказчике.</p>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="table-two-btns__item">
                                                        <a class="task-but green" data-toggle="modal" href="#"
                                                           data-target="#rev-send" data-rev-type="positive">
                                                            <span>Задание выполнено</span>
                                                        </a>
                                                        @include('tasks.blocks.share_tooltip')
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="table-two-btns__item">
                                                        <a class="task-but orange" data-toggle="modal" href="#"
                                                           data-target="#rev-send" data-rev-type="negative">
                                                            <span>Задание не выполнено</span></a>
                                                        <a href="#" class="link_bondi orange">
                                                            <svg width="1em" height="1em" class="icon icon-flag ">
                                                                <use xlink:href="{{asset('/s/images/useful/svg/theme/symbol-defs.svg#icon-flag')}}"></use>
                                                            </svg>
                                                            <span>Пожаловаться на задание</span></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div class="l-task-card__table-edit">
                                            <a href="{{route('task.copy', ['userTasks' => $task->id])}}"
                                               class="but but_edit orange"><span>Создать ещё раз</span></a>
                                            @include('tasks.blocks.share_tooltip')
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="l-task-card__recom clearfix">
                                <svg width="1em" height="1em" class="icon icon-warning ">
                                    <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg')}}#icon-warning"></use>
                                </svg>
                                <div class="l-task-card__recom-txt">
                                    <p>Если вы доверяете исполнителю ценные вещи, крупную задачу или впускаете в дом,
                                        проверьте его паспорт и попросите подписать документы. Шаблон расписки, типового
                                        договора и другие полезные советы</p><a href="{{url('faq')}}">смотрите в
                                        рекомендациях.</a>
                                </div>
                            </div>
                            @include('tasks.list_blocks._task_reviews')
                            <div class="l-task-card__bid">
                                <h5>{{ trans_choice('tasks.offers.count',$task->offers_count) }}</h5>
                                @if($task->offers->isNotEmpty())
                                    <div class="l-task-card__bid-sort"><span class="sort">Сортировать</span>
                                        <a class="offer-sort"
                                           href="{{route('task.offers.sort',$task)}}?sort=rate">
                                            <span>по рейтингу</span>
                                        </a>
                                        <a class="offer-sort" href="{{route('task.offers.sort',$task)}}?sort=reviews">
                                            <span>по отзывам</span>
                                        </a>
                                        <a class="offer-sort active"
                                           href="{{route('task.offers.sort',$task)}}?sort=date">
                                            <span>по времени</span>
                                        </a>
                                    </div>
                                    <div class="task-offers-list">
                                        @include('tasks.list_blocks._task_offers',['offers'=>$task->offers])
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @include('tasks.preview_blocks.right_bar')
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @auth
        @if($task->status=='in-progress')
            @if($task->is_creator)
                @include('tasks.modals.customer_review')
            @else
                @include('tasks.modals.executor_review')
            @endif
        @else
            @if($task->is_creator)
                @include('tasks.modals.select_executor_modal')
            @endif
        @endif
    @endauth
@endsection
