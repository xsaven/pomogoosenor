@extends('layouts.main')

@section('title', 'Мои заказы')

@section('content')
    <div class="l-all-tasks-wrapper">
        <form action="{{route('task.sort')}}" onsubmit="return false" id="task-sort-form">
            <div class="container">
                <div class="row">
                    @include('tasks.list_blocks.left_bar')
                    <div class="col-md-8">
                        <div class="all-tasks__filter">
                            <div class="all-tasks__filter-ttl"><span>Все категории</span>
                                <a href="#all-tasks__city-modal" id="city-name-link" data-toggle="modal">

                                    {{\App\TasksFilterCities::where('city_id', '=', $sort_data['city'])->first()->name}}
                                </a>
                                <input type="hidden" name="city" value="1" id="filter-city">
                            </div>
                            @if(Auth::check())
                                <div class="all-tasks__filter-body">
                                    <div class="all-tasks__filter-form">
                                        <div class="all-tasks__filter-line">
                                            <div class="all-tasks__filter-input-wrap">
                                                <input type="text" name="q" placeholder="Поиск по заданиям.."
                                                       value="{{$sort_data['q']}}" id="task-search-query"/>
                                                <svg width="1em" height="1em"
                                                     class="icon icon-search all-tasks__filter-submit">
                                                    <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-search')}}"></use>
                                                </svg>
                                                </button>
                                            </div>
                                            <div class="all-tasks__filter-select-wrap">
                                                <select class="selectpicker" name="status">
                                                    <option data-content="&lt;svg width=&quot;1em&quot; height=&quot;1em&quot; class=&quot;icon icon-mark icon-mark_green&quot;&gt;&lt;use xmlns:xlink=&quot;http://www.w3.org/1999/xlink&quot; xlink:href=&quot;/s/images/useful/svg/theme/symbol-defs.svg#icon-mark&quot;&gt;&lt;/use&gt;&lt;/svg&gt;&lt;span&gt;Открытые&lt;/span&gt;"
                                                            {{ $sort_data['status'] == 1?'selected':'' }} value="1">Открытые
                                                    </option>
                                                    <option data-content="&lt;svg width=&quot;1em&quot; height=&quot;1em&quot; class=&quot;icon icon-mark icon-mark_red&quot;&gt;&lt;use xmlns:xlink=&quot;http://www.w3.org/1999/xlink&quot; xlink:href=&quot;/s/images/useful/svg/theme/symbol-defs.svg#icon-mark&quot;&gt;&lt;/use&gt;&lt;/svg&gt;&lt;span&gt;Завершенные&lt;/span&gt;"
                                                            {{ $sort_data['status'] == 3?'selected':'' }} value="3">
                                                        Завершенные
                                                    </option>
                                                    <option data-content="&lt;svg width=&quot;1em&quot; height=&quot;1em&quot; class=&quot;icon icon-mark icon-mark_orange&quot;&gt;&lt;use xmlns:xlink=&quot;http://www.w3.org/1999/xlink&quot; xlink:href=&quot;/s/images/useful/svg/theme/symbol-defs.svg#icon-mark&quot;&gt;&lt;/use&gt;&lt;/svg&gt;&lt;span&gt;Выполняются&lt;/span&gt;"
                                                            {{ $sort_data['status'] == 2?'selected':'' }} value="2">
                                                        Выполняются
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="all-tasks__filter-checkboxes">
                                            <ul class="h-list">
                                                <li>
                                                    <label class="check-label">
                                                        <input name="no_offers"
                                                               type="checkbox" value="1" {{ $sort_data['no_offers']?'checked':'' }}/>
                                                        <i class="checkbox"></i>
                                                        <span>Еще без предложений</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    {{--<label class="check-label">--}}
                                                    {{--<input id="sort-no-offers" type="checkbox" {{ $hot?'checked':'' }}/><i class="checkbox"></i><span>Горячие задания</span>--}}
                                                    {{--</label>--}}
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="all-tasks__filter-body">
                                    <div class="all-tasks__filter-txt"><span>Новые задания доступны только для исполнителей Pomogoo</span>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <button type="button" class="all-tasks__map-trig">
                            <svg width="1em" height="1em" class="icon icon-expand ">
                                <use xlink:href="<?php echo e(asset('/s/images/useful/svg/theme/symbol-defs.svg#icon-expand')); ?>"></use>

                            </svg>
                            <span class="hide-text">Cкрыть карту</span><span class="show-text">Раскрыть карту</span>
                        </button>
                        <div class="all-tasks__list" id="tasks-list">
                            @include('tasks.list_blocks.all_tasks_items')
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    @widget('Footer')
    @include('tasks.list_blocks.modals')

@endsection

@include('tasks.blocks.js')

