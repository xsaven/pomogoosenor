<?php /** @var \App\UserTasks $task */ ?>
@extends('layouts.main')

@section('title', $task->name)

@section('content')
    <div class="l-task-card-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="l-task-card">
                        <div class="l-task-card__content">
                            <div class="l-task-card__header">
                                <h1 class="h3">{{$task->name}}</h1><span class="e-tag e-tag_aqua-island"><span>{{$task->base_cost}}
                                        руб.</span></span>
                                <div class="c-task-card__tools">
                                    <span class="c-task-card__tool">{!! $task->status() !!}</span>
                                    <span class="c-task-card__tool">{{$task->views_count}} просмотров</span>
                                    <span class="c-task-card__tool">Создано {{$task->created_at}}</span>
                                    <span class="c-task-card__tool">{{$task->subcategory->name}}</span>
                                </div>
                            </div>
                            <div class="l-task-card__table @if(isset($task->data['address'])) map @endif">
                                @if(isset($task->data['address']))
                                    <div id="card-task-map" class="l-task-card__table-item clearfix map">
                                        {{--<div id="card-task-map" class="map_single"></div>--}}
                                        <div id="bid-map"></div>
                                    </div>

                                    @push('scripts')
                                        <script>
                                            var reBuildPoints = function () {
                                                var locations = {!! json_encode($task->data['address']) !!};
                                                var addresses = locations;
                                                if (locations.length >= 2) {
                                                    var pointA = locations[0]['name'];
                                                    delete locations[0];
                                                    var pointEnd = locations[locations.length - 1]['name'];
                                                    delete locations[locations.length - 1];
                                                    var waypoints = [];
                                                    $.each(locations, function (key, val) {
                                                        if (val !== undefined)
                                                            waypoints[waypoints.length] = {location: val['name']};
                                                    });
                                                    displayRoute(pointA, pointEnd, waypoints, addresses);
                                                }
                                            };

                                            $(function () {
                                                setTimeout(function () {
                                                    reBuildPoints();
                                                }, 1000);
                                            });
                                        </script>
                                    @endpush

                                    <div class="l-task-card__table-item clearfix">
                                        <div class="table-item__key">Адрес</div>
                                        @if(count($task->data['address'])==1)
                                            <div class="table-item__val">{{$task->data['address'][0]['name']}}</div>
                                        @else
                                            <div class="table-item__val">

                                                @foreach($task->data['address'] as $key => $address)
                                                    @if($key == 0)
                                                        <div class="table-item__val-adr"><span
                                                                    class="orange">От:</span> {{$address['name']}}
                                                        </div>
                                                    @else
                                                        <div class="table-item__val-adr"><span
                                                                    class="green">До:</span> {{$address['name']}}
                                                        </div>
                                                    @endif
                                                @endforeach
                                                <div class="table-item__val-distance">Примерное расстояние: <span
                                                            id="total_km"></span></div>
                                            </div>
                                        @endif
                                    </div>
                                @endif
                                @if(isset($task->data['date-and-time']))
                                    <div class="l-task-card__table-item clearfix">
                                        <div class="table-item__key">Начать</div>
                                        <div class="table-item__val">{{$task->data['date-and-time']['from_date']}}
                                            , {{$task->data['date-and-time']['from_time']}}</div>
                                    </div>
                                    @if(isset($task->data['date-and-time']['to_date']) && !empty($task->data['date-and-time']['to_date']))
                                        <div class="l-task-card__table-item clearfix">
                                            <div class="table-item__key">Законичить</div>
                                            <div class="table-item__val">{{$task->data['date-and-time']['to_date']}}
                                                , {{$task->data['date-and-time']['to_time']}}</div>
                                        </div>
                                    @endif
                                @endif

                                <div class="l-task-card__table-item clearfix">
                                    <div class="table-item__key">Стоимость</div>
                                    <div class="table-item__val">{{$task->base_cost}} руб.</div>
                                </div>
                                {{--<div class="l-task-card__table-item clearfix">
                                    <div class="table-item__key">Оплата</div>
                                    <div class="table-item__val">Онлайн</div>
                                </div>--}}
                                <div class="l-task-card__table-item need clearfix">
                                    <div class="table-item__key">Нужно</div>
                                    <div class="table-item__val">
                                        <p>{!! $task->description !!}</p>
                                    </div>
                                </div>
                                @if(Auth::check() && user()->is_worker())
                                    @if($task->status=='opened' && !$task->hasUserOffers(user()->id))
                                        <div class="l-task-card__table-add">
                                            <div class="l-task-card__table-add-wrp clearfix">
                                                <a href="#" data-toggle="modal"
                                                   data-target="#task-offer-modal" class="table-btns__add">
                                                    <b>Добавить предложение </b>
                                                    <span>Чтобы отправить свои контакты заказчику </span>
                                                </a>
                                                <div class="table-btns__add-lnks">
                                                    <a href="#" data-toggle="tooltip" data-target="#shareTooltip">Поделиться
                                                        ссылкой</a>
                                                    <a href="#" data-toggle="modal" data-target="#block-task-modal">Пожаловаться
                                                        на задание</a>
                                                </div>
                                            </div>
                                        </div>
                                    @elseif(($task->getOriginal('status')==\App\UserTasks::STATUS_IN_PROGRESS || $task->getOriginal('status') == \App\UserTasks::STATUS_IN_ARBITRATION) && $task->is_executor)
                                        @if(!$task->executor_review)
                                            <div class="l-task-card__table-two-btns">
                                                <div class="table-two-btns__head">
                                                    <p>Пожалуйста, укажите статус задания после того, как оно будет
                                                        выполнено или не выполнено.</p>
                                                    <p>После этого вы сможете оставить отзыв о заказчике.</p>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="table-two-btns__item">
                                                            <a class="task-but green" data-toggle="modal" href="#"
                                                               data-target="#rev-send" data-rev-type="positive">
                                                                <span>Задание выполнено</span>
                                                            </a>
                                                            @include('tasks.blocks.share_tooltip')

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="table-two-btns__item">
                                                            <a class="task-but orange" data-toggle="modal" href="#"
                                                               data-target="#rev-send" data-rev-type="negative">
                                                                <span>Задание не выполнено</span></a>
                                                            <a href="#" class="link_bondi orange">
                                                                <svg width="1em" height="1em" class="icon icon-flag ">
                                                                    <use xlink:href="{{asset('/s/images/useful/svg/theme/symbol-defs.svg#icon-flag')}}"></use>
                                                                </svg>
                                                                <span>Пожаловаться на задание</span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @else
                                        <div class="l-task-card__table-btns">
                                            <a href="" onclick="return false" class="table-btns__add">
                                                <b>Добавить предложение</b>
                                                <span>Чтобы отправить свои контакты заказчику</span>
                                            </a>
                                            @include('tasks.blocks.share_tooltip')
                                        </div>
                                    @endif
                                @else
                                    <div class="l-task-card__table-btns"><strong>Если вы хотите получить подобную
                                            услугу</strong>
                                        <div class="l-task-card__to-create">
                                            <a href="{{route('task.copy', ['userTasks' => $task->id])}}"
                                               class="but but_y-orange">
                                                <span>Создайте такое же задание</span>
                                            </a>
                                            <a href="{{route('testing.start')}}" class="link_bondi">или станьте
                                                исполнителем</a>
                                        </div>
                                        <div class="text-center" style="margin-top: 20px;">
                                            @include('tasks.blocks.share_tooltip')
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="l-task-card__recom clearfix">
                                <svg width="1em" height="1em" class="icon icon-warning ">
                                    <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg')}}#icon-warning"></use>
                                </svg>
                                <div class="l-task-card__recom-txt">
                                    <p>Если вы доверяете исполнителю ценные вещи, крупную задачу или впускаете в дом,
                                        проверьте его паспорт и попросите подписать документы. Шаблон расписки, типового
                                        договора и другие полезные советы</p><a href="{{url('faq')}}">смотрите в
                                        рекомендациях.</a>
                                </div>
                            </div>
                            @include('tasks.list_blocks._task_reviews')
                            <div class="l-task-card__bid">
                                <h5>{{ trans_choice('tasks.offers.count',$task->offers_count) }}</h5>

                                @if($task->offers->isNotEmpty() && Auth::check())
                                    <div class="task-offers-list">
                                        @include('tasks.list_blocks._task_offers',['offers'=>$task->offers()->whereUserId(user()->id)->get()])
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                @include('tasks.preview_blocks.right_bar')
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @auth
        @if($task->getOriginal('status')==\App\UserTasks::STATUS_IN_PROGRESS || $task->getOriginal('status') == \App\UserTasks::STATUS_IN_ARBITRATION)
            @if($task->is_creator)
                @include('tasks.modals.customer_review')
            @else
                @include('tasks.modals.executor_review')
            @endif
        @else
            @include('tasks.modals.offer_modal')
        @endif

        @include('tasks.modals.block')


    @endauth
@endsection
