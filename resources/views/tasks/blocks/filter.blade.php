<div class="all-tasks__sidebar-collapses">
    <div class="l-prof-set__subs-item">
        <div class="l-prof-set__subs-item-but collapsed">
            <label class="check-label main-label">
                <input type="checkbox"/><i class="checkbox"></i><span>Все категории </span>
            </label>
        </div>
    </div>
    <div class="l-prof-set__subs-item">
        <div data-toggle="collapse" data-target="#serv_2" class="l-prof-set__subs-item-but">
            <label class="check-label main-label">
                <input type="checkbox"/><i class="checkbox"></i><span>Курьерские услуги </span>
            </label>
        </div>
        <div id="serv_2" class="l-prof-set__subs-item-cont collapse in">
            <div class="l-prof-set__subs-item-cont-wrp">
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги пешего курьера <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги курьера на легковом авто <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Купить и доставить <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Срочная доставка <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка продуктов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка еды из ресторанов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Курьер на день <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Другая посылка <b>( от 550 руб.)</b></span>
                </label>
            </div>
        </div>
    </div>
    <div class="l-prof-set__subs-item">
        <div data-toggle="collapse" data-target="#serv_3" class="l-prof-set__subs-item-but collapsed">
            <label class="check-label main-label">
                <input type="checkbox"/><i class="checkbox"></i><span>Бытовой ремонт </span>
            </label>
        </div>
        <div id="serv_3" class="l-prof-set__subs-item-cont collapse">
            <div class="l-prof-set__subs-item-cont-wrp">
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги пешего курьера <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги курьера на легковом авто <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Купить и доставить <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Срочная доставка <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка продуктов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка еды из ресторанов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Курьер на день <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Другая посылка <b>( от 550 руб.)</b></span>
                </label>
            </div>
        </div>
    </div>
    <div class="l-prof-set__subs-item">
        <div data-toggle="collapse" data-target="#serv_4" class="l-prof-set__subs-item-but collapsed">
            <label class="check-label main-label">
                <input type="checkbox"/><i class="checkbox"></i><span>Грузоперевозки </span>
            </label>
        </div>
        <div id="serv_4" class="l-prof-set__subs-item-cont collapse">
            <div class="l-prof-set__subs-item-cont-wrp">
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги пешего курьера <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги курьера на легковом авто <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Купить и доставить <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Срочная доставка <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка продуктов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка еды из ресторанов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Курьер на день <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Другая посылка <b>( от 550 руб.)</b></span>
                </label>
            </div>
        </div>
    </div>
    <div class="l-prof-set__subs-item">
        <div data-toggle="collapse" data-target="#serv_5" class="l-prof-set__subs-item-but collapsed">
            <label class="check-label main-label">
                <input type="checkbox"/><i class="checkbox"></i><span>Уборка и помощь по хозяйству </span>
            </label>
        </div>
        <div id="serv_5" class="l-prof-set__subs-item-cont collapse">
            <div class="l-prof-set__subs-item-cont-wrp">
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги пешего курьера <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги курьера на легковом авто <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Купить и доставить <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Срочная доставка <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка продуктов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка еды из ресторанов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Курьер на день <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Другая посылка <b>( от 550 руб.)</b></span>
                </label>
            </div>
        </div>
    </div>
    <div class="l-prof-set__subs-item">
        <div data-toggle="collapse" data-target="#serv_6" class="l-prof-set__subs-item-but collapsed">
            <label class="check-label main-label">
                <input type="checkbox"/><i class="checkbox"></i><span>Виртуальный помощник </span>
            </label>
        </div>
        <div id="serv_6" class="l-prof-set__subs-item-cont collapse">
            <div class="l-prof-set__subs-item-cont-wrp">
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги пешего курьера <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги курьера на легковом авто <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Купить и доставить <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Срочная доставка <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка продуктов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка еды из ресторанов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Курьер на день <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Другая посылка <b>( от 550 руб.)</b></span>
                </label>
            </div>
        </div>
    </div>
    <div class="l-prof-set__subs-item">
        <div data-toggle="collapse" data-target="#serv_7" class="l-prof-set__subs-item-but collapsed">
            <label class="check-label main-label">
                <input type="checkbox"/><i class="checkbox"></i><span>Компьютерная помощь </span>
            </label>
        </div>
        <div id="serv_7" class="l-prof-set__subs-item-cont collapse">
            <div class="l-prof-set__subs-item-cont-wrp">
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги пешего курьера <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги курьера на легковом авто <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Купить и доставить <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Срочная доставка <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка продуктов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка еды из ресторанов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Курьер на день <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Другая посылка <b>( от 550 руб.)</b></span>
                </label>
            </div>
        </div>
    </div>
    <div class="l-prof-set__subs-item">
        <div data-toggle="collapse" data-target="#serv_8" class="l-prof-set__subs-item-but collapsed">
            <label class="check-label main-label">
                <input type="checkbox"/><i class="checkbox"></i><span>Мероприятия и промо-акции </span>
            </label>
        </div>
        <div id="serv_8" class="l-prof-set__subs-item-cont collapse">
            <div class="l-prof-set__subs-item-cont-wrp">
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги пешего курьера <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги курьера на легковом авто <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Купить и доставить <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Срочная доставка <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка продуктов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка еды из ресторанов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Курьер на день <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Другая посылка <b>( от 550 руб.)</b></span>
                </label>
            </div>
        </div>
    </div>
    <div class="l-prof-set__subs-item">
        <div data-toggle="collapse" data-target="#serv_9" class="l-prof-set__subs-item-but collapsed">
            <label class="check-label main-label">
                <input type="checkbox"/><i class="checkbox"></i><span>Дизайн </span>
            </label>
        </div>
        <div id="serv_9" class="l-prof-set__subs-item-cont collapse">
            <div class="l-prof-set__subs-item-cont-wrp">
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги пешего курьера <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги курьера на легковом авто <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Купить и доставить <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Срочная доставка <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка продуктов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка еды из ресторанов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Курьер на день <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Другая посылка <b>( от 550 руб.)</b></span>
                </label>
            </div>
        </div>
    </div>
    <div class="l-prof-set__subs-item">
        <div data-toggle="collapse" data-target="#serv_10" class="l-prof-set__subs-item-but collapsed">
            <label class="check-label main-label">
                <input type="checkbox"/><i class="checkbox"></i><span>Web-разработка </span>
            </label>
        </div>
        <div id="serv_10" class="l-prof-set__subs-item-cont collapse">
            <div class="l-prof-set__subs-item-cont-wrp">
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги пешего курьера <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги курьера на легковом авто <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Купить и доставить <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Срочная доставка <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка продуктов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка еды из ресторанов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Курьер на день <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Другая посылка <b>( от 550 руб.)</b></span>
                </label>
            </div>
        </div>
    </div>
    <div class="l-prof-set__subs-item">
        <div data-toggle="collapse" data-target="#serv_11" class="l-prof-set__subs-item-but collapsed">
            <label class="check-label main-label">
                <input type="checkbox"/><i class="checkbox"></i><span>Фото- и видео-услуги </span>
            </label>
        </div>
        <div id="serv_11" class="l-prof-set__subs-item-cont collapse">
            <div class="l-prof-set__subs-item-cont-wrp">
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги пешего курьера <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги курьера на легковом авто <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Купить и доставить <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Срочная доставка <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка продуктов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка еды из ресторанов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Курьер на день <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Другая посылка <b>( от 550 руб.)</b></span>
                </label>
            </div>
        </div>
    </div>
    <div class="l-prof-set__subs-item">
        <div data-toggle="collapse" data-target="#serv_12" class="l-prof-set__subs-item-but collapsed">
            <label class="check-label main-label">
                <input type="checkbox"/><i class="checkbox"></i><span>Установка и ремонт техники </span>
            </label>
        </div>
        <div id="serv_12" class="l-prof-set__subs-item-cont collapse">
            <div class="l-prof-set__subs-item-cont-wrp">
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги пешего курьера <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги курьера на легковом авто <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Купить и доставить <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Срочная доставка <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка продуктов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка еды из ресторанов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Курьер на день <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Другая посылка <b>( от 550 руб.)</b></span>
                </label>
            </div>
        </div>
    </div>
    <div class="l-prof-set__subs-item">
        <div data-toggle="collapse" data-target="#serv_13" class="l-prof-set__subs-item-but collapsed">
            <label class="check-label main-label">
                <input type="checkbox"/><i class="checkbox"></i><span>Красота и здоровье </span>
            </label>
        </div>
        <div id="serv_13" class="l-prof-set__subs-item-cont collapse">
            <div class="l-prof-set__subs-item-cont-wrp">
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги пешего курьера <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги курьера на легковом авто <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Купить и доставить <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Срочная доставка <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка продуктов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка еды из ресторанов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Курьер на день <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Другая посылка <b>( от 550 руб.)</b></span>
                </label>
            </div>
        </div>
    </div>
    <div class="l-prof-set__subs-item">
        <div data-toggle="collapse" data-target="#serv_14" class="l-prof-set__subs-item-but collapsed">
            <label class="check-label main-label">
                <input type="checkbox"/><i class="checkbox"></i><span>Ремонт цифровой техники </span>
            </label>
        </div>
        <div id="serv_14" class="l-prof-set__subs-item-cont collapse">
            <div class="l-prof-set__subs-item-cont-wrp">
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги пешего курьера <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги курьера на легковом авто <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Купить и доставить <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Срочная доставка <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка продуктов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка еды из ресторанов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Курьер на день <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Другая посылка <b>( от 550 руб.)</b></span>
                </label>
            </div>
        </div>
    </div>
    <div class="l-prof-set__subs-item">
        <div data-toggle="collapse" data-target="#serv_15" class="l-prof-set__subs-item-but collapsed">
            <label class="check-label main-label">
                <input type="checkbox"/><i class="checkbox"></i><span>Юридическая помощь </span>
            </label>
        </div>
        <div id="serv_15" class="l-prof-set__subs-item-cont collapse">
            <div class="l-prof-set__subs-item-cont-wrp">
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги пешего курьера <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги курьера на легковом авто <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Купить и доставить <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Срочная доставка <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка продуктов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка еды из ресторанов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Курьер на день <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Другая посылка <b>( от 550 руб.)</b></span>
                </label>
            </div>
        </div>
    </div>
    <div class="l-prof-set__subs-item">
        <div data-toggle="collapse" data-target="#serv_16" class="l-prof-set__subs-item-but collapsed">
            <label class="check-label main-label">
                <input type="checkbox"/><i class="checkbox"></i><span>Репетиторы и образование </span>
            </label>
        </div>
        <div id="serv_16" class="l-prof-set__subs-item-cont collapse">
            <div class="l-prof-set__subs-item-cont-wrp">
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги пешего курьера <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги курьера на легковом авто <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Купить и доставить <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Срочная доставка <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка продуктов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка еды из ресторанов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Курьер на день <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Другая посылка <b>( от 550 руб.)</b></span>
                </label>
            </div>
        </div>
    </div>
    <div class="l-prof-set__subs-item">
        <div data-toggle="collapse" data-target="#serv_17" class="l-prof-set__subs-item-but collapsed">
            <label class="check-label main-label">
                <input type="checkbox"/><i class="checkbox"></i><span>Ремонт транспорта </span>
            </label>
        </div>
        <div id="serv_17" class="l-prof-set__subs-item-cont collapse">
            <div class="l-prof-set__subs-item-cont-wrp">
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги пешего курьера <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Услуги курьера на легковом авто <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Купить и доставить <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Срочная доставка <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка продуктов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Доставка еды из ресторанов <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Курьер на день <b>( от 550 руб.)</b></span>
                </label>
                <label class="check-label">
                    <input type="checkbox"/><i class="checkbox"></i><span>Другая посылка <b>( от 550 руб.)</b></span>
                </label>
            </div>
        </div>
    </div>
</div>