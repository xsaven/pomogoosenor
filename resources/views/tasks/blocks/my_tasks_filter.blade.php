<div class="all-tasks__filter-body my_tasks_filter">
        @if($executor_tasks->isEmpty())
                <div class="all-tasks__filter-txt"><span>У вас пока нет созданных заданий и черновиков</span></div>
        @endif
</div>
<div class="all-tasks__filter-body my_tasks_filter hidden">
        @if($creator_tasks->isEmpty())
                <div class="all-tasks__filter-txt"><span>У вас пока нет созданных заданий и черновиков</span></div>
        @endif
</div>