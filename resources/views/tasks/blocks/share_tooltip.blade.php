<a class="link_bondi" data-toggle="popover"
   data-content='{{ Widget::run('SocialShare',['url'=>url()->current(),'flat'=>true]) }}' data-placement="bottom">
    <svg width="1em" height="1em" class="icon icon-twtr ">
        <use xlink:href="{{asset('/s/images/useful/svg/theme/symbol-defs.svg#icon-twtr')}}"></use>
    </svg>
    <span>Поделиться ссылкой</span>
</a>