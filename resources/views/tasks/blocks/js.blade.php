@push('scripts')
    <script>
        function initMap(locations) {
            let defaultMapCoords = (typeof Object.values(locations)[0] !== 'undefined' && typeof Object.values(locations)[0].coords !== 'undefined') ? Object.values(locations)[0].coords : {
                lat: 55.753960,
                lng: 37.620393
            };

            defaultMapCoords = {
                lat: parseInt(defaultMapCoords.lat),
                lng: parseInt(defaultMapCoords.lng),
            };

            let map = new google.maps.Map(document.getElementById('all-tasks__map'), {
                center: defaultMapCoords,
                mapTypeId: 'roadmap',
                disableDefaultUI: true,
                zoom: 3
            });

            let map_block = $('.all-tasks__map-inner');
            let bounds = new google.maps.LatLngBounds();

            function update_map() {
                map.setZoom(18);
                bounds = new google.maps.LatLngBounds();
                for (i = 0; i < locations.length; i++) {
                    var position = new google.maps.LatLng(locations[i].coords.lat, locations[i].coords.lng);
                    bounds.extend(position);
                }

                if (locations.length > 1) {
                    map.fitBounds(bounds);
                } else {
                    var position = new google.maps.LatLng(locations[0].coords.lat, locations[0].coords.lng);
                    map.setCenter(position);
                }

            }

            window.boundsHash = null;
            map.addListener('bounds_changed', function () {
                let bounds = [];
                let border = map.getBounds();
                $.each(locations, function (i, element) {
                    if (typeof element.coords === 'undefined') return true;

                    var position = new google.maps.LatLng(element.coords.lat, element.coords.lng);

                    if (border && border.contains(position)) {
                        bounds.push(i);
                    }
                });

                if (boundsHash !== null && bounds.length !== 0 && bounds.length !== boundsHash.length) {
                    setTimeout(function () {
                        form = $('#task-sort-form');
                        var data = form.serialize();
                        var url = form.prop('action');
                        var subcategories = [];
                        $.each($('.category-checkbox'), function (key, category) {
                            if ($(category).prop('checked') == true) {
                                subcategories.push($(category).val())
                            }
                        });
                        data += '&subcategories=' + subcategories.join();
                        data += '&map=' + bounds.join();

                        $.get(url, data, function (response) {
                            $('#tasks-list').html(response);
                        });
                    }, 500);

                }
                window.boundsHash = bounds;
            });


            $.each(locations, function (i, element) {
                if (typeof element.coords === 'undefined') return true;
                console.log(element.coords);
                var position = new google.maps.LatLng(element.coords.lat, element.coords.lng);

                bounds.extend(position);

                let infoWindow = new google.maps.InfoWindow({
                    content: '' +
                        'Задание: ' + element.balloon.name + '<br/>' +
                        'Описание: ' + element.balloon.description + '<br/>' +
                        'Цена: ' + element.balloon.price
                });

                let marker = new google.maps.Marker({
                    position: position,
                    icon: element.icon_src,
                    map: map,
                    title: element.title
                });

                google.maps.event.addListener(marker, 'click', function () {
                    infoWindow.open(map, marker);
                });

                google.maps.event.addListener(map, 'click', function () {
                    infoWindow.close();
                });

                // Automatically center the map fitting all locations on the screen
                if (locations.length > 1) {
                    map.fitBounds(bounds);
                }

            });


            // $(window).on('resize', function () {
            //     update_map();
            // });
            map_block.on('map_replace', function () {
                google.maps.event.trigger(map, 'resize');
                update_map();
            });
            $('.all-tasks__map-trig').on('click', function (e) {
                var bth = $('.all-tasks__map-trig');
                if (map_block.closest('.all-tasks__sidebar').length > 0) {
                    bth.addClass('active');
                    $('.all-tasks__filter').after(map_block);
                } else {
                    bth.removeClass('active');
                    $('.all-tasks__map-wrap').prepend(map_block);
                }

                map_block.trigger('map_replace');
            });

        }
    </script>
@endpush