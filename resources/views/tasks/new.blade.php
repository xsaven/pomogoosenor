@extends('layouts.main')

@section('title', $selectCategory->name)

@section('content')
    <div class="l-bid">
        @guest()
            <div class="l-bid-head">
                <div class="container">
                    <div class="l-bid-head__ttl">- {{$selectSubcategory->top_text}}</div>
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <div class="l-bid-head__item clearfix"><b class="l-bid-head__item-numb">1</b>
                                <div class="l-bid-head__item-txt">
                                    <div class="l-bid-head__item-name">Заполните заявку</div>
                                    <div class="l-bid-head__item-about">Мы быстро оповестим исполнителей Pomogoo <br>о
                                        вашей заявке.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="l-bid-head__item clearfix"><b class="l-bid-head__item-numb">2</b>
                                <div class="l-bid-head__item-txt">
                                    <div class="l-bid-head__item-name">Получите предложения</div>
                                    <div class="l-bid-head__item-about">Заинтерисаванные исполнители предложат вам <br>свои
                                        услуги.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="l-bid-head__item clearfix"><b class="l-bid-head__item-numb">3</b>
                                <div class="l-bid-head__item-txt">
                                    <div class="l-bid-head__item-name">Выберите исполнителя</div>
                                    <div class="l-bid-head__item-about">Выберите подходящее для вас предложение по цене
                                        <br>или рейтингу исполнителя
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endguest
        <div class="l-bid__body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="l-bid-write-wrp">
                            <div class="l-bid-write">
                                <div class="l-bid-write__ttl">Заполните заявку</div>
                                <div id="createdForm">
                                    @include('tasks.forms.new')
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="l-bid-faq">
                            <div class="l-bid-faq__ttl">Частые вопросы</div>
                            <ul>
                                @if (isset($faq))
                                    @foreach($faq as $row)
                                        <li><a href="{{url('faq#'.$row->slug)}}">{{$row->question}}</a></li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @widget('Footer')

    <script>
        var selectCategory = function (obj) {

            let form = $('#new-task-form');
            let data = form.serialize();

            jQuery.ajax({
                url: '/task/store',
                method: 'POST',
                dataType: 'json',
                data: data,
                success: function (res) {
                    location = obj.val();
                }
            });

        }

    </script>
@stop

@push('scripts')
    <script>
        $(document).ready(function () {
            $(window).keydown(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
        });
    </script>
@endpush

@php(Session::forget('_old_input'))