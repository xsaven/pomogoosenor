<?php /** @var \App\Category[] $categories */ ?>
<div class="col-md-4">
    <form class="filter-form">
        <div class="l-catalog-sidebar">
            <ul>
                <li>
                    <input type="radio" class="hidden" name="subcategory" value="" id="subcategory_all">
                    <a href="javascript:">
                        <label for="subcategory_all">Все категории</label>
                    </a>
                </li>
                @foreach($categories as $category)
                    <li>
                        <a data-toggle="collapse" href="#category_{{$category->id}}">{{$category->name}}</a>
                        <ul id="category_{{$category->id}}"
                            class="collapse {{$category->id==$sort_data['category']?'in':''}}">
                            @foreach($category->subcategories as $subcategory)
                                <li>
                                    <input type="radio" class="hidden" name="subcategory" value="{{$subcategory->id}}"
                                           id="subcategory_{{$subcategory->id}}">
                                    <a class="link-to-category @if($subcategory->id == $sort_data['subcategory']??0) active @endif "
                                       href="javascript:"><label
                                                for="subcategory_{{$subcategory->id}}">{{$subcategory->name}}</label></a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endforeach
            </ul>
        </div>
    </form>
    @include('blocks.banner')
    @widget('Blog')
</div>
{{--{{route('executors.sort')}}?subcategory={{$subcategory->id}}--}}