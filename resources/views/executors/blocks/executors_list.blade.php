<?php /** @var \App\User[] $executors */ ?>
<div class="l-cat-serv__body">
    @foreach($executors as $executor)
        <div class="l-cat-serv__item">
            <div class="l-cat-serv__item-left">
                <a href="{{route('profile_show',['user'=>$executor])}}">

                    <div class="h-object-fit">
                        <img src="{{asset($executor->avatar())}}" alt=""/>
                    </div>
                </a>

                @include('blocks.user.reviews_small',['user'=>$executor])
            </div>
            <div class="l-cat-serv__item-right">
                <div class="l-cat-serv__item-right-head clearfix">
                    <div class="l-cat-serv__it-rght-hd-left">
                        <a href="{{route('profile_show',['user'=>$executor])}}">
                            <div class="l-cat-serv__item-name">{{ $executor->name }}</div>
                        </a>
                        @include('blocks.user.badges_small')
                    </div>
                    @if($executor->profile->workerProfile->min_price)
                        <div class="l-cat-serv__it-rght-hd-right">{{$executor->profile->workerProfile->min_price}}
                            руб./час
                        </div>
                    @endif
                </div>
                @if($executor->online)
                    <div class="l-cat-serv__it-rght-hd-status">Сейчас на сайте</div>
                @else
                    <div class="l-cat-serv__it-rght-hd-status-offline">Не на сайте</div>
                @endif
                <div class="l-cat-serv__item-right-body">
                    <p>{!! $executor->profile->workerProfile->about_me_limited !!}</p>
                </div>
                <div class="l-cat-serv__item-right-footer">
                    @if(Auth::check())
                        @if(!$executor->is_busy)
                            <a href="{{route('profile_show',['user'=>$executor])}}" class="but but_bondi">
                                <span>Предложить задание</span>
                            </a>
                        @endif
                    @else
                        <a href="#" data-toggle="modal" data-target="#fastregmail" class="but but_bondi"><span>Предложить задание</span></a>
                    @endif
                </div>
            </div>
        </div>
    @endforeach
</div>
{{ $executors->links() }}