@extends('layouts.main')

@section('title', 'Исполнители')

@section('content')
    <div class="l-catalog-wrapper">
        <div class="container">
            <div class="row row_no-pad">
                @include('executors.blocks.categories')
                <div class="col-md-8">
                    <div class="l-catalog-services">
                        <div class="l-cat-serv__head">
                            @if($sort_data['subcategory'])
                                <b id="subcategory-label">{{\App\Subcategory::find($sort_data['subcategory'])->name}}</b>
                            @else
                                <b id="subcategory-label">Все категории</b>
                            @endif
                            <form class="l-cat-serv__bottom clearfix filter-form">
                                <div class="l-cat-serv__bottom-left">
                                    <select title="По рейтингу" class="selectpicker" name="type">
                                        <option data-icon="ico ico-orange-star" value="rate">По рейтингу</option>
                                        <option data-icon="ico ico-green-user" value="new">Новые исполнители</option>
                                    </select>
                                    <label>
                                        <input type="checkbox" name="online" value="true"/>
                                        <i></i><span>Сейчас на сайте</span>
                                    </label>
                                </div>
                                <div class="l-cat-serv__bottom-right">
                                    <a href="#executors-city-modal" id="city-name-link" data-toggle="modal">
                                        {{\App\TasksFilterCities::where('city_id', '=', $sort_data['city'])->first()->name}}
                                    </a>
                                </div>
                            </form>
                        </div>
                        <div id="executors-list">
                            @include('executors.blocks.executors_list')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('executors.modals.city_modal')
@endsection