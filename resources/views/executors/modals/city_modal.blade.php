<div id="executors-city-modal" class="modal fade modal-mob-numb all-tasks__city-modal">
    <div class="modal-dialog">
        <div class="modal-body">
            <button type="button" data-dismiss="modal" class="but-modal-close"></button>
            <div class="modal-welcome__txt modal-welcome__txt-left">
                <h2>Выберите город, чтобы увидеть созданные в нем задания</h2>
            </div>
            <div class="modal-welcome__bottom">
                <form class="filter-form">
                    <ul class="h-list radio-list">
                        @foreach(\App\TasksFilterCities::all() as $city)
                            <li class="col-md-5 text-left">
                                <label class="radio-label">
                                    <input data-city-name="{{$city->name}}" type="radio" name="city" class="city-filter" value="{{$city->city_id}}" {{$city->city_id==$sort_data['city']?'checked':''}}/>
                                    <i class="radio"></i>
                                    <span>{{$city->name}}</span>
                                </label>
                            </li>
                        @endforeach
                    </ul>
                </form>
            </div>
            <div class="modal-welcome__bottom-txt">
                <p>Изменение города влияет только на отображение заданий на странице.</p>
                <p>Чтобы настроить подписку на задания в интересующем вас городе, укажите нужный город в <a href="#">«Общих
                        настройках»</a>
                    на странице вашего профиля.
                </p>
            </div>
        </div>
    </div>
</div>