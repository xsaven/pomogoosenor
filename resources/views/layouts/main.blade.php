@extends('layouts.basic')

@section('body')
    <div id="fb-root"></div>
    <div class="l-page-wrapper">
        @widget('MainMenu')
        @yield('content')
    </div>
    @widget('Footer')
@endsection