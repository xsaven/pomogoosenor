<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    @include('_partials._head')
    @include('_partials._header')
    <title>{{ config('app.name', 'Pomogoo') }} | @yield('title')</title>
    @widget('Seo')
</head>
<body>
@yield('body')

<div id="modalsWrapper">
    @if(auth()->check())
        <div id="vue-chat-wrapper">
            <chat-layout></chat-layout>
        </div>
        @include('modals.after_register')
        @include('modals.confirm')
    @else
        @include('modals.register')
        @include('modals.login')
    @endif
    @yield('modals')
</div>
<div id="pre_rendered_notifies" style="display: none;">
    @if(Session::has('warning'))
        @foreach(Session::get('warning') as $warning)
            <div class="notify_warning">{{$warning}}</div>
        @endforeach
    @endif
    @if(Session::has('success'))
        @foreach(Session::get('success') as $warning)
            <div class="notify_success">{{$warning}}</div>
        @endforeach
    @endif
    @if(Session::has('danger'))
        @foreach(Session::get('danger') as $warning)
            <div class="notify_danger">{{$warning}}</div>
        @endforeach
    @endif
    @if(Session::has('validation_errors'))
        @foreach( Session::get('validation_errors') as $input_name => $input_errors)
            @foreach($input_errors as $error)
                <div data-name="{{ $input_name }}" class="validation_error">{{$error}}</div>
            @endforeach
        @endforeach
    @endif

    {{--VALIDATE RULE--}}
    {{--@if ($errors->any())--}}
        {{--@foreach ($errors->all() as $error)--}}
            {{--<div class="notify_warning">{{$error}}</div>--}}
        {{--@endforeach--}}
    {{--@endif--}}
        {{-- TODO Check http://pomogoo/testing/steps_all Call to a member function any() on boolean --}}

</div>
<div class='onesignal-customlink-container'></div>
@include('_partials._footer')

@yield('script')
<script src="{{asset('/js/jquery-2.2.4.min.js')}}" ></script>
<script src="{{asset('/js/app.js')}}"></script>

@stack('scripts')
</body>
</html>


