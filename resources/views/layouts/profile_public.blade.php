@extends('layouts.basic')

@section('body')
    <div id="fb-root"></div>
    <div class="l-page-wrapper">
        @widget('MainMenu')
        <div class="l-profile">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        @yield('content')
                    </div>
                    <div class="col-md-3">
                        @include('profile.public._partials._right')
                    </div>
                </div>
            </div>
        </div>
    </div>
    @widget('Footer')
@stop