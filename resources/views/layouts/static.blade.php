@extends('layouts.basic')

@section('title'){{ $static_page->title }} @endsection
@section('body')
    <div id="fb-root"></div>

    <div class="l-page-wrapper">
        @widget('MainMenu')
        <div class="l-tech-wrapper">
                                {!! $static_page->content !!}
        </div>
        @widget('Footer')
    </div>

@endsection