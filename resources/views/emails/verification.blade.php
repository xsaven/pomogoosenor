@extends('emails.custom.layout')

@section('content')
    @if(isset($title))
        <div class="oformlenie_email__main-header">
            <h2>{{$title}}</h2>
        </div>
    @endif

    <a href="{{$url}}">Перейдите по ссылке для подтверждения email</a>

    <div class="oformlenie_email__main-content">
        <div class="oformlenie_email__main-content__img"><img
                    src="{{asset('s/images/tmp_file/oformlenie_email/mail.png')}}" alt=""></div>
    </div>
@endsection