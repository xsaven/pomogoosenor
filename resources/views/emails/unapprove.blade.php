@extends('emails.custom.layout')

@section('content')
    {{$title}}
    "<a href="{{url('task/preview', ['id' => $task->id])}}">{{$task->name}}</a>"
@endsection