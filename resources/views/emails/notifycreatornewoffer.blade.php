@extends('emails.custom.layout')

@section('content')
    {{$title}}
    <a href="{{route('task.review', ['userTask' => $task])}}">Перейти к заданию</a>
@endsection