<div class="oformlenie_email__social">
    <div class="oformlenie_email__social-links">
        <div class="oformlenie_email__social-link"><a href="{{url('/')}}" class="link_bondi">О нас</a></div>
        <div class="oformlenie_email__social-link"><a href="{{url('/how-it-work')}}" class="link_bondi">Как это работает</a></div>
        <div class="oformlenie_email__social-link"><a href="{{url('/faq')}}" class="link_bondi">Частые вопросы</a></div>
    </div>
    <div class="oformlenie_email__social-links">
        <div class="oformlenie_email__social-link"><span>Служба поддержки в Москве <a
                        href="tel:+7 (925) 55 33 2111">+7 (925) 55 33 2111</a></span></div>
        <div class="oformlenie_email__social-link"><span>Служба поддержки в Челябинске<a href="tel:+7 (925) 55 33 2111">

                      +7 (925) 55 33 2111</a></span></div>
    </div>
    <div class="oformlenie_email__social-links">
        <div class="oformlenie_email__social-link"><span>E-mai:<a href="mailto:{{env('MAIL_USERNAME')}}">

                      {{env('MAIL_USERNAME')}}</a></span></div>
    </div>
    <div class="oformlenie_email__social-links oformlenie_email__social-links__circles"><span><a href="#"
                                                                                                 class="social__vk">
                    <svg width="1em" height="1em" class="icon icon-conf-vk ">
                      <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-conf-vk')}}"></use>
                    </svg></a></span><span><a href="#" class="social__fb">
                    <svg width="1em" height="1em" class="icon icon-conf-fb ">
                      <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-conf-fb')}}"></use>
                    </svg></a></span><span><a href="#" class="social__tw">
                    <svg width="1em" height="1em" class="icon icon-conf-tw ">
                      <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-conf-tw')}}"></use>
                    </svg></a></span><span><a href="#" class="social__ok">
                    <svg width="1em" height="1em" class="icon icon-conf-ok ">
                      <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-conf-ok')}}"></use>
                    </svg></a></span><span><a href="#" class="social__goog">
                    <svg width="1em" height="1em" class="icon icon-conf-google ">
                      <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-conf-google')}}"></use>
                    </svg></a></span></div>
</div>