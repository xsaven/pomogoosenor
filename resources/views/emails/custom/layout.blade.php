<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    {{--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;amp;subset=cyrillic"--}}
          {{--rel="stylesheet"/>--}}
    <link href="{{asset('s/css/all.css')}}" rel="stylesheet"/>
</head>
<body>
<div id="fb-root"></div>
<div class="l-page-wrapper">
    <div class="oformlenie_email">
        <div class="container">
            <div class="oformlenie_email__header"><a href="{{url('/')}}" class="c-header__logo"><span>Pomogoo</span></a></div>
            <div class="oformlenie_email__main">

                @yield('content')
                @include('emails.custom.component.social')
            </div>
            @include('emails.custom.component.footer')
        </div>
    </div>
</div>
{{--<script data-main="s/js/main" src="s/js/require.js"></script>--}}
</body>
</html>