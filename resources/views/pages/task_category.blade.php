@extends('layouts.basic')

@section('title')Категории заданий @endsection
@section('body')
    <div id="fb-root"></div>
    <div class="l-page-wrapper">
        @widget('MainMenu')
        <div class="l-tech-wrapper">
            <div class="container">
                <div class="categoria_zadanija">
                    <div class="categoria_zadanija_header">
                        <h2>Выберите категорию задания</h2>
                    </div>
                    <div class="categoria_zadanija_content_wrap">
                        @foreach($cat_tree as $category)
                            <div class="categoria_zadanija_content">
                                <div class="categoria_zadanija_content_ico"></div>
                                <div class="categoria_zadanija_content_list">
                                    <ul class="h-list">
                                        <li>
                                            <a href="{{route('task.new', [
                                            'category' => $category->slug,
                                            'subcategory' => $defaultSlug,
                                            'select' => true
                                            ])}}">
                                                {{$category->name}}
                                            </a>
                                            <span>
                                                <svg width="1em" height="1em" class="icon {{$category->icon}} ">
                                                    <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#{{$category->icon}}"></use>
                                                </svg>
                                            </span>
                                        </li>
                                        @if($category->subcategories)
                                            @foreach($category->subcategories as $subcategory)
                                                <li>
                                                    <a href="{{route('task.new', [
                                                    'category' => $category->slug,
                                                    'subcategory' => $subcategory->slug,
                                                    'select' => true
                                                    ])}}">
                                                        {{$subcategory->name}}
                                                    </a>
                                                </li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @widget('Footer')
    </div>

@endsection