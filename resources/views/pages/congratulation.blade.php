@extends('layouts.main')
@section('title') Подтверждение email @stop
@section('content')
    <div class="container">
        <div class="confirm-container text-center " style="margin-top: 50px">
            <h3>Вы успешно подтвердили email</h3>
        </div>
    </div>
@stop