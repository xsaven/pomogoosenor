@extends('layouts.basic')

@section('title'){{$page->title}} @endsection
@section('body')

    <div id="fb-root"></div>
    <div class="l-page-wrapper">
        @widget('MainMenu')
        <div class="l-tech-wrapper">
            <div class="container">
                <div class="row row_no-pad">
                    @widget('Pages')
                    <div class="col-md-9">
                        <div class="l-cust-rev l-perf-rev">
                            <h1>Отзывы исполнителей о Pomogoo</h1>
                            <div class="l-cust-rev__annotation">
                                {!! $page->content !!}
                            </div>
                            <div class="l-cust-rev__content">
                                @foreach($reviews as $review)
                                    @if(!$review->user) @continue @endif
                                    <div class="l-cust-rev__item clearfix">
                                        <div class="l-cust-rev__item-img h-object-fit"><img
                                                    src="{{$review->user->profile->avatar_thumb}}" alt=""/></div>
                                        <div class="l-cust-rev__item-txt">
                                            <div class="l-cust-rev__item-txt-wrp clearfix">
                                                <div class="l-cust-rev__item-user"><span
                                                            class="l-cust-rev__item-user-name"><a
                                                                href="{{url('profile', ['id' => $review->user->id])}}">{{$review->user->name}}</a></span>
                                                </div>
                                                <div class="l-cust-rev__item-done"><span
                                                            class="l-cust-rev__item-done-txt">Количество выполненых заданий: {{$review->user->getTaskCount()}}
                                                        . Средняя оценка: {{$review->user->avg_mark['total']}}</span><span
                                                            class="l-cust-rev__item-done-stars">
                                                        @for($i = 0; $i < $review->user->avg_mark['total']; $i++)
                                                            <svg width="1em" height="1em" class="icon icon-star ">
                              <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-star"></use>
                            </svg>
                                                        @endfor
                                                    </span></div>
                                                <div class="l-cust-rev__item-rev">
                                                    {!! $review->text !!}
                                                </div>
                                                <div class="l-cust-rev__item-caption"><a href="{{$review->source}}">Источник</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="l-cust-rev__btns">
                                <a href="{{url('/task-categories')}}" class="but but_bondi but_bondi_lg">
                                    <span>Создать задание</span>
                                </a>
                                <a href="{{url('/how-performer')}}" class="c-check-link">
                                    <span>Может быть вы хотите стать исполнителем Pomogoo?</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @widget('Footer')
    </div>

@endsection