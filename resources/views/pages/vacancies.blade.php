@extends('layouts.basic')

@section('title')Вакансии@endsection
@section('body')

    <div id="fb-root"></div>
    <div class="l-page-wrapper">
        @widget('MainMenu')
        <div class="l-tech-wrapper">
            <div class="container">
                <div class="row row_no-pad">
                    @widget('Pages')
                    <div class="col-md-9">
                        <div class="l-vacancies">
                            <h1>Вакансии</h1>
                            <div class="l-vacancies__section">
                                <div class="l-vacancies__annotation">Мы не заманиваем людей офисными пуфиками и
                                    плюшками.
                                    <br>Мы ищем сотрудников, которые, прежде всего, хотят работать над интереснейшим
                                    проектом и развивать себя в опытной команде, получать удовольствие и гордиться тем,
                                    что
                                    они делают!
                                </div>
                            </div>
                            <div class="l-vacancies__section">
                                <h2>Наш офис</h2>
                                <div class="l-vacancies__img h-object-fit"><img src="s/images/tmp_file/office.jpg"
                                                                                alt=""/>
                                </div>
                            </div>
                            <div class="l-vacancies__section">
                                <h2>Что мы предлагаем?</h2>
                                <ul>
                                    <li>Удобный рабочий график, как правило, с 11 до 20 часов.</li>
                                    <li>Любая необходимая техника и софт на вашем рабочем месте.</li>
                                    <li>Работа в атмосфере интересного проекта без лишних бюрократических и
                                        корпоративных
                                        заморочек.
                                    </li>
                                    <li>Оплачиваемое участие в профессиональных конференциях, курсах повышения
                                        квалификации,
                                        оплачиваемая учебная литература и т.д.
                                    </li>
                                    <li>Оформление в соответствии с ТК РФ.</li>
                                </ul>
                            </div>
                            <div class="l-vacancies__items">
                                @foreach ($vacancies as $vacancy)
                                    <div class="l-vacancies__item"><a
                                                href="{{url('vacancy', ['id' => $vacancy->id])}}">{{$vacancy->name}}</a><span>{{$vacancy->salary}}</span>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @widget('Footer')
    </div>


@endsection