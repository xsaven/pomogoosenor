@extends('layouts.basic')

@section('title'){{$page->title}} @endsection
@section('body')

    <div id="fb-root"></div>
    <div class="l-page-wrapper">
        @widget('MainMenu')
        {!! $page->content !!}
        <div class="l-no-risk-faq">
            <div class="container">
                <h6>Частые вопросы</h6>
                @foreach($questions as $key => $question)
                    <div class="l-faq__dropdowns-item">
                        <button data-toggle="collapse" data-target="#faq_{{$key}}"
                                class="l-faq__dropdowns-item-btn clearfix">
                            <svg width="1em" height="1em" class="icon icon-quote ">
                                <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-quote"></use>
                            </svg>
                            <span>{{$question->question}}</span>
                        </button>
                        <div id="faq_{{$key}}" class="l-faq__dropdowns-item-content-wrp collapse">
                            <div class="l-faq__dropdowns-item-content">
                                {!! $question->answer !!}<a href="{{url('faq')}}">Подробнее</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        @widget('Footer')
    </div>

@endsection