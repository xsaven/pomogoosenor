@extends('layouts.basic')

@section('title')Отзывы исполнителей @endsection
@section('body')

    <div id="fb-root"></div>
    <div class="l-page-wrapper">
        @widget('MainMenu')
        <div class="l-tech-wrapper">
            <div class="container">
                <div class="row row_no-pad">
                    @widget('Pages')
                    <div class="col-md-9">
                        <div class="l-tech-content">
                            <div class="l-faq">
                                <h1>Частые вопросы</h1>
                                <div id="faq" class="l-faq__dropdowns">
                                    @foreach($collection as $key => $item)
                                        <div @if($key > 9) hidden @endif onload="getActive({{$item->slug}})"
                                             id="{{$item->slug}}"
                                             class="l-faq__dropdowns-item">
                                            <button data-toggle="collapse" data-target="#faq_{{$key}} "
                                                    class="l-faq__dropdowns-item-btn">
                                                <svg width="1em" height="1em" class="icon icon-quote">
                                                    <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-quote"></use>
                                                </svg>
                                                <span>{{$item->question}}</span>
                                            </button>
                                            <div id="faq_{{$key}}" class="l-faq__dropdowns-item-content-wrp collapse">
                                                <div class="l-faq__dropdowns-item-content">
                                                    {!! $item->answer !!}
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                                @if(count($collection) >9)
                                    <div id="show-more" class="l-faq__btn"><span style="cursor:pointer;"
                                                                                 class="but but_bondi"><span>Показать еще</span></span>
                                    </div>
                                @endif

                                <div id="hide-more" hidden class="l-faq__btn"><span style="cursor:pointer;"
                                                                                    class="but but_bondi"><span>Скрыть</span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @widget('Footer')
    </div>

    @push('scripts')
        <script>

            jQuery('#show-more').on('click', function () {
                jQuery('#faq .l-faq__dropdowns-item').show();
                jQuery('#show-more').hide();
                jQuery('#hide-more').show();
            });

            jQuery('#hide-more').on('click', function () {
                jQuery('#faq .l-faq__dropdowns-item').slice(10, jQuery('#faq .l-faq__dropdowns-item').length).hide();
                jQuery('#hide-more').hide();
                jQuery('#show-more').show();
            });

            jQuery('#faq .l-faq__dropdowns-item').each(function (i, element) {
                if (window.location.hash === '#' + element.id) {
                    jQuery(this).find('.l-faq__dropdowns-item-btn').addClass('active');
                    jQuery(this).find('.l-faq__dropdowns-item-content-wrp').addClass('in');
                }
            })
        </script>
    @endpush

@endsection