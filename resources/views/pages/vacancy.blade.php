@extends('layouts.basic')

@section('title'){{$vacancy->name}} @endsection
@section('body')

    <div id="fb-root"></div>
    <div class="l-page-wrapper">
        @widget('MainMenu')
        <div class="l-tech-wrapper">
            <div class="container">
                <div class="row row_no-pad">
                    @widget('Pages')
                    <div class="col-md-9">
                        <div class="l-vacancy-full">
                            <div class="l-vacancy-full__section">
                                <h1>{{$vacancy->name}}</h1>
                                <div class="c-blue-table">
                                    <table>
                                        <tbody>
                                        <tr>
                                            <th>Уровень зарплаты</th>
                                            <th>Город</th>
                                            <th>Требуемый опыт работы</th>
                                        </tr>
                                        <tr>
                                            <td>{{$vacancy->salary}}</td>
                                            <td>{{$vacancy->city}}</td>
                                            <td>{{$vacancy->experience}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="pomogoo-logo">Pomogoo</div>
                            </div>
                            <div class="l-vacancy-full__description">
                                {!! $vacancy->content !!}
                            </div>
                            <div class="l-vacancy-full__btn"><a href="#" data-toggle="modal"
                                                                data-target="#modal-vacancy"
                                                                class="but but_bondi but_bondi_lg"><span>Откликнуться </span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @widget('Footer')
    </div>


    <div id="modal-vacancy" class="modal fade l-modal-vacancy" style="padding-left: 15px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="l-modal-vacancy__head">Укажите ваши данные для отклика на вакансию <b>{{$vacancy->name}}
                        {{$vacancy->salary}}</b></div>
                <div class="l-modal-vacancy__body">
                    <form id="vacancy-form" enctype="multipart/form-data"
                          data-toggle="validator"
                          class="form-check" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="vacancy_id" value="{{$vacancy->id}}">
                        <label class="input-wrap">
                            <input data-rule-required="true"
                                   data-msg-required="Имя - обязательное поле"
                                   type="text"
                                   placeholder="Имя"
                                   name="name"
                                   class="form-control">
                            <small class="errorMsg"></small>
                        </label>
                        <label class="input-wrap">
                            <input data-rule-required="true"
                                   data-msg-required="Фамилия - обязательное поле"
                                   type="text"
                                   placeholder="Фамилия"
                                   name="second_name"
                                   class="form-control">
                            <small class="errorMsg"></small>
                        </label>
                        <label class="input-wrap">
                            <input data-rule-required="true"
                                   data-msg-required="Email - обязательное поле"
                                   data-rule-email="true"
                                   data-msg-email="Неправильный email"
                                   type="email"
                                   placeholder="Email"
                                   name="email"
                                   class="form-control">
                            <small class="errorMsg"></small>
                        </label>
                        <div id="file_name"></div>
                        <label class="add-file">

                            <span class="add-file__button">
                                <svg width="1em" height="1em" class="icon icon-clip ">
                                     <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg')}}#icon-clip"></use>
                                </svg>
                                <span>Добавить резюме</span>
                            </span>
                            <input
                                    name="cv"
                                    data-rule-required="true"
                                    data-msg-required="Резюме - обязательное поле"
                                    type="file">
                            <small class="errorMsg"></small>
                        </label>
                        <label class="check-wrap">

                            <input
                                    name="t_and_c"
                                    data-rule-required="true"
                                    data-msg-required="Нужно дать согласие на обработку личных данных"
                                    type="checkbox"><span class="checkbox"></span><span
                                    class="checkbox-txt">Я даю согласие на обработку<br>личных данных</span>
                            <small class="errorMsg"></small>
                        </label>
                        <button type="submit" class="but but_bondi"><span>Откликнуться</span></button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
    <script>


        $('#vacancy-form').submit(function (event) {
            if (!event.target.validate()) {
                event.preventDefault();
                return false;
            }
        });

    </script>
@endpush

@push('scripts')
    <script>

        function deleteCV() {
            $('input[name="cv"]')[0].value = null;
            $('#file_name').html('');
        }


        $('input[name="cv"]').on('input', function (event) {
            $('#file_name').html(event.target.files[0].name + ' <span style="color: red; cursor: pointer" onclick="deleteCV()">удалить</span>')
        });
    </script>
@endpush()