@extends('layouts.basic')

@section('title'){{$page->title}} @endsection
@section('body')

    <div id="fb-root"></div>
    <div class="l-page-wrapper">
        @widget('MainMenu')
        <div class="l-tech-wrapper">
            <div class="container">
                <div class="row row_no-pad">
                    @widget('Pages')
                    <div class="col-md-9">
                        <div class="l-security" id="safe-and-guarantees">
                            {!! $page->content !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @widget('Footer')
    </div>

@endsection