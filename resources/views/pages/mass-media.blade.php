@extends('layouts.basic')

@section('title')СМИ о нас @endsection
@section('body')

    <div id="fb-root"></div>
    <div class="l-page-wrapper">
        @widget('MainMenu')
        <div class="l-tech-wrapper">
            <div class="container">
                <div class="row row_no-pad">
                    @widget('Pages')
                    <div class="col-md-9">
                        <div class="l-mass-media" id="safe-and-guarantees">
                            @foreach($collection as $post)
                                <div class="l-mass-media__item">
                                    <div class="l-mass-media__item-head">
                                        <div class="date">{{$post->date}}</div>
                                        <div class="title"><span>{{$post->source}}</span> / {{$post->title}}</div>
                                    </div>
                                    <p>{{$post->content}}</p>
                                    <div class="h-object-fit"><img src="{{url('uploads/'.$post->img)}}" alt=""/></div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @widget('Footer')
    </div>

@endsection