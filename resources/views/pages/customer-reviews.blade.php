@extends('layouts.basic')

@section('title'){{$page->title}} @endsection
@section('body')

    <div id="fb-root"></div>

    <div class="l-page-wrapper">
        @widget('MainMenu')
        <div class="l-tech-wrapper">
            <div class="container">
                <div class="row row_no-pad">
                    @widget('Pages')
                    <div class="col-md-9">
                        <div class="l-cust-rev">
                            {!! $page->content !!}
                            <div class="l-cust-rev__content">
                                @foreach($reviews as $review)
                                    <div class="l-cust-rev__item clearfix">
                                        <div class="l-cust-rev__item-img h-object-fit"><img
                                                    src="{{url('uploads/'. $review->img) }}" alt=""/></div>
                                        <div class="l-cust-rev__item-txt">
                                            <div class="l-cust-rev__item-txt-wrp clearfix">
                                                <div class="l-cust-rev__item-user"><span
                                                            class="l-cust-rev__item-user-name">{{$review->name}}</span><span
                                                            class="l-cust-rev__item-user-about">{{$review->label}}</span>
                                                </div>
                                                <div class="l-cust-rev__item-rev">
                                                    {!! $review->text !!}
                                                </div>
                                                <div class="l-cust-rev__item-caption"><a href="{{$review->source}}">Источник</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="l-cust-rev__btns">
                                <a href="{{url('/task-categories')}}" class="but but_bondi but_bondi_lg">
                                    <span>Создать задание</span>
                                </a>
                                <a href="{{url('/how-performer')}}" class="c-check-link">
                                    <span>Может быть вы хотите стать исполнителем Pomogoo?</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @widget('Footer')
    </div>

@endsection