@extends('layouts.basic')

@section('title') Завершите регистрацию @stop
<!-- Main Content -->
@section('body')
    <div class="l-page-wrapper">
        @widget('MainMenu',['display' => 'none'])
        <main>
            <div class="container">
                <div class="registration-header">
                    <h2>Завершение регистрации</h2><span>Пожалуйста, проверьте введенные ниже данные</span>
                </div>
                <div class="registration-content__wrap">
                    <div class="registration-content">
                        <div class="registration-content__left">
                            <div class="l-prof-def__ava-photo-add l-prof-def__ava-photo-add__noheight">
                                <label>
                                    <input type="file" id="add_photo" name="avatar">
                                    <div class="icon_wrapp-add">
                                        <svg width="1em" height="1em" class="icon icon-addfotoico ">
                                            <div class="registration-content__img h-object-fit"><img
                                                        class="avatar_preview"
                                                        src="{{$user->profiles->avatar_full or 's/images/tmp_file/lichnaja_infa/1.png'}}"
                                                        alt=""></div>
                                        </svg>
                                        <span class="link_bondi">Изменить фото</span>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="registration-content__right">

                            @if($errors->any())
                                <div id="pre_rendered_notifies">
                                    @foreach ($errors->all() as $error)
                                        <div data-name="{{ $error }}"
                                             class="validation_error">{{$error}}</div>
                                    @endforeach
                                </div>
                            @endif
                            <form id="finishRegForm" enctype="multipart/form-data" method="POST"
                                  action="{{route('finish_social')}}">
                                {!! csrf_field() !!}
                                <div class="steps-content__form-input">
                                    <label>
                                        <input name="name" value="{{$user->name or ''}}" type="text"
                                               placeholder="Теодора Джейсон"
                                               class="form-control">
                                    </label>
                                </div>
                                <div class="steps-content__form-input">
                                    <label>
                                        <input name="email" value="{{$user->email or ''}}" type="email"
                                               placeholder="Email"
                                               class="form-control">
                                    </label>
                                </div>
                                <div class="steps-content__form-input l-prof-set__form-line clearfix tel">
                                    <div class="form-line__input tel-wrap">
                                        <select name="phone_code" class="selectpicker">
                                            <option>+7</option>
                                            {{--<option>+38</option>--}}
                                        </select>
                                        <label class="tel-wrap mask">
                                            <input name="phone" type="text" placeholder=""
                                                   class="tel-input form-control">
                                        </label>
                                    </div>
                                </div>
                                <div class="steps-content__form-input">
                                    <label class="selectize_label">
                                        <input name="city" type="text" placeholder="Город"
                                               class="form-control form-data">
                                    </label>
                                    <span class="example">Например: Москва, Санкт-Петербург</span>
                                </div>

                                <div class="registration-content__button">
                                    <button type="submit"
                                            class="but but_bondi"><span>Зарегестрироваться</span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection

@section('script')
    <script>
        window.onload = function () {
            var xhr;
            new autoComplete({
                selector: 'input[name="city"]',
                source: function (term, response) {
                    try {
                        xhr.abort();
                    } catch (e) {
                    }
                    xhr = $.getJSON('{{route('find_city')}}', {query: term}, function (data) {
                        response(data);
                    });
                },
                renderItem: function (item, search) {
                    search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                    var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
                    return '<div class="autocomplete-suggestion" value="' + item.id + '" data-val="' + item.name + '">' + item.name + '</div>';
                }
            });
        };

    </script>
@endsection