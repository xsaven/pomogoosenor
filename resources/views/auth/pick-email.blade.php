@include('admin.partials.header')
<div style="margin-top: 10%;"></div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">В Вашем аккаунте социальной сети не указан email, пожалуйста, введите его вручную </div>
                <div class="panel-body">
                    @if(!isset($success))
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>{{ trans('quickadmin::auth.whoops') }}</strong> {{ trans('quickadmin::auth.some_problems_with_input') }}
                                <br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal"
                              role="form"
                              method="POST"
                              action="{{ url('handle-email') }}">
                            <input type="hidden"
                                   name="_token"
                                   value="{{ csrf_token() }}">

                            <input type="hidden" name="social_id" value="{{$_GET['id']}}">
                            <input type="hidden" name="network" value="{{$_GET['network']}}">

                            <div class="form-group">
                                <label class="col-md-4 control-label">Email</label>

                                <div class="col-md-6">
                                    <input type="email"
                                           class="form-control"
                                           name="email"
                                           value="{{ old('email') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit"
                                            class="btn btn-primary"
                                            style="margin-right: 15px;">
                                        Отправить
                                    </button>
                                </div>
                            </div>
                        </form>
                    @else
                        <h3>На Вашу посту отправлено письмо для подтверждения владения даным Email-ом</h3>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin.partials.footer')
