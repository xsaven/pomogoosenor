@extends('layouts.basic')

@section('title') Завершите регистрацию @stop
<!-- Main Content -->
@section('body')
    <div class="l-page-wrapper">
        @widget('MainMenu',['display' => 'none'])
        <main>
            <div class="container">
                <div class="registration-header">
                    <h2>Завершение регистрации</h2><span>Пожалуйста, проверьте введенные ниже данные</span>
                </div>
                <div class="registration-content__wrap">
                    <div class="registration-content">
                        <div class="registration-content__left registration-content__left-socials">
                            <div class="registration-content__socials-text"><span>Используйте социальные сети для быстрой регистрации</span>
                            </div>
                            @include('modals.parts.social-button')
                        </div>
                        <div class="registration-content__right">
                            @if($errors->any())
                                <div id="pre_rendered_notifies">
                                    @foreach ($errors->all() as $error)
                                        <div data-name="{{ $error }}"
                                             class="validation_error">{{$error}}</div>
                                    @endforeach
                                </div>
                            @endif
                            <form id="finishRegForm" enctype="multipart/form-data" method="POST"
                                  action="{{route('register')}}">
                                {!! csrf_field() !!}
                                <div class="steps-content__form-input">
                                    <label>
                                        <input
                                                data-rule-required="true" data-msg-required="Имя - обязательное поле"
                                                name="name" type="text" placeholder="Теодора Джейсон"
                                                value="{{old('name')}}"
                                                class="form-control">
                                        <div class="errorMsg"></div>
                                    </label>
                                </div>
                                <div class="steps-content__form-input">
                                    <label>
                                        <input data-rule-required="true" data-msg-required="Email - обязательное поле"
                                               data-rule-email="true" data-msg-email="Некорректный email"
                                               name="email" value="{{$email or ''}}" type="email" placeholder="Email"
                                               class="form-control">
                                        <div class="errorMsg"></div>
                                    </label>
                                </div>
                                <div class="steps-content__form-input l-prof-set__form-line clearfix tel">
                                    <div class="form-line__input tel-wrap">
                                        <select name="phone_code" class="selectpicker">
                                            <option value="+7">+7</option>
                                            {{--<option value="+38">+38</option>--}}
                                        </select>
                                        <label class="tel-wrap mask">
                                            <input
                                                    data-rule-minlength="15"
                                                    data-rule-required="true"
                                                    data-msg-required="Телефон - обязательное поле"
                                                    data-msg-minlength="Телефон - обязательное поле"
                                                    name="phone" type="text" placeholder=""
                                                    value="{{old('phone')}}"
                                                    class="tel-input form-control">
                                            <div class="errorMsg"></div>
                                        </label>
                                        @push('scripts')
                                            <script>
                                                $('[name="phone"]').on("keyup", function () { $(this).val($(this).val().replace("(8", "(")); })
                                            </script>
                                        @endpush
                                    </div>
                                </div>
                                <div class="steps-content__form-input" id="select-city">
                                    <label class="selectize_label">
                                        <input
                                                data-rule-required="true"
                                                data-msg-required="Город - обязательное поле"
                                                name="city_select" type="text" placeholder="Город"
                                                value="{{old('city')}}"
                                                class="form-control form-data">
                                        <div class="errorMsg"></div>
                                    </label>
                                    <input type="hidden" name="city">


                                    <span class="example">Например: Москва, Санкт-Петербург</span>
                                </div>

                                <div class="l-prof-def__ava-photo-add l-prof-def__ava-photo-add__noheight">

                                    <label id="avatar-picker">
                                        <input type="file" id="add_photo" name="avatar">
                                        <div class="icon_wrapp-add">
                                            <svg width="1em" height="1em" class="icon icon-addfotoico ">
                                                <div class="registration-content__img h-object-fit"><img
                                                            class="avatar_preview"
                                                            src="{{asset('s/images/tmp_file/lichnaja_infa/1.png')}}"
                                                            alt=""></div>
                                            </svg>
                                            <span class="link_bondi">Изменить фото</span>
                                            <span style="display: none;" id="delete_image" class="link_bondi">Удалить фото</span>

                                        </div>


                                        <div>
                                            <small style="padding-left: 10px; font-size: 12px">Максимальный размер
                                                2000х2000
                                            </small>
                                        </div>
                                        <div class="errorMsg"></div>

                                    </label>

                                </div>

                                <div class="registration-content__button">
                                    <button type="submit"
                                            class="but but_bondi disabled"><span>Зарегестрироваться</span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection

@section('script')
    <script>
        window.onload = function () {


            var xhr;
            new autoComplete({
                selector: 'input[name="city_select"]',
                minChars: 0,
                source: function (term, response) {
                    try {
                        xhr.abort();
                    } catch (e) {
                    }
                    xhr = $.getJSON('{{route('find_city')}}', {query: term}, function (data) {
                        response(data);

                    });
                },
                renderItem: function (item, search) {
                    search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                    var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
                    return '<div class="autocomplete-suggestion" value="' + item.id + '" data-val="' + item.name + '">' + item.name + '</div>';
                },
                onSelect: function (event, term, item) {
                    $('input[name="city"]')[0].value = item.getAttribute('data-val');
                }
            });

            $('input[name="city_select"]').on('input', function () {
                $('input[name="city"]')[0].value = null;
            });

            $('#add_photo').on('input', function () {
                $('#delete_image').show();
            });

            $('#avatar-picker').on('click', function () {
                if (event.target.id === 'delete_image') {
                    $('#delete_image').hide();
                    $('#add_photo').val(null);
                    $('.avatar_preview')[0].src = '{{asset('s/images/tmp_file/lichnaja_infa/1.png')}}'
                    event.preventDefault();
                    return false;
                }

            });
        };

    </script>
@endsection