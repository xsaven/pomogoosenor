@extends('layouts.basic')

@section('title') Завершите регистрацию @stop
<!-- Main Content -->
@section('body')
    <div class="l-page-wrapper">
        @widget('MainMenu',['display' => 'none'])
        <main>
            <div class="container">
                <div class="registration-header">
                    <h2>Восстановления пароля</h2><span>Пожалуйста, проверьте введенные ниже данные</span>
                </div>
                <div class="registration-content__wrap">
                    <div class="registration-content">
                        <div class="registration-content__left registration-content__left-socials">
                            <div class="registration-content__socials-text"><span>Используйте социальные сети для быстрого входа</span>
                            </div>
                            @include('modals.parts.social-button')
                        </div>
                        <div class="registration-content__right">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            {{--@if($errors->any())--}}
                            {{--<div id="pre_rendered_notifies">--}}
                            {{--@foreach ($errors->all() as $error)--}}
                            {{--<div data-name="{{ $error }}"--}}
                            {{--class="validation_error">{{$error}}</div>--}}
                            {{--@endforeach--}}
                            {{--</div>--}}
                            {{--@endif--}}
                            <form id="reset-password" class="form-horizontal" method="POST"
                                  action="{{ route('password.request') }}">
                                {{ csrf_field() }}

                                <input type="hidden" data- name="token" value="{{ $token }}">

                                <input id="email" type="hidden" class="form-control" placeholder="Email"
                                       name="email"
                                       value="{{ old('email', request()->email) }}" required>

                                <div class="steps-content__form-input">
                                    <label>
                                        <input
                                                data-rule-required="true" data-msg-required="Пароль - обязательное поле"
                                                id="password" type="password" class="form-control" placeholder="Пароль"
                                                name="password"
                                                value="{{ old('password') }}">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                {{ $errors->first('password') }}
                                            </span>
                                        @endif
                                    </label>
                                </div>

                                <div class="steps-content__form-input">
                                    <label>
                                        <input
                                                data-rule-required="true"
                                                data-msg-required="Подтверждение пароля - обязательное поле"
                                                id="password_confirmation" type="password" class="form-control"
                                                placeholder="Подтвердите пароль"
                                                name="password_confirmation"
                                                value="{{ old('password_confirmation') }}">
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                                {{ $errors->first('password_confirmation') }}
                                            </span>
                                        @endif
                                    </label>
                                </div>

                                <div class="registration-content__button">
                                    <button type="submit"
                                            class="but but_bondi">
                                        <span>Сохранить</span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection

@push('scripts')
    <script>
        document.addEventListener("DOMContentLoaded", function () {
            setTimeout(function () {
                requirejs(['validator'], function () {
                    $('#reset-password').on('submit', function (event) {
                        $(this).validate({
                            rules: {
                                password_confirmation: {
                                    equalTo: "#password"
                                }
                            },
                            messages: {
                                password_confirmation: {
                                    equalTo: 'Пароли не совпадают'
                                }
                            }
                        });

                        if (!$(this).valid()) {
                            event.preventDefault();
                            return false;
                        }
                    });
                })
            }, 0)
        })
    </script>
@endpush