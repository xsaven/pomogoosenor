@extends('layouts.basic')

@section('title') Завершите регистрацию @stop
<!-- Main Content -->
@section('body')
    <div class="l-page-wrapper">
        @widget('MainMenu',['display' => 'none'])
        <main>
            <div class="container">
                <div class="registration-header">
                    <h2>Восстановления пароля</h2><span>Пожалуйста, проверьте введенные ниже данные</span>
                </div>
                <div class="registration-content__wrap">
                    <div class="registration-content">
                        <div class="registration-content__left registration-content__left-socials">
                            <div class="registration-content__socials-text"><span>Используйте социальные сети для быстрого входа</span>
                            </div>
                            @include('modals.parts.social-button')
                        </div>
                        <div class="registration-content__right">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            @if($errors->any())
                                <div id="pre_rendered_notifies">
                                    @foreach ($errors->all() as $error)
                                        <div data-name="{{ $error }}"
                                             class="validation_error">{{$error}}</div>
                                    @endforeach
                                </div>
                            @endif
                            <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                                {{ csrf_field() }}
                                <div class="steps-content__form-input">
                                    <label>
                                        <input id="email" type="email" class="form-control" placeholder="Email"
                                               name="email"
                                               value="{{ old('email') }}" required>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </label>
                                </div>

                                <div class="registration-content__button">
                                    <button type="submit"
                                            class="but but_bondi">
                                        <span>Отправить на email</span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection
