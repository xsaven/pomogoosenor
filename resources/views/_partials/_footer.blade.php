{{--Scripts--}}

<script data-main="{{url('/s/js/main')}}" src="{{url('/s/js/require.js')}}"></script>
<script src="{{asset('js/auto-complete.min.js')}}"></script>

@if(session()->has('register_status'))
    @if(session()->has('first_login'))
        <div id="show-modal-first-login"></div>
        {{session()->forget('first_login')}}
    @else
        <div id="show-modal-need-confirm"></div>
    @endif
@endif
