{{--FONTS--}}
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;amp;subset=cyrillic"
      rel="stylesheet"/>
{{--STYLE--}}
<link href="{{asset('s/css/all.css')}}" rel="stylesheet"/>
<link rel="stylesheet" href="{{asset('css/auto-complete.css')}}">
<link rel="stylesheet" href="{{asset('bower_components/bootstrap-datapicker/css/bootstrap-datepicker3.css')}}">
<link rel="stylesheet" href="{{asset('bower_components/Bootstrap-Popover/dist/jquery.webui-popover.min.css')}}">
<link rel="stylesheet" href="{{asset('bower_components/Bootstrap-Popover/dist/jquery.webui-popover.min.css')}}">
<link rel="manifest" href="/manifest.json" />
{{--<link rel="stylesheet" href="{{asset('bower_components/bootstrap-datapicker/css/bootstrap-datepicker.standalone.css')}}">--}}
@if(Auth::check())
<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>

<script>
    window.OneSignalAppId = "{{env('ONESIGNAL_APP_ID')}}";
    window.PusherKey = "{{env('PUSHER_APP_KEY')}}";
</script>
@endif

<meta name="csrf-token" content="{{ csrf_token() }}">