<button class="lfm_pick" type="button">Выбрать изображение</button>
<input type="hidden" name="{{$name}}"
       @if(isset($value))
       value="{{$value}}"
       @endif
       class="lfm_pick_input">
<p class="help-block">Если не указать этот параметр, то будет использоваться стандартное изображение</p>
@if(isset($value))
<img style="width: 30%; margin-top: 20px; height: auto;" src="{{$value}}">
@endif