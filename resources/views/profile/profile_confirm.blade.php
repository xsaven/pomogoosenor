@extends('layouts.profile')

@section('title') Личная информация @stop

@section('content')
    <div class="l-profile__left">
        @include('profile._partials._info')
        <div class="l-profile-tabs">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#about-me" data-toggle="tab">Обо мне</a></li>
                <li><a href="#scores" data-toggle="tab">Счета</a></li>
                <li><a href="#favorites" data-toggle="tab">Избранное</a></li>
                <li><a href="#settings" data-toggle="tab">Настройки</a></li>
            </ul>
            <div class="tab-content">
                @include('profile.tabs.fill_info')
                @include('profile.tabs.scores')
                @include('profile.tabs.favorites')
                @include('profile.tabs.setting')
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        window.onload = function () {
            var xhr;
            new autoComplete({
                selector: 'input[name="city"]',
                source: function (term, response) {
                    try {
                        xhr.abort();
                    } catch (e) {
                    }
                    xhr = $.getJSON('{{route('find_city')}}', {query: term}, function (data) {
                        response(data);
                    });
                },
                renderItem: function (item, search) {
                    search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                    var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
                    return '<div class="autocomplete-suggestion" value="' + item.id + '" data-val="' + item.name + '">' + item.name + '</div>';
                }
            });
        };

    </script>
@endsection