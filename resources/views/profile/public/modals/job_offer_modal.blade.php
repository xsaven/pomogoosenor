<?php
/**
 * @var \App\User $user
 * @var \App\UserTasks[]|\Illuminate\Support\Collection $tasks_to_offer
 */
$tasks_to_offer = \App\UserTasks::getTasksToOffer();
?>

{{--<div id="job-offer-modal" class="modal fade modal-mob-numb">--}}
{{--<div class="modal-dialog">--}}
{{--<div class="modal-body">--}}
{{--<button type="button" data-dismiss="modal" class="but-modal-close"></button>--}}
{{--<div class="modal-welcome__txt modal-welcome__txt-left">--}}
{{--<h2 class="l-profile-pub__annotation">Выберите задание, которе вы хотите предложить--}}
{{--<a href="{{route('profile_show',['user'=>$user])}}">{{ $user->name }}</a>--}}
{{--</h2>--}}
{{--</div>--}}
{{--<div class="modal-welcome__bottom">--}}
{{--@if($tasks_to_offer->isNotEmpty())--}}
{{--<form class="filter-form" method="post" action="{{route('executors.offer-job')}}">--}}
{{--{{csrf_field()}}--}}
{{--<input type="hidden" name="executor_id" value="{{$user->id}}">--}}
{{--<select id="card-list-select" data-type="executor" class="selectpicker" name="task_id">--}}
{{--@foreach($tasks_to_offer as $task)--}}
{{--<option value="{{$task->id}}">{{$task->name}}</option>--}}
{{--@endforeach--}}
{{--</select>--}}
{{--<button type="submit" class="but but_bondi">Предложить</button>--}}
{{--</form>--}}
{{--@else--}}
{{--<h5>У вас нет созданных заданий</h5>--}}
{{--<a class="but but_bondi " href="{{route('categories_list')}}">Создать задание</a>--}}
{{--@endif--}}
{{--</div>--}}
{{--<div class="modal-welcome__bottom-txt text-center">--}}
{{--<p class="l-profile__sidebar-annot">Исполнитель получит уведомление и сможет оказать вам свои--}}
{{--услуги--}}
{{--</p>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}

<div id="job-offer-modal" class="modal fade modal-mob-numb">
    <div class="modal-dialog">
        <form class="modal-body" method="post" action="{{route('executors.offer-job')}}">
            {{csrf_field()}}
            <input type="hidden" name="executor_id" value="{{$user->id}}">

            <button type="button" data-dismiss="modal" class="but-modal-close"></button>
            <div class="modal-welcome__txt modal-welcome__txt-left">
                <h2>Выберите задание, которое <br>хотите предложить<br>исполнителю</h2>
            </div>
            @if($tasks_to_offer&& $tasks_to_offer->isNotEmpty())
                <div class="modal-select">
                    <select class="selectpicker change__butcon" name="task_id">
                        @foreach($tasks_to_offer as $task)
                            <option value="{{$task->id}}">{{$task->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="modal-welcome__bottom modal-welcome__bottom-left">
                    <button type="submit" class="but but_bondi">Предлождить задание</button>
                </div>
            @else
                <h5>У вас нет созданных заданий</h5>
                <div class="modal-welcome__bottom modal-welcome__bottom-left">
                    <a href="{{route('categories_list')}}" class="but but_bondi">Создать новое задание</a>
                </div>
            @endif
            <div class="modal-welcome__txt modal-welcome__txt-left">
                <p>Исполнитель получит уведомление и сможет оказать вам свои услуги.</p>
            </div>
        </form>
    </div>
</div>