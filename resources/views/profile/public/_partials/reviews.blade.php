<div class="l-prof-ab-me__reviews">
    <div class="reviews__head">
        <b>Отзывы</b>
        <span>
            <svg width="1em" height="1em" class="icon icon-like ">
              <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-like')}}"></use>
            </svg>
            <strong>{{$user->positive_reviews_count}}</strong>
        </span>
        <span class="negative-review-left">
            <svg width="1em" height="1em" class="icon icon-dislike">
              <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-like')}}"></use>
            </svg>
            <strong>{{$user->negative_reviews_count}}</strong>
        </span>
    </div>

    @foreach($reviews as $review)

        <div class="reviews__body-user clearfix">
            <div class="reviews__body-user-photo h-object-fit">
                <img src="{{$review->user->avatar()}}" alt="Аватар {{$review->user->name}}"/>
            </div>
            <div class="reviews__body-user-info clearfix">
                <a href="{{url('profile', ['id'=> $review->user->id])}}">
                    <div class="reviews__body-user-name">{{$review->user->name}}</div>
                </a>
                <div class="reviews__body-user-about">
                    <span class="about-left">Отзывы</span>
                    @if($review->user->positive_reviews_count)
                        <svg width="1em" height="1em" class="icon icon-like ">
                            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-like')}}"></use>
                        </svg>
                        <span>{{$review->user->positive_reviews_count}}</span>
                    @endif
                    @if($review->user->negative_reviews_count)
                        <svg width="1em" height="1em" class="icon icon-dislike ">
                            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-like-2')}}"></use>
                        </svg>
                        <span>{{$review->user->negative_reviews_count}}</span>
                    @endif
                    <span> - {{ $review->isCreator()?'заказчик':'исполнитель' }}</span>
                </div>
                <div class="reviews__body-user-txt">
                    <div class="txt-top">
                        @if($review->task)
                            @if($review->isExecutor())
                                Отзыв о выполнении задания
                            @else
                                Отзыв на задание
                            @endif
                            «{{$review->task->name}}»
                        @endif
                    </div>
                    <div class="txt-middle">
                        {{$review->text}}
                    </div>
                    @include('blocks._review_stars')
                </div>
            </div>
            <div class="reviews__body-user-time">{{$review->getCountDayAgo()}} назад</div>
        </div>
    @endforeach
    {{$reviews->links()}}
</div>