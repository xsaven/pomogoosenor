<div class="l-catalog-banner">
    <div class="l-catalog-banner__img"><img
                src="{{asset('s/images/tmp_file/cat-banner.png')}}" alt=""/>
    </div>
    <div class="l-catalog-banner__ttl">Как уберечься от мошенников?</div>
    <div class="l-catalog-banner__btn"><a href="{{url('faq#cheater')}}"
                                          class="but but_bondi"><span>Подробнее</span></a>
    </div>
</div>