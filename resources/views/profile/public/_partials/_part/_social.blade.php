<?php /** @var \App\User $user */ ?>
<div class="l-profile__sidebar-confirm">
    <div class="confirm-top">
        <h5>Подтвержденные контакты</h5>
        @if($user->hasVerifiedContacts())
            @if($user->profile->phone_verified)
                <div class="confirm-top__item clearfix active">
                    <div class="confirm-top__item-ico">
                        <svg width="1em" height="1em" class="icon icon-conf-tel ">
                            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-conf-tel')}}"></use>
                        </svg>
                    </div>
                    <div class="confirm-top__item-txt">
                        <div class="confirm-top__item-name">Телефон</div>
                        <div class="confirm-top__item-inf">
                            Подтвержден
                        </div>
                    </div>
                </div>
            @endif
            @if($user->verified)
                <div class="confirm-top__item clearfix @if($user->verified) active @endif ">
                    <div class="confirm-top__item-ico">
                        <svg width="1em" height="1em" class="icon icon-conf-mail ">
                            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-conf-mail')}}"></use>
                        </svg>
                    </div>
                    <div class="confirm-top__item-txt">
                        <div class="confirm-top__item-name">Email</div>
                        <div class="confirm-top__item-inf">
                            Подтвержден
                        </div>
                    </div>
                </div>
            @endif
            @if(count($user->socialAccounts))
                @foreach($user->socialAccounts as $item)
                    <div class="confirm-top__item clearfix active">
                        <div class="confirm-top__item-ico">
                            <svg width="1em" height="1em" class="icon icon-conf-{{$item->social_type}}">
                                <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-conf-'.$item->social_type_short)}}"></use>
                            </svg>
                        </div>
                        <div class="confirm-top__item-txt">
                            <div class="confirm-top__item-name">
                                {{$item->caption}}
                            </div>
                            <div class="confirm-top__item-inf">
                                <span>{{$user->name}}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        @else
            <p>
            <div class="l-profile__sidebar-annot">
                <span>
                    У пользователя нет <br>
                    подтвержденных котактов
                </span>
            </div>
            </p>
        @endif
    </div>
</div>