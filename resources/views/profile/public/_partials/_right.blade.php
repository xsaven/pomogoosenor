<div class="l-profile__sidebar">
    @if($user->profile->is_worker)
        @if($user->profile->workerProfile->min_price)
            <div class="l-profile__sidebar-price">
                <span>Цена за час работы</span><b>{{$user->profile->workerProfile->min_price}} руб./час</b></div>
        @endif
        @if(!$user->is_busy)
            <a href="#job-offer-modal" id="job-offer-link" data-toggle="modal" class="but but_bondi propose">
                <span>Предложить задание</span>
            </a>
            <div class="l-profile__sidebar-annot">Исполнитель получит уведомление и сможет оказать вам свои
                услуги
            </div>
        @else
            <span class="but but_bondi propose busy"><span>Исполнитель занят</span></span>
        @endif


    @endif
    @include('profile.public._partials._part._social')
    <div class="l-profile__sidebar-posts">
        @include('profile.public._partials._sidebar_favorites')
        @widget('Blog')
    </div>
</div>
@if($user->profile->is_worker)
    @include('profile.public.modals.job_offer_modal')
@endif