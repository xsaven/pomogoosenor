<?php
/** @var \App\User $user */
?>
<div class="l-profile__sidebar-favorite">
    <h5>В избранном</h5>
    @if($followers_count = $user->followers->count())
    <span>Этого пользователя добавили</span>
    <span>в избранное {{$followers_count}} человек</span>
    <div class="row">
        @foreach($user->followers as $follower)
        <div class="sidebar-favorite__col">
            <a href="{{route('profile_show',$follower)}}" class="sidebar-favorite__item h-object-fit">
                <img src="{{$follower->avatar()}}" alt="{{$follower->name}}" title="{{$follower->name}}"/>
            </a>
        </div>
        @endforeach
    </div>
    <a href="#" class="link_bondi">Показать всех</a>
    @else
        <span>Этого пользователя пока никто не добавил в избранное</span>
    @endif
</div>