<?php
/** @var \App\User $user */
$count_executed = $user->tasks_executed()->count();
$count_created = $user->tasks_created()->count();
?>
<div class="l-prof-def__vals">
    @if($count_executed)
        {{ trans_choice('profile.tasks.executed_label', $user->profile->getOriginal('sex'))}} {{ trans_choice('profile.tasks.tasks', $count_executed, ['count' => $count_executed]) }}
    @endif
    {{ $count_executed && $count_created?', ':'' }}
    @if($count_created)
        {{trans_choice('profile.tasks.created_label', $user->profile->getOriginal('sex'))}} {{ trans_choice('profile.tasks.tasks', $count_created, ['count' => $count_created]) }}
    @endif
</div>
@if($user->positive_reviews_count || $user->negative_reviews_count)
    <div class="l-prof-def__rating">
        <span>Средняя оценка: {{ $user->avg_mark['total'] }}</span>

        <div class="l-prof-def__rating-stars">
            @for($i = 1; $i <= 5;$i++)

                @if($user->avg_mark['total'] >= $i)
                    <svg width="1em" height="1em" class="icon icon-star">
                        <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star')}}"></use>
                    </svg>
                @else
                    <svg width="1em" height="1em" class="icon icon-star2">
                        <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star2')}}"></use>
                    </svg>
                @endif
            @endfor
        </div>
        <span>
    ({{ trans_choice('profile.reviews.count', $count = $user->getReviewsCount(),compact('count')) }})
</span>
    </div>

@else
    <div class="l-prof-def__vals">Нет оценок</div>
@endif

<div class="l-prof-def__icons-2 active">
    <div class="l-prof-def__icons-2">
        <a href="#"
           class="l-prof-def__icons--icon icon-rat-1{{$user->getStickerProjectsCount() ? '-active' : null}}">
                        <span class="l-prof-def__icons--icon-num">{{$user->getStickerProjectsCount() ? $user->getStickerProjectsCount() : 50}}
                            +</span>
            <svg width="1em" height="1em" class="icon icon-like ">
                <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-like"></use>
            </svg>
        </a>
        <a href="#"
           class="l-prof-def__icons--icon icon-rat-2{{$user->getPositionByPositiveReviews() ? '-active' : null}}"><span
                    class="l-prof-def__icons--icon-attr">ТОП</span><span
                    class="l-prof-def__icons--icon-num"> {{$user->getPositionByPositiveReviews() ? $user->getPositionByPositiveReviews() : 500}}</span>
            <svg width="1em" height="1em" class="icon icon-winners-cup-silhouette ">
                <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-winners-cup-silhouette"></use>
            </svg>
        </a>
        <a href="#"
           class="l-prof-def__icons--icon icon-rat-3{{$user->getPercentTasks() ? '-active' : null}}"><span
                    class="l-prof-def__icons--icon-attr">%</span><span
                    class="l-prof-def__icons--icon-num">{{$user->getPercentTasks()}}</span></a><a
                href="#">
        </a>

        @if($user->cleanerStatus())
            <a href="#" class="icon-rat-4">
                <svg width="1em" height="1em"
                     class="icon icon-rat-4">
                    <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-rat-4"></use>
                </svg>
            </a>
        @endif
    </div>
</div>