<div class="l-prof-ab-me__works">
    <h5>Виды выполняемых работ</h5>

    @foreach($user->getCategoriesByTasks() as $category)
        @php($rating = $user->getPositionByPositiveReviewsByCategory($category->id))
        @php($taskCount = $user->getCountTasksByCategory($category->id))
        @if($rating)
            <div class="l-prof-ab-me__works-item"><a
                        href="{{route('task.new', ['category' => $category->category->slug, 'subcategory' => $category->slug])}}">
                    {{$category->category->name}}
                </a>
                <span>{{$rating}}
                    место в рейтинге категории, выполнено {{\App\Helpers\DateTime::formatted($taskCount, ['задание', 'задания', 'заданий'])}}</span>
                <p>{{$category->name}}</p>
            </div>
        @endif
    @endforeach


</div>