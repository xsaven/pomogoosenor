<?php
/** @var \App\User $user */
?>
<div class="l-prof-ab-me__mean">
    <div class="mean-head"><b>Средняя оценка: {{$user->avg_mark['total']}}</b>
        <div class="mean-head__stars">
            @for($i = 1; $i <= 5;$i++)
                @if($user->avg_mark['total'] >= $i)
                    <svg width="1em" height="1em" class="icon icon-star">
                        <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star')}}"></use>
                    </svg>
                @else
                    <svg width="1em" height="1em" class="icon icon-star2">
                        <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star2')}}"></use>
                    </svg>
                @endif
            @endfor
        </div>
    </div>
    <div class="mean-body">
        <div class="mean-body__item"><span>Качество</span>
            <div class="mean-body__item-stars">
                @for($i = 1; $i <= 5;$i++)
                    @if($user->avg_mark['quality'] >= $i)
                        <svg width="1em" height="1em" class="icon icon-star">
                            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star')}}"></use>
                        </svg>
                    @else
                        <svg width="1em" height="1em" class="icon icon-star2">
                            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star2')}}"></use>
                        </svg>
                    @endif
                @endfor
            </div>
        </div>
        <div class="mean-body__item"><span>Вежливость</span>
            <div class="mean-body__item-stars">
                @for($i = 1; $i <= 5;$i++)
                    @if($user->avg_mark['politeness'] >= $i)
                        <svg width="1em" height="1em" class="icon icon-star">
                            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star')}}"></use>
                        </svg>
                    @else
                        <svg width="1em" height="1em" class="icon icon-star2">
                            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star2')}}"></use>
                        </svg>
                    @endif
                @endfor
            </div>
        </div>
        <div class="mean-body__item"><span>Цена</span>
            <div class="mean-body__item-stars">
                @for($i = 1; $i <= 5;$i++)
                    @if($user->avg_mark['price'] >= $i)
                        <svg width="1em" height="1em" class="icon icon-star">
                            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star')}}"></use>
                        </svg>
                    @else
                        <svg width="1em" height="1em" class="icon icon-star2">
                            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star2')}}"></use>
                        </svg>
                    @endif
                @endfor
            </div>
        </div>
        <div class="mean-body__item"><span>Пунктуальность</span>
            <div class="mean-body__item-stars">
                @for($i = 1; $i <= 5;$i++)
                    @if($user->avg_mark['punctuality'] >= $i)
                        <svg width="1em" height="1em" class="icon icon-star">
                            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star')}}"></use>
                        </svg>
                    @else
                        <svg width="1em" height="1em" class="icon icon-star2">
                            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star2')}}"></use>
                        </svg>
                    @endif
                @endfor
            </div>
        </div>
        <div class="mean-body__item"><span>Адекватность</span>
            <div class="mean-body__item-stars">
                @for($i = 1; $i <= 5;$i++)
                    @if($user->avg_mark['adequacy'] >= $i)
                        <svg width="1em" height="1em" class="icon icon-star">
                            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star')}}"></use>
                        </svg>
                    @else
                        <svg width="1em" height="1em" class="icon icon-star2">
                            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star2')}}"></use>
                        </svg>
                    @endif
                @endfor
            </div>
        </div>
    </div>
</div>