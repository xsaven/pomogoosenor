<?php /**
 * @var \App\User $user
 * @var \App\Galleries[] $galleries
 */ ?>
<div class="l-profile-pub__examps">
    <h5>Примеры работ</h5>
    <div class="row">
        @foreach($galleries as $gallery)
            <div class="col-md-6 col-sm-6">
                <a href="{{route('galleries.show',compact('gallery'))}}" class="l-profile-pub__examps-item">
                    <div class="examps-item__img h-object-fit">
                        <img src="{{asset($gallery->gallery_avatar)}}" alt=""></div>
                    <div class="examps-item__txt clearfix">
                        <div class="examps-item__txt-name">{{$gallery->title}}</div>
                        <div class="examps-item__txt-count">
                            <svg width="1em" height="1em" class="icon icon-camera ">
                                <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-camera')}}"></use>
                            </svg>
                            <span>{{$gallery->content_items_count}}</span>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>
</div>