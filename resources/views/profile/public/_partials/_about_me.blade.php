<?php
/** @var \App\User $user */
?>

@if($user->profile->is_worker)
    <div class="l-profile-pub__annotation">

        <svg width="1em" height="1em" class="icon icon-annot ">
            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-annot')}}"></use>
        </svg>

        <p>Чтобы воспользоваться моими услугами, нажмите кнопку
            <a href="#job-offer-modal" id="job-offer-link" data-toggle="modal">Предложить задание</a>
            Сотрудничаю с условием, что о моей работе будет оставлен отзыв на Pomogoo.
        </p>

    </div>
@else
    <br>
@endif

<h5>Обо мне</h5>
<p>{!! $user->profile->workerProfile->about_me !!}</p>
