<?php
/** @var \App\User $user */
?>
<div class="l-profile-default">
    <div class="l-prof-def__hello clearfix">
        <div class="l-prof-def__head-views active">
            <svg width="1em" height="1em" class="icon icon-eye ">
                <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-eye')}}"></use>
            </svg>
            <span>{{\App\Helpers\Formatter::formatted($user->profile->getViews(), ['просмотр профиля', 'просмотра профиля', 'просмотров профиля'])}} </span>
        </div>
        @if($user->online)
            <div class="l-prof-def__hello-was">Сейчас на сайте</div>
        @else
            <div class="l-prof-def__hello-was">{{ trans_choice('profile.tasks.was', $user->profile->getOriginal('sex'))}} на сайте {{$user->last_activity_diff}}</div>
        @endif
    </div>
    <div class="l-prof-def__head clearfix">
        <div class="l-prof-def__head-ttl">Здраствуйте, меня зовут, {{$user->profile->name or 'Пользователь'}}!</div>
    </div>
    <div class="l-prof-def__sub-head clearfix">
        <div class="l-prof-def__ava">
            <div class="steps-content__input-file">
                <div class="steps-content__form-input__img h-object-fit">
                    <img class="avatar_preview" src="{{ $user->profiles->avatar_full or '' }}" alt>
                </div>
            </div>
        </div>
        <div class="l-prof-def__city-wrp">
            <div class="l-prof-def__city">
                @if($user->profile->age)
                    <span class="years">
                    {{trans_choice('profile.age', $age = $user->profile->age,compact('age'))}}
                </span>
                @endif
                <svg width="1em" height="1em" class="icon icon-mark ">
                    <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-mark')}}"></use>
                </svg>
                <span>
                    {{$user->profiles->cities->name or ''}}
                </span>
            </div>
            @include('profile.public._partials._head_reviews')
            @auth
                <div class="l-prof-def__btns clearfix">
                    <a href="#"
                       @if($user->isChatAvailable())
                       onclick="VueChat.getChatByUserId({{$user->id}})"
                       class="l-prof-def__button">
                        @else
                            class="l-prof-def__button disabled">
                        @endif
                        <span>Задать вопрос</span>
                    </a>
                    @php($followed_by_me = $user->isFollowedByMe())
                    {{ Form::open(['method' => 'DELETE', 'route' => ['web.followers.unfollow', $user],'class'=>'favoritesForm '.($followed_by_me?'':'collapse')]) }}
                    <button type="submit" class="l-prof-def__button favorite">
                        <svg width="1em" height="1em" class="icon icon-heart-red">
                            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-heart')}}"></use>
                        </svg>
                        <span>В избранном</span>
                    </button>
                    {{Form::close()}}
                    {{ Form::open(['method' => 'POST', 'route' => ['web.followers.follow', $user],'class'=>'favoritesForm '.(!$followed_by_me?'':'collapse')]) }}
                    <button type="submit" class="l-prof-def__button favorite">
                        <svg width="1em" height="1em" class="icon">
                            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-heart')}}"></use>
                        </svg>
                        <span>В избранное</span>
                    </button>
                    {{Form::close()}}
                </div>
            @endauth
        </div>
    </div>
</div>
