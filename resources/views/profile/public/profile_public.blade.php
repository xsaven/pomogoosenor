@extends('layouts.profile_public')
<?php /** @var \App\User $user */ ?>
@section('title')
    @if($user->profiles->is_worker)
        {{$user->name}}, исполнитель с {{LocalizedCarbon::parse($user->created_at)->toDateString()}}
    @else
        Профиль {{$user->name}}
    @endif
@stop

@section('content')
    <div class="l-profile__left">
        <div class="l-profile-default">
        @include('profile.public._partials._info')
        </div>
        <div class="l-profile-pub">
            @include('profile.public._partials._about_me')
            @include('profile.public._partials._work_examples')
            @include('profile.public._partials._work_types')
            @include('profile.public._partials._marks')
            @include('profile.public._partials.reviews')
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        window.onload = function () {
            var xhr;
            new autoComplete({
                selector: 'input[name="city"]',
                source: function (term, response) {
                    try {
                        xhr.abort();
                    } catch (e) {
                    }
                    xhr = $.getJSON('{{route('find_city')}}', {query: term}, function (data) {
                        response(data);
                    });
                },
                renderItem: function (item, search) {
                    search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                    var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
                    return '<div class="autocomplete-suggestion" value="' + item.id + '" data-val="' + item.name + '">' + item.name + '</div>';
                }
            });
        };

    </script>
@endpush