@extends('layouts.profile')

@section('title') Личная информация @stop

@section('content')
    <div class="l-profile__left">
        @include('profile._partials._info')
        <div class="l-profile-tabs">
            <ul class="nav nav-tabs" data-tab="{{$tab}}">
                <li class="{{empty(request()->tab) ? 'active' : ""}}"><a href="#about-me" data-toggle="tab">Обо мне</a></li>
                @if($user->profiles->is_worker)
                    <li><a href="#payment" data-toggle="tab">Счета</a></li>
                @endif
                <li class="{{request()->tab=='favorites' ? 'active' : ""}}"><a href="#favorites" data-toggle="tab">Избранное</a></li>
                <li class="{{request()->tab=='settings' ? 'active' : ""}}"><a href="#settings" data-toggle="tab">Настройки</a></li>
            </ul>
            <div class="tab-content">
                @include('profile.tabs.about_me')
                @if($user->profiles->is_worker)
                    @include('profile.tabs.scores')
                @endif
                @include('profile.tabs.favorites')
                @include('profile.tabs.setting')
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        window.onload = function () {
            var xhr;
            new autoComplete({
                selector: 'input[name="city"]',
                source: function (term, response) {
                    try {
                        xhr.abort();
                    } catch (e) {
                    }
                    xhr = $.getJSON('{{route('find_city')}}', {query: term}, function (data) {
                        response(data);
                    });
                },
                renderItem: function (item, search) {
                    search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                    var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
                    return '<div class="autocomplete-suggestion" value="' + item.id + '" data-val="' + item.name + '">' + item.name + '</div>';
                }
            });
        };

    </script>
@endpush