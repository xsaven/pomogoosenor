<div class="l-profile__sidebar-confirm">
    <div class="confirm-top">
        <h5>Подтвержденные контакты</h5>
        @if($user->profiles->phone_verified)
            <div class="confirm-top__item clearfix active">
                <div class="confirm-top__item-ico">
                    <svg width="1em" height="1em" class="icon icon-conf-tel ">
                        <use fill="#FFB53B"
                             xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-conf-tel')}}"></use>
                    </svg>
                </div>
                <div class="confirm-top__item-txt">
                    <div class="confirm-top__item-name">Телефон</div>
                    <div class="confirm-top__item-inf">
                        <span>{{$user->profiles->phone_code}}{{$user->profiles->phone}}</span>
                    </div>
                </div>
            </div>
        @endif
        @if($user->verified)

            <div class="confirm-top__item clearfix  active  ">
                <div class="confirm-top__item-ico">
                    <svg width="1em" height="1em" class="icon icon-conf-mail ">
                        <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-conf-mail')}}"></use>
                    </svg>
                </div>
                <div class="confirm-top__item-txt">
                    <div class="confirm-top__item-name">Email</div>
                    <div class="confirm-top__item-inf"><a href="#" data-toggle="modal" data-target="#confirm"
                                                          class="bind-contact link_bondi">подтвердить</a><span>{{$user->email}}</span>
                    </div>
                </div>
            </div>
        @endif
        @if(count($user->socialAccounts))
            @foreach($user->socialAccounts as $item)
                <div class="confirm-top__item clearfix active">
                    <a href="{{route('social.detach',['provider'=>$item->social_type])}}"
                       class="remove-bind">отвязать</a>
                    <div class="confirm-top__item-ico">
                        <svg width="1em" height="1em" class="icon icon-conf-{{$item->social_type}}">
                            <use fill="#FFB53B"
                                 xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-conf-'.$item->social_type_short)}}"></use>
                        </svg>
                    </div>
                    <div class="confirm-top__item-txt">
                        <div class="confirm-top__item-name">
                            {{$item->caption}}
                        </div>
                        <div class="confirm-top__item-inf">
                            <span>{{$user->name}}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
    <div class="confirm-middle">Повысьте доверие пользователей к себе – привяжите ваши аккаунты
        социальных сетей к профилю Pomogoo. Мы обязуемся не раскрывать ваши контакты.
    </div>
    <div class="confirm-bottom">
        @if(!$user->verified)

            <div class="confirm-top__item clearfix ">
                <div class="confirm-top__item-ico">
                    <svg width="1em" height="1em" class="icon icon-conf-mail ">
                        <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-conf-mail')}}"></use>
                    </svg>
                </div>
                <div class="confirm-top__item-txt">
                    <div class="confirm-top__item-name">Email</div>
                    <div class="confirm-top__item-inf"><a href="#" data-toggle="modal" data-target="#confirm"
                                                          class="bind-contact link_bondi">подтвердить</a><span>{{$user->email}}</span>
                    </div>
                </div>
            </div>
        @endif
        @foreach($user->getUnavailableSocialAccounts() as $item)
            <div class="confirm-top__item clearfix">
                <div class="confirm-top__item-ico">
                    <svg width="1em" height="1em" class="icon icon-conf-{{$item->social_type_short}}">
                        <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-conf-'.$item->social_type_short)}}"></use>
                    </svg>
                </div>
                <div class="confirm-top__item-txt">
                    <div class="confirm-top__item-name">{{$item->caption}}</div>
                    <div class="confirm-top__item-inf"><a
                                href="{{route('social.attach',['provider'=>$item->social_type])}}"
                                class="bind-contact link_bondi">привязать</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>