<?php
/** @var \App\User $user */
?>
<div class="l-profile-default">
    <div class="l-prof-def__head clearfix">
        <div class="l-prof-def__head-ttl">Здраствуйте, {{$user->name or 'Пользователь'}}!</div>
        <div class="l-prof-def__head-views">
            <svg width="1em" height="1em" class="icon icon-eye ">
                <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-eye')}}"></use>
            </svg>
            <span>{{\App\Helpers\Formatter::formatted($user->profile->getViews(), ['просмотр профиля', 'просмотра профиля', 'просмотров профиля'])}} </span>
        </div>
    </div>
    <div class="l-prof-def__sub-head clearfix">
        <div class="l-prof-def__ava">
            <div class="steps-content__input-file">
                <div class="steps-content__form-input__img h-object-fit">
                    <img class="avatar_preview" src="{{ $user->profiles->avatar_full or '' }}" alt>
                </div>

                <label class="type_file">
                    <input id="add_photo" type="file">
                    <span>Изменить фото
                                <svg width="1em" height="1em" class="icon icon-photoap ">
                                  <use xlink:href="{{ symbolSvg('icon-photoap') }}"></use>
                                </svg>
                            </span>
                </label>
                <label id="delete_avatar" class="type_file">
                    Удалить фото
                </label>

            </div>
        </div>
        <div class="l-prof-def__city-wrp">
            <div class="l-prof-def__city">
                @if($user->profile->age)
                    <span class="years">
                        {{trans_choice('profile.age', $age = $user->profile->age,compact('age'))}}
                    </span>
                @endif
                <svg width="1em" height="1em" class="icon icon-mark ">
                    <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-mark')}}"></use>
                </svg>
                <span>
                    {{$user->profiles->cities->name or ''}}
                </span>
            </div>
            @if($user->profiles->is_worker)
                @if($user->positive_reviews_count || $user->negative_reviews_count)
                    {{--TODO Если есть оценки то код ниже--}}
                    <div class="l-prof-def__vals">
                        {{ mb_ucfirst(trans_choice('profile.tasks.executed', $count = $user->tasks_executed()->count(),compact('count'))) }}
                        ,
                        {{ trans_choice('profile.tasks.created', $count = $user->tasks_created()->count(),compact('count')) }}
                    </div>
                    <div class="l-prof-def__rating"><span>Средняя оценка: {{ $user->avg_mark['total'] }}</span>
                        <div class="l-prof-def__rating-stars">
                            @for($i = 1; $i <= 5;$i++)

                                @if($user->avg_mark['total'] >= $i)
                                    <svg width="1em" height="1em" class="icon icon-star">
                                        <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star')}}"></use>
                                    </svg>
                                @else
                                    <svg width="1em" height="1em" class="icon icon-star2">
                                        <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star2')}}"></use>
                                    </svg>
                                @endif
                            @endfor
                        </div>
                        <span>
                            ({{ trans_choice('profile.reviews.count', $count = $user->getReviewsCount(),compact('count')) }}
                            )

                        </span>
                    </div>



                    {{--<div class="l-prof-def__icons"><a href="#">--}}
                    {{--<svg width="1em" height="1em" class="icon icon-rat-1 ">--}}
                    {{--<use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-rat-1')}}"></use>--}}
                    {{--</svg>--}}
                    {{--</a><a href="#">--}}
                    {{--<svg width="1em" height="1em" class="icon icon-rat-2 ">--}}
                    {{--<use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-rat-2')}}"></use>--}}
                    {{--</svg>--}}
                    {{--</a><a href="#">--}}
                    {{--<svg width="1em" height="1em" class="icon icon-rat-3 ">--}}
                    {{--<use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-rat-3')}}"></use>--}}
                    {{--</svg>--}}
                    {{--</a><a href="#">--}}
                    {{--<svg width="1em" height="1em" class="icon icon-rat-4 ">--}}
                    {{--<use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-rat-4')}}"></use>--}}
                    {{--</svg>--}}
                    {{--</a>--}}
                    {{--</div>--}}
                @else
                    <div class="l-prof-def__vals">Нет оценок</div>
                @endif

                <div class="l-prof-def__icons-2">
                    <a href="#"
                       class="l-prof-def__icons--icon icon-rat-1{{$user->getStickerProjectsCount() ? '-active' : null}}">
                        <span class="l-prof-def__icons--icon-num">{{$user->getStickerProjectsCount() ? $user->getStickerProjectsCount() : 50}}
                            +</span>
                        <svg width="1em" height="1em" class="icon icon-like ">
                            <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-like"></use>
                        </svg>
                    </a>
                    <a href="#"
                       class="l-prof-def__icons--icon icon-rat-2{{$user->getPositionByPositiveReviews() ? '-active' : null}}"><span
                                class="l-prof-def__icons--icon-attr">ТОП</span><span
                                class="l-prof-def__icons--icon-num"> {{$user->getPositionByPositiveReviews() ? $user->getPositionByPositiveReviews() : 500}}</span>
                        <svg width="1em" height="1em" class="icon icon-winners-cup-silhouette ">
                            <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-winners-cup-silhouette"></use>
                        </svg>
                    </a>
                    <a href="#"
                       class="l-prof-def__icons--icon icon-rat-3{{$user->getPercentTasks() ? '-active' : null}}"><span
                                class="l-prof-def__icons--icon-attr">%</span><span
                                class="l-prof-def__icons--icon-num">{{$user->getPercentTasks()}}</span></a><a
                            href="#">
                    </a>

                    @if($user->cleanerStatus())
                        <a href="#" class="icon-rat-4">
                            <svg width="1em" height="1em"
                                 class="icon icon-rat-4">
                                <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-rat-4"></use>
                            </svg>
                        </a>
                    @endif
                </div>
            @endif
        </div>
    </div>
</div>
