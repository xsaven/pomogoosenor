<div class="l-profile__sidebar">
    @if(!$user->is_worker())
        <a href="{{url('/how-performer')}}" class="l-profile__sidebar-quest">
            <svg width="1em" height="1em" class="icon icon-shield ">
                <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-shield')}}"></use>
            </svg>
            <span>Хотите стать исполнителем Pomogoo?</span>
        </a>
    @endif

    @include('profile._partials._part._social')
    <div class="l-profile__sidebar-posts">
        @include('profile._partials._part._baner')
        @widget('Blog')
    </div>
</div>