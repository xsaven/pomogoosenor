<div id="payment" class="tab-pane fade">
    <div class="l-profile-score">
        <div class="l-profile-score__ballace">
            <h5>Баланс: {{$user->profile->balance}} рублей</h5>
            <p>Поддерживайте положительный баланс для возможности оставлять
                предложения</p>
            <form class="l-profile-score__suppl l-bid-write__form clearfix" method="post"
                  action="{{route('payment.pay')}}">
                {{csrf_field()}}
                <label><span>Пополнит счет на</span>
                    <input type="text" value="400" name="amount" class="form-control"/>
                    <span class="rub">₽</span>
                    <span class="min-price">Минимальная сумма 400 руб.</span>
                </label>
                <button type="submit" class="but but_bondi"><span>Пополнить счет</span>
                </button>
            </form>
            {{--<a href="#" class="but but_bondi bind-score"><span>Привязать счет к аккаунту</span></a>--}}
            <div class="l-profile-score__story l-bid-write__form">
                <div class="l-profile-score__story-head">
                    <h5>История операций</h5>
                    <select id="transactions-period" class="selectpicker" data-action="{{url('profile/transactions')}}">
                        <option value="month">за месяц</option>
                        <option value="week">за неделю</option>
                        <option value="year">за год</option>
                    </select>
                </div>
                <div id="l-profile-score__story-bd" class="l-profile-score__story-bd">
                    @include('profile.tabs.transactions.transactions')
                </div>
            </div>
            <div class="l-profile-score__faq">
                <h5>Частые вопросы</h5>
                <div class="l-profile-score__faq-item"><b>Условия работы с Pomogoo</b>
                    <p>Pomogoo списывает с исполнителей фиксированную оплату за
                        возможность оставлять к заданиям предложения с контактными
                        данными. Стоимость одного предложения зависит от категории
                        заданий и начинается от 20 рублей. Оплата за предложения не
                        возвращается.</p>
                </div>
                <div class="l-profile-score__faq-item"><b>Какая минимальная сумма для
                        пополнения счета</b>
                    <p>400 рублей.</p>
                </div>
                <div class="l-profile-score__faq-item"><b>Я могу сделать возврат
                        денежных средств со своего счета Pomogoo?</b>
                    <p>Да. Для того, чтобы вернуть деньги, необходимо отправить заявку с
                        запросом на вывод по адресу money@pomogoo.com. Средства будут
                        возвращены на ваш счет, с которого производилось пополнение
                        баланса в Pomogoo в течение 5 рабочих дней с учетом комиссии
                        платежной системы.</p>
                </div>
                <div class="l-profile-score__faq-item"><b>По какой причине я могу
                        лишиться статуса исполнителя?</b>
                    <p>Полный список причин описан в правилах сайта в разделе
                        <a href="{{url('/security')}}">Верификация Пользователя.</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    document.getElementById('transactions-period').addEventListener("change", function () {
        let self = this;
        $.ajax({
            url: self.dataset.action + '/' + self.value,
            success: function (data) {
                document.getElementById('l-profile-score__story-bd').innerHTML = data;
            },
        });
    });
</script>