<div id="favorites" class="tab-pane fade {{request()->tab=='favorites' ? 'in active' : ""}}">
    <div class="l-profile-favorite">
        @if($user->favorite_users->isNotEmpty())
            @foreach($user->favorite_users as $favorite_user)
                @include('profile.favorites.user')
            @endforeach
        @else
            <div class="l-prof-ab-me__revs">
                <h5>У вас нет избранных пользователей</h5>
                <svg width="1em" height="1em" class="icon icon-annot">
                    <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-annot')}}"></use>
                </svg>
                <p>Они появятся после того, как вы добавите кого-то в избранные</p>
            </div>
        @endif
    </div>
</div>