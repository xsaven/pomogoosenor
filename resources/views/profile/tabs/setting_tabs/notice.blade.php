@php($sub = user()->subscribe)
<div id="notice" class="tab-pane fade">
    <form class="l-prof-set__notice" method="post" action="{{route('profile_subscribe')}}">
        {{csrf_field()}}
        <h5>Получать уведомления:</h5>
        <label class="check-label">
            <input name="system_notification" type="checkbox" value="1" {{$sub->system_notification?'checked':''}}/><i class="checkbox"></i><span>Системные уведомления</span>
        </label>
        <label class="check-label">
            <input name="sms_new_message" type="checkbox" value="1" {{$sub->sms_new_message?'checked':''}}/><i class="checkbox"></i><span>СМС о новом личном сообщении в чате</span>
        </label>
        <label class="check-label">
            <input name="news" type="checkbox" value="1" {{$sub->news?'checked':''}}/><i class="checkbox"></i><span>Я хочу получать новости сайта</span>
        </label>
        <p>Подписываться на задания могут только исполнители с
            <a href="{{url('/security')}}">подтвержденным аккаунтом.</a></p>
        <button type="submit" class="but but_bondi">
            <span>Сохранить</span></button>
    </form>
</div>