@if(isset($cat_tree))
    <div id="subscribe" class="tab-pane fade">
        <form id="cat-from" method="post" action="{{route('profile.save_settings')}}">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="l-prof-set__subs">
                <h5>1. Выберите категории</h5>
                <div class="l-prof-conf__drops">
                    @php($expanded = 'true')
                    @foreach($cat_tree as $parent)
                        <div role="tab" class="panel-heading">
                            <a role="button"
                               data-toggle="collapse"
                               data-parent="#accordion"
                               href="#TAB_{{$parent->id}}"
                               aria-expanded="{{ $expanded }}"
                               aria-controls="TAB_{{$parent->id}}"
                               class="steps-content__collapse">{{$parent->name}}</a>
                        </div>
                        <div id="TAB_{{$parent->id}}" role="tabpanel"
                             class="panel-collapse collapse @if($expanded == 'true') in @endif">
                            @foreach($parent->subcategories as $child)
                                <div class="panel-body">
                                    <label class="check-label">
                                        <input @if(is_array(old('selected_cat')) && in_array($child->id,old('selected_cat')) || ($user->categories->where('id',$child->id)->first())) checked
                                               @endif name="selected_cat[]" value="{{ $child->id }}" type="checkbox"/>
                                        <i class="checkbox"></i>
                                        <span>{{ $child->name }}</span>
                                    </label>
                                </div>
                            @endforeach
                        </div>
                        @php($expanded = 'false')
                    @endforeach
                </div>
            </div>
            {{--<div class="l-prof-set__geo">--}}
                {{--<h5>2. Геопозиция. Свердловская и Челябинская--}}
                    {{--область</h5>--}}
                {{--<p>Выберите метро или районы, из которых хотите получать--}}
                    {{--уведомления о новых заданиях:</p>--}}
                {{--<div class="l-prof-set__geo-change">--}}
                    {{--<div class="l-prof-set__geo-item"><span>Свердловск (выбран весь город)</span><a--}}
                                {{--href="#" class="but but_y-orange"><span>Изменить станции метро</span></a>--}}
                    {{--</div>--}}
                    {{--<div class="l-prof-set__geo-item"><span>Челябинская область (не выбрано)</span><a--}}
                                {{--href="#" class="but but_y-orange"><span>Выбрать район на карте</span></a>--}}
                    {{--</div>--}}
                    {{--<label class="l-prof-set__geo-label check-label">--}}
                        {{--<input type="checkbox"/><i class="checkbox"></i><span>Отправлять уведомление, если новое задание находится рядом со мной. Если на вашем телефоне установлено мобильное приложение.</span>--}}
                    {{--</label>--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="l-prof-set__types">
                <h5>2. Типы уведомлений</h5>
                <div class="l-prof-set__types-line"><span>Уведомлять меня по:</span>
                    <label class="check-label">
                        <input name="subscribe[email]" value="0" type="hidden">
                        <input name="subscribe[email]" value="1" type="checkbox"
                               @if($user->subscribe_email) checked @endif/><i class="checkbox"></i><span>
                                          <svg width="1em" height="1em" class="icon icon-mail ">
                                            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-mail')}}"></use>
                                          </svg><b>E-mail</b></span>
                    </label>
                    <label class="check-label">
                        <input name="subscribe[push]" value="0" type="hidden">
                        <input name="subscribe[push]" value="1" type="checkbox"
                               @if($user->subscribe_push) checked @endif/><i class="checkbox"></i><span>
                                          <svg width="1em" height="1em" class="icon icon-smartphone ">
                                            <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-smartphone')}}"></use>
                                          </svg><b>Push</b></span>
                    </label>
                </div>
            </div>
            <div class="l-prof-set__button">
                <button type="submit" class="but but_bondi"><span>Сохранить</span>
                </button>
            </div>
        </form>
    </div>

    <div id="data-save" class="modal fade modal-mob-numb">
        <div class="modal-dialog">
            <div class="modal-body">
                <button type="button" data-dismiss="modal" class="but-modal-close"></button>
                <div class="modal-reg_success__txt">
                    <h5 id="email-sent-modal-message" style="margin-top: 20px;">Успех!</h5>
                    <p>Данные сохранены!</p>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>

            document.getElementById('cat-from').addEventListener('submit', function (event) {
                event.preventDefault();

                let form = event.target;
                jQuery(form).blockForm();
                jQuery.ajax({
                    url: form.action,
                    data: jQuery(form).serialize(),
                    type: 'POST',
                }).done(function (result) {
                    jQuery(form).unblockForm();
                    jQuery("#data-save").modal();
                }).fail(function (result) {
                    jQuery(form).unblockForm();

                    let modal = $('#modal_error_message');
                    let error_div = modal.find('.alert.alert-error');
                    modal.modal('show');
                    error_div.html('');
                    error_div.append('<p>Ошибка сохранения формы</p>');
                });
            });

        </script>
    @endpush
@endif
