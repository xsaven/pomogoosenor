<?php /** @var \App\User $user */ ?>
<div id="main-set" class="tab-pane fade in active">
    <form action="{{route('profile_main_update')}}"
          class="l-prof-set__main-set" id="profile-update"
          method="POST">
        {{method_field('PUT')}}
        {{csrf_field()}}
        <div class="l-bid-write__form">
            <div class="l-prof-set__form-line clearfix">
                <div class="data-type req">Имя</div>
                <div class="form-line__input">
                    <input name="name" type="text"
                           data-rule-required="true" data-msg-required="Имя - обязательное поле"
                           value="{{$user->getOriginal('name','')}}"
                           class="form-control"/>
                </div>
            </div>
            <div class="l-prof-set__form-line clearfix">
                <div class="data-type">Фамилия</div>
                <div class="form-line__input">
                    <input type="text" name="surname"
                           value="{{$user->profiles->surname or ''}}"
                           class="form-control"/>
                </div>
            </div>
            <div class="l-prof-set__form-line clearfix">
                <div class="data-type req">Город</div>
                <label class="form-line__input selectize_label">
                    <input name="city" type="text"
                           value="{{$user->profiles->cities->name or ''}}"
                           placeholder="Город"
                           data-rule-required="true" data-msg-required="Город - обязательное поле"
                           class="form-control form-data">
                </label>
            </div>
            <div class="l-prof-set__form-line clearfix">
                <div class="data-type req">День Рождения</div>
                <div class="form-line__input">
                    <div class="form-line__input-select">
                        <select title="День" name="day" id=""
                                class="selectpicker">
                            @for($i=1;$i<=31;$i++)
                                <option @if(isset($user->profiles->birthday) && $user->profiles->birthday->day == $i)  selected
                                        @endif value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="form-line__input-select">
                        <select name="month" title="Месяц"
                                class="selectpicker">
                            <option @if(isset($user->profiles->birthday) && $user->profiles->birthday->month == '1')  selected
                                    @endif class="special"
                                    value="1">
                                Январь
                            </option>
                            <option @if(isset($user->profiles->birthday) && $user->profiles->birthday->month == '2')  selected
                                    @endif class="special"
                                    value="2">
                                Февраль
                            </option>
                            <option @if(isset($user->profiles->birthday) && $user->profiles->birthday->month == '3')  selected
                                    @endif class="special"
                                    value="3">Март
                            </option>
                            <option @if(isset($user->profiles->birthday) && $user->profiles->birthday->month == '4')  selected
                                    @endif class="special"
                                    value="4">
                                Апрель
                            </option>
                            <option @if(isset($user->profiles->birthday) && $user->profiles->birthday->month == '5')  selected
                                    @endif class="special"
                                    value="5">Май
                            </option>
                            <option @if(isset($user->profiles->birthday) && $user->profiles->birthday->month == '6')  selected
                                    @endif class="special"
                                    value="6">Июнь
                            </option>
                            <option @if(isset($user->profiles->birthday) && $user->profiles->birthday->month == '7')  selected
                                    @endif class="special"
                                    value="7">Июль
                            </option>
                            <option @if(isset($user->profiles->birthday) && $user->profiles->birthday->month == '8')  selected
                                    @endif class="special"
                                    value="8">
                                Август
                            </option>
                            <option @if(isset($user->profiles->birthday) && $user->profiles->birthday->month == '9')  selected
                                    @endif class="special"
                                    value="9">
                                Сентябрь
                            </option>
                            <option @if(isset($user->profiles->birthday) && $user->profiles->birthday->month == '10')  selected
                                    @endif class="special"
                                    value="10">
                                Октябрь
                            </option>
                            <option @if(isset($user->profiles->birthday) && $user->profiles->birthday->month == '11')  selected
                                    @endif class="special"
                                    value="11">
                                Ноябрь
                            </option>
                            <option @if(isset($user->profiles->birthday) && $user->profiles->birthday->month == '12')  selected
                                    @endif class="special"
                                    value="12">
                                Декабрь
                            </option>
                        </select>
                    </div>
                    <div class="form-line__input-select">
                        <select name="year" title="Год"
                                class="selectpicker">
                            @for($i=2017;$i>1940;$i--)
                                <option @if(isset($user->profiles->birthday) && $user->profiles->birthday->year == $i)  selected
                                        @endif value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                        <input type="hidden" name="birtday">
                    </div>
                </div>
            </div>
            <div class="l-prof-set__form-line clearfix">
                <div class="data-type req">Пол</div>
                <div class="form-line__input">
                    <div class="l-prof-set__sex clearfix">
                        <label>
                            <input type="radio" value="0"
                                   checked="checked"
                                   name="sex"/><span
                                    class="l-prof-set__sex-but">Мужчина</span>
                        </label>
                        <label>
                            <input type="radio" value="1"
                                   @if($user->profiles->getOriginal('sex') == '1') checked="checked"
                                   @endif
                                   name="sex"/><span
                                    class="l-prof-set__sex-but">Женщина</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="l-prof-set__form-line clearfix tel">
                <div class="data-type req">Телефон</div>
                <div class="form-line__input">
                    <select name="phone_code" class="selectpicker">
                        <option value="+7"
                                @if($user->profiles->phone_code == '+7') selected @endif>
                            +7
                        </option>
                        {{--<option value="+380"--}}
                                {{--@if($user->profiles->phone_code == '+380') selected @endif>--}}
                            {{--+380--}}
                        {{--</option>--}}
                    </select>
                    <input data-rule-required="true" data-msg-required="Телефон - обязательное поле"
                           type="tel" name="phone"
                           value="{{$user->profiles->phone or ''}}"
                           class="form-control"/>
                    <div class="label-txt" id="email">Телефон скрыт от других
                        пользователей
                    </div>
                </div>
            </div>
            <div class="l-prof-set__form-line clearfix">
                <div class="data-type req">E-mail</div>
                <div class="form-line__input">
                    <input data-rule-required="true" data-msg-required="Email - обязательное поле"
                    <input data-msg-email="Неправильный email"
                           type="email" name="email"
                           value="{{$user->email}}"
                           class="form-control"/>
                    <div class="label-txt">Почтовый адрес скрыт от
                        других пользователей
                    </div>
                </div>
            </div>
            <button type="submit" class="but but_bondi"><span>Сохранить</span>
            </button>
        </div>
    </form>
    <div class="l-prof-set__change-pass">
        <div class="l-prof-set__main-set">
            <form action="{{route('profile_main_update')}}"
                  id="password-update"
                  method="POST">
                {{method_field('PUT')}}
                {{csrf_field()}}
                <h5>Изменить пароль</h5>
                <div class="l-bid-write__form">
                    <div class="l-prof-set__form-line clearfix">
                        <div class="data-type">Пароль</div>
                        <div class="form-line__input">
                            <input data-rule-required="true" data-msg-required="Пароль - обязательное поле"
                                   name="password" type="password"
                                   id="password"
                                   class="form-control"/>
                        </div>
                    </div>
                    <div class="l-prof-set__form-line clearfix">
                        <div class="data-type">Подтвердить</div>
                        <div class="form-line__input">
                            <input data-rule-required="true"
                                   data-msg-required="Подтверждение пароля - обязательное поле"
                                   name="password_confirmation"
                                   id="password_confirmation"
                                   type="password"
                                   class="form-control"/>
                        </div>
                    </div>
                </div>
                <button type="submit" class="but but_bondi">
                    <span>Сохранить</span>
                </button>
            </form>
        </div>
    </div>
    <form action="{{route('profile_remove')}}"
          id="remove-user-finally"
          method="POST">
        <div class="l-prof-set__delete-profile">
            <h5>Вы можете удалить свой профиль на Pomogoo</h5>
            {{method_field('DELETE')}}
            {{csrf_field()}}
            <a onclick="event.preventDefault();
                                                     document.getElementById('remove-user-finally').submit();"
               href="#"
               class="but but_bondi">
                <span>Удалить профиль</span></a>
        </div>
    </form>
</div>


<div id="user-updated" class="modal fade modal-mob-numb">
    <div class="modal-dialog">
        <div class="modal-body">
            <button type="button" data-dismiss="modal" class="but-modal-close"></button>
            <div class="modal-reg_success__txt">
                <span id="user-updated-message" style="color: black; margin-top: 20px;"></span>
            </div>
        </div>
    </div>
</div>