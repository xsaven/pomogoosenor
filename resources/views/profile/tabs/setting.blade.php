<div id="settings" class="tab-pane fade {{request()->tab=='settings' ? 'in active' : ""}}">
    <div class="l-profile-settings">
        <div class="l-prof-set__wrp">
            <div class="l-prof-set__tabs">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#main-set" data-toggle="tab">Общие настройки</a></li>
                    <li><a href="#notice" data-toggle="tab">Уведомления</a></li>
                    @if($user->profiles->is_worker)
                        <li><a href="#subscribe" data-toggle="tab">Подписка на задания</a>
                        </li>
                    @endif
                </ul>
                <div class="tab-content">
                    @include('profile.tabs.setting_tabs.main_set')
                    @include('profile.tabs.setting_tabs.notice')
                    @if($user->profiles->is_worker)
                        @include('profile.tabs.setting_tabs.subscribe')
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>