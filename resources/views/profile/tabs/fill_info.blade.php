<div id="about-me" class="tab-pane fade in active">
    <form id="worker-create" class="l-prof-conf" action="{{route('worker_create')}}" method="POST">
        {{csrf_field()}}
        <div class="l-prof-conf__item">
            <div class="l-prof-conf__head">
                <h5>1. Отметьте категории, которые вам интересны</h5>
                <p>Выберите категории заданий, которые вам будет интересно выполнять.</p>
                <p>А также вы будете получать рассылку только по выбранным категориям..</p>
            </div>
            <div class="l-prof-conf__drops">
                @php($expanded = 'true')
                @foreach($cat_tree as $parent)
                    <div role="tab" class="panel-heading">
                        <a role="button"
                           data-toggle="collapse"
                           data-parent="#accordion"
                           href="#TAB_{{$parent->id}}"
                           aria-expanded="{{ $expanded }}"
                           aria-controls="TAB_{{$parent->id}}"
                           class="steps-content__collapse">{{$parent->name}}</a>
                    </div>
                    <div id="TAB_{{$parent->id}}" role="tabpanel"
                         class="panel-collapse collapse @if($expanded == 'true') in @endif">
                        @foreach($parent->subcategories as $child)
                            <div class="panel-body">
                                <label class="check-label">
                                    <input @if(is_array(old('selected_cat')) && in_array($child->id,old('selected_cat')) || ($user->categories->where('id',$child->id)->first())) checked
                                           @endif name="selected_cat[]" value="{{ $child->id }}" type="checkbox"/>
                                    <i class="checkbox"></i>
                                    <span>{{ $child->name }}</span>
                                </label>
                            </div>
                        @endforeach
                    </div>
                    @php($expanded = 'false')
                @endforeach
            </div>
        </div>
        <div class="l-prof-conf__item">
            <div class="l-prof-conf__head">
                <h5>2. Напишите о себе </h5>
                <div class="l-prof-ab-me__head-mdl">
                                    <textarea name="about_me"
                                              placeholder="Напишите о себе, укажите образование, профессиональное образование, род деятельности, перечислите виды услуг…"
                                              class="form-control">{!! $user->profiles->workerProfiles->about_me or old('about_me') !!}</textarea>
                </div>
            </div>
        </div>
        <div class="l-prof-conf__item">
            <div class="l-prof-conf__head">
                <h5>3. Загрузите фотографию</h5><span>Важно: изображение должно быть четким, лицо должно занимать 2/3 фотографии.</span>
            </div>
            <div class="l-prof-conf__upload">

                <div class="l-prof-conf__upload-imgs clearfix">
                    <div class="l-prof-conf__big-ava h-object-fit"><img class="avatar_preview"
                                                                        src="{{$user->profiles->avatar_full}}"

                                                                        alt=""/></div>
                    <div class="l-prof-conf__sm-ava h-object-fit"><img class="avatar_preview"
                                                                       src="{{$user->profiles->avatar_thumb}}"
                                                                       alt=""/></div>
                </div>
                <div class="l-prof-def__ava">

                </div>
                <p>Минимальный размер — 180×180 px..</p>
                <div class="steps-content__input-file">
                    <label class="type_file">
                        <input name="avatar" id="add_photo" type="file">
                        <span>Изменить фото
                                <svg width="1em" height="1em" class="icon icon-photoap ">
                                  <use xlink:href="{{ symbolSvg('icon-photoap') }}"></use>
                                </svg>
                            </span>
                    </label>
                </div>
            </div>
        </div>
        <div class="l-prof-conf__item">
            <div class="l-prof-conf__head">
                <h5>4. Укажите услуги и цены </h5><span>Чтобы заказчикам было проще ориентироваться в стоимости ваших услуг при создании задания,</span><span>укажите среднюю цену за час работы и подробно распишите стоимость услуг.</span>
            </div>
            <div class="l-prof-conf__min">
                <div class="l-prof-conf__min-input">
                    <p>Минимальная цена за час работы:</p>
                    <input name="min_price" value="{{$user->profiles->workerProfiles->min_price or old('min_price')}}"
                           type="number"
                           class="form-control"/><span>руб.</span>
                </div>
                <div class="l-prof-conf__min-textarea">
                                    <textarea name="about_price"
                                              placeholder="Пример 1: 800 руб. за 1 час. Пример 2: от 600 руб. Пример 3 — от 600 руб. за кв. м."
                                              class="form-control"> {{$user->profiles->workerProfiles->about_price or old('about_price')}}</textarea>
                    <button type="submit" class="but but_bondi"><span>Сохранить</span></button>
                </div>
                <div class="l-prof-conf__min-annotation"><b>*</b><span>Звездочкой отмечены поля, обязательные для заполнения..</span>
                </div>
            </div>
        </div>
        <div class="l-prof-ab-me__vals">
            <h5>У вас нет оценок в настоящий момент</h5>
            <p>Заказчики оценивают всех исполнителей по разнообразным критериям таким как цена,
                качество, вежливость</p>
        </div>
        <div class="l-prof-ab-me__revs">
            <h5>Отзывов пока нет</h5>
            <svg width="1em" height="1em" class="icon icon-revs ">
                <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-revs')}}"></use>
            </svg>
            <p>Отзывы появятся после того, как вы создадите или выполните задание</p>
        </div>
    </form>
</div>
