<?php /** @var \App\User $user */ ?>
<div id="about-me" class="tab-pane fade {{empty(request()->tab) ? 'in active' : ""}}">
    <div class="l-profile-about-me">
        @if($user->is_worker())
            @if($user->is_busy)
                <div class="l-prof-ab-me__busy">
                    <h5>Если вы не заняты или не выполняете заказ </h5>
                    <p>
                        Количество принятих заявок влияет на ваш рейтинг и место в каталоге.
                        Поэтому, если вы не заняты нажмите кнопку «Я свободен» и ваш статус изменится на «Я свободен».
                    </p>
                    <a href="{{route('change_busy_state')}}?is_busy=0" class="but but_y-blue">
                        <svg width="1em" height="1em" class="icon" style="transform: scaleY(-1);">
                            <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-play"></use>
                        </svg>
                        <span>Я свободен</span>
                    </a>
                </div>
            @else
                <div class="l-prof-ab-me__busy">
                    <h5>Если вы заняты или уже выполняете заказ </h5>
                    <p>Количество принятих заявок влияет на ваш рейтинг и место в каталоге.
                        Поэтому, если вы заняты или уже выполняете заказ, нажмите кнопку «Я занят».
                        Вы не ищезнете из каталога, но ваш статус изменится на «Я занят».
                    </p>
                    <a href="{{route('change_busy_state')}}?is_busy=1" class="but but_y-orange">
                        <svg width="1em" height="1em" class="icon icon-pause ">
                            <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-pause"></use>
                        </svg>
                        <span>Я занят</span>
                    </a>
                </div>
            @endif
        @endif
        {{ Form::open(['method' => 'POST', 'route' => ['update_about_me'],'class'=>'l-prof-ab-me__head updateAboutMeForm']) }}

        <div class="l-prof-ab-me__head-top">
            <h5>Немного о себе</h5>
            @if($about_me = $user->profile->workerProfile->about_me)
                <a href="#">
                    <svg width="1em" height="1em" class="icon icon-pensil ">
                        <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-pensil')}}"></use>
                    </svg>
                    <span>Редактировать</span>
                </a>
                <div class="about-me-text">{!! $user->profile->workerProfile->about_me_html !!}</div>
            @else
                <a href="#">
                    <svg width="1em" height="1em" class="icon icon-pensil ">
                        <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-pensil')}}"></use>
                    </svg>
                    <span>Добавить</span>
                </a>
                <div class="about-me-text"></div>
            @endif
        </div>
        <div class="l-prof-ab-me__head-mdl">
                <textarea
                        placeholder="Напишите о себе, укажите образование, профессиональное образование, род деятельности, перечислите виды услуг…"
                        class="form-control about-me-textarea">{{($about_me = $user->profile->workerProfile->about_me)?$about_me:''}}</textarea>
        </div>
        <div class="l-prof-ab-me__head-bot">
            <button type="submit" class="but but_bondi">
                <span>Сохранить</span>
            </button>
        </div>

        {{Form::close()}}
        <div class="l-prof-ab-me__add-video">
            @if($user->profile->workerProfile->video)
                <video class="video_preview" src="{{$user->profile->workerProfile->video}}" controls></video>
            @else
                <div class="l-prof-ab-me__add-video-head">
                    <h5>Добавьте видеоролик о себе</h5>
                </div>
                <video class="video_preview" style="display: none" controls></video>
            @endif

            <div class="l-prof-ab-me__add-video-bd">Профили с видео получают больше
                внимания и вызывают больше доверия у авторов заданий. Вероятность, что
                выберут вас, увеличится на 25%.
            </div>
            <div class="l-prof-ab-me__add-video-foot">
                <label>
                    <input accept="video/mp4,video/x-m4v,video/*"
                           class="add_video" type="file" name="video"
                           data-upload-url="{{route('profile.upload_video')}}"/><span
                            class="link_bondi">Выберите файл</span>
                </label>
                <div class="ab-formats">mov, avi, flv, wmv, mpg и mp4. До 2Мб</div>
                <div class="error"></div>
            </div>
        </div>

        <div class="l-prof-ab-me__examps">
            <h5>Примеры работ</h5>
            <div class="row">
                @foreach($galleries as $gallery)
                    <div class="col-md-6 col-sm-6">
                        <a href="{{route('galleries.show',compact('gallery'))}}" class="l-profile-pub__examps-item">
                            <div class="examps-item__img h-object-fit">
                                <img src="{{asset($gallery->gallery_avatar)}}" alt=""></div>
                            <div class="examps-item__txt clearfix">
                                <div class="examps-item__txt-name">{{$gallery->title}}</div>
                                <div class="examps-item__txt-count">
                                    <svg width="1em" height="1em" class="icon icon-camera ">
                                        <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-camera')}}"></use>
                                    </svg>
                                    <span>{{$gallery->content_items_count}}</span>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>

            <p>Если у вас есть примеры выполненной вами работы, обязательно прикрепите
                их, это покажет вас в лучшем свете в глазах автора задания. А также вы
                будете вызывать больше доверия как исполнитель.</p>
            <a href="{{route('galleries.create')}}" class="but but_bondi">
                <svg width="1em" height="1em" class="icon icon-camera ">
                    <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-camera')}}"></use>
                </svg>
                <span>Создать фотоальбом</span>
            </a>
        </div>
        @if($user->profile->is_worker)
            {{ Form::open(['method' => 'POST', 'route' => ['update_min_price'],'class'=>'l-prof-ab-me__head services updateMinPrice']) }}
            <div class="l-prof-ab-me__head-top">
                <h5>Услуги и цены</h5><a href="#">
                    <svg width="1em" height="1em" class="icon icon-pensil ">
                        <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-pensil"></use>
                    </svg>
                    <span>Редактировать</span></a>
                <div class="l-profile-score__suppl l-bid-write__form clearfix">
                    <label><span>Минимальная цена за час работы</span>
                        <input id="about-me-min-price" type="text"
                               value="{{$user->profile->workerProfile->min_price}}" class="form-control"/><span
                                class="rub">₽</span>
                    </label>
                </div>
                <div class="min-price-not-active">{{$user->profile->workerProfile->about_price}}</div>
            </div>

            <div class="l-prof-ab-me__head-mdl services">
                    <textarea id="about-me-about-price"
                              placeholder="Стоимость услуг подбираются индивидуально, в зависимости от сложности и объема работ."
                              class="form-control">{{$user->profile->workerProfile->about_price}}</textarea>
            </div>
            <div class="l-prof-ab-me__head-bot">
                <button type="submit" class="but but_bondi"><span>Сохранить</span></button>
            </div>
            {{Form::close()}}
        @endif
        @include('profile.public._partials._marks')
        @include('profile.public._partials.reviews')
        @if(!$user->is_worker())
            <div class="l-prof-ab-me__cond">
                <p>Выполнять заказы на Pomogoo могут только совершеннолетние пользователи,
                    прошедшие процедуру подтверждения профиля.</p>
                <a href="{{route('testing.start')}}" class="but but_bondi">
                    <span>Я хочу стать исполнителем</span>
                </a>
            </div>
        @endif
    </div>
</div>