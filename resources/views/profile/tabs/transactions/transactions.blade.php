@if(isset($all_transactions))
    <ul class="nav nav-tabs">
        <li class="active"><a href="#all-operations" data-toggle="tab">Все
                операции</a></li>
        <li><a href="#funding" data-toggle="tab">Пополнения счета</a>
        </li>
        <li><a href="#dismiss" data-toggle="tab">Списание со счета</a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="all-operations" class="tab-pane fade in active">

            @forelse($all_transactions as $transaction)
                <p>{{$transaction->description}} | {{$transaction->amount}}</p>
            @empty
                <p>За данный период транзакций не было</p>
            @endforelse
        </div>
        <div id="funding" class="tab-pane fade">
            @forelse($received_transactions as $transaction)
                <p>{{$transaction->description}} | {{$transaction->amount}}</p>
            @empty
                <p>За данный период транзакций не было</p>
            @endforelse
        </div>
        <div id="dismiss" class="tab-pane fade">
            @forelse($sent_transactions as $transaction)
                <p>{{$transaction->description}} | {{$transaction->amount}}</p>
            @empty
                <p>За данный период транзакций не было</p>
            @endforelse
        </div>
    </div>
@endif