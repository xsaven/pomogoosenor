<?php
/** @var \App\User $favorite_user */
?>
<div class="l-cat-serv__item">
    <div class="l-cat-serv__item-left">
        <div class="h-object-fit">
            <a href="{{route('profile_show',['user'=>$favorite_user])}}">
                <img src="{{$favorite_user->profile->avatar_thumb}}" alt="Аватар пользователя {{$favorite_user->name}}"/>
            </a>
        </div>
        <div class="l-cat-serv__item-left-revs"><span
                    class="revs-name">Отзывы</span>
            @if($favorite_user->positive_reviews_count)
                <span class="revs-count">
                                <svg width="1em" height="1em" class="icon icon-like">
                                  <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-like-2')}}"></use>
                                </svg>
                <span>{{$favorite_user->positive_reviews_count}}</span>
            </span>
            @endif
            @if($favorite_user->negative_reviews_count)
                <span class="revs-count">
                                <svg width="1em" height="1em" class="icon icon-dislike">
                                  <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-like-2')}}"></use>
                                </svg>
                <span>{{$favorite_user->negative_reviews_count}}</span>
            </span>
            @endif
        </div>
        <div class="l-cat-serv__item-left-stars">
            @for($i = 1; $i <= 5;$i++)

                @if($favorite_user->avg_mark['total'] >= $i)
                    <svg width="1em" height="1em" class="icon icon-star">
                        <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star')}}"></use>
                    </svg>
                @else
                    <svg width="1em" height="1em" class="icon icon-star2">
                        <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star2')}}"></use>
                    </svg>
                @endif
            @endfor
        </div>
    </div>
    <div class="l-cat-serv__item-right">
        <div class="l-cat-serv__item-right-head clearfix">
            <div class="l-cat-serv__it-rght-hd-left">
                <div class="l-cat-serv__item-name">
                    <a href="{{route('profile_show',['user'=>$favorite_user])}}">
                        {{$favorite_user->name}}
                    </a>
                </div>
            </div>
            <div class="l-cat-serv__it-rght-hd-right"></div>
        </div>
        <div class="l-cat-serv__it-rght-hd-status not-online">{{ trans_choice('profile.tasks.was', $user->profile->getOriginal('sex'))}}  на сайте {{$favorite_user->last_activity_diff}}
        </div>
        <div class="l-cat-serv__item-right-body">
            <p>
                @if($favorite_user->profile->workerProfile->about_me)
                    {!! $favorite_user->profile->workerProfile->about_me !!}
                @else

                    Анкета пока не заполнена. Скоро исполнитель расскажет о себе!
                @endif
            </p>
        </div>
        <div class="l-cat-serv__item-right-footer"><a href="#"
                                                      class="but but_bondi"><span>Предложить задание</span></a>
        </div>
    </div>
</div>