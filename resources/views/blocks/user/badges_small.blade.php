<?php /** @var \App\User $executor */ ?>
<div class="l-cat-serv__item-icons">
    @if($executor->getStickerProjectsCount())
        <div class="l-cat-serv__item-icon">
            <i class="ico ico-type-one"></i>
            <div class="popup"><span>За выполненные задания</span></div>
        </div>
    @endif
    @if($executor->getPositionByPositiveReviews())
        <div class="l-cat-serv__item-icon"><i class="ico ico-type-two"></i>
            <div class="popup"><span>ТОП исполнителей Pomogoo</span></div>
        </div>
    @endif
    @if($executor->getPercentTasks())
        <div class="l-cat-serv__item-icon"><i class="ico ico-type-three"></i>
            <div class="popup"><span>Процент успешно выполненных заданий</span></div>
        </div>
    @endif
    @if($executor->cleanerStatus())
        <div class="l-cat-serv__item-icon"><i class="ico ico-type-four"></i>
            <div class="popup"><span>Мастер уборки</span></div>
        </div>
    @endif

</div>