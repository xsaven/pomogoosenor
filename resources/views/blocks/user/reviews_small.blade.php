<?php /** @var \App\User $user */?>
@if($executor->positive_reviews_count || $executor->negative_reviews_count)
    <div class="l-cat-serv__item-left-revs">
        <span class="revs-name">Отзывы</span>
        @if($executor->positive_reviews_count)
            <span class="revs-count">
                            <svg width="1em" height="1em" class="icon icon-like">
                                <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-like-2')}}"></use>
                            </svg>
                            <span>{{$executor->positive_reviews_count}}</span>
                        </span>
        @endif
        @if($executor->negative_reviews_count)
            <span class="revs-count">
                            <svg width="1em" height="1em" class="icon icon-dislike">
                                  <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-like-2')}}"></use>
                            </svg>
                            <span>{{$executor->negative_reviews_count}}</span>
                         </span>
        @endif
    </div>
    <div class="l-cat-serv__item-left-stars">
        @for($i = 1; $i <= 5;$i++)
            @if($executor->avg_mark['total'] >= $i)
                <svg width="1em" height="1em" class="icon icon-star">
                    <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star')}}"></use>
                </svg>
            @else
                <svg width="1em" height="1em" class="icon icon-star2">
                    <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star2')}}"></use>
                </svg>
            @endif
        @endfor
    </div>
@endif
