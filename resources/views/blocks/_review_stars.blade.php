<?php /** @var $review \App\Review */ ?>
<div class="txt-bottom">
    @foreach($review->mark_fields as $mark_field)
        @if($review->getAttribute($mark_field) != 0)
            <div class="txt-bottom__item">
                <span>{{ trans('reviews.rate.'.$mark_field) }}</span>
                <div class="txt-bottom__item-stars">
                    @for($i = 1; $i <= 5;$i++)
                        @if(round($review->getAttribute($mark_field)) >= $i)
                            <svg width="1em" height="1em" class="icon icon-star">
                                <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star')}}"></use>
                            </svg>
                        @else
                            <svg width="1em" height="1em" class="icon icon-star2">
                                <use xlink:href="{{asset('s/images/useful/svg/theme/symbol-defs.svg#icon-star2')}}"></use>
                            </svg>
                        @endif
                    @endfor
                </div>
            </div>
        @endif
    @endforeach
</div>