<?php /** @var \App\User $user */ ?>
@extends('layouts.main')
@section('title') Личная информация @stop
@section('content')

    <div class="steps">
        <div class="container">
            <div class="steps__wrapper">
                <div class="step active"><span>1. Личная информация</span></div>
                <div class="step"><span>2. Онлайн-тестирование</span></div>
                <div class="step"><span>3. Подтверждение данних</span></div>
            </div>
        </div>
    </div>
    <form action="{{url('testing/fill_info')}}" method="POST" enctype="multipart/form-data" id="testing_form_info"
          class="steps-content">
        {{ csrf_field() }}
        <div class="container">
            <div class="steps-content__header">
                <h2>Правила</h2>
            </div>
            <div class="steps-content__content">
                <p>Выполнять задания на Pomogoo могут только пользователи, которые прошли процедуру верификации. Данная
                    процедура бесплатная, занимает 10–15 минут и состоит из 3 шагов: заполнение личных данных,
                    прохождение теста на <a href="#" class="link_bondi">знание механики работы сервиса,</a><br>а также
                    оформления профиля.</p>
                <p>В настоящее время исполнителями Pomogoo могут стать только совершеннолетние пользователи. Мы оставляем
                    за собой право отказать в присвоении статуса исполнителя без объяснения причин.</p>
            </div>
            <div class="steps-content__header">
                <h3>1. Личные данные</h3>
            </div>
            <div class="steps-content__form">
                <div class="row">
                    <div class="col-lg-5 col-md-6">
                        <div class="steps-content__form-input"><span>Имя</span>
                            <label>
                                <input value="{{old('name')??$user->profile->name }}" name="name" type="text"
                                       placeholder="Виктория" class="form-control">
                            </label>
                        </div>
                        <div class="steps-content__form-input"><span>Фамилия</span>
                            <label>
                                <input value="{{old('surname')??$user->profiles->surname}}" name="surname" type="text"
                                       placeholder="Хриновськая" class="form-control">
                            </label>
                        </div>
                        <div class="steps-content__form-input"><span>Город</span>
                            <label class="selectize_label">
                                @php($city = $user->profiles->cities)
                                <input name="city" type="text" placeholder="Город"
                                       class="form-control form-data" value="{{old('city')??$city->name}}">
                            </label>
                        </div>
                        <div class="steps-content__form-input__wrap">
                            <div class="steps-content__form-input"><span>День Рождения</span>
                                <div class="steps-content__form-input__wrap_new">
                                    <select name="day" title="День" class="selectpicker">
                                        @for($i = 1; $i <= 31; $i++)
                                            <option @if(old('day') == $i) selected @endif value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                    <select name="month" title="Месяц" class="selectpicker">
                                        @for($i = 1; $i <= 12; $i++)
                                            <option @if(old('month') == $i) selected
                                                    @endif value="{{$i}}">{{ ucfirst(\Jenssegers\Date\Date::createFromDate(1995,$i,1)->format('F')) }}</option>
                                        @endfor
                                    </select>
                                    <select name="year" title="Год" class="selectpicker">
                                        @php($max_year = \Carbon\Carbon::now()->format('Y'))))
                                        @for($i = 1970; $i <= ($max_year - 18); $i++)
                                            <option @if(old('year') == $i) selected
                                                    @endif value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-6 visible-lg visible-md">
                        <div class="steps-content__form-info">
                            <p>Самый простой способ лишиться статуса исполнителя на Pomogoo — это указать недостоверную
                                информацию.</p>
                            <p>После верификации мы просим случайное число исполнителей предоставить паспорт или иной
                                документ для подтверждения информации.</p>
                        </div>
                    </div>
                </div>
                <div class="steps-content__form-input steps-content__form-input__noheight"><span>Фотография</span>
                    <div class="steps-content__input-file">
                        <div class="steps-content__form-input__img h-object-fit">
                            <img class="avatar_preview" src="{{ $user->profiles->avatar_full or '' }}" alt>
                        </div>
                        <label class="type_file">
                            <input id="add_photo" type="file">
                            <span>Изменить фото
                                <svg width="1em" height="1em" class="icon icon-photoap ">
                                  <use xlink:href="{{ symbolSvg('icon-photoap') }}"></use>
                                </svg>
                            </span>
                        </label>
                    </div>
                    <div class="steps-content__par">
                        <p>Загрузите вашу фотографию (хорошее качество, лицо крупным планом). Профили без фото
                            отклоняются во время проверки данных.</p>
                        <p>Минимальный размер: 180×180 px</p>
                    </div>
                </div>
            </div>
            <div class="steps-content__header">
                <h3>2. Подтверждение телефона</h3>
            </div>
            <div class="steps-content__content">
                <p>На указанный номер мы отправим СМС с решением о присвоении вам статуса исполнителя Pomogoo. </p>
                <p>
                    Телефон <span>{{ $user->profiles->phone_code }}{{ $user->profiles->phone }}</span>
                    @if(!$user->profiles->phone_verified)
                        <span>
                            <button class="but but_bondi but_xs" type="button" data-toggle="modal"
                                    data-target="#modal-mob-numb">
                                Подтвердить
                            </button>
                        </span>
                    @endif
                </p>
                <input type="hidden" name="phone_code" value="{{ $user->profiles->phone_code }}">
                <input type="hidden" name="phone_number" value="{{ $user->profiles->phone }}">
                <input type="hidden" name="phone_verified" value="{{ $user->profiles->phone_verified }}">
            </div>
            <div class="steps-content__header">
                <h3>3. Укажите ваш аккаунт хотя бы в одной соцсети</h3>
            </div>
            <div class="steps-content__socials">
                <span @if($user->socialAccounts->where('social_type','vkontakte')->first()) class="conected" @endif>
                    <a href="{{route('social.attach',['provider'=>'vkontakte'])}}" target="_blank" class="social__vk">
                        <svg width="1em" height="1em" class="icon icon-conf-vk ">
                          <use xlink:href="{{ symbolSvg('icon-conf-vk') }}"></use>
                        </svg>
                    </a></span>
                <span
                        @if($user->socialAccounts->where('social_type','facebook')->first()) class="conected" @endif><a
                            href="{{route('social.attach',['provider'=>'facebook'])}}" target="_blank"
                            class="social__fb">
                <svg width="1em" height="1em" class="icon icon-conf-fb ">
                  <use xlink:href="{{ symbolSvg('icon-conf-fb') }}"></use>
                </svg></a></span><span
                        @if($user->socialAccounts->where('social_type','mailru')->first()) class="conected" @endif><a
                            href="{{route('social.attach',['provider'=>'mailru'])}}" target="_blank"
                            class="social__ml">
                <svg width="1em" height="1em" class="icon icon-conf-ml ">
                  <use xlink:href="{{ symbolSvg('icon-conf-ml') }}"></use>
                </svg></a></span><span
                        @if($user->socialAccounts->where('social_type','google')->first()) class="conected" @endif><a
                            href="{{route('social.attach',['provider'=>'google'])}}" target="_blank"
                            class="social__google">
                <svg width="1em" height="1em" class="icon icon-conf-google ">
                  <use xlink:href="{{ symbolSvg('icon-conf-google') }}"></use>
                </svg></a></span>
            </div>
            <div class="steps-content__header">
                <h3>4. Доверенное лицо</h3>
            </div>
            <div class="steps-content__content">
                <p>Укажите человека (родственника, коллегу, друга), который может подтвердить информацию о вас.<br>В
                    верификации будет отказано, если мы не сможем связаться с вашим доверенным лицом, или он не
                    подтвердит указанные вами данные.</p>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="steps-content__form-input_row"><span>Имя</span>
                        <label>
                            <input name="confidant_name" type="text" placeholder="Имя" class="form-control"
                                   value="{{old('confidant_name')}}">
                        </label>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="steps-content__form-input_row"><span>Фамилия</span>
                        <label>
                            <input name="confidant_surname" type="text" placeholder="Фамилия" class="form-control"
                                   value="{{old('confidant_surname')}}">
                        </label>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="steps-content__form-input_row"><span>Кем вам приходится</span>
                        <label>
                            <input name="confidant_status" type="text" placeholder="Кем вам приходится"
                                   class="form-control" value="{{old('confidant_status')}}">
                        </label>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="steps-content__form-input_row steps-content__form-input_row-select"><span>Телефон</span>
                        <div class="steps-content__form-input_row-wrapper">
                            <select name="confidant_phone_prefix" class="selectpicker">
                                <option value="+7">+7</option>
                                {{--<option value="+38">+38</option>--}}
                            </select>
                            <label class="tel-wrap mask">
                                <input name="confidant_phone" type="text" placeholder="Телефон"
                                       class="tel-input form-control" value="{{old('confidant_phone')}}">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="steps-content__header">
                <h3>5. Отметьте категории, которые вам интересны</h3>
            </div>
            <div class="steps-content__content">
                <p>Пожалуйста, укажите не более трех категорий, в которых вы хотели бы выполнять задания на Pomogoo.<br>После
                    прохождения верификации вы сможете выбрать больше категорий.</p>
            </div>
            @php($expanded = 'true')
            @foreach($cat_tree as $parent)
                <div role="tab" class="panel-heading">
                    <a
                            role="button"
                            data-toggle="collapse"
                            data-parent="#accordion"
                            href="#TAB_{{$parent->id}}"
                            aria-expanded="{{ $expanded }}"
                            aria-controls="TAB_{{$parent->id}}"
                            class="steps-content__collapse"
                    >{{$parent->name}}</a>
                </div>
                <div id="TAB_{{$parent->id}}" role="tabpanel"
                     class="panel-collapse collapse @if($expanded == 'true') in @endif">
                    @foreach($parent->subcategories as $child)
                        <div class="panel-body">
                            <label class="check-label">
                                <input @if(is_array(old('selected_cat')) && in_array($child->id,old('selected_cat'))) checked
                                       @endif name="selected_cat[]" value="{{ $child->id }}" type="checkbox"/>
                                <i class="checkbox"></i>
                                <span>{{ $child->name }}</span>
                            </label>
                        </div>
                    @endforeach
                </div>
                @php($expanded = 'false')
            @endforeach

            <div class="step_access__wrap">
                <div class="step_access">
                    <label class="check-label">
                        <input @if(old('confirm_1')) checked @endif value="true" name="confirm_1" type="checkbox"><i
                                class="checkbox"></i><span>Подтверждаю, что я прохожу верификацию на Pomogoo впервые</span>
                    </label>
                </div>
                <div class="step_access">
                    <label class="check-label">
                        <input @if(old('confirm_2')) checked @endif value="true" name="confirm_2" type="checkbox"><i
                                class="checkbox"></i><span>Подтверждаю, что я указал свои личные данные достоверно</span>
                    </label>
                </div>
                <div class="step_access step_access__link">
                    <label class="check-label">
                        <input @if(old('confirm_3')) checked @endif value="true" name="confirm_3" type="checkbox"><i
                                class="checkbox"></i>
                    </label>
                    <span>Я согласен с <a href="{{url('rules')}}"
                                          class="link_bondi">соглашением на обработку персональных данных				</a></span>
                </div>
            </div>
            <div class="step__button-wrap">
                <button type="submit" class="but but_bondi">Сохранить и продолжить</button>
                <span>Онлайн-тестирование</span>
            </div>
        </div>
    </form>
    @include('modals.fill_info')
@stop


@section('script')
    <script>
        window.onload = function () {
            var xhr;
            new autoComplete({
                selector: 'input[name="city"]',
                source: function (term, response) {
                    try {
                        xhr.abort();
                    } catch (e) {
                    }
                    xhr = $.getJSON('{{route('find_city')}}', {query: term}, function (data) {
                        response(data);
                    });
                },
                renderItem: function (item, search) {
                    search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                    var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
                    return '<div class="autocomplete-suggestion" value="' + item.id + '" data-val="' + item.name + '">' + item.name + '</div>';
                }
            });
        };

    </script>
@endsection