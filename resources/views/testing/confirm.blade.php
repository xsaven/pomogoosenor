@extends('layouts.main')
@section('title') Подтверждение данних @stop
@section('content')

    <div class="steps">
        <div class="container">
            <div class="steps__wrapper">
                <div class="step"><span>1. Личная информация</span></div>
                <div class="step"><span>2. Онлайн-тестирование</span></div>
                <div class="step active"><span>3. Подтверждение данних</span></div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="confirm-container">
            @if(\Auth::user()->load_profile()->is_worker == '0')
                <h3>Вы успешно завершили тестирование</h3>
                <p>После модерации Ваших даных Вы получите статус "Исполнитель"</p>
            @else
                <h3>Вы успешно прошли модерирование</h3>
                <p>Теперь Вы можете браться за работу</p>
            @endif
        </div>
    </div>
@stop