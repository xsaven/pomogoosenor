@extends('layouts.main')
@section('title') Онлайн-тестирование @stop
@section('content')

    <div class="steps">
        <div class="container">
            <div class="steps__wrapper">
                <div class="step"><span>1. Личная информация</span></div>
                <div class="step active"><span>2. Онлайн-тестирование</span></div>
                <div class="step"><span>3. Подтверждение данних</span></div>
            </div>
        </div>
    </div>

    <form class="steps-content">
        <div class="container">
            <div class="steps-content__content">
                <p>Вам предстоит пройти тест на знание основных правил работы исполнителей проекта Pomogoo.</p>
                <p>Пожалуйста, убедитесь, что устройство, с которого вы собираетесь пройти тест, исправно, а
                    интернет-соединение надежно.<br>Любые технические неполадки на вашей стороне могут помешать вам
                    пройти тест. Мы не рекомендуем проходить тест с мобильных устройств.</p>
            </div>
            <div class="steps-content__header">
                <h2>Пожалуйста, внимательно ознакомьтесь с инструкцией:</h2>
            </div>
            <div class="steps-content__infolist">
                <ul class="h-list">
                    <li>Не переживайте! На выполнение теста вам отводится неограниченное количество времени и попыток.
                        Если вы не <br>прошли тест с первого раза, то в конце вы увидите вопросы, в которых допустили
                        ошибки, и сможете их <br>исправить.
                    </li>
                    <li>Каждый вопрос теста предполагает выбор одного или нескольких вариантов ответа.</li>
                    <li>В каждом вопросе есть подсказки. Они представляют собой ссылки на основные правила работы
                        сервиса. Перед <br>началом тестирования мы настоятельно рекомендуем вам ознакомиться с ними:
                    </li>
                </ul>
                @foreach($test_links as $test_link)
                    <span><a href="{{$test_link->link}}" target="_blank" class="link_bondi step_link">{{$test_link->text}}</a></span>
                @endforeach
            </div>
            <div class="steps-content__form-info steps-content__form-info-minpad">
                <h4>Уверены что хотите пройти тест сейчас?</h4>
                <p>Вы можете приступить к тесту позже. Первый шаг повторно заполнять не понадобится.</p><a
                        href="{{url('testing/start_steps')}}" class="but but_bondi">Начать тест</a>
            </div>
        </div>
    </form>
@stop