@extends('layouts.main')
@section('title') Онлайн-тестирование @endsection
@section('content')

    @php($old_unswers = false)
    @if(Session::has('formatted_unswers'))
        @php($old_unswers = Session::get('formatted_unswers'))
    @endif

    @php($errors = false)
    @if(Session::has('testing_errors'))
        @php($errors = Session::get('testing_errors'))
    @endif

    <div class="steps">
        <div class="container">
            <div class="steps__wrapper">
                <div class="step"><span>1. Личная информация</span></div>
                <div class="step active"><span>2. Онлайн-тестирование</span></div>
                <div class="step"><span>3. Подтверждение данних</span></div>
            </div>
        </div>
    </div>
    <form method="POST" action="{{url('testing/submit_step')}}" class="steps-content">
        {{ csrf_field() }}
        <div class="container">
            <div class="breadcrumbs">
                <ul class="h-list">
                    @foreach($patterns as $index => $pattern)
                        @if($index == $active_pattern)
                            <li><span>{{$pattern->name}}</span></li>
                        @else
                            <li><a href="javascript:void(0);">{{$pattern->name}}</a></li>
                        @endif
                    @endforeach
                </ul>
            </div>
            <div class="check_list ">
                @foreach($patterns[$active_pattern]->questions as $counter => $task)
                    <div class="check_list-par">
                        <p>{{($counter + 1)}}. {{$task->title}}</p>
                        @foreach($task->answers as $unswer_index => $unswer)
                            <div class="check_list-check">
                                @php($wrong_answer = (!$errors && $old_unswers) || ($old_unswers && in_array($unswer->id,$errors)))
                                <label class="check-label @if($wrong_answer) allert @endif">
                                    <input @if(isset($old_unswers[$task->id]) && $old_unswers && in_array($unswer->id,$old_unswers[$task->id])) checked @endif name="questions[{{$task->id}}][]" type="checkbox" value="{{$unswer->id}}"/>
                                    <i class="checkbox"></i>
                                </label>
                                <span @if($wrong_answer) class="allert" @endif>
                                    {{ $unswer->title }}
                                    @if($wrong_answer)
                                        <a data-right='{{json_encode($task->answers)}}' href="#" class="link_bondi">посмотреть правилные ответы</a>
                                    @endif
                                </span>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
            <div class="step__button-wrap">
                <button type="submit" class="but but_bondi">Дальше</button>
                <span>Продолжения и комментарии</span>
            </div>
        </div>
    </form>
@endsection