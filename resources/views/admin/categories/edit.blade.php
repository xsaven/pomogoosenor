@extends('admin.layouts.master')

@section('content')

    <div class="row">
        <div class="col-sm-10 col-sm-offset-2">
            <h1>{{ trans('quickadmin::templates.templates-view_edit-edit') }}</h1>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! Form::model($category, array('files' => true,'class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'PATCH', 'route' => array(config('quickadmin.route').'.categories.update', $category->id))) !!}

    <div class="form-group">
        {!! Form::label('name', 'Название*', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('name', old('name',$category->name), array('class'=>'form-control')) !!}

        </div>
    </div>
    <div class="form-group">
        {!! Form::label('slug', 'Слаг*', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('slug', old('slug',$category->slug), array('class'=>'form-control')) !!}

        </div>
    </div>
    <div class="form-group">
        {!! Form::label('order', 'Порядок', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('order', old('order',$category->order), array('class'=>'form-control')) !!}

        </div>
    </div>
    <div class="form-group">
        {!! Form::label('order', 'Иконка', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('icon', old('icon',$category->icon), array('class'=>'form-control')) !!}

        </div>
    </div>

    <div class="form-group">
        {!! Form::label('active', 'Отображать', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::hidden('active','') !!}
            {!! Form::checkbox('active', 1, $category->active == 1) !!}

        </div>
    </div>
    <div class="form-group">
        {!! Form::label('image', 'Изображение', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            <img style="width: 50%; height: auto; margin-bottom: 20px;" src="{{url($category->image)}}" alt="slide">

            {!! Form::file('image') !!}
            {!! Form::hidden('image_src_w', 4096) !!}
            {!! Form::hidden('image_src_h', 4096) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('icon_image', 'Изображение иконки', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            <img src="{{url($category->icon_image)}}" alt="slide" width="100" height="100">

            {!! Form::file('icon_image') !!}
            {!! Form::hidden('image_src_w', 4096) !!}
            {!! Form::hidden('image_src_h', 4096) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('marker[opened][file]', 'Маркер для открытых заданий', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            <img src="{{url($category->marker['opened']['file']??'')}}" alt="slide" width="100" height="100">

            {!! Form::file('marker[opened][file]') !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('marker[opened][slug]', 'Маркер для открытых заданий (слаг)', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('marker[opened][slug]', old('marker[opened][slug]',$category->marker['opened']['slug']??''), array('class'=>'form-control')) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('marker[in_progress][file]', 'Маркер для выполняющихся заданий', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            <img src="{{url($category->marker['in_progress']['file']??'')}}" alt="slide" width="100" height="100">

            {!! Form::file('marker[in_progress][file]') !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('marker[in_progress][slug]', 'Маркер для выполняющихся заданий (слаг)', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('marker[in_progress][slug]', old('marker[in_progress][slug]',$category->marker['in_progress']['slug']??''), array('class'=>'form-control')) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('marker[finished][file]', 'Маркер для закрытых заданий', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            <img src="{{url($category->marker['finished']['file']??'')}}" alt="slide" width="100" height="100">

            {!! Form::file('marker[finished][file]') !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('marker[finished][slug]', 'Маркер для закрытых заданий (слаг)', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('marker[finished][slug]', old('marker[finished][slug]',$category->marker['finished']['slug']??''), array('class'=>'form-control')) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            {!! Form::submit(trans('quickadmin::templates.templates-view_edit-update'), array('class' => 'btn btn-primary')) !!}
            {!! link_to_route(config('quickadmin.route').'.categories.index', trans('quickadmin::templates.templates-view_edit-cancel'), null, array('class' => 'btn btn-default')) !!}
        </div>
    </div>

    {!! Form::close() !!}

@endsection