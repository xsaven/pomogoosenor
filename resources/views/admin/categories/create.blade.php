@extends('admin.layouts.master')

@section('content')

    <div class="row">
        <div class="col-sm-10 col-sm-offset-2">
            <h1>{{ trans('quickadmin::templates.templates-view_create-add_new') }}</h1>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! Form::open(array('files' => true,'route' => config('quickadmin.route').'.categories.store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!}

    <div class="form-group">
        {!! Form::label('name', 'Название*', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('name', old('name'), array('class'=>'form-control')) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('slug', 'Псевдоним*', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('slug', old('slug'), array('class'=>'form-control')) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('order', 'Порядок', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('order', old('order'), array('class'=>'form-control')) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('order', 'Иконка', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('icon', old('icon'), array('class'=>'form-control')) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('active', 'Отображать', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::hidden('active','') !!}
            {!! Form::checkbox('active', 1, true) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('image', 'Изображение', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::file('image') !!}
            {!! Form::hidden('image_src_w', 4096) !!}
            {!! Form::hidden('image_src_h', 4096) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('icon_image', 'Изображение иконки', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::file('icon_image') !!}
            {!! Form::hidden('image_src_w', 4096) !!}
            {!! Form::hidden('image_src_h', 4096) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('marker[opened]', 'Маркер для открытых заданий', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::file('marker[opened]') !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('marker[opened][slug]', 'Маркер для открытых заданий (слаг)', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('marker[opened][slug]', old('marker[opened][slug]'), array('class'=>'form-control')) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('marker[in_progress]', 'Маркер для выполняющихся заданий', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::file('marker[in_progress]') !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('marker[in_progress][slug]', 'Маркер для выполняющихся заданий (слаг)', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('marker[in_progress][slug]', old('marker[in_progress][slug]'), array('class'=>'form-control')) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('marker[finished]', 'Маркер для закрытых заданий', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::file('marker[finished]') !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('marker[finished][slug]', 'Маркер для закрытых заданий (слаг)', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('marker[finished][slug]', old('marker[finished][slug]'), array('class'=>'form-control')) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            {!! Form::submit( trans('quickadmin::templates.templates-view_create-create') , array('class' => 'btn btn-primary')) !!}
        </div>
    </div>

    {!! Form::close() !!}

@endsection