@extends('admin.layouts.master')

@section('content')

    @include('admin.posts.UI')

<div class="row">
    <div class="col-sm-10 col-sm-offset-2">
        <h1>{{ trans('quickadmin::templates.templates-view_create-add_new') }}</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
        	</div>
        @endif
    </div>
</div>

{!! Form::open(array('route' => config('quickadmin.route').'.posts.store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!}

<div class="form-group">
    {!! Form::label('user_id', 'Автор', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::select('user_id') !!}
        
    </div>
</div>

<div class="form-group">
    {!! Form::label('related', 'Материалы по теме', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        <select multiple name="related[]" id="related"></select>
    </div>
</div>

<div class="form-group">
    {!! Form::label('title', 'Заголовок*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('title', old('title'), array('class'=>'form-control')) !!}
        
    </div>
</div>
<div class="form-group">
    {!! Form::label('subtitle', 'Подзаголовок', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('subtitle', old('subtitle'), array('class'=>'form-control')) !!}
        
    </div>
</div>

<div class="form-group">
    {!! Form::label('preview', 'Главное изображение', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        <div style="padding: 20px; border: 1px solid #ccc;  margin-bottom: 20px;" class="col-sm-12">
            <button id="pick_main" class="lfm_pick" type="button">Выбрать изображение</button>
            <input type="hidden" name="preview" class="lfm_pick_input">
        </div>
    </div>
</div>
<div class="form-group">
    {!! Form::label('titres', 'Титры', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        <textarea name="titres" id="titres"></textarea>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Контент</label>
    <div class="col-sm-10">
        <div id="the_content"></div>
        <div class="btn-group">
            <button type="button" id="add_text" class="btn btn-primary">Добавить текстовый блок</button>
            <button type="button" id="add_image" class="btn btn-primary">Добавить блок с изображением</button>
            <button type="button" id="add_video" class="btn btn-primary">Добавить блок с видео</button>
        </div>
    </div>
</div>


<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
      {!! Form::submit( trans('quickadmin::templates.templates-view_create-create') , array('class' => 'btn btn-primary')) !!}
    </div>
</div>


{!! Form::close() !!}

@endsection

@section('javascript')
    <script src="/vendor/laravel-filemanager/js/content_builder.js"></script>

    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>

    <script type="text/javascript" src="{{url('quickadmin/js/selectize.min.js')}}"></script>

    <script type="text/javascript">
        CKEDITOR.replace( 'titres' );
        $('#pick_main').filemanager('image');
        $('#user_id').selectize({
            valueField: 'id',
            labelField: 'name',
            searchField: ['name'],
            maxOptions: 10,
            render: {
                option: function (item, escape) {
                    var title = escape(item.name);
                    return '<div>' + title + '</div>';
                }
            },
            load: function(query, callback) {

                if (!query.length) return callback();
                jQuery.ajax({
                    url: '/usersapi/find',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        query: query,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },
                    error: function() {
                        callback();
                    },
                    success: function(res) {
                        console.log(res)
                        callback(res);
                    }
                });
            }
        });
        $('#related').selectize({
            valueField: 'id',
            labelField: 'name',
            searchField: ['name'],
            maxOptions: 10,
            maxItems: 3,
            render: {
                option: function (item, escape) {
                    var title = escape(item.name);
                    return '<div>' + title + '</div>';
                }
            },
            load: function(query, callback) {

                if (!query.length) return callback();
                jQuery.ajax({
                    url: '/postsapi/find',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        query: query,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },
                    error: function() {
                        callback();
                    },
                    success: function(res) {
                        callback(res);
                    }
                });
            }
        });
    </script>

@stop