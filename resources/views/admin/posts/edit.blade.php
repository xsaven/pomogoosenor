@extends('admin.layouts.master')

@section('content')
    @include('admin.posts.UI')

<div class="row">
    <div class="col-sm-10 col-sm-offset-2">
        <h1>{{ trans('quickadmin::templates.templates-view_edit-edit') }}</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
        	</div>
        @endif
    </div>
</div>

{!! Form::model($posts, array('class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'PATCH', 'route' => array(config('quickadmin.route').'.posts.update', $posts->id))) !!}

<div class="form-group">

    {!! Form::label('user_id', 'Автор', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        <select name="user_id" id="user_id">
            @php($author = \App\User::find($posts->user_id))
            @if($author)
                <option value="{{$posts->user_id}}">{{ $author->name }}</option>
            @endif
        </select>
    </div>
</div>

<div class="form-group">
    {!! Form::label('related', 'Материалы по теме', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        <select multiple name="related[]" id="related">
            @if($posts->related)
                @php($titres_ids = explode(',',$posts->related))
                @foreach($titres_ids as $t_id)
                    @php($post = \App\Posts::where('id',$t_id)->first())
                    <option selected value="{{$t_id}}">{{$post->title}}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>

<div class="form-group">
    {!! Form::label('title', 'Заголовок*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('title', old('title',$posts->title), array('class'=>'form-control')) !!}
        
    </div>
</div>
<div class="form-group">
    {!! Form::label('subtitle', 'Подзаголовок', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('subtitle', old('subtitle',$posts->subtitle), array('class'=>'form-control')) !!}
        
    </div>
</div>

<div class="form-group">
    {!! Form::label('preview', 'Главное изображение', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        <div style="padding: 20px; border: 1px solid #ccc;  margin-bottom: 20px;" class="col-sm-12">
            <button id="pick_main" class="lfm_pick" type="button">Выбрать изображение</button>
            <input value="{{$posts->preview}}" type="hidden" name="preview" class="lfm_pick_input">
            @if($posts->preview)
                <img style="width: 100%; height: auto; margin-top: 20px;" src="{{url($posts->preview)}}" alt="">
            @endif
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('titres', 'Титры', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        <textarea name="titres" id="titres">{!! $posts->titres !!}</textarea>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Контент</label>
    <div class="col-sm-10">
        <div id="the_content">
            @if(!is_null($posts->content))
                @php($content = json_decode($posts->content))
                @php($counter = 1)
                @if($content && is_array($content))
                    @foreach($content as $block)
                        @if($block->type == 'text')
                            @php($editor_id = 'initedEditor'.$counter)
                            <div class="content_item content_text">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input value="{{$block->title}}" type="text" placeholder="Заголовок блока" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <textarea id="{{$editor_id}}" class="cke-new">{!! $block->content !!}</textarea>
                                    </div>
                                </div>
                                <button class="btn btn-danger remove_block" type="button">Удалить блок</button>
                            </div>
                        @endif
                        @if($block->type == 'image')
                            <div class="content_item content_image">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button class="lfm_pick" type="button">Выбрать изображение</button>
                                        <input value="{{$block->path}}" disabled type="text" class="lfm_pick_input">
                                        <img style="width: 100%; height: auto; margin-top: 20px;" src="{{url($block->path)}}" alt="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input value="{{$block->caption}}" type="text" placeholder="Подпись изображения" class="form-control caption">
                                    </div>
                                </div>
                                <button class="btn btn-danger remove_block" type="button">Удалить блок</button>
                            </div>
                        @endif
                        @if($block->type == 'video')
                            <div class="content_item content_video">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <textarea class="form-control" placeholder="Код для вставки">{{ $block->embed }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input value="{{$block->caption}}" type="text" placeholder="Подпись видео" class="form-control">
                                    </div>
                                </div>
                                <button class="btn btn-danger remove_block" type="button">Удалить блок</button>
                            </div>
                        @endif
                        @php($counter ++)
                    @endforeach
                @endif
            @endif
        </div>
        <div class="btn-group">
            <button type="button" id="add_text" class="btn btn-primary">Добавить текстовый блок</button>
            <button type="button" id="add_image" class="btn btn-primary">Добавить блок с изображением</button>
            <button type="button" id="add_video" class="btn btn-primary">Добавить блок с видео</button>
        </div>
    </div>
</div>





<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
      {!! Form::submit(trans('quickadmin::templates.templates-view_edit-update'), array('class' => 'btn btn-primary')) !!}
      {!! link_to_route(config('quickadmin.route').'.posts.index', trans('quickadmin::templates.templates-view_edit-cancel'), null, array('class' => 'btn btn-default')) !!}
    </div>
</div>

{!! Form::close() !!}

@endsection

@section('javascript')
    <script src="/vendor/laravel-filemanager/js/content_builder.js"></script>

    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
    <script type="text/javascript" src="{{url('quickadmin/js/selectize.min.js')}}"></script>

    <script type="text/javascript">
        $('#the_content .content_image').find('.lfm_pick').filemanager('image');
        $('#pick_main').filemanager('image');

        CKEDITOR.replace( 'titres' );

        $('#the_content .cke-new').each(function () {
            var id = $(this).attr('id');
            CKEDITOR.replace( id );
        });
        $('#user_id').selectize({
            valueField: 'id',
            labelField: 'name',
            searchField: ['name'],
            maxOptions: 10,
            render: {
                option: function (item, escape) {
                    var title = escape(item.name);
                    return '<div>' + title + '</div>';
                }
            },
            load: function(query, callback) {

                if (!query.length) return callback();
                jQuery.ajax({
                    url: '/usersapi/find',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        query: query,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },
                    error: function() {
                        callback();
                    },
                    success: function(res) {
                        console.log(res)
                        callback(res);
                    }
                });
            }
        });
        $('#related').selectize({
            valueField: 'id',
            labelField: 'name',
            searchField: ['name'],
            maxOptions: 10,
            maxItems: 3,
            render: {
                option: function (item, escape) {
                    var title = escape(item.name);
                    return '<div>' + title + '</div>';
                }
            },
            load: function(query, callback) {

                if (!query.length) return callback();
                jQuery.ajax({
                    url: '/postsapi/find',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        query: query,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },
                    error: function() {
                        callback();
                    },
                    success: function(res) {
                        callback(res);
                    }
                });
            }
        });

    </script>

@stop