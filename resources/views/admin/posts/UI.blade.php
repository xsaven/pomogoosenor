<div id="postUI" style="display: none">
    <div id="blank_text">
        <div class="content_item content_text">
            <div class="form-group">
                <div class="col-sm-12">
                    <input type="text" placeholder="Заголовок блока" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <textarea class="cke-new"></textarea>
                </div>
            </div>
            <button class="btn btn-danger remove_block" type="button">Удалить блок</button>
        </div>
    </div>
    <div id="blank_image">
        <div class="content_item content_image">
            <div class="form-group">
                <div class="col-sm-12">
                    <button class="lfm_pick" type="button">Выбрать изображение</button>
                    <input disabled type="text" class="lfm_pick_input">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <input type="text" placeholder="Подпись изображения" class="form-control caption">
                </div>
            </div>
            <button class="btn btn-danger remove_block" type="button">Удалить блок</button>
        </div>
    </div>
    <div id="blank_video">
        <div class="content_item content_video">
            <div class="form-group">
                <div class="col-sm-12">
                    <textarea class="form-control" placeholder="Код для вставки"></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <input type="text" placeholder="Подпись видео" class="form-control">
                </div>
            </div>
            <button class="btn btn-danger remove_block" type="button">Удалить блок</button>
        </div>
    </div>
</div>


