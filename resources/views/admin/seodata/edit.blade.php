@extends('admin.layouts.master')

@section('content')
    <div class="row">
        <div class="col-sm-10 col-sm-offset-2">
            <h1>{{ trans('quickadmin::templates.templates-view_edit-edit') }}</h1>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                    </ul>
                </div>
            @endif
        </div>
    </div>


    {!! Form::model($seodata, array('files' => true, 'class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'PATCH', 'route' => array(config('quickadmin.route').'.seodata.update', $seodata->id))) !!}

    <div class="form-group">
        {!! Form::label('url_mask', 'Маска URL*', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('url_mask', old('url_mask',$seodata->url_mask), array('class'=>'form-control')) !!}
            <p class="help-block">напр. /blog/*</p>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('description', 'Описание', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::textarea('description', old('description',$seodata->description), array('class'=>'form-control')) !!}

        </div>
    </div>
    <div class="form-group">
        {!! Form::label('keywords', 'Ключевые слова', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::textarea('keywords', old('keywords',$seodata->keywords), array('class'=>'form-control')) !!}
        </div>
    </div>
    <label class="col-sm-2 control-label">OpenGraph</label>
    <div class="col-sm-10">
        <div class="content_item content_text">
            <div class="form-group">
                {!! Form::label('social_data[og_title]', 'Заголовок', array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-10">
                    {!! Form::text('social_data[og_title]', old('social_data[og_title]',$seodata->social_data['og_title']), array('class'=>'form-control')) !!}

                </div>
            </div>
            <div class="form-group">
                {!! Form::label('social_data[og_description]', 'Описание', array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-10">
                    {!! Form::textarea('social_data[og_description]', old('social_data[og_description]',$seodata->social_data['og_description']), array('class'=>'form-control')) !!}

                </div>
            </div>
            <div class="form-group">
                {!! Form::label('social_data[og_url]', 'URL', array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-10">
                    {!! Form::text('social_data[og_url]', old('social_data[og_url]',$seodata->social_data['og_url']), array('class'=>'form-control')) !!}
                    <p class="help-block">Если не указать этот параметр, то будет использоваться стандартная URL</p>
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('social_data[og_image]', 'Изображение', array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-10">
                    {!! Form::lfmImage('social_data[og_image]',$seodata->social_data['og_image']) !!}
                </div>
            </div>
        </div>
    </div>
    <label class="col-sm-2 control-label">Twitter</label>
    <div class="col-sm-10">
        <div class="content_item content_text">
            <div class="form-group">
                {!! Form::label('social_data[twitter_card]', 'Card', array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-10">
                    {!! Form::select('social_data[twitter_card]', $twitter_card, old('social_data[twitter_card]',$seodata->social_data['twitter_card']), array('class'=>'form-control')) !!}

                </div>
            </div>
            <div class="form-group">
                {!! Form::label('social_data[twitter_title]', 'Заголовок', array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-10">
                    {!! Form::text('social_data[twitter_title]', old('social_data[twitter_title]',$seodata->social_data['twitter_title']), array('class'=>'form-control')) !!}

                </div>
            </div>
            <div class="form-group">
                {!! Form::label('social_data[twitter_description]', 'Описание', array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-10">
                    {!! Form::text('social_data[twitter_description]', old('social_data[twitter_description]',$seodata->social_data['twitter_description']), array('class'=>'form-control')) !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('social_data[twitter_image]', 'Изображение', array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-10">
                    {!! Form::lfmImage('social_data[twitter_image]',$seodata->social_data['twitter_image']) !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('social_data[twitter_url]', 'URL', array('class'=>'col-sm-2 control-label')) !!}
                <div class="col-sm-10">
                    {!! Form::text('social_data[twitter_url]', old('social_data[twitter_url]',$seodata->social_data['twitter_url']), array('class'=>'form-control')) !!}
                    <p class="help-block">Если не указать этот параметр, то будет использоваться стандартная URL</p>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('social_data[image_src]', 'Тег image_src ', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::lfmImage('social_data[image_src]',$seodata->social_data['image_src']) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            {!! Form::submit(trans('quickadmin::templates.templates-view_edit-update'), array('class' => 'btn btn-primary')) !!}
            {!! link_to_route(config('quickadmin.route').'.seodata.index', trans('quickadmin::templates.templates-view_edit-cancel'), null, array('class' => 'btn btn-default')) !!}
        </div>
    </div>

    {!! Form::close() !!}

@endsection

@section('javascript')
    <script src="/vendor/laravel-filemanager/js/content_builder.js"></script>

    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>

    <script type="text/javascript">
        $('.lfm_pick').each(function () {
            $(this).filemanager('image');
        });
    </script>

@endsection