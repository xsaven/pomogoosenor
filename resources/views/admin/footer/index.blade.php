@extends('admin.layouts.master')

@section('content')


    {!! Form::open(array('files' => true,  'route' => config('quickadmin.route').'.footer.update', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!}

    <div class="form-group">
        {!! Form::label('tel', 'Телефоны', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::textarea('tel', old('tel', isset($data['tel']) ?  $data['tel'] : ''), array('class'=>'form-control ckeditor', 'rows' => 2)) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('work', 'График работы', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::textarea('work', old('work', isset($data['work']) ? $data['work'] : ''), array('class'=>'form-control ckeditor', 'rows' => 2)) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('email', 'Email', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::textarea('email', old('email', isset($data['email']) ? $data['email'] : ''), array('class'=>'form-control ckeditor',  'rows' => 2)) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('text_1', 'Левый блок', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::textarea('text_1', old('text_1', isset($data['text_1']) ? $data['text_1'] : ''), array('class'=>'form-control ckeditor',  'rows' => 2)) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('text_2', 'Центральный блок', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::textarea('text_2', old('text_2', isset($data['text_2']) ? $data['text_1'] : ''), array('class'=>'form-control ckeditor',  'rows' => 2)) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('text_3', 'Правый блок', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::textarea('text_3', old('text_3',isset($data['text_3']) ? $data['text_3'] : ''), array('class'=>'form-control ckeditor',  'rows' => 2)) !!}
        </div>
    </div>

    @for($i=1; $i<=5; $i++)
        <div class="form-group">
            {!! Form::label('image_'.$i, 'Изображение '.$i, array('class'=>'col-sm-2 control-label')) !!}
            <div class="col-sm-10">
                @if(isset($data['image_'.$i]))
                    <img style="width: 100px; height: auto; margin-bottom: 20px;"
                         src="{{url('uploads/' . $data['image_'.$i])}}"
                         alt="slide">
                @endif
                {!! Form::file('image_'.$i) !!}
            </div>
        </div>
    @endfor



    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            {!! Form::submit( trans('quickadmin::templates.templates-view_edit-update') , array('class' => 'btn btn-primary')) !!}
        </div>
    </div>


    {!! Form::close() !!}

@endsection


@section('javascript')
    <script src="/vendor/laravel-filemanager/js/content_builder.js"></script>

    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>

    <script type="text/javascript" src="{{url('quickadmin/js/selectize.min.js')}}"></script>

    <script type="text/javascript">
        CKEDITOR.replace('titres');
        $('#pick_main').filemanager('image');
        $('#user_id').selectize({
            valueField: 'id',
            labelField: 'name',
            searchField: ['name'],
            maxOptions: 10,
            render: {
                option: function (item, escape) {
                    var title = escape(item.name);
                    return '<div>' + title + '</div>';
                }
            },
            load: function (query, callback) {

                if (!query.length) return callback();
                jQuery.ajax({
                    url: '/usersapi/find',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        query: query,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },
                    error: function () {
                        callback();
                    },
                    success: function (res) {
                        console.log(res)
                        callback(res);
                    }
                });
            }
        });
        $('#related').selectize({
            valueField: 'id',
            labelField: 'name',
            searchField: ['name'],
            maxOptions: 10,
            maxItems: 3,
            render: {
                option: function (item, escape) {
                    var title = escape(item.name);
                    return '<div>' + title + '</div>';
                }
            },
            load: function (query, callback) {

                if (!query.length) return callback();
                jQuery.ajax({
                    url: '/postsapi/find',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        query: query,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },
                    error: function () {
                        callback();
                    },
                    success: function (res) {
                        callback(res);
                    }
                });
            }
        });
    </script>

@stop