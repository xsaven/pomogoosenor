@extends('admin.layouts.master')

@section('content')

    @if ($transactions->count())
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">{{ trans('quickadmin::templates.templates-view_index-list') }}</div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-hover table-responsive datatable" id="datatable">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Откуда</th>
                        <th>Куда</th>
                        <th>Сумма</th>
                        <th>Тип транзакции</th>

                        <th>&nbsp;</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($transactions as $row)
                        <tr>
                            <td>{{ $row->id }}</td>
                            <td>
                                <a href="{{route('profile_show', ['id' =>  $row->sender->id])}}">{{ $row->sender->name }}</a>
                            </td>
                            <td>
                                <a href="{{route('profile_show', ['id' =>  $row->recipient->id])}}">{{ $row->recipient->name }}</a>
                            </td>
                            <td>{{ $row->amount }}</td>
                            <td>{{ $row->getTypeLabel() }}</td>

                            <td>
                                {!! link_to_route(config('quickadmin.route').'.transactions.edit', 'Просмотр', array($row->id), array('class' => 'btn btn-xs btn-info')) !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @else
        {{ trans('quickadmin::templates.templates-view_index-no_entries_found') }}
    @endif

    <script>
        document.addEventListener('DOMContentLoaded', function () {
            let table = $('#datatable').DataTable();
            table.order([0, 'desc']).draw();
        });
    </script>

@endsection


