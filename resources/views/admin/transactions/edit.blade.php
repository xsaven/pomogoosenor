@extends('admin.layouts.master')

@section('content')
    <table class="table">
        <tbody>
        <tr>
            <th>ID</th>
            <td>{{$transaction->id}}</td>
        </tr>
        <tr>
            <th>Откуда</th>
            <td>
                <a href="{{route('profile_show', ['id' =>  $transaction->sender->id])}}">{{ $transaction->sender->name }}</a>
            </td>
        </tr>
        <tr>
            <th>Куда</th>
            <td>
                <a href="{{route('profile_show', ['id' =>  $transaction->recipient->id])}}">{{ $transaction->recipient->name }}</a>
            </td>
        </tr>
        <tr>
            <th>Сумма</th>
            <td>{{$transaction->amount}}</td>
        </tr>
        <tr>
            <th>Тип</th>
            <td>{{$transaction->getTypeLabel()}}</td>
        </tr>
        <tr>
            <th>Данные транзакции</th>
            <td>
                @foreach(json_decode($transaction->content) as $key => $row)
                    {{$key}}: {{$row}}<br>
                @endforeach

            </td>
        </tr>
        </tbody>
    </table>


@endsection