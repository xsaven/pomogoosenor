<div class="form-group">
    {!! Form::label($setting->key, $setting->label, array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('setting['.$setting->key.']', $setting->value, array('class'=>'form-control')) !!}
    </div>
</div>