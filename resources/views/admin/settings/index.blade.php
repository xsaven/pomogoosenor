@extends('admin.layouts.master')

@section('content')

    {!! Form::open(array(
        'url' => url('admin/update_settings'),
        'method' => 'post',
        'class' => 'form-horizontal',
        'files' => true,
    )) !!}

        @foreach($settings as $setting)
            @include('admin.settings.groups.'.$setting->type, compact('setting'))
        @endforeach

        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
                {!! Form::submit( 'Сохранить', array('class' => 'btn btn-primary')) !!}
            </div>
        </div>

    {!! Form::close() !!}
@endsection