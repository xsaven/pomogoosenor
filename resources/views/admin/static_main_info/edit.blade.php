@extends('admin.layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-10 col-sm-offset-2">
        <h1>{{ trans('quickadmin::templates.templates-view_edit-edit') }}</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
        	</div>
        @endif
    </div>
</div>

{!! Form::model($static_main_info, array('class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'PATCH', 'route' => array(config('quickadmin.route').'.staticmaininfo.update', $static_main_info->id))) !!}

<div class="form-group">
    {!! Form::label('key', 'Псевдоним*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('key', old('key',$static_main_info->key), array('class'=>'form-control')) !!}
        <p class="help-block">напр. email</p>
    </div>
</div><div class="form-group">
    {!! Form::label('comment', 'Комментарий', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('comment', old('comment',$static_main_info->comment), array('class'=>'form-control')) !!}
        <p class="help-block">напр. Почта</p>
    </div>
</div><div class="form-group">
    {!! Form::label('value', 'Значение*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('value', old('value',$static_main_info->value), array('class'=>'form-control')) !!}
        <p class="help-block">напр. pomogoo@gmail.com</p>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
      {!! Form::submit(trans('quickadmin::templates.templates-view_edit-update'), array('class' => 'btn btn-primary')) !!}
      {!! link_to_route(config('quickadmin.route').'.staticmaininfo.index', trans('quickadmin::templates.templates-view_edit-cancel'), null, array('class' => 'btn btn-default')) !!}
    </div>
</div>

{!! Form::close() !!}

@endsection