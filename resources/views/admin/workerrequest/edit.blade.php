@extends('admin.layouts.master')

@section('content')

    <div class="row">
        <div class="col-sm-10">
            <h1>Модерация</h1>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        {!! implode('', $errors->all('
                        <li class="error">:message</li>
                        ')) !!}
                    </ul>
                </div>
            @endif
        </div>
    </div>
    <form action="{{url('admin/moderate_worker')}}" id="main" class="form-horizontal" method="POST">
        <input type="hidden" name="user_id" value="{{$user->id}}">
        <input type="hidden"
               name="_token"
               value="{{ csrf_token() }}">
        <div class="row">
            <div class="col-md-3">
                @if($profile->avatar_full)
                    <img style="width: 100%;" src="{{ url($profile->avatar_full) }}" alt="">
                @else
                    Фото не задано
                @endif
            </div>
            <div class="col-md-9">
                <h2 style="margin-top: 0;">{{$profile->name}} {{$profile->surname}}</h2>
                <p>День рождения: <strong>{{$profile->birthday}}</strong></p>
                <p>Тел. номер: <strong>{{$profile->phone}}</strong></p>
                <p>Город: <strong>{{ \App\City::find($profile->city_id)->name }}</strong></p>
                @if($confidant)
                    <h2>Доверенное лицо</h2>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Имя</th>
                                <th>Фамилия</th>
                                <th>Кем приходится</th>
                                <th>Телефон</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>{{$confidant->name}}</th>
                                <td>{{$confidant->surname}}</td>
                                <td>{{$confidant->status}}</td>
                                <td>{{$confidant->phone}}</td>
                            </tr>
                        </tbody>
                    </table>
                @endif
                @if(!$cats->isEmpty())
                    <h2>Выбранные категории:</h2>
                    @foreach($cats as $cat)
                        <p><strong>{{ $cat->name }}</strong></p>
                    @endforeach
                @endif
            </div>

        </div>
        <div class="form-group">
            <hr>
            <div class="col-sm-6">
                {!! Form::submit('Подтвердить исполнителя', ['class' => 'btn btn-primary pull-right']) !!}
            </div>
            <div class="col-sm-6">
                {!! Form::submit('Опровергнуть исполнителя', ['id' => 'bad-moderate', 'class' => 'btn btn-danger']) !!}
            </div>
        </div>

    </form>

@endsection

@section('javascript')
    <script type="text/javascript">
        $('#bad-moderate').on('click',function(){
           var reason = prompt('Введите причину');
           $('#main').append('<input type="hidden" name="reason" value="'+reason+'">');
        });
    </script>
@stop


