@extends('admin.layouts.master')

@section('content')

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">Заявки на исполнителей</div>
        </div>
        <div class="portlet-body">
            <table id="datatable" class="table table-striped table-hover table-responsive datatable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Имя</th>
                    <th>Модерировать</th>
                </tr>
                </thead>

                <tbody>
                @foreach ($moderatableUsers as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name() }}</td>
                        <td>
                            <a class="btn btn-xs btn-info" href="{{url('/admin/workerrequest/'.$user->id)}}">Просмотреть</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
	</div>

@endsection