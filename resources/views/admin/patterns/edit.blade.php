@extends('admin.layouts.master')

@section('content')
    @include('admin.patterns.UI')

<div class="row">
    <div class="col-sm-10 col-sm-offset-2">
        <h1>{{ trans('quickadmin::templates.templates-view_edit-edit') }}</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
        	</div>
        @endif
    </div>
</div>

{!! Form::model($pattern, array('class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'PATCH', 'route' => array(config('quickadmin.route').'.patterns.update', $pattern->id))) !!}

<div class="form-group">
    {!! Form::label('name', 'Название шага*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('name', old('name',$pattern->name), array('class'=>'form-control')) !!}
        
    </div>
</div><div class="form-group">
    {!! Form::label('active', 'Активный', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::hidden('active','') !!}
        {!! Form::checkbox('active', 1, $pattern->active == 1) !!}
        
    </div>
</div><div class="form-group">
    {!! Form::label('order', 'Порядок шага', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('order', old('order',$pattern->order), array('class'=>'form-control')) !!}
        
    </div>
</div>

<div class="form-group">
    {!! Form::label('tasks', 'Вопросы', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        <div id="pattern_content">
            @if(!is_null($pattern->questions))
                @if(!empty($pattern->questions))
                    @foreach($pattern->questions as $question)
                        <div class="content_item content_text">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input value="{{ $question->title }}" type="text" placeholder="Вопрос" data-id="{{ $question->id }}" class="form-control quest">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="unsers_wrapper">
                                        @if(isset($question->answers) && !empty($question->answers))
                                            @foreach($question->answers as $answer)
                                                <div class="form-group">
                                                    <div class="col-sm-12 unswer-collection">
                                                        <input type="text" value="{{$answer->title}}" data-id="{{ $answer->id }}" placeholder="Ответ" class="form-control unswer">
                                                        <label>
                                                            <input @if($answer->is_true) checked  @endif type="checkbox" class="is_true">
                                                            Этот ответ является правильным
                                                        </label>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <button class="btn btn-default add_unswer" type="button">Добавить вариант ответа</button>
                                </div>
                            </div>
                            <button class="btn btn-danger remove_block" type="button">Удалить вопрос</button>
                        </div>
                    @endforeach
                @endif
            @endif
        </div>
        <button id="add_quest" class="btn btn-primary" type="button">Добавить вопрос</button>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
      {!! Form::submit(trans('quickadmin::templates.templates-view_edit-update'), array('class' => 'btn btn-primary')) !!}
      {!! link_to_route(config('quickadmin.route').'.patterns.index', trans('quickadmin::templates.templates-view_edit-cancel'), null, array('class' => 'btn btn-default')) !!}
    </div>
</div>

{!! Form::close() !!}

@endsection

@section('javascript')
    <script src="/vendor/laravel-filemanager/js/pattern_builder.js"></script>
@stop