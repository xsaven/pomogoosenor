<div style="display: none;" id="patternsUI">
    <div id="blank_quest">
        <div class="content_item content_text">
            <div class="form-group">
                <div class="col-sm-12">
                    <input type="text" placeholder="Вопрос" class="form-control quest">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="unsers_wrapper"></div>
                    <button class="btn btn-default add_unswer" type="button">Добавить вариант ответа</button>
                </div>
            </div>
            <button class="btn btn-danger remove_block" type="button">Удалить вопрос</button>
        </div>
    </div>

    <div id="blank_unswer">

        <div class="form-group">
            <div class="col-sm-12 unswer-collection">
                <input type="text" placeholder="Ответ" class="form-control unswer">
                <label>
                    <input type="checkbox" class="is_true">
                    Этот ответ является правильным
                </label>
            </div>
        </div>

    </div>

</div>