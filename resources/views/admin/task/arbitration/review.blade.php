@extends('admin.layouts.master')

@section('title')Арбитраж@endsection

@section('content')


    <div class="container-fluid">
        <div class="row">
            <div class="btn-group">
                <a class="btn btn-success" href="{{route('admin.tasks.arbitration.confirm', ['id' => $task->id])}}">
                    Подтвердить сделку
                </a>
                <a class="btn btn-danger" href="{{route('admin.tasks.arbitration.cancel', ['id' => $task->id])}}">
                    Отменить сделку
                </a>
                <a class="btn btn-info" href="{{route('profile_show', ['id' => $task->creator_id])}}">
                    Профиль заказчика
                </a>
                <a class="btn btn-info" href="{{route('profile_show', ['id' => $task->executor_id])}}">
                    Профиль исполнителя
                </a>
                <a class="btn btn-info" href="{{route('task.review', ['id' => $task->id])}}">
                    Перейти к заданию
                </a>
            </div>
        </div>

        <div class="row">
            <h2>Чат пользователей</h2>
            <div style="height: 350px; overflow-y: scroll; overflow-x: hidden ">
                @foreach($messages as $message)
                    <div class="row">
                        <div class="col-md-10" style="font-size: 17px;"><a
                                    href="{{route('profile_show', ['id'=>$message->users->id])}}">{{$message->users->name}}</a><br/>
                            <span style="margin-left: 10px;">{{$message->text}}</span>
                        </div>
                        <div class="col-md-2" style="font-size: 11px">({{$message->created_at}})</div>
                    </div>
                @endforeach
            </div>
        </div>

    </div>

@endsection