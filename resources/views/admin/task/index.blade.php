@extends('admin.layouts.master')

@section('content')

    @if ($taskCollection->count())
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">Задания</div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-hover table-responsive datatable" id="datatable">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Название</th>
                        <th>Пользователь</th>
                        <th>Статус</th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($taskCollection as $row)
                        <tr>
                            <td>{{$row->id }}</td>
                            <td><a href="{{route('task.review', ['userTasks' => $row])}}">{{ $row->name }}</a></td>
                            <td>
                                @if($row->creator && $row->creator->profile)<a
                                        href="{{route('profile_show', ['id' => $row->creator->id])}}">{{ $row->creator->profile->name }}</a>@endif
                            </td>
                            <td>{{ $row->status }}</td>
                            <td>
                                @if($row->getOriginal('status') == \App\UserTasks::STATUS_MODERATE)
                                    <a href="{{route('admin.tasks.approve', ['task' => $row])}}"
                                       class="btn btn-xs btn-success">Подтвердить</a>
                                @elseif($row->getOriginal('status') == \App\UserTasks::STATUS_OPENED)
                                    <a href="{{route('admin.tasks.unapprove', ['task' => $row])}}"
                                       class="btn btn-xs btn-danger">Отклонить</a>
                                @else
                                    <a href="#" disabled class="btn btn-xs btn-success">Подтвердить</a>
                                @endif
                                <a href="{{route('task.review', ['userTasks' => $row])}}"
                                   class="btn btn-xs btn-info">Перейти к заданию</a>
                                <a href="{{route('admin.tasks.delete', ['task' => $row])}}"
                                   onclick="return confirm('Вы действительно хотите удалить задание?');"
                                   class="btn btn-xs btn-danger">Удалить</a>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    @else
        {{ trans('quickadmin::templates.templates-view_index-no_entries_found') }}
    @endif

@endsection

<script>
    document.addEventListener('DOMContentLoaded', function () {
        let table = $('#datatable').DataTable();
        table.order([0, 'desc']).draw();
    });
</script>
