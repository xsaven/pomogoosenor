@extends('admin.layouts.master')

@section('content')

    <div class="row">
        <div class="col-sm-10 col-sm-offset-2">
            <h1>{{ trans('quickadmin::templates.templates-view_edit-edit') }}</h1>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                    </ul>
                </div>
            @endif
        </div>
    </div>

    <style>
        #sortable1, #sortable2 {
            border: 1px solid #eee;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            float: left;
            margin-right: 10px;
        }

        #sortable1 li, #sortable2 li {
            margin: 0 5px 5px 5px;
            padding: 5px;
            font-size: 1.2em;
        }

        #sortable1 li i {
            display: none;
        }

        #sortable1 li {
            cursor: move;
        }

        #sortable2 li .fa-arrows {
            cursor: move;
        }

        #sortable2 li input {
            width: 85%;
        }
    </style>

    {!! Form::model($subcategory, array('class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'PATCH', 'route' => array(config('quickadmin.route').'.subcategories.update', $subcategory->id))) !!}

    <div class="form-group">
        {!! Form::label('name', 'Элементы формы*', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            <ul id="sortable1" class="connectedSortable col-sm-5">
                @foreach(\App\FormFields::all() as $field)
                    <li class="ui-state-default" field-id="{{$field->id}}"><span
                                class="rebuildToInput">{{$field->name}}</span> <a onclick="$(this).parent().remove()"><i
                                    class="fa fa-remove"></i></a></li>
                @endforeach
            </ul>

            <ul id="sortable2" class="connectedSortable col-sm-5">
                @if(!empty($subcategory->form_fields))
                    @foreach($subcategory->form_fields as $field)
                        @foreach($field as $fId => $val)
                            <li class="ui-state-default"><i class='fa fa-arrows'></i> <input
                                        name='form_fields[][{{$fId}}]' type='text' value='{{$val}}'> <a
                                        onclick="$(this).parent().remove()"><i class="fa fa-remove"></i></a></li>
                        @endforeach
                    @endforeach
                @endif
            </ul>
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('category_id', 'Ценовой диапазон*', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::select('range_id', \App\PriceRangersMask::pluck("name", "id"), old('range_id',$subcategory->range_id), array('class'=>'form-control')) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('name', 'Название*', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('name', old('name',$subcategory->name), array('class'=>'form-control')) !!}
            <p class="help-block">напр. Услуги курьера</p>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('slug', 'Псевдоним*', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('slug', old('slug',$subcategory->slug), array('class'=>'form-control')) !!}
            <p class="help-block">напр. courier</p>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('question', 'Вопрос*', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('question', old('question',$subcategory->question), array('class'=>'form-control')) !!}
            <p class="help-block">напр. Ищете курьера на день?</p>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('description', 'Описание*', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('description', old('description',$subcategory->description), array('class'=>'form-control')) !!}
            <p class="help-block">напр. Укажите детали, количесто материалов</p>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('example_title', 'Пример названия*', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('example_title', old('example_title',$subcategory->example_title), array('class'=>'form-control')) !!}
            <p class="help-block">напр.Нужен курьер на день</p>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('hint_description', 'Подсказка в описании', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::textarea('hint_description', old('hint_description',$subcategory->hint_description), array('class'=>'form-control ckeditor')) !!}
            <p class="help-block">напр. Не забудьте указать: - Описание груза</p>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('tags', 'Теги*', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::textarea('tags', old('tags',$subcategory->tags), array('class'=>'form-control')) !!}
            <p class="help-block">напр. Уборка квартиры, уборка дома...</p>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('top_text', 'Вопрос*', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('top_text', old('top_text',$subcategory->top_text), array('class'=>'form-control')) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('price_from', 'Средняя цена*', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('price_from', old('price_from',$subcategory->price_from), array('class'=>'form-control')) !!}

        </div>
    </div>
    <div class="form-group">
        {!! Form::label('price_min', 'Минимальная цена*', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('price_min', old('price_min',$subcategory->price_min), array('class'=>'form-control')) !!}

        </div>
    </div>
    <div class="form-group">
        {!! Form::label('order', 'Порядок*', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('order', old('order',$subcategory->order), array('class'=>'form-control')) !!}

        </div>
    </div>
    <div class="form-group">
        {!! Form::label('default', 'По умолчанию', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::hidden('default','') !!}
            {!! Form::checkbox('default', 1, $subcategory->default == 1) !!}

        </div>
    </div>
    <div class="form-group">
        {!! Form::label('category_id', 'Категория*', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::select('category_id', $categories, old('category_id',$subcategory->category_id), array('class'=>'form-control')) !!}

        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            {!! Form::submit(trans('quickadmin::templates.templates-view_edit-update'), array('class' => 'btn btn-primary')) !!}
            {!! link_to_route(config('quickadmin.route').'.subcategories.index', trans('quickadmin::templates.templates-view_edit-cancel'), null, array('class' => 'btn btn-default')) !!}
        </div>
    </div>

    {!! Form::close() !!}

@endsection