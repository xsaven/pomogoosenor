@extends('admin.layouts.master')

@section('content')

    <div class="col-md-5">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-2">
                <h1>{{ trans('quickadmin::admin.users-edit-edit_user') }}</h1>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            {!! implode('', $errors->all('
                            <li class="error">:message</li>
                            ')) !!}
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        {!! Form::open(['route' => ['users.update', $user->id], 'class' => 'form-horizontal', 'method' => 'PATCH']) !!}

        <div class="form-group">
            {!! Form::label('name', trans('quickadmin::admin.users-edit-name'), ['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::text('name', old('name', $user->name), ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.users-edit-name_placeholder')]) !!}
            </div>
        </div>


        <div class="form-group">
            {!! Form::label('email', trans('quickadmin::admin.users-edit-email'), ['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::email('email', old('email', $user->email), ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.users-edit-email_placeholder')]) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('password', trans('quickadmin::admin.users-edit-password'), ['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::password('password', ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.users-edit-password_placeholder')]) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('role_id', trans('quickadmin::admin.users-edit-role'), ['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::select('role_id', $roles, old('role_id', $user->role_id), ['class'=>'form-control']) !!}
            </div>
        </div>

        <hr/>

        <div class="row">
            <div class="col-sm-10 col-sm-offset-2">
                <h2>Профиль</h2>
            </div>
        </div>

        <br/>
        <div class="form-group">
            {!! Form::label('profile[name]', 'Имя', ['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::text('profile[name]', old('profile[name]', $user->profile->name), ['class'=>'form-control', 'Имя']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('profile[surname]', 'Фамилия', ['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::text('profile[surname]', old('profile[surname]', $user->profile->surname), ['class'=>'form-control', 'Фамилия']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('profile[patronymic]', 'Отчество', ['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::text('profile[patronymic]', old('profile[patronymic]', $user->profile->patronymic), ['class'=>'form-control', 'Отчество']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('profile[phone]', 'Телефон', ['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::tel('profile[phone]', old('profile[phone]', $user->profile->phone), ['class'=>'form-control', 'Телефон']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('profile[birthday]', 'Дата рождения', ['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::date('profile[birthday]', old('profile[birthday]', $user->profile->birthday), ['class'=>'form-control', 'Дата рождения']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('profile[is_worker]', 'Рабочий профиль', ['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::checkbox('profile[is_worker]', true, old('profile[is_worker]', $user->profile->is_worker)) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('profile[moderated_worker]', 'Провереный работник', ['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::checkbox('profile[moderated_worker]', true, old('profile[moderated_worker]', $user->profile->moderated_worker)) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('profile[documents_moderated]', trans('quickadmin::admin.users-edit-documents_moderated'), ['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::checkbox('profile[documents_moderated]', true, old('profile[documents_moderated]', $user->profile->documents_moderated)) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('profile[interview_complete]', trans('quickadmin::admin.users-edit-interview_complete'), ['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::checkbox('profile[interview_complete]', true, old('profile[interview_complete]', $user->profile->interview_complete)) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('profile[balance]', 'Счет', ['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::number('profile[balance]', old('profile[balance]', $user->profile->balance), ['class'=>'form-control', 'step' => '0.01']) !!}
            </div>
        </div>


        <div class="form-group">
            {!! Form::label('profile[sex]', 'Пол', ['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::select('profile[sex]', [0=>'Мужчина', 1 => 'Женщина'], old('profile[sex]', $user->profile->getOriginal('sex')), ['class'=>'form-control', 'Пол']) !!}
            </div>
        </div>


        <div class="form-group">
            {!! Form::label('profile[city_id]', 'Город', ['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::select('profile[city_id]', $cityCollection, old('profile[city_id]', $user->profile->city_id), ['class'=>'form-control', 'Город']) !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
                {!! Form::submit(trans('quickadmin::admin.users-edit-btnupdate'), ['class' => 'btn btn-primary']) !!}
            </div>
        </div>
        {!! Form::close() !!}

    </div>




    <div class="col-md-7">

        @if ($transactions->count())
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">Внутрение транзакции сайта</div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-hover table-responsive datatable" id="datatable">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Отправитель</th>
                            <th>Получатель</th>
                            <th>Сумма</th>
                            <th>Hash</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($transactions as $row)
                            <tr>
                                <td>{{$row->id}}</td>
                                <td>
                                    @if($row->sender)
                                        <a href="{{route('profile_show', ['id' =>$row->sender_id])}}">{{$row->sender->name}}</a>
                                    @endif
                                </td>
                                <td>
                                    @if($row->receiver)
                                        <a href="{{route('profile_show', ['id' =>$row->receiver_id])}}">{{$row->receiver->name}}</a>
                                    @endif
                                </td>
                                <td>{{$row->amount}}</td>
                                <td>{{$row->hash}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @else
            {{ trans('quickadmin::templates.templates-view_index-no_entries_found') }}
        @endif
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function () {
            let table = $('#datatable').DataTable();

            table.order([0, 'desc']).draw();
        });
    </script>

@endsection


