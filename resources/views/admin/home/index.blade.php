@extends('admin.layouts.master')

@section('content')


    {!! Form::open(array('files' => true,  'route' => config('quickadmin.route').'.home.update', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!}

    <div class="form-group">
        {!! Form::label('title', 'Заголовок', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('title', old('title', isset($data['title']) ?  $data['title'] : ''), array('class'=>'form-control')) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('sub-title', 'Подзаголовок', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('sub-title', old('sub-title', isset($data['sub-title']) ?  $data['sub-title'] : ''), array('class'=>'form-control')) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('search-placeholder', 'Поле поиска', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('search-placeholder', old('search-placeholder', isset($data['search-placeholder']) ?  $data['search-placeholder'] : ''), array('class'=>'form-control')) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('search-button', 'Кнопка поиска', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('search-button', old('search-button', isset($data['search-button']) ?  $data['search-button'] : ''), array('class'=>'form-control')) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('search-example', 'Пример поиска', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('search-example', old('search-example', isset($data['search-example']) ?  $data['search-example'] : ''), array('class'=>'form-control')) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('categories-title', 'Заголовок над категориями', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('categories-title', old('categories-title', isset($data['categories-title']) ?  $data['categories-title'] : ''), array('class'=>'form-control')) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('all-categories', 'Текст кнопки все категории', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('all-categories', old('all-categories', isset($data['all-categories']) ?  $data['all-categories'] : ''), array('class'=>'form-control')) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('blog-title', 'Заголовок блога', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('blog-title', old('blog-title', isset($data['blog-title']) ?  $data['blog-title'] : ''), array('class'=>'form-control')) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('bottom-block-title', 'Заголовок нижнего блока', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('bottom-block-title', old('bottom-block-title', isset($data['bottom-block-title']) ?  $data['bottom-block-title'] : ''), array('class'=>'form-control')) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('main-info-block', 'Основной блок информации', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::textarea('main-info-block', old('main-info-block', isset($data['main-info-block']) ?  $data['main-info-block'] : ''), array('class'=>'form-control ckeditor')) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('story-block-title', 'Заголовок блока с кнопками', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('story-block-title', old('story-block-title', isset($data['story-block-title']) ?  $data['story-block-title'] : ''), array('class'=>'form-control')) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('add-task-text', 'Текст кнопки разместить задание', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('add-task-text', old('add-task-text', isset($data['add-task-text']) ?  $data['add-task-text'] : ''), array('class'=>'form-control')) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('executor-text', 'Текст кнопки стать исполнителем', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('executor-text', old('executor-text', isset($data['executor-text']) ?  $data['executor-text'] : ''), array('class'=>'form-control')) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('right-bottom-block', 'Правый нижний блок', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::textarea('right-bottom-block', old('right-bottom-block', isset($data['right-bottom-block']) ?  $data['right-bottom-block'] : ''), array('class'=>'form-control ckeditor')) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('all-tasks-button', 'Кнопка: показать все задание', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('all-tasks-button', old('all-tasks-button', isset($data['all-tasks-button']) ?  $data['all-tasks-button'] : ''), array('class'=>'form-control')) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('right-bottom-block-title-1', 'Правый нижний блок, как стать исполнителем', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('right-bottom-block-title-1', old('right-bottom-block-title-1', isset($data['right-bottom-block-title-1']) ?  $data['right-bottom-block-title-1'] : ''), array('class'=>'form-control')) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('right-bottom-block-title-2', 'Правый нижний блок, безопасность', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('right-bottom-block-title-2', old('right-bottom-block-title-2', isset($data['right-bottom-block-title-2']) ?  $data['right-bottom-block-title-2'] : ''), array('class'=>'form-control')) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('right-bottom-block-title-3', 'Правый нижний блок, задания', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('right-bottom-block-title-3', old('right-bottom-block-title-3', isset($data['right-bottom-block-title-3']) ?  $data['right-bottom-block-title-3'] : ''), array('class'=>'form-control')) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            {!! Form::submit( trans('quickadmin::templates.templates-view_edit-update') , array('class' => 'btn btn-primary')) !!}
        </div>
    </div>


    {!! Form::close() !!}

@endsection


@section('javascript')
    <script src="/vendor/laravel-filemanager/js/content_builder.js"></script>

    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>

    <script type="text/javascript" src="{{url('quickadmin/js/selectize.min.js')}}"></script>

    <script type="text/javascript">
        CKEDITOR.replace('titres');
        $('#pick_main').filemanager('image');
        $('#user_id').selectize({
            valueField: 'id',
            labelField: 'name',
            searchField: ['name'],
            maxOptions: 10,
            render: {
                option: function (item, escape) {
                    var title = escape(item.name);
                    return '<div>' + title + '</div>';
                }
            },
            load: function (query, callback) {

                if (!query.length) return callback();
                jQuery.ajax({
                    url: '/usersapi/find',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        query: query,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },
                    error: function () {
                        callback();
                    },
                    success: function (res) {
                        console.log(res)
                        callback(res);
                    }
                });
            }
        });
        $('#related').selectize({
            valueField: 'id',
            labelField: 'name',
            searchField: ['name'],
            maxOptions: 10,
            maxItems: 3,
            render: {
                option: function (item, escape) {
                    var title = escape(item.name);
                    return '<div>' + title + '</div>';
                }
            },
            load: function (query, callback) {

                if (!query.length) return callback();
                jQuery.ajax({
                    url: '/postsapi/find',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        query: query,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },
                    error: function () {
                        callback();
                    },
                    success: function (res) {
                        callback(res);
                    }
                });
            }
        });
    </script>

@stop