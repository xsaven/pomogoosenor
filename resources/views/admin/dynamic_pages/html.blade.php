@extends('admin.layouts.master')

@section('content')

    <div class="row">
        <div class="col-sm-10 col-sm-offset-2">
            <h1>{{ $name }}</h1>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! Form::model($data, array('class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'POST', 'route' => array(config('quickadmin.route').'.page.update'))) !!}

    {!! Form::hidden('slug', $slug) !!}

    <div class="form-group">
        {!! Form::label('name', 'Имя', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('name', old('name', $name), array('class'=>'form-control')) !!}

        </div>
    </div>


    <div class="form-group">
        {!! Form::label('data[title]', 'Заголовок', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::text('data[title]', old('data[title]',isset($data['title']) ? $data['title'] : null), array('class'=>'form-control')) !!}

        </div>
    </div>


    <div class="form-group">
        {!! Form::label('data[description]', 'SEO описание документа', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::textarea('data[description]', old('data[description]',isset($data['description']) ? $data['title'] : null), array('class'=>'form-control')) !!}

        </div>
    </div>
    <div class="form-group">
        {!! Form::label('data[keywords]', 'SEO ключевые слова', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::textarea('data[keywords]', old('data[keywords]',isset($data['keywords']) ? $data['title'] : null), array('class'=>'form-control')) !!}

        </div>
    </div>
    <div class="form-group">
        {!! Form::label('data[content]', 'Контент*', array('class'=>'col-sm-2 control-label')) !!}
        <div class="col-sm-10">
            {!! Form::textarea('data[content]', old('data[content]',isset($data['content']) ? $data['title'] : null), array('class'=>'form-control ckeditor')) !!}

        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            {!! Form::submit(trans('quickadmin::templates.templates-view_edit-update'), array('class' => 'btn btn-primary')) !!}
        </div>
    </div>

    {!! Form::close() !!}

@endsection