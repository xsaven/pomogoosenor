<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
<script src="{{ url('quickadmin/js') }}/timepicker.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.4.5/jquery-ui-timepicker-addon.min.js"></script>
<script src="{{asset('s/js/plugins/ckeditor/ckeditor.js')}}"></script>
{{--<script src="{{ url('quickadmin/js') }}/adapter/jquery.js"></script>--}}
<script src="{{ url('quickadmin/js') }}/bootstrap.min.js"></script>
<script src="{{ url('quickadmin/js') }}/main.js"></script>

<script>

    var removeSelectedFormField = function(obj){

    }

    $( "#sortable1" ).sortable({
        connectWith: ".connectedSortable",
        forcePlaceholderSize: false,
        helper: function (e, li) {
            copyHelper = li.clone().insertAfter(li);
            return li.clone();
        },
        stop: function () {
            copyHelper && copyHelper.remove();
            var newObj = $('#sortable2 .rebuildToInput');
            var val = newObj.html();
            var fId = newObj.parent().attr('field-id');
            newObj.html("<i class='fa fa-arrows'></i> <input name='form_fields[]["+fId+"]' type='text' value='"+val+"'>");
            newObj.removeClass('rebuildToInput');
        }
    }).disableSelection();

    $( "#sortable2" ).sortable({
        handle: '.fa-arrows',
        receive: function (e, ui) {
            copyHelper = null;
        }
    }).disableSelection().css('min-height', $( "#sortable1" ).height()+7);



    $('.datepicker').datepicker({
        autoclose: true,
        dateFormat: "{{ config('quickadmin.date_format_jquery') }}"
    });

    $('.datetimepicker').datetimepicker({
        autoclose: true,
        dateFormat: "{{ config('quickadmin.date_format_jquery') }}",
        timeFormat: "{{ config('quickadmin.time_format_jquery') }}"
    });

    $('#datatable').dataTable( {
        "language": {
            "url": "{{ trans('quickadmin::strings.datatable_url_language') }}"
        }
    });

</script>
