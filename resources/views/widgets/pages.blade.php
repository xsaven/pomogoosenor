<div class="col-md-3">
    <div class="l-tech-sidebar">
        <ul class="h-list">
            <li><a href="{{ url('/how-it-work') }}" {{ (Request::is('how-it-work') ? 'class=active' : '') }}>Как это работает</a></li>
            <li><a href="{{ url('/no-risk') }}" {{ (Request::is('no-risk') ? 'class=active' : '') }}>Сделка без риска</a></li>
            <li><a href="{{ url('/security') }}" {{ (Request::is('security') ? 'class=active' : '') }}>Безопасность и гарантии</a></li>
            <li><a href="{{ url('/how-performer') }}" {{ (Request::is('how-performer') ? 'class=active' : '') }}>Стать исполнителем</a></li>
            <li><a href="{{ url('/rewards') }}" {{ (Request::is('rewards') ? 'class=active' : '') }}>Награды и рейтинг</a></li>
            <li><a href="{{ url('/performers-review') }}" {{ (Request::is('performers-review') ? 'class=active' : '') }}>Отзывы исполнителей	</a></li>
            <li><a href="{{ url('/customer-review') }}" {{ (Request::is('customer-review') ? 'class=active' : '') }}>Отзывы заказчиков</a></li>
        </ul>
    </div>
</div>