@if($config['flat'])
    <div style="width: 100%" class="social-share-list">
        @else
            <div class="c-card-sidebar__soc social-share-list">
                <span>Помогите друзьям решать их задачи.</span>
                <span>Расскажите им о Pomogoo:</span>
                @endif

                <span class="share-data-info" data-share-url="{{ $config['url'] }}"
                      data-text="{{ $config['message'] }}"></span>
                <div class="c-card-sidebar__soc-links clearfix">
                    <a href class="init-vk-share init-share">
                        <svg width="1em" height="1em" class="icon icon-conf-vk ">
                            <use xlink:href="/s/images/useful/svg/theme/symbol-defs.svg#icon-conf-vk"></use>
                        </svg>
                    </a>
                    <a href class="init-fb-share init-share">
                        <svg width="1em" height="1em" class="icon icon-conf-fb ">
                            <use xlink:href="/s/images/useful/svg/theme/symbol-defs.svg#icon-conf-fb"></use>
                        </svg>
                    </a>
                    <a href class="init-tw-share  init-share">
                        <svg width="1em" height="1em" class="icon icon-conf-tw ">
                            <use xlink:href="/s/images/useful/svg/theme/symbol-defs.svg#icon-conf-tw"></use>
                        </svg>
                    </a>
                    <a href class="init-ok-share  init-share">
                        <svg width="1em" height="1em" class="icon icon-conf-ok ">
                            <use xlink:href="/s/images/useful/svg/theme/symbol-defs.svg#icon-conf-ok"></use>
                        </svg>
                    </a>
                    <a href class="init-go-share  init-share">
                        <svg width="1em" height="1em" class="icon icon-conf-google ">
                            <use xlink:href="/s/images/useful/svg/theme/symbol-defs.svg#icon-conf-google"></use>
                        </svg>
                    </a></div>
            </div>
    </div>