<?php /**
 * @var \App\Posts[] $posts
 * @var \App\Posts $mainPost
 */
?>
@if($mainPost)
    <div class="l-catalog-posts">
        <div class="l-catalog-posts__ttl">Новые публикации в блоге</div>
        <a href="{{url('blog/post', ['slug' => $mainPost->slug])}}">
            <div class="l-catalog-posts__item-img h-object-fit">
                <img src="{{asset($mainPost->preview)}}" alt=""/>
            </div>
            <div class="l-catalog-posts__item-ttl">{{$mainPost->title}}</div>
            <div class="l-catalog-posts__item-txt">{{$mainPost->subtitle_limited}}</div>
        </a>
        <div class="l-catalog-posts__subs">
            @foreach($posts as $post)
                <a href="{{url('blog/post', ['slug' => $post->slug])}}"><span>{{$post->title}}</span></a>
            @endforeach
        </div>

    </div>
@endif