@if(!$slides->isEmpty())
    <div class="l-main-slider">
        @foreach($slides as $slide)
            <div class="l-main-slider__slides-item h-object-fit">
                <img src="{{url('uploads/'.$slide->image)}}" alt="slide"/>
            </div>
        @endforeach
    </div>
@endif