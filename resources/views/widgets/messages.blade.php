@if (session('status'))
    <div id="modal_message" class="modal fade modal-mob-numb">
        <div class="modal-dialog">
            <div class="modal-body">
                <button type="button" data-dismiss="modal" class="but-modal-close"></button>
                <div class="modal-reg_success__txt">
                    <h3 style="margin-top: 20px;"> @if(session('message_title')){{session('message_title')}}@else
                            Успех@endif</h3>
                    <div class="alert alert-{{session('status')}}">
                        @if(session('message'))
                            <p>{{session('message')}}</p>
                        @endif
                        @if(session('messages'))
                            <p>{{session('messages')}}</p>
                        @endif
                        @if(session('messages') && is_array(session('messages')))
                            <ul>
                                @foreach(session('messages') as $message)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            window.onload = function () {
                setTimeout($('#modal_message').modal('show'))
            }
        </script>
    @endpush
@endif
@if (isset($errors) && count($errors) > 0)
<div id="modal_error_message" class="modal fade modal-mob-numb"
     style="display: {{isset($errors) && count($errors) > 0?'block':'none'}};">
    <div class="modal-dialog">
        <div class="modal-body">
            <button type="button" data-dismiss="modal" class="but-modal-close"></button>
            <div class="modal-reg_success__txt">
                <h3 style="margin-top: 20px;">Ошибка</h3>
                <div class="alert alert-error">

                        @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
@endif

@if (isset($errors) && count($errors) > 0)
    @push('scripts')
        <script>
            $(function () {
                setTimeout(function () {
                    $('#modal_error_message').modal('show');
                }, 500);
            });
        </script>
    @endpush
@endif