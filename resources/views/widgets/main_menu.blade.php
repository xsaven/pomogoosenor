<header class="c-header js-header @if(Request::segment(1) == 'testing') c-header__noborder @endif {{$config['classes']}} @if($logged_in) login @endif">
    <div class="container">
        <div class="c-header__wrp clearfix" id="app">
            <div class="c-header__left">
                <button type="button" class="c-header__menu_small-trigger"><i></i><i></i><i></i></button>
                <a href="/" class="c-header__logo"><span>Pomogoo</span></a>
                <ul class="h-list c-header__menu_small">
                    <li><a href="{{url('/how-it-work')}}">Как это работает</a></li>
                    <li><a href="{{ url('/how-performer') }}">Как стать исполнителем</a></li>
                    <li><a href="{{url('blog/posts')}}">Наш блог</a></li>
                    <li><a href="{{ url('/customer-review') }}">Отзывы заказчиков</a></li>
                    <li><a href="{{ url('/rules') }}">Правила сервиса</a></li>
                    <li><a href="{{ url('/faq') }}">Частые вопросы</a></li>
                    <li><a href="{{ url('/contacts') }}">Контакты</a></li>
                </ul>
            </div>
            @if($config['display'] == 'all')
                <div class="c-header__center">
                    <ul class="c-header__menu_big h-list_inline-block">
                        <li class="menu__create"><a href="#" class="menu__create-link">Создать задание</a>
                            <ul class="h-list menu__create-list" id="create_task_menu">
                                @foreach($cat_tree as $parent)
                                    <li class="menu__create-list-item">
                                        <a href="{{route('task.new', ['category' => $parent->slug, 'subcategory' => $parent->defaultSubcategory()->slug, 'select' => true])}}"
                                           class="menu__create-list-link">{{$parent->name}}</a>
                                        <ul class="h-list menu__create-sub-list">
                                            <li class="title-link-item">
                                                <a href="{{route('task.new', ['category' => $parent->slug, 'subcategory' => $defaultSlug, 'select' => true])}}">{{$parent->name}}</a>
                                            </li>
                                            @if($parent->subcategories)
                                                @foreach($parent->subcategories as $child)
                                                    <li>
                                                        <a href="{{route('task.new', ['category' => $parent->slug, 'subcategory' => $child->slug, 'select' => true])}}">{{$child->name}}</a>
                                                    </li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="menu__all"><a href="{{route('task.all')}}">Все задания</a></li>
                        <li class="menu__performers"><a href="{{route('executors.index')}}">Исполнители</a></li>
                        @if($logged_in)
                            <li class="menu__where"><a href="{{route('task.list')}}">Мои заказы</a></li>
                        @else
                            <li class="menu__where"><a href="{{url('/how-it-work')}}">Куда я попал?</a></li>
                        @endif
                    </ul>
                </div>
                <div class="c-header__right">
                    @if(!$logged_in)
                        <a data-toggle="modal" data-target="#fastregmail" href="#">
                            <svg width="1em" height="1em" class="icon icon-reg ">
                                <use xlink:href="{{symbolSvg('icon-reg')}}"></use>
                            </svg>
                            <span>Регистрация</span>
                        </a>
                        <a data-toggle="modal" data-target="#loginModal" href="#">
                            <svg width="1em" height="1em" class="icon icon-lock ">
                                <use xlink:href="{{symbolSvg('icon-lock')}}"></use>
                            </svg>
                            <span>Вход</span>
                        </a>
                    @else
                        @if(user()->is_worker())
                            <a href="#" class="c-header__right-money">
                                @if(user()->profile && user()->profile->balance != 0)
                                    {{user()->profile->balance}}
                                @endif
                                ₽</a>
                        @endif
                        <event-counter :event_count='{!! user()->newMsgSum() !!}'
                                       :id="{{auth()->id()}}">
                        </event-counter>

                        <div class="c-header__right-user c-header__right-item">
                            <div class="c-header__right-user-ava-wrp">
                                <div class="c-header__right-user-ava h-object-fit">
                                    <img src="{{ \Auth::user()->avatar() }}" alt=""/>
                                </div>
                                <span>{{ \Auth::user()->name }}</span>
                            </div>
                            <div class="c-header__right-user-menu">
                                <a href="{{route('profile_main')}}" class="user-menu__item">
                                    <svg width="1em" height="1em" class="icon icon-user-1 ">
                                        <use xlink:href="{{symbolSvg('icon-user-1')}}"></use>
                                    </svg>
                                    <span>Профиль</span>
                                </a>
                                <a href="{{route('profile_tab',['tab'=>'favorites'])}}" class="user-menu__item">
                                    <svg width="1em" height="1em" class="icon icon-heart ">
                                        <use xlink:href="{{symbolSvg('icon-heart')}}"></use>
                                    </svg>
                                    <span>Избранное</span>
                                </a>
                                @if(user()->is_worker())
                                    <a href="{{route('profile_tab',['tab'=>'payment'])}}" class="user-menu__item">
                                        <svg width="1em" height="1em" class="icon icon-pig ">
                                            <use xlink:href="{{symbolSvg('icon-pig')}}"></use>
                                        </svg>
                                        <span>На счете {{ \Auth::user()->bill() }} ₽</span>
                                    </a>
                                @endif
                                <a href="{{route('profile_tab',['tab'=>'settings'])}}" class="user-menu__item">
                                    <svg width="1em" height="1em" class="icon icon-settings ">
                                        <use xlink:href="{{symbolSvg('icon-settings')}}"></use>
                                    </svg>
                                    <span>Настройки</span>
                                </a>
                                <a href="{{url('logout')}}" class="user-menu__item">
                                    <svg width="1em" height="1em" class="icon icon-exit ">
                                        <use xlink:href="{{symbolSvg('icon-exit')}}"></use>
                                    </svg>
                                    <span>Выход</span>
                                </a>
                            </div>
                        </div>


                    @endif

                </div>
            @endif

        </div>
    </div>
</header>




