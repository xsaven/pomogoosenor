@if(!empty($seo_data))
    @php($base_photo = 'https://images.pexels.com/photos/273222/pexels-photo-273222.jpeg?w=480')
<meta name="description" content="{{ $seo_data->description }}">
<meta name="keywords" content="{{ $seo_data->keywords }}">
<meta property="og:title" content="{{ $seo_data->social_data['og_title'] }}" />
<meta property="og:type" content="{{ $seo_data->social_data['og_description'] }}" />
<meta property="og:url" content="{{ $seo_data->social_data['og_url'] ?? url()->current() }}" />
<meta property="og:image" content="{{ $seo_data->social_data['og_image'] ?? $base_photo }}" />

<meta name="twitter:card" content="{{ $seo_data->social_data['twitter_card'] }}">
<meta name="twitter:site" content="@pomogoo">
<meta name="twitter:title" content="{{ $seo_data->social_data['twitter_title'] }}">
<meta name="twitter:description" content="{{ $seo_data->social_data['twitter_description'] }}">
<meta name="twitter:image" content="{{ $seo_data->social_data['twitter_image'] ?? $base_photo }}">
<meta name="twitter:url" content="{{ $seo_data->social_data['twitter_url'] ?? url()->current() }}">
<link rel="image_src" href="{{ url('/').$seo_data->social_data['image_src'] ?? $base_photo }}" />
@endif