@extends('layouts.profile')

@section('title') Создание нового альбома @stop

@section('content')
    <div class="l-profile__left">
        <div class="l-profile__album">
            <div class="l-profile__album-back">
                <a href="{{route('profile_main')}}">
                    <svg width="1em" height="1em" class="icon icon-arrow ">
                        <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-arrow"></use>
                    </svg>
                    <span>Вернуться в профиль</span>
                </a>
            </div>
            <h2 class="l-profile__album-ttl">Редактирование альбома</h2>
            <form class="l-profile__album-form" method="post" action="{{route('galleries.update')}}" id="galleriesForm">
                {{csrf_field()}}
                <input type="hidden" value="{{$gallery->id}}" name="id">
                <div class="l-profile__album-form-wrp">
                    <label class="clearfix">
                        <span>Название*</span>
                        <input type="text" value="{{$gallery->title}}" name="title"
                               placeholder="Например: «День Рождения» или «Оформления свадьбы»" class="form-control">
                    </label>
                    <label class="clearfix"><span>Описание*</span>
                        <textarea
                                placeholder="Опишите какие работы представлены в этом альбоме, в чем их особенность, когда они были выполнены, в каких целях и т.д. "
                                name="description" class="form-control">{{$gallery->description}}</textarea>
                    </label>
                </div>
                <div class="l-profile__album-form-btns">
                    <div class="l-profile__album-form-btns-wrp clearfix">
                        <label class="but but_bondi">
                            Добавить файл
                            <input type="file" accept="image/*" class="hidden" multiple onchange="readURL(this);"/>
                        </label>
                        <button type="submit" class="but but_bondi">
                            <span>Сохранить и  вернуться в профиль</span>
                        </button>
                    </div>
                    <p>Размер фотографии должен быть больше 1000x1000 пикселей</p>
                </div>
            </form>
            <div class="l-profile__album-upload">
                <div class="row" id="gallery_photos">
                    <div class="col-md-6 col-sm-6 clone hidden">
                        <div class="album-upload__item">
                            <button type="button" class="delete"><i class="delete-cross"></i>
                                <svg width="1em" height="1em" class="icon icon-back ">
                                    <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-back"></use>
                                </svg>
                            </button>
                            <div class="h-object-fit">
                                <img src="s/images/tmp_file/upl1.jpg" alt=""></div>
                            <input type="hidden" name="is_preview" value="0">
                            <div class="album-upload__item-txt">Использовать как превью</div>
                        </div>
                    </div>
                    @foreach($gallery->content_items as $key => $item)
                        <div class="col-md-6 col-sm-6 old-items-container">
                            <div class="album-upload__item">
                                <input type="hidden" class="old-items" value="{{$item->id}}" name="images[{{$key}}]">
                                <button type="button" class="delete"><i class="delete-cross"></i>
                                    <svg width="1em" height="1em" class="icon icon-back">
                                        <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-back"></use>
                                    </svg>
                                </button>
                                <div class="h-object-fit">
                                    <img src="{{url($item->file)}}" alt=""></div>

                                <input type="hidden" name="is_preview" value="{{$item->gallery_content->is_avatar}}">
                                <div class="album-upload__item-txt">
                                    @if($item->gallery_content->is_avatar)
                                        Установлено как превью
                                    @else
                                        Использовать как превью
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="l-profile__album-back">
                <a href="{{route('profile_main')}}">
                    <svg width="1em" height="1em" class="icon icon-arrow ">
                        <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-arrow"></use>
                    </svg>
                    <span>Вернуться в профиль</span>
                </a>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        var files_to_upload = [];

        $('.old-items-container').each(function (item, element) {
            $(element).find('button').click(function () {
                $(element).remove();
                files_to_upload = $.grep(files_to_upload, function (e) {
                    return e.name != file.name && e.lastModified != file.lastModified;
                });
                input.files = files_to_upload;
            });

            $(element).find('.album-upload__item').click(function () {
                $('[name="is_preview"]').val(0);
                $('.album-upload__item-txt').html('Использовать как превью');
                $(element).find('[name="is_preview"]').val(1);
                $(element).find('.album-upload__item-txt').html('Установлено как превью');
            });
        });

        function readURL(input) {
            var files = input.files;
            if (files) {
                $(files).each(function (key, file) {

                    if (file.size > 1048576) {
                        showErrorModal("Изображение слишком большое");
                        return false;
                    }

                    files_to_upload.push(file);
                    var reader = new FileReader();
                    reader.onload = function (e) {

                        var div = $('.clone.hidden').clone();
                        div.removeClass('hidden');
                        div.removeClass('clone');
                        div.find('.album-upload__item').addClass('new-image');
                        var img = $(div).find('img');
                        img.attr('src', e.target.result);
                        div.appendTo('#gallery_photos');
                        div.find('.album-upload__item').click(function () {
                            $('[name="is_preview"]').val(0);
                            $('.album-upload__item-txt').html('Использовать как превью');
                            div.find('[name="is_preview"]').val(1);
                            div.find('.album-upload__item-txt').html('Установлено как превью');
                        });
                        div.find('button').click(function () {
                            div.remove();
                            files_to_upload = $.grep(files_to_upload, function (e) {
                                return e.name != file.name && e.lastModified != file.lastModified;
                            });
                            input.files = files_to_upload;
                            console.log(files_to_upload);
                        })
                    };

                    reader.readAsDataURL(file);
                })
            }
        }

        $('#galleriesForm').on('submit', function (e) {
            e.preventDefault();
            var formData = new FormData(e.target);

            if(files_to_upload.length === 0 && $('.old-items').length === 0) {
                showErrorModal('Сначала добавьте фотографии');
                return false;
            }

            $(files_to_upload).each(function (index, file) {
                formData.append('file[' + index + ']', file);
            });

            $('.old-items').each(function (index, value) {
                formData.append('images[' + index + ']', value.value);
            });

            let checkedPreview = jQuery(':input[name=is_preview][value=1]').first().closest('.album-upload__item').find('.old-items').val()

            if (typeof checkedPreview !== 'undefined') {
                formData.append('preview', checkedPreview);
            } else {
                $('.new-image').each(function (index, element) {
                    if ($(element).find(':input[name=is_preview][value=1]').length !== 0) {
                        formData.append('preview_new', index);
                    }
                })
            }

            $.ajax({
                url: '/galleries/update',
                data: formData,
                type: 'POST',
                contentType: false,
                processData: false
            }).done(function () {
                location.replace('/profile');
            })
                .fail(function (result) {
                    var error_message = result.responseJSON.error.message;
                    if (error_message) {
                        showErrorModal(error_message);
                    }
                });
            return false;
        });

        function showErrorModal(errors) {
            var errors = Array.isArray(errors) ? errors : [errors];
            var modal = $('#modal_error_message');
            var error_div = modal.find('.alert.alert-error');
            modal.modal('show');
            error_div.html('');
            $(errors).each(function (key, error) {
                error_div.append('<p>' + error + '</p>');
            });
        }

    </script>

@endpush