@extends('layouts.profile')

@section('title') Создание нового альбома @stop

@section('content')
    <div class="l-profile__left">
        <div class="l-profile__album">
            <div class="l-profile__album-back">
                <a href="{{route('profile_main')}}">
                    <svg width="1em" height="1em" class="icon icon-arrow ">
                        <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-arrow"></use>
                    </svg>
                    <span>Вернуться в профиль</span>
                </a>
            </div>
            <h2 class="l-profile__album-ttl">Создание нового альбома</h2>
            <form class="l-profile__album-form" method="post" action="{{route('galleries.store')}}" id="galleriesForm">
                {{csrf_field()}}
                <div class="l-profile__album-form-wrp">
                    <label class="clearfix">
                        <span>Название*</span>
                        <input type="text" name="title" placeholder="Например: «День Рождения» или «Оформления свадьбы»"
                               class="form-control">
                    </label>
                    <label class="clearfix"><span>Описание*</span>
                        <textarea
                                placeholder="Опишите какие работы представлены в этом альбоме, в чем их особенность, когда они были выполнены, в каких целях и т.д. "
                                name="description" class="form-control"></textarea>
                    </label>
                </div>
                <div class="l-profile__album-form-btns">
                    <div class="l-profile__album-form-btns-wrp clearfix">
                        <label class="but but_bondi">
                            Добавить файл
                            <input type="file" accept="image/*" class="hidden" multiple onchange="readURL(this);"/>

                        </label>

                        <button type="submit" class="but but_bondi">
                            <span>Сохранить и  вернуться в профиль</span>
                        </button>
                    </div>
                    <p>Размер фотографии должен быть не больше 1 мб</p>
                </div>
            </form>
            <div class="l-profile__album-upload">
                <div class="row" id="gallery_photos">
                    <div class="col-md-6 col-sm-6 clone hidden">
                        <div class="album-upload__item">
                            <button type="button" class="delete"><i class="delete-cross"></i>
                                <svg width="1em" height="1em" class="icon icon-back ">
                                    <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-back"></use>
                                </svg>
                            </button>
                            <div class="h-object-fit">
                                <img src="s/images/tmp_file/upl1.jpg" alt="">
                            </div>

                            <input type="hidden" name="is_preview" value="0">

                            <div class="album-upload__item-txt">Использовать как превью</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="l-profile__album-back">
                <a href="{{route('profile_main')}}">
                    <svg width="1em" height="1em" class="icon icon-arrow ">
                        <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-arrow"></use>
                    </svg>
                    <span>Вернуться в профиль</span>
                </a>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        var files_to_upload = [];

        function readURL(input) {
            var files = input.files;
            if (files) {
                $(files).each(function (key, file) {

                    if (file.size > 1048576) {
                        showErrorModal("Изображение слишком большое");
                        return false;
                    }

                    files_to_upload.push(file);
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var div = $('.clone.hidden').clone();
                        div.removeClass('hidden');
                        div.removeClass('clone');
                        div.find('.album-upload__item').addClass('new-image');
                        var img = $(div).find('img');
                        img.attr('src', e.target.result);
                        div.appendTo('#gallery_photos');
                        div.find('.album-upload__item').click(function () {
                            $('[name="is_preview"]').val(0);
                            $('.album-upload__item-txt').html('Использовать как превью');
                            div.find('[name="is_preview"]').val(1);
                            div.find('.album-upload__item-txt').html('Установлено как превью');
                        });
                        div.find('button').click(function () {
                            div.remove();
                            files_to_upload = $.grep(files_to_upload, function (e) {
                                return e.name != file.name && e.lastModified != file.lastModified;
                            });
                            input.files = files_to_upload;
                            console.log(files_to_upload);
                        })
                    };

                    reader.readAsDataURL(file);
                })
            }
        }

        $('#galleriesForm').on('submit', function (e) {
            e.preventDefault();
            var formData = new FormData(e.target);

            if(files_to_upload.length === 0) {
                showErrorModal('Сначала добавьте фотографии');
                return false;
            }

            $(files_to_upload).each(function (index, file) {
                console.log(file.name);
                formData.append('file[' + index + ']', file);
            });

            $('.new-image').each(function (index, element) {
                if ($(element).find(':input[name=is_preview][value=1]').length !== 0) {
                    formData.append('preview_new', index);
                }
            });

            $.ajax({
                url: '/galleries',
                data: formData,
                type: 'POST',
                contentType: false,
                processData: false
            }).done(function () {
                location.replace('/profile');
            })
                .fail(function (result) {
                    var error_message = result.responseJSON.error.message;
                    if (error_message) {
                        showErrorModal(error_message);
                    }
                });
            return false;
        });

        function showErrorModal(errors) {
            var errors = Array.isArray(errors) ? errors : [errors];
            var modal = $('#modal_error_message');
            var error_div = modal.find('.alert.alert-error');
            modal.modal('show');
            error_div.html('');
            $(errors).each(function (key, error) {
                error_div.append('<p>' + error + '</p>');
            });
        }


    </script>

@endpush