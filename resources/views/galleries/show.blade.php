<?php /** @var \App\Galleries $gallery */ ?>
@extends('layouts.main')

@section('title') {{$gallery->title}} @stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="l-profile__left">
                    <div class="l-profile__album">
                        <div class="l-profile__album-back">
                            @if($gallery->user->id === $currentUser->id)
                                <a href="{{route('profile_main')}}">
                                    <svg width="1em" height="1em" class="icon icon-arrow ">
                                        <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-arrow"></use>
                                    </svg>
                                    <span>Вернуться в профиль</span>
                                </a>
                                <a href="{{route('galleries.edit', ['gallery' => $gallery])}}">
                                    <svg width="1em" height="1em" class="icon icon-arrow ">
                                        <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-arrow"></use>
                                    </svg>
                                    <span>Редакрировать альбом</span>
                                </a>
                                <a href="{{route('galleries.delete', ['gallery' => $gallery])}}">
                                    <svg width="1em" height="1em" class="icon icon-arrow ">
                                        <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-arrow"></use>
                                    </svg>
                                    <span>Удалить альбом</span>
                                </a>
                            @endif
                        </div>
                        <h2 class="l-profile__album-ttl">{{$gallery->title}}</h2>
                    </div>
                </div>
                <div id="aniimated-thumbnials">
                    @foreach($gallery->content_items as $content_item)
                        <a href="{{asset($content_item->path)}}" class="col-md-6 col-sm-6">
                            <img src="{{asset($content_item->path)}}" alt="Картинка"/>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <link type="text/css" rel="stylesheet" href="/s/js/lib/lightgallery/src/css/lightgallery.css"/>

    <!-- lightgallery plugins -->
    {{--<script src="/s/js/lib/lightgallery/demo/js/lg-thumbnail.min.js"></script>--}}
    {{--<script src="/s/js/lib/lightgallery/demo/js/lg-fullscreen.min.js"></script>--}}
    <script>
        $(document).ready(function () {
            lightGallery(document.getElementById('aniimated-thumbnials'), {
                thumbnail: true
            });
        });
    </script>
@endpush