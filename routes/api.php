<?php

//use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Illuminate\Http\Request $request) {
//    return $request->user();
//});

Route::group(['as' => 'api.'],function () {
    Route::post('/{provider}/auth', 'API\Auth\ApiLoginController@authSocial')->name('social_auth');

    Route::post('login', 'API\Auth\ApiLoginController@login')->name('login');
    Route::post('refresh-token', 'API\Auth\ApiLoginController@refresh')->name('refresh');
    Route::post('register', 'API\Auth\ApiRegisterController@register')->name('register');

//// Password Reset Routes...
//Route::post('password/reset', 'API\Auth\ResetPasswordController@sendResetEmail');
    Route::post('password/email', 'API\Auth\ResetPasswordController@sendResetLinkEmail');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    /*
     * Check auth
     */
    Route::group(['middleware' => ['jwt-auth', 'online-status','is-force-authorized']], function () {
        /*
     * Main page Route
     */
        Route::get('/', 'API\HomeController@index');

        /*
         * Social AUTH
         */


        /*
         * Post Route
         */
        Route::group(['prefix' => 'blog'], function () {
            Route::get('post/{slug}', 'API\BlogController@show');
            Route::get('posts', 'API\BlogController@index');
        });

        /*
        * Register Route
        */

// Authentication Routes...

// Registration Routes...
//Route::get('register', 'API\Auth\RegisterController@ApiRegisterController');


        /*
         * Auth user
         */
        /*
         * Logout
         */
        Route::get('logout', 'API\Auth\ApiLoginController@logout')->name('logout');
        /*
        * Testing
        */
        Route::group(['prefix' => 'testing'], function () {
            Route::get('steps-all', 'API\TestingController@getAllSteps');
            Route::post('fill-info', 'API\ApiProfileController@postFillInfo');
            Route::post('submit-step', 'API\TestingController@submitStep');
        });

        Route::group(['prefix' => 'verification'], function () {
            Route::post('phone', 'API\VerificationController@sendPhoneVerification');
            Route::get('phone', 'API\VerificationController@checkPhoneVerification');
        });

        Route::group(['prefix' => 'notifications','as'=>'notifications.','namespace'=>'API\Notifications'], function (){
           Route::post('set-one-signal-user','NotificationsController@setOneSignalUser')->name('set-one-signal-user');
        });

        Route::group(['prefix' => 'chat', 'as'=>'chat.'], function () {
            Route::get('/initialize', 'ChatController@initializeChat')->name('initialize');
            Route::get('/thread', 'ChatController@getThreadMessage')->name('get-thread');
            Route::post('/send-message', 'ChatController@sendMessage')->name('send-message');
            Route::post('/mark-as-read', 'ChatController@markAsRead')->name('mark-as-read');


        });

        Route::group(['as'=>'tasks.','prefix'=>'tasks'],function(){
            Route::post('/','API\Tasks\TasksController@store');
            Route::group(['as'=>'drafts.'],function() {
                Route::post('/drafts','API\Tasks\TasksController@storeDraft')->name('store');
                Route::get('/drafts','API\Tasks\TasksController@getDrafts')->name('index');
            });
            Route::post('/{task}','API\Tasks\TasksController@update');
            Route::post('/{task}/report','API\Tasks\ReportsController@store')->name('report');
            Route::group(['as'=>'reviews.'],function() {
                Route::post('/{task}/review', 'API\Tasks\ReviewsController@store')->name('store');
                Route::put('/{task}/review/{review}', 'API\Tasks\ReviewsController@update')->name('update');
            });
            Route::group(['as'=>'offers.'],function() {
                Route::post('/{task}/offer', 'API\Tasks\OffersController@store')->name('store');
            });
            Route::group(['as'=>'comments.','namespace'=>'API\Tasks'], function () {
                Route::post('/{task}/comments', 'CommentsController@store')->name('store');
                Route::delete('/{task}/comments/{comment}', 'CommentsController@destroy');
            });
            Route::get('/my','API\Tasks\TasksController@myTasks');
            Route::get('/all','API\Tasks\TasksController@all');
            Route::get('/statistic','API\Tasks\TasksController@getStatistic');
            Route::get('/suggest-search','API\Tasks\TasksController@searchSuggest');
            Route::get('/search-data','API\Tasks\TasksController@getSearchData');
            Route::post('/{task}/cancel','API\Tasks\TasksController@cancel');
            Route::post('/{task}/executor','API\Tasks\TasksController@selectExecutor');
            Route::get('/{task}','API\Tasks\TasksController@show');
        });

        Route::group(['prefix' => 'content-upload'], function () {
            Route::post('/', 'API\ContentController@store');
        });



        Route::group(['prefix' => 'template'], function () {
            Route::get('/', 'API\TemplateController@index');
            Route::post('/', 'API\TemplateController@store');
            Route::put('/{template}', 'API\TemplateController@update');
            Route::delete('/{template}', 'API\TemplateController@destroy');
        });

        Route::group(['prefix' => 'transactions'], function () {
            Route::get('/', 'API\TransactionsController@index');
        });

        Route::group(['as' => 'payments.', 'prefix' => 'payments'], function(){
            Route::post('pay', 'API\Payment\PaymentsController@pay')->name('pay');
            Route::get('redirect', 'API\Payment\PaymentsController@redirect')->name('redirect');
            Route::post('bind-card', 'API\Payment\PaymentsController@bindCard')->name('bind-card');
            Route::delete('unbind-card', 'API\Payment\PaymentsController@unbindCard')->name('unbind-card');
            Route::post('pay', 'API\Payment\PaymentsController@pay')->name('pay');
            Route::get('in/{hash}', 'API\Payment\PaymentsController@in')->name('in');
            Route::get('test', 'API\Payment\PaymentsController@test')->name('test');
            Route::group(['as' => 'sbr.', 'prefix' => 'sbr', 'namespace' => 'Payment'], function(){
                Route::get('index','SBRController@index')->name('index');
                Route::get('bind-payer-card','SBRController@bindPayerCard')->name('bind-payer-card');
                Route::match(['get', 'post'],'handle-bind-payer-card','SBRController@handleBindPayerCard')->name('handle-bind-payer-card');
                Route::get('bind-beneficiary-card','SBRController@400')->name('bind-beneficiary-card');
                Route::match(['get', 'post'],'handle-bind-beneficiary-card','SBRController@handleBindBeneficiaryCard')->name('handle-bind-beneficiary-card');
                Route::get('pay/{task}','SBRController@pay')->name('pay');
                Route::match(['get', 'post'],'handle-pay','SBRController@handlePay')->name('handle-pay');
            });
        });

        /*
         *  Profile
         */
        Route::group(['as'=>'profile.','prefix' => 'profile'], function () {
            Route::group(['prefix' => 'gallery'], function () {
                Route::get('/', 'API\GalleryController@index')->name('show  ');
                Route::post('/', 'API\GalleryController@store');
                Route::get('/examples', 'API\GalleryController@getExamples');
                Route::post('/{gallery}', 'API\GalleryController@update');
                Route::delete('/{gallery}', 'API\GalleryController@destroy');
            });

            Route::get('menu-info', 'API\ApiProfileController@getMenuInfo')->name('menu_info');
            Route::get('/', 'API\ApiProfileController@index');
            Route::get('/{user}', 'API\ApiProfileController@show');
            Route::get('/{user}/reviews', 'API\Tasks\ReviewsController@index');
            Route::post('prevalidate', 'API\ApiProfileController@postPrevalidate');
            Route::put('auth-credentials', 'API\ApiProfileController@changeAuthCredentials');
            Route::post('create', 'API\ApiProfileController@postCreateProfile');
            Route::put('/', 'API\ApiProfileController@update');
            Route::put('subscribe', 'API\SubscribeController@subscribe')->name('subscribe');
            Route::post('subscribe-category/{category}', 'API\SubscribeController@subscribeCategory')->name('subscribe');
            Route::delete('unsubscribe-category/{category}', 'API\SubscribeController@unsubscribeCategory')->name('subscribe');

            Route::post('avatar', 'API\ApiProfileController@apiLoadAvatar')->name('load_avatar');
            Route::put('about-me', 'API\ApiProfileController@updateAboutMe')->name('update_about_me');

            Route::post('/attach-social/{provider}', 'API\Auth\ApiLoginController@authSocial')->name('attach_social');

        });

        Route::group(['prefix' => 'followers'], function () {
            Route::get('/', 'API\FavoritesController@followers');
            Route::get('/{user}', 'API\FavoritesController@userFollowers');
            Route::post('/{user}', 'API\FavoritesController@store')->name('follow');
            Route::delete('/{user}', 'API\FavoritesController@remove')->name('unfollow');
        });

        Route::group(['prefix' => 'lists'], function () {
            Route::get('/categories', 'API\ListsController@getCategories');
            Route::get('/flat-categories', 'API\ListsController@getFlatCategories');
            Route::get('/cards', 'API\ListsController@getWOCards');
        });

        Route::group(['prefix'=>'notifications','as'=>'notifications.'],function (){
            Route::get('/', 'NotificationsController@index');
            Route::post('/read-all', 'NotificationsController@readAll');
        });

        /*
        * Get JSON City
        */

        /*
         * Test
         */
        Route::get('test', function () {
            //$offer = \App\TaskOffers::find(238);
//            $comment = \App\TaskComments::first();

//            user()->notify(new \App\Notifications\TaskOfferCreated($offer));
//            user()->notify(new \App\Notifications\CommentCreated($comment));
//            user()->notify(new \App\Notifications\DataVerified());
//            $message = [
//                'offer' => [
//                    'id' => $offer->id,
//                ],
//                'task' => [
//                    'id' => $offer->task->id,
//                    'name' => $offer->task->name
//                ]
//            ];
//            $notification = \App\UserNotification::create([
//                'user_id' => user()->id,
//                'message' => $message,
//                'type' => \App\UserNotification::TYPE_OFFER_CREATED,
//            ]);
//            return $notification;
        });

        Route::post('/broadcasting/endpoint',function(){
            return Broadcast::auth(request());
        });
    });

    Route::group(['prefix' => 'location'], function () {
        Route::get('find-city', 'API\LocationController@findCity')->name('find_city');
        Route::get('cities', 'API\LocationController@getAllCities')->name('all_cities');
    });

});