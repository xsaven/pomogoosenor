<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/**
 * Route for UI
 * TODO Delete if it becomes unnecessary
 */
Route::get('ui', function () {
    return (new \App\Helpers\SVGViewer\SvgViewer('s/images/useful/svg/theme/symbol-defs.svg'))->rengerSvg();
});

Route::get('lara-logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::get('hack-test', function () {
    //$task = \App\UserTasks::find(469);

//    $deal = new \App\Services\WalletOneSafeDeal\WOSafeDeal();
////        $data = $deal::deal()->confirmDeal($task->deal->id);;
//
//    $data = $deal::payer()->getPayerRefunds($task->creator_id);
////    $data = $deal::deal()->cancelDeal($task->deal->id);;
////    $data = $deal::beneficiary()->getBeneficiaryPayouts($task->executor_id);
////    $data = $deal::deal()->completeDeal($task->deal->id);
//    dd($data);

    /*$task->update([
        'executor_id' => 0,
        'status' => 1,
        'executor_card_id' => null
    ]);
    \App\Review::whereTaskId($task->id)->forceDelete();
    \App\UserTaskDeal::query()->delete();*/

    dd("А хрен там! =)");


//    \App\User::find($task->creator_id)->notify(new \App\Notifications\PaymentStatusChanged($task, false));
});

//Route::get('test', function () {
////    $task = \App\UserTasks::find(request('task_id'));
//    $base_url = 'https://maps.googleapis.com/maps/api/geocode/json?';
//    $params = '&language=en&result_type=administrative_area_level_1';
//    $client = new \GuzzleHttp\Client();
////    $lat = $task->data['address'][0]['lat'] ?? false;
////    $lon = $task->data['address'][0]['lon'] ?? false;
//    $lat = '55.755826';
//    $lon = '37.617300';
//    if ($lat && $lon) {
//        $full_url = $base_url . 'latlng=' . $lat . ',' . $lon . '&key=' . config('services.google.maps_api_key').$params;
////        dd($full_url);
//        try {
//            $result =  json_decode_try($client->get($full_url)->getBody()->getContents());
//            $city = $result['results'][0]['address_components'][0]['long_name'];
//            if (strpos($city,' Oblast')) {
//                $city = stristr($city, ' Oblast', true);
//            }
//        }catch (Exception $e){
//            $city = \App\City::first()->name;
//        }
//    }
//    dd($city);
//    return '';
//    $safeDeal = new App\Services\WalletOneSafeDeal\WOSafeDeal();
//    return $safeDeal::deal()->getDeal('59113ed6-a95d-4e9b-a742-5c2ca20cad2f');
//    return $safeDeal::web()->payDeal([
//        "PlatformDealId" => 'c7769d6e-5233-11e8-9c2d-fa7ae01bbebc', // Идентификатор сделки на стороне площадки
//        "ReturnUrl" => 'http://pom.laraman.ru/payments', // Урл возврата пользователя
//    ]);
//});

Route::post('payments', 'Payment\SBRController@handleWalletOneRequest');


//Route::get('/', 'HomeController@index')->name('home');

Route::post('pay/in_request', 'Payment\ProcessorController@in_request')->name('in_request');

Route::group(['middleware' => ['web']], function () {

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index');

    Auth::routes();

    Route::post('/finish-register/prevalidate', 'Auth\RegisterController@ajaxValidate')->name('register_prevalidate');

    /*
     * Tasks group
     * */
    Route::group(['as' => 'task.', 'prefix' => 'task', 'namespace' => 'Tasks'], function () {
        Route::group(['middleware' => 'auth'], function () {
            Route::get('list', 'ListController@index')->name('list');
            Route::post('report/{id}', 'PreviewController@report')->name('report');
            Route::post('comment/{userTasks}', 'PreviewController@comment')->name('comment');
            Route::get('edit/{category}/{subcategory}/{userTasks}', 'EditController@index')->name('edit');
            Route::post('edit/{category}/{subcategory}/{userTasks}', 'EditController@edit')->name('edit');
            Route::post('field_edit/{userTasks}/{field}', 'EditController@field_edit')->name('edit.field');
            Route::post('/{task}/executor', 'EditController@selectExecutor')->name('edit.select-executor');
        });
        Route::post('store', 'NewController@storeOldData')->name('store.old');
        Route::get('all', 'ListController@all')->name('all');
        Route::get('sort', 'ListController@sort')->name('sort');
        Route::get('preview/{userTasks}', 'PreviewController@index')->name('review');

        Route::post('/{task}/review', 'ReviewsController@store')->name('review.store');
        Route::post('/{task}/offers', 'OffersController@store')->name('offers.store');
        Route::post('/offers/{offer}', 'OffersController@update')->name('offers.update');
        Route::delete('/offers/{offer}', 'OffersController@remove')->name('offers.remove');
        Route::get('/{task}/offers', 'PreviewController@getTaskReviews')->name('offers.sort');
        Route::get('new/{category}/{subcategory?}', 'NewController@index')->name('new');
        Route::get('copy/{userTasks}', 'NewController@copy_new')->name('copy');
        Route::post('create/{category}/{subcategory}', 'NewController@create')->name('create');
        Route::post('search', 'NewController@search')->name('search');
    });

    /*
     * Payment grout
     */

    Route::group(['as' => 'payment.', 'prefix' => 'payment', 'namespace' => 'Payment', 'middleware' => 'auth'], function () {
        Route::post('pay', 'ProcessorController@pay')->name('pay');
        Route::get('in/{hash}', 'ProcessorController@in')->name('in');
        Route::get('test', 'ProcessorController@test')->name('test');
        Route::get('cancel', 'ProcessorController@cancel')->name('cancel');
        Route::group(['as' => 'sbr.', 'prefix' => 'sbr'], function () {
            Route::get('index', 'SBRController@index')->name('index');
            Route::get('bind-payer-card', 'SBRController@bindPayerCard')->name('bind-payer-card');
            Route::match(['get', 'post'], 'handle-bind-payer-card', 'SBRController@handleBindPayerCard')->name('handle-bind-payer-card');
            Route::get('bind-beneficiary-card', 'SBRController@bindBeneficiaryCard')->name('bind-beneficiary-card');
            Route::match(['get', 'post'], 'handle-bind-beneficiary-card', 'SBRController@handleBindBeneficiaryCard')->name('handle-bind-beneficiary-card');
            Route::post('pay/{task}', 'SBRController@pay')->name('pay');
            Route::match(['get', 'post'], 'handle-pay', 'SBRController@handlePay')->name('handle-pay');
        });
    });

    Route::group(['as' => 'executors.', 'prefix' => 'executors'], function () {
        Route::get('/', 'ExecutorsController@index')->name('index');
        Route::get('/sort', 'ExecutorsController@sort')->name('sort');
        Route::post('/offer-job', 'ExecutorsController@offerJob')->name('offer-job');
    });

    Route::group(['prefix' => 'templates'], function () {
        Route::get('/list', 'API\TemplateController@index');
        Route::post('/store', 'API\TemplateController@store');
        Route::put('/{template}', 'API\TemplateController@update');
        Route::delete('/{template}', 'API\TemplateController@destroy');
    });

    Route::group(['prefix' => 'profile'], function () {
        Route::get('/{user}', 'ProfileController@show')->name('profile_show')->where('user', '[0-9]+');;
    });

    /*
     * Get Register user email (modal register)
     */
//    Route::middleware(['guest'])->group(function () {

    Route::post('/get/email/register', 'Auth\RegisterController@getEmail')->name('get_email');

    /*
    * Google
    */
    Route::get('/google/login', 'Auth\SocialLoginContoller@redirectToGoogleProvider')->name('social_google_auth');
    Route::get('/google/callback', 'Auth\SocialLoginContoller@handleGoogleProviderCallback')->name('social_google_callback');

    /*
     * Facebook
     */
    Route::get('/facebook/login', 'Auth\SocialLoginContoller@redirectToFacebookProvider')->name('social_facebook_auth');
    Route::get('/facebook/callback', 'Auth\SocialLoginContoller@handleFacebookProviderCallback')->name('social_facebook_callback');

    /*
     * Vkontakte
     */
    Route::get('/vk/login', 'Auth\SocialLoginContoller@redirectToVkProvider')->name('social_vk_auth');
    Route::get('/vk/callback', 'Auth\SocialLoginContoller@handleVkProviderCallback')->name('social_vk_callback');

    /*
     * Mail ru
     */
    Route::get('/mailru/login', 'Auth\SocialLoginContoller@redirectToMailProvider')->name('social_mailru_auth');
    Route::get('/mailru/callback', 'Auth\SocialLoginContoller@handleMailProviderCallback')->name('social_mailru_callback');
//    });

    /*
     * Congratulation
     */
    Route::get('/email/congratulation', 'Auth\RegisterController@congratulation')->name('congratulation');

    /*
     * Verification email
     */

    Route::get('/email/verification/{token}/{email}', 'Auth\RegisterController@checkToken')->name('email_verified');

    /*
     * Step 2 email register (After modal)
     */

    Route::get('/finish/registration', 'Auth\RegisterController@endRegister')->name('finish_register');

    Route::get('/finish/social/registration', 'Auth\SocialLoginContoller@endRegisterSocial')->name('finish_register_social');

    /*
     * Save data (step 2)
     */
    Route::post('create', 'ProfileController@postCreateProfile')->name('profile_create');


    /*
     * ????
     */
    Route::get('pick-email', 'Auth\LoginController@pickeEmail');
    Route::post('handle-email', 'Auth\LoginController@handleEmail');

    /*
     * Get JSON City
     */
    Route::group(['prefix' => 'location'], function () {
        Route::match(['get', 'post'], 'find-city', 'LocationController@postFindCity')->name('find_city');
    });

    Route::match(['get', 'post'], 'find-subcategory', 'SubcategoryController@findSubcategory')->name('find_subcategory');


//Route::post('/get/city/all','')


    Route::group(['middleware' => 'auth'], function () {

        Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
            Route::post('update_settings', 'Admin\SettingsController@update');
            Route::post('home/update', 'Admin\HomeController@update')->name('admin.home.update');
            Route::get('workerrequest/{user_id}', 'Admin\WorkerRequestController@show');
            Route::post('moderate_worker', 'Admin\WorkerRequestController@update');
            Route::get('vacancy', 'Admin\VacancyController@index')->name('admin.vacancy');
            Route::get('vacancy/create', 'Admin\VacancyController@add')->name('admin.vacancy.add');
            Route::post('vacancy/create', 'Admin\VacancyController@create')->name('admin.vacancy.create');
            Route::get('vacancy/delete/{id}', 'Admin\VacancyController@delete')->name('admin.vacancy.delete');
            Route::get('vacancy/edit/{id}', 'Admin\VacancyController@edit')->name('admin.vacancy.edit');
            Route::post('vacancy/update', 'Admin\VacancyController@update')->name('admin.vacancy.update');
            Route::get('page/edit/{slug}', 'Admin\DynamicPagesController@edit')->name('admin.page.edit');
            Route::post('page/update', 'Admin\DynamicPagesController@update')->name('admin.page.update');
            Route::post('footer', 'Admin\FooterController@update')->name('admin.footer.update');
            Route::get('tasks', 'Admin\TaskController@index')->name('admin.tasks');
            Route::get('arbitration', 'Admin\ArbitrationTaskController@index')->name('admin.tasks.arbitration');
            Route::get('arbitration/review/{id}', 'Admin\ArbitrationTaskController@review')->name('admin.tasks.arbitration.review');
            Route::get('arbitration/confirm/{id}', 'Admin\ArbitrationTaskController@confirm')->name('admin.tasks.arbitration.confirm');
            Route::get('arbitration/cancel/{id}', 'Admin\ArbitrationTaskController@cancel')->name('admin.tasks.arbitration.cancel');
            Route::get('tasks/approve/{task}', 'Admin\TaskController@approve')->name('admin.tasks.approve');
            Route::get('tasks/unapprove/{task}', 'Admin\TaskController@unapprove')->name('admin.tasks.unapprove');
            Route::get('tasks/delete/{task}', 'Admin\TaskController@delete')->name('admin.tasks.delete');
            Route::get('reports/delete/{task}', 'Admin\TaskReportsController@deleteTask')->name('admin.reports.tasks.delete');
        });

        /*
         * Logout
         */
        Route::get('/logout', 'Auth\LoginController@logout');


        /*
        *  Send mail
        */
        Route::post('/send/register/email', 'Auth\RegisterController@sendEmail')->name('send_email');

        /*
         * Finish register (social)
         */
        Route::post('/finish/social/update', 'Auth\SocialLoginContoller@finishRegister')->name('finish_social');
        /*
         * Remove social relate
         */


        Route::group(['prefix' => 'profile'], function () {
            Route::get('/', 'ProfileController@index')->name('profile_main');
            Route::get('/transactions/{sort}', 'ProfileController@transactions')->name('profile_transactions');
            Route::get('/change-busy-state', 'ProfileController@changeBusyState')->name('change_busy_state');
            Route::get('/{tab}', 'ProfileController@index')->name('profile_tab')
                ->where('tab', 'settings|payment|favorites');
            Route::put('update', 'ProfileController@update')->name('profile_main_update');
            Route::post('update-about-me', 'ProfileController@updateAboutMe')->name('update_about_me');
            Route::post('update-min-price', 'ProfileController@updateMinPrice')->name('update_min_price');
            Route::put('/password/update', 'ProfileController@changePassword')->name('profile_password_update');
            Route::post('/subscribe', 'ProfileController@subscribe')->name('profile_subscribe');
            Route::delete('/profile/remove', 'ProfileController@destroy')->name('profile_remove');
            Route::post('upload-video', 'ProfileController@uploadVideo')->name('profile.upload_video');
            Route::post('save-settings', 'ProfileController@saveSettings')->name('profile.save_settings');
        });

        Route::group(['prefix' => 'galleries', 'as' => 'galleries.'], function () {
            Route::get('/create', 'GalleryController@create')->name('create');
            Route::get('/{gallery}', 'GalleryController@show')->name('show');
            Route::post('/update', 'GalleryController@update')->name('update');
            Route::get('/{gallery}/edit', 'GalleryController@edit')->name('edit');
            Route::get('/{gallery}/delete', 'GalleryController@delete')->name('delete');
            Route::post('/', 'GalleryController@store')->name('store');
        });

        Route::group(['as' => 'social.', 'prefix' => 'social'], function () {
            Route::get('/{provider}/attach', 'AttachSocialController@attach')->name('attach');
            Route::get('/{provider}/callback', 'AttachSocialController@handleProviderCallback')->name('callback');
            Route::get('/{provider}/detach', 'AttachSocialController@detach')->name('detach');
        });

        Route::group(['prefix' => 'lists'], function () {
            Route::get('/categories', 'API\ListsController@getCategories');
            Route::get('/flat-categories', 'API\ListsController@getFlatCategories');
            Route::get('/cards', 'API\ListsController@getWOCards');
        });

        Route::post('/workers/store', 'WorkerController@store')->name('worker_create');

        Route::post('ajax-load-avatar', 'ProfileController@ajaxLoadAvatar');
        Route::post('ajax-delete-avatar', 'ProfileController@ajaxDeleteAvatar');

        Route::group(['prefix' => 'testing', 'as' => 'testing.'], function () {
            Route::get('start', 'TestingController@maybeStartTesting')->name('start');
            Route::get('info', 'TestingController@step_info');
            Route::get('steps', 'TestingController@step_testing');
            Route::get('steps_all', 'TestingController@step_all');
            Route::get('start_steps', 'TestingController@startSteps');
            Route::post('fill_info', 'ProfileController@postFillInfo');
            Route::post('submit_step', 'TestingController@submitStep');
            Route::get('confirm', 'TestingController@confirmPage');
        });

        Route::group(['prefix' => 'followers', 'as' => 'web.followers.'], function () {
            Route::get('/', 'API\FavoritesController@followers')->name('list');
            Route::get('/{user}', 'API\FavoritesController@userFollowers')->name('list_by_user');
            Route::post('/{user}', 'API\FavoritesController@store')->name('follow');
            Route::delete('/{user}', 'API\FavoritesController@remove')->name('unfollow');
        });

        Route::group(['prefix' => 'verification'], function () {
            Route::post('phone', 'ProfileController@sendPhoneVerification')->name('send_phone_verification');
            Route::get('phone', 'ProfileController@checkPhoneVerification')->name('check_phone_verification');
        });


        /**
         * Chat Initialize
         */
        Route::get('/initialize-chat', 'ChatController@initializeChat')->name('initialize-chat');
        /**
         * Lot chat
         */
        Route::post('/send_lot_message', 'ChatController@sendMessage')->name('send-lot-message');
        Route::get('/get_lot_thread', 'ChatController@getLotThread')->name('get-lot-thread');
        Route::post('/get_lot_message', 'ChatController@getThreadMessage')->name('get-lot-message');
        /**
         * Global chat
         */
        Route::post('/send_global_message', 'ChatController@sendMessage')->name('send-global-message');
//        Route::get('/test', 'ChatController@getGlobalThread')->name('get-global-thread');
        Route::post('/get_global_message', 'ChatController@getThreadMessage')->name('get-global-message');
        //New User get for Chat
        Route::post('/get_new_chat_user', 'ChatController@getNewUserToThread')->name('new_chat_user');
        Route::post('/create_new_chat', 'ChatController@createNewChat')->name('create-new-chat');

        /*
         * Mark as read msg
         */

        Route::post('/mark_as_read', 'ChatController@markAsRead')->name('mark_as_read');

        Route::group(['prefix' => 'notifications', 'as' => 'notifications.'], function () {
            Route::get('/', 'NotificationsController@index');
            Route::post('/read-all', 'NotificationsController@readAll');
        });
    });


    Route::group(['prefix' => 'usersapi'], function () {
        Route::post('find', 'UsersApi@postFindUser');
    });
    Route::group(['prefix' => 'postsapi'], function () {
        Route::post('find', 'BlogController@findByName');
    });


    Route::group(['prefix' => 'blog'], function () {
        Route::get('post/{slug}', 'BlogController@show');
        Route::get('posts', 'BlogController@index');
    });


    Route::post('email-subscribe', 'SubscribeController@subscribe');


//static page
    Route::get('/how-it-work', 'Page\PageController@index');
    Route::get('/customer-review', 'Page\PageController@customerReview');
    Route::get('/performers-review', 'Page\PageController@performersReview');
    Route::get('/rewards', 'Page\PageController@rewards');
    Route::get('/how-performer', 'Page\PageController@howPerformer');
    Route::get('/no-risk', 'Page\PageController@noRisk');
    Route::get('/security', 'Page\PageController@security');
    Route::get('/mass-media', 'Page\PageController@massMedia');
    Route::get('/contacts', 'Page\PageController@contacts');
    Route::get('/rules', 'Page\PageController@rules');
    Route::get('/receipt', 'Page\PageController@receipt');
    Route::get('/faq', 'Page\PageController@faq');
    Route::get('/task-categories', 'Page\PageController@task_categories')->name('categories_list');
    Route::get('/vacancies', 'Page\VacanciesController@index')->name('vacancies');
    Route::get('/vacancy/{id}', 'Page\VacanciesController@vacancy')->name('vacancy');
    Route::post('/vacancy/{id}', 'Page\VacanciesController@save')->name('vacancy');





    Route::get('/cache-flush', 'CacheController@flush');

//end


    /*
     * Admin
     */

});


//Route::get('/page/how-it-works', 'Admin\WorkerRequestController@update');

/*
 * Clear
 */

//Route::get('/clear-cache', function () {
//    $exitCode = Artisan::call('optimize');
//    $exitCode = Artisan::call('cache:clear');
//    $exitCode = Artisan::call('view:clear');
//    $exitCode = Artisan::call('config:cache');
//    $exitCode = Artisan::call('config:clear');
//
//    // return what you want
//});
