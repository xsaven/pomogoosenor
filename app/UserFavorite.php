<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 30 Jan 2018 16:06:13 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * App\UserFavorite
 *
 * @property int $user_id
 * @property int $subscriber_id
 * @property-read \App\User $subscriber
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserFavorite whereSubscriberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserFavorite whereUserId($value)
 * @mixin \Eloquent
 */
class UserFavorite extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'user_id' => 'int',
		'subscriber_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'subscriber_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\User::class);
	}

    public function subscriber()
    {
        return $this->belongsTo(\App\User::class);
	}
}
