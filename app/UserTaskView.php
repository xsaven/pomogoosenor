<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Mar 2018 18:38:05 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * App\UserTaskView
 *
 * @property int $user_id
 * @property int $user_tasks_id
 * @property-read \App\User $user
 * @property-read \App\UserTasks $user_task
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTaskView whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTaskView whereUserTasksId($value)
 * @mixin \Eloquent
 */
class UserTaskView extends BaseModel
{
	public $incrementing = false;
	public $timestamps = false;

    protected $primaryKey = 'user_id';

    protected $table = 'user_tasks_view';

	protected $casts = [
		'user_id' => 'int',
		'user_tasks_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'user_tasks_id'
	];

	public function user_task()
	{
		return $this->belongsTo(UserTasks::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
