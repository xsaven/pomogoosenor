<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class FAQ extends Model
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $table = 'faq';

    protected $fillable = [
        'question',
        'answer',
        'show_on_menu',
        'slug'
    ];

    protected $casts = [
        'show_on_menu' => 'bool '
    ];

    public static function boot()
    {
        parent::boot();

        FAQ::observe(new UserActionsObserver);
    }


}