<?php namespace App\libraries\ModelCache;

use App\libraries\ModelCache\Traits\Cachable;
use Illuminate\Database\Eloquent\Model;

abstract class CachedModel extends Model
{
    use Cachable;
}
