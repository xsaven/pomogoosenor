<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PaymentStatus
 *
 * @property int $id
 * @property int $code
 * @property string $description
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentStatus whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentStatus whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentStatus whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PaymentStatus extends Model
{
    //
}
