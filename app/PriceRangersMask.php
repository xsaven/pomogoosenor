<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PriceRangersMask
 *
 * @property int $id
 * @property string $name
 * @property array $data
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read mixed $formatted_data
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PriceRangersMask whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PriceRangersMask whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PriceRangersMask whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PriceRangersMask whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PriceRangersMask whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PriceRangersMask extends BaseModel
{
    protected $table = 'price_rangers_masks';

    protected $fillable = ['name', 'data'];

    protected $casts = [
        'data' => 'array'
    ];

    public $format_data = false;

    protected $hidden = [
        'id',
        'name',
        'created_at',
        'updated_at'
    ];

    protected $appends = [
        'formatted_data'
    ];

    public function getFormattedDataAttribute()
    {
        $captions = ['Маленький', 'Небольшой', 'Средний', 'Большой', 'Крупный'];
        foreach ($captions as $index=>$caption){
            $data[] = [
                'name' => $caption,
                'value' => (($index+1 == count($captions))?'от ':'до ').$this->data[$index]
            ];
        }
        return $data??[];
    }

    /**
     * Get range step by UserTask cost
     *
     * @param $task_cost
     * @return int|string
     */
    public function getStepByTaskCost($task_cost)
    {
        $step = 0;
        if ($task_cost){
            $cost = trim(explode('-', $task_cost)[1]);
            $formatted_data = $this['formatted_data'];
            array_unshift($formatted_data, $formatted_data[0]);
            foreach ($formatted_data as $key => $val){
                if($val['value']==$cost) $step = $key;
            }
            if(isset($cost[1]) && $cost[0]=='От') $step++;
            $step = isset($cost[1]) ? $step*5000 : 'false';
        }
        return $step;
    }
}
