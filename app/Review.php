<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 31 Jan 2018 10:15:46 +0000.
 */

namespace App;

use App\Events\ReviewStored;
use App\Helpers\DateTime;
use App\Http\Controllers\Payment\SBRController;
use App\Notifications\FinishTask;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Expression;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * App\Review
 *
 * @property int $id
 * @property bool $positive
 * @property int $type
 * @property string $text
 * @property int $politeness
 * @property int $punctuality
 * @property int $adequacy
 * @property int $price
 * @property int $quality
 * @property int $task_id
 * @property int $user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\UserTasks $task
 * @property-read \App\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Review onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review whereAdequacy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review wherePoliteness($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review wherePositive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review wherePunctuality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review whereQuality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review whereTaskId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Review withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Review withoutTrashed()
 * @mixin \Eloquent
 */
class Review extends BaseModel
{
    protected $table = 'reviews';

    use SoftDeletes;

    protected $perPage = 15;

    const IS_EXECUTOR = 0;
    const IS_CREATOR = 1;
    public $mark_fields = ['politeness', 'punctuality', 'adequacy', 'price', 'quality'];

    protected $casts = [
        'positive' => 'bool',
        'type' => 'int',
        'politeness' => 'int',
        'punctuality' => 'int',
        'adequacy' => 'int',
        'price' => 'int',
        'quality' => 'int',
        'task_id' => 'int',
        'user_id' => 'int'
    ];

    protected $hidden = [
        'deleted_at',
        'created_at',
//        'task_id',
        'final_task_price'
    ];
    protected $attributes = [
        'politeness' => 0,
        'punctuality' => 0,
        'adequacy' => 0,
        'price' => 0,
        'quality' => 0,
        'final_task_price' => 0
    ];

    protected $fillable = [
        'positive',
        'type',
        'text',
        'politeness',
        'punctuality',
        'adequacy',
        'price',
        'quality',
        'task_id',
        'user_id',
        'final_task_price'
    ];

    protected $appends = [
        'average_mark'
    ];

    public function task()
    {
        return $this->belongsTo(UserTasks::class);
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getTypeAttribute($type)
    {
        return $type == self::IS_EXECUTOR ? 'executor' : 'creator';
    }

    public function getPolitenessAttribute($val)
    {
        return $val ?? 0;
    }


    public function getPunctualityAttribute($val)
    {
        return $val ?? 0;
    }


    public function getAdequacyAttribute($val)
    {
        return $val ?? 0;
    }


    public function getPriceAttribute($val)
    {
        return $val ?? 0;
    }


    public function getQualityAttribute($val)
    {
        return $val ?? 0;
    }

    public function getAverageMarkAttribute()
    {

        $mark_count = 0;
        $avg_mark = 0;
        foreach ($this->mark_fields as $mark_field) {
            if ($mark = $this->getAttribute($mark_field)) {
                $avg_mark += $mark;
                $mark_count++;
            }
        }
        if ($mark_count === 0) return 0;
        return round($avg_mark / $mark_count);
    }

    private static function isUnique($task_id)
    {
        return self::where('user_id', user()->id)
            ->where('task_id', $task_id)
            ->get()
            ->isEmpty();
    }

    /**
     * @return bool
     */
    public function isExecutor()
    {
        return $this->getOriginal('type') == self::IS_EXECUTOR;
    }

    /**
     * @return bool
     */
    public function isCreator()
    {
        return $this->getOriginal('type') == self::IS_CREATOR;
    }

    /**
     * Edit task review
     *
     * @param UserTasks $task
     * @param array $data
     * @return bool
     */
    public function edit(UserTasks $task, $data)
    {
        if ($task->is_executor || $task->is_creator) {
            $data['task_id'] = $task->id;
            $data['user_id'] = user()->id;
            $data['type'] = $task->is_executor ? 0 : 1;
            return (boolean)$this->update($data);
        }
        return false;
    }

    /**
     * Store task review
     *
     * @param UserTasks $task
     * @param array $data
     * @return bool
     */
    public static function store(UserTasks $task, $data)
    {
        //dd($task, $data['positive'] === "1");

        if ($task->is_executor || $task->is_creator) {
            if (self::isUnique($task->id)) {
                $data['task_id'] = $task->id;
                $data['user_id'] = user()->id;
                $type = $task->is_executor ? 0 : 1;
                $data['type'] = $type;
                if ($review = self::create($data)) {
                    SBRController::log(['review store']);
                    event(new ReviewStored($task));
                }
                if (!\Auth::user()->is_worker()) {
                    //$task->status = $data['positive'] === "1" ? UserTasks::STATUS_FINISHED : UserTasks::STATUS_IN_ARBITRATION;
                    //$task->save();

                    User::find($task->executor_id)->notify(new FinishTask($task));
                    \Mail::send(new \App\Mail\FinishTask($task->executor->email));

                }else {

                    User::find($task->creator_id)->notify(new FinishTask($task, 1));
                    \Mail::send(new \App\Mail\FinishTask(User::find($task->creator_id)->email));
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Get reviews list for user
     *
     * @param $user_id
     * @param bool $positive
     * @return mixed
     */
    public static function getSimpleListForUser($user_id, $positive = false)
    {
        $response = self::where(function ($q) use ($user_id) {
            $q->whereHas('task', function ($q) use ($user_id) {
                $q->where('creator_id', $user_id)->where('reviews.type', 0);
            })
                ->orWhereHas('task', function ($q) use ($user_id) {
                    $q->where('executor_id', $user_id)->where('reviews.type', 1);
                });
        })
            ->orderByDesc('created_at')
            ->get();

        return $response;
    }

    /**
     * Get reviews list for user
     *
     * @param $user_id
     * @param bool $positive
     * @return mixed
     */
    public static function getListForUser($user_id, $positive = false)
    {
        $response = self::with('user', 'task')
            ->when($positive !== false, function ($q) use ($positive) {
                $q->where('positive', $positive);
            })
            ->where(function ($q) use ($user_id) {
                $q->whereHas('task', function ($q) use ($user_id) {
                    $q->where('creator_id', $user_id)->where('reviews.type', 0);
                })
                    ->orWhereHas('task', function ($q) use ($user_id) {
                        $q->where('executor_id', $user_id)->where('reviews.type', 1);
                    });
            })
            ->orderByDesc('created_at')
            ->paginate();
        return $response;
    }

    public function getCountDayAgo()
    {
        return DateTime::formatted(date_diff(new \DateTime(), $this->created_at)->d, ['день', 'дня', 'дней']);
    }


}
