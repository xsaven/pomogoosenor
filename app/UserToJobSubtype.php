<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UserToJobSubtype
 *
 * @property int $user_id
 * @property int $subtype_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserToJobSubtype whereSubtypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserToJobSubtype whereUserId($value)
 * @mixin \Eloquent
 */
class UserToJobSubtype extends Model
{
    //
}
