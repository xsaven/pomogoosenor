<?php

namespace App;

use Cache;
use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;

/**
 * App\SeoData
 *
 * @property int $id
 * @property string $url_mask
 * @property string|null $keywords
 * @property string|null $description
 * @property string|null $og_title
 * @property string|null $og_description
 * @property string|null $og_url
 * @property string|null $og_image
 * @property string|null $twitter_card
 * @property string|null $twitter_title
 * @property string|null $twitter_description
 * @property string|null $twitter_image
 * @property string|null $twitter_url
 * @property string|null $image_src
 * @property array $social_data
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeoData whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeoData whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeoData whereImageSrc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeoData whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeoData whereOgDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeoData whereOgImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeoData whereOgTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeoData whereOgUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeoData whereSocialData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeoData whereTwitterCard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeoData whereTwitterDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeoData whereTwitterImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeoData whereTwitterTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeoData whereTwitterUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeoData whereUrlMask($value)
 * @mixin \Eloquent
 */
class SeoData extends Model
{
    protected $table = 'seodata';

    public $timestamps = false;

    protected $fillable = [
        'url_mask',
        'description',
        'keywords',
        'social_data',
    ];

    protected $casts = [
        'social_data' => 'array'
    ];

    protected $social_data_base = [
        'og_title' => '',
        'og_description' => '',
        'og_url' => '',
        'og_image' => '',
        'twitter_card' => '',
        'twitter_title' => '',
        'twitter_description' => '',
        'twitter_image' => '',
        'twitter_url' => '',
        'image_src' => ''
    ];


    public static $twitter_card = ["summary" => "summary", "summary_large_image" => "summary_large_image", "app" => "app"];

    public static function boot()
    {
        parent::boot();
        self::saved(function ($model){
            Cache::forever('seo_data',self::all());
        });
        SeoData::observe(new UserActionsObserver);
    }

    public function getSocialDataAttribute($social_data)
    {
        $social_data = json_decode_try($social_data);
        $diff = array_diff_key($this->social_data_base, $social_data);
        return array_merge($social_data, $diff);
    }

}