<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 07 Feb 2018 16:08:13 +0000.
 */

namespace App;

use Guzzle\Common\Collection;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * App\Question
 *
 * @property int $id
 * @property string $title
 * @property int $pattern_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Answer[] $answers
 * @property-read \App\Pattern $pattern
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Question whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Question wherePatternId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Question whereTitle($value)
 * @mixin \Eloquent
 */
class Question extends Eloquent
{
    public $timestamps = false;

    protected $casts = [
        'pattern_id' => 'int'
    ];

    protected $fillable = [
        'title',
        'pattern_id'
    ];

    protected $hidden = [
        'pattern_id'
    ];

    public function pattern()
    {
        return $this->belongsTo(Pattern::class);
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public static function loadAll()
    {
        return self::with('answers')
            ->whereHas('pattern', function (Pattern $pattern){
                $pattern->whereActive(1);
            })
            ->orderBy('order', 'ASC')
            ->get();
    }

    /**
     * Delete records by IDs
     *
     * @param Collection $avalieble_ids
     * @param int $pattern_id
     */
    public static function deleteUnavailableIds($avalieble_ids, $pattern_id)
    {
        if ($avalieble_ids->filter()->count() > 0) {
            self::whereNotIn('id', $avalieble_ids)->wherePatternId($pattern_id)->delete();
        }
    }


    /**
     * @param $questions
     * @param Pattern $pattern
     * @return Question[]
     */
    public static function syncManyToPattern($questions, Pattern $pattern)
    {
        $avalieble_ids = collect($questions)->pluck('id');
        self::deleteUnavailableIds($avalieble_ids, $pattern->id);
        foreach ($questions as $question) {
            yield self::updateOrCreate(['id' => $question['id'] ?? 0], [
                'title' => $question['title'],
                'pattern_id' => $pattern->id,
            ]);
        }
    }
}
