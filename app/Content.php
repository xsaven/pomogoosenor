<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;

/**
 * App\Content
 *
 * @property int $id
 * @property string $type
 * @property string $file
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Galleries[] $galleries
 * @property-read mixed $path
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Content whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Content whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Content whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Content whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Content whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Content extends BaseModel
{

    protected $table = 'content';

    protected $fillable = [
        'type',
        'file',
        'is_avatar'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'pivot',
        'file'
    ];

    protected $appends = [
        'path',
    ];

    public static $type = ["image" => "image", "video" => "video", "document" => "document",];


    public static function boot()
    {
        parent::boot();

        Content::observe(new UserActionsObserver);
    }

    public function galleries()
    {
        return $this->belongsToMany(Galleries::class, 'content_gallery', 'content_id', 'gallery_id');
    }

    public function gallery_content()
    {
        return $this->hasOne(ContentGallery::class, 'content_id');
    }

    public static function getUserGalleries($user_id)
    {
        return self::whereHas('galleries', function ($gallery) use ($user_id) {
            return $gallery->whereUserId($user_id);
        })->get();
    }

    public function getPathAttribute()
    {
        return url('/') . '/' . $this->attributes['file'];
    }
}