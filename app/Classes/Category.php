<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 31.08.2017
 * Time: 16:47
 */

namespace app\Classes;

use App\Category as Cat;

class Category {

    public static function getTree(){

        return Cat::where([
                ['active','1'],
            ])
            ->orderBy('order')
            ->get();
    }

}