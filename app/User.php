<?php

namespace App;

use App\Decorators\Categories\FlatCategoriesDecorator;
use App\Helpers\CustomAppends;
use App\Http\Resources\APICollection;
use App\Http\Resources\Profile\UserReviewsResource;
use App\Http\Resources\User\CreditCardsResource;
use App\Notifications\ResetPassword;
use Cache;
use Carbon\Carbon;
use function Couchbase\defaultDecoder;
use Doctrine\DBAL\Query\QueryBuilder;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use App\Role;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Debug\Dumper;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;
use Laraveldaily\Quickadmin\Traits\AdminPermissionsTrait;
use LocalizedCarbon;
use phpDocumentor\Reflection\Types\Self_;

use Illuminate\Pagination\Paginator;


/**
 * App\User
 *
 * @property int $id
 * @property int|null $role_id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property bool $verified
 * @property bool $is_busy
 * @property mixed $last_activity
 * @property string|null $verification_token
 * @property string|null $remember_token
 * @property string $onesignal_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $card
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Category[] $categories
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CreditCard[] $credit_cards
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DialogMessage[] $dialogMessage
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Dialog[] $dialogs
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $favorite_users
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $followers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Galleries[] $galleries
 * @property-read mixed $avg_mark
 * @property-read mixed $badges
 * @property-read mixed $has_avg_mark
 * @property-read mixed $last_activity_diff
 * @property-read mixed $negative_reviews_count
 * @property-read bool $online
 * @property-read mixed $positive_reviews_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Decorators\Categories\FlatCategoriesDecorator[] $subscribed_categories
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DialogParticipant[] $participants
 * @property-read \App\Profile $profile
 * @property-read \App\Profile $profiles
 * @property-read \App\Role|null $role
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SocialAccount[] $socialAccounts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SocialAccount[] $social_accaounts
 * @property-read \App\Subscribe $subscribe
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UserTasks[] $tasks
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UserTasks[] $tasks_created
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UserTasks[] $tasks_executed
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Template[] $templates
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\TransactionsLog[] $transactions
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\User onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIsBusy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereOnesignalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereVerificationToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereVerified($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\User withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CreditCard[] $beneficiary_cards
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CreditCard[] $payer_cards
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User executorsOnly()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User withoutMe()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastActivity($value)
 */
class User extends BaseModel implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{

    public function __invoke($id)
    {
        return self::find($id);
    }

    use Authenticatable, Authorizable, CanResetPassword, AdminPermissionsTrait, Notifiable, SoftDeletes;

    protected $table = 'users';

    protected $perPage = 3;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name',
        'email',
        'password',
        'verification_token',
        'onesignal_id',
        'is_busy',
        'subscribe_email',
        'subscribe_push'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token', 'verification_token', 'pivot'];

    public $appends = [
        'online',
        'last_activity_diff',
        'positive_reviews_count',
        'negative_reviews_count',
        'avg_mark',
        'has_avg_mark',
        'badges',
        'subscribed_categories'
    ];

    protected $attributes = [
        'role_id' => Role::ROLE_USER,
        'verified' => false
    ];

    protected $casts = [
        'verified' => 'boolean',
        'is_busy' => 'boolean',
    ];

    protected $dates = ['deleted_at'];


    /*
     * Relation
     */

    public function followers()
    {
        return $this->belongsToMany(User::class, 'user_favorites', 'user_id', 'subscriber_id');
    }

    public function favorite_users()
    {
        return $this->belongsToMany(User::class, 'user_favorites', 'subscriber_id', 'user_id');
    }

    public function tasks_created()
    {
        return $this->hasMany(UserTasks::class, 'creator_id')->where('status', '<>', 0);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'user_id');
    }

    public function tasks_executed()
    {
        return $this->hasMany(UserTasks::class, 'executor_id');
    }

    public function tasks()
    {
        return $this->hasMany(UserTasks::class, 'executor_id')->orWhere('creator_id', $this->id);
    }


    public function getReviewsCount()
    {
        return $this->getReviews()->count();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|CreditCard[]
     */
    public function credit_cards()
    {
        return $this->hasMany(CreditCard::class);
    }

    public function beneficiary_cards()
    {
        return $this->hasMany(CreditCard::class)->whereType(CreditCard::BENEFICIARY_TYPE);
    }

    public function payer_cards()
    {
        return $this->hasMany(CreditCard::class)->whereType(CreditCard::PAYER_TYPE);
    }

    public function received_transactions()
    {
        return $this->hasMany(Payment::class, 'receiver_id');
    }

    /**
     * Get user reviews
     *
     * @return \Illuminate\Support\Collection
     */
    public function getReviews()
    {
        /** @var LengthAwarePaginator $reviews_list */
        $reviews_list = Review::getSimpleListForUser($this->id);

        return $reviews_list;
    }

    public function profiles()
    {
        return $this->hasOne('App\Profile', 'user_id');
    }

    public function subscribe()
    {

        return $this->hasOne('App\Subscribe', 'user_id')->withDefault(true);
    }

    public function socialAccounts()
    {
        return $this->hasMany('App\SocialAccount', 'user_id');
    }

    /**
     * Get unavailable social accounts
     *
     * @return \Generator
     */
    public function getUnavailableSocialAccounts()
    {
        $user_social_types = $this->social_accaounts->pluck('social_type')->toArray();
        $default_social_types = array_keys(SocialAccount::$social_types_short);
        $types_diff = array_diff($default_social_types, $user_social_types);
        foreach ($types_diff as $social_type) {
            yield new SocialAccount(['social_type' => $social_type]);
        }
    }

    public function categories()
    {
        return $this->belongsToMany(Subcategory::class, 'user_to_job_subtypes', 'user_id', 'subtype_id');
    }

//    public function subcategories()
//    {
//        return $this->hasMany(UserToJobSubtype::class, 'user_id');
//    }

    public function subscribed_categories()
    {
        return $this->belongsToMany(
            FlatCategoriesDecorator::class,
            'user_to_job_subtypes',
            'user_id',
            'subtype_id'
        )->select(['id', 'name']);
    }

    public function galleries()
    {
        return $this->hasMany(Galleries::class);
    }

    public function templates()
    {
        return $this->hasMany(Template::class);
    }

    public function transactions()
    {
        return $this->hasMany(TransactionsLog::class, 'sender_id')->orWhere('recipient_id', $this->id);
    }

    public function participants()
    {
        return $this->hasMany(DialogParticipant::class, 'user_id')->orderByDesc('id');
    }

    public function dialogs()
    {
        return $this->hasManyThrough(Dialog::class, DialogParticipant::class);
    }

    public function dialogMessage()
    {
        return $this->hasMany(DialogMessage::class, 'sender_id');
    }

    public function routeNotificationForSmscru()
    {
        $uid = md5(user()->id);
        return cache("user.$uid.verification.phone")['phone'];
    }


    /*
     * Get model
     */

    public function getUserByToken($token, $email)
    {
        return $this->where('verification_token', $token)
            ->where('email', $email)
            ->firstOrFail();
    }

    public function getUserWithClientProfile($id = null)
    {
        if ($this->id && $id == null) {
            $id = $this->id;
        }
        $user = $this->where('id', $id)
            ->with([
                'profiles.cities.country',
                'profiles.workerProfiles',
                'profiles.cities.region',
                'subscribe',
                'socialAccounts',
                'galleries' => function ($q) {
                    $q->whereHas('content_items');
                },
                'followers' => function ($q) {
                    $q->with('profile')->paginate(3);
                },
                'favorite_users' => function ($q) {
                    $q->with('profile')->paginate(3);
                },
            ])
            ->withCount([
                'followers',
                'tasks_created',
            ])
            ->firstOrFail()->toArray();
        if ($user) {
            $reviews = $this->getReviews();
            if ($reviews->isNotEmpty()) {
                $random_review = $reviews->random(1)->first();
                $user['random_review'] = [new UserReviewsResource($random_review)];
            } else {
                $user['random_review'] = [];
            }
            $user['rate'] = [[
                'category_name' => 'Курьерские услуги',
                'rate' => 344,
                'tasks_count' => 2
            ],
                [
                    'category_name' => 'Бытовой ремонт',
                    'rate' => 1228,
                    'tasks_count' => 5
                ]
            ];
        }
        return $user;
    }

    public function getCategoriesRate()
    {
        $subscribed_categories = $this->categories()->pluck('id');
        $tasks = UserTasks::select('executor_id')
            ->selectRaw('COUNT(executor_id) as count')
            ->selectRaw('categories.name as category')
            ->join('subcategories', 'user_tasks.subcategory_id', '=', 'subcategories.id')
            ->join('categories', 'subcategories.category_id', '=', 'categories.id')
            ->whereHas('subcategory', function ($q) use ($subscribed_categories) {
                $q->whereHas('category', function ($q) use ($subscribed_categories) {
                    $q->whereIn('id', $subscribed_categories);
                });
            })
            ->where('status', UserTasks::STATUS_FINISHED)
            ->whereDoesntHave('creator_review', function ($q) {
                /** @var Builder $q */
                $q->where('positive', 0)
                    ->orWhere('politeness', '<', 4)
                    ->orWhere('price', '<', 4)
                    ->orWhere('quality', '<', 4);
            })
            ->groupBy('categories.name', 'executor_id')
            ->get()
            ->sortByDesc('count')
            ->groupBy('category');
        return $tasks;

    }

    public function getChatRelatedData(User $to_user)
    {
        $status = '';
        $task = $to_user->tasks_created()
            ->where('status', UserTasks::STATUS_IN_PROGRESS)
            ->where('executor_id', $this->id)
            ->orderByDesc('created_at')
            ->first();
        $status = $task ? 'is_executor' : '';
        if (!$task) {
            $offer = TaskOffers::whereHas('task', function ($q) use ($to_user) {
                $q->where('creator_id', $to_user->id);
            })
                ->where('user_id', $this->id)
                ->orderByDesc('created_at')
                ->first();
            if ($offer) {
                $task = $offer->task;
                $status = 'left_offer';
            }
        }
        if ($status) {
            $status_message = trans_choice('chat.user_status.' . $status, $this->profile->getOriginal('sex'));
        }

        $locked = false;
        if ($task && $task->locked == "1") $locked = true;

        return [
            'status' => $status,
            'status_message' => $status_message ?? '',
            'task_id' => $task->id ?? 0,
            'task_url' => $task ? route('task.review', $task) : '',
            'task_name' => $task->name ?? '',
            'show_phone' => $task && $locked ? true : false
        ];
    }

//    public function getCategoriesRate()
//    {
//        $task_colums = array_keys(UserTasks::first()->original);
//        $subscribed_categories = $this->categories()->pluck('id');
//        $categories =  Category::with(['subcategories.tasks'=>function($q){
//            /** @var Builder $q  */
//            $q
//                ->where('status', UserTasks::STATUS_FINISHED)
//                ->whereDoesntHave('creator_review',function ( $q){
//                    /** @var Builder $q  */
//                   $q->where('positive',0)
//                       ->orWhere('politeness','<',4)
//                       ->orWhere('price','<',4)
//                       ->orWhere('quality','<',4);
//                })
//                ->groupBy(array_keys(UserTasks::first()->original));
//        }])->whereIn('id',$subscribed_categories)
//            ->get();
//        return $categories;
//    }

    /**
     * Check if authenticated user follow current user
     *
     * @return bool
     */
    public function isFollowedByMe()
    {
        if (\Auth::check()) {
            return $this->followers->whereIn('id', user()->id)->isNotEmpty();
        }
        return false;
    }

    public function convert($size)
    {
        $unit = array('b', 'kb', 'mb', 'gb', 'tb', 'pb');
        return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function getUserWithProfile()
    {

        $user = $this->where('id', $this->id)
            ->with([
                'profiles.cities.country',
                'profiles.workerProfiles',
                'profiles.cities.region',
                'subscribe',
                'socialAccounts',
                'galleries' => function ($q) {
                    $q->whereHas('content_items');
                },
                'followers' => function ($q) {
                    $q->with('profile')->paginate(3);
                },
                'favorite_users' => function ($q) {
                    $q->with('profile')->paginate(3);
                },
            ])
            ->withCount([
                'followers',
                'tasks_created'
            ])
            ->when(($this->profiles && $this->profiles->is_worker), function ($q) {
                $q->withCount(['tasks_executed']);
            })
            ->firstOrFail();
        if ($user) {
            $reviews = $this->getReviews();
            if ($this->id != user()->id) {
                $is_favorite = user()->favorite_users()->where('user_favorites.user_id', $this->id)->count();
                $user['is_favorite'] = $is_favorite >= 1;
            }
            if ($reviews->isNotEmpty()) {
                $random_review = $reviews->random(1)->first();
                $user['random_review'] = [new UserReviewsResource($random_review)];
            } else {
                $user['random_review'] = [];
            }
            $user['rate'] = [[
                'category_name' => 'Курьерские услуги',
                'rate' => 344,
                'tasks_count' => 2
            ],
                [
                    'category_name' => 'Бытовой ремонт',
                    'rate' => 1228,
                    'tasks_count' => 5
                ]
            ];
        }
        return $user;
    }

    /**
     * Find user by email
     *
     * @param string $email
     * @return Model|null|static
     */
    public static function findByEmail($email)
    {
        return self::with('profile')->where('email', $email)->first();
    }


    public function getSubscribedCategoriesAttribute()
    {
        $subs_categories = $this->subscribed_categories()->get()->toArray();
        $categories = FlatCategoriesDecorator::all()->toArray();
        foreach ($categories as &$category) {
            $category['active'] = false;
        }
        unset($category);
        foreach ($categories as &$category) {
            foreach ($subs_categories as $subs_category) {
                if ($category['id'] == $subs_category['id'])
                    $category['active'] = true;
            }
        }
        return $categories;
    }

    /**
     * Check if user is online
     *
     * @return bool
     */
    public function getOnlineAttribute()
    {
        return Cache::tags('online-users')->has($this->id);
    }

    /**
     * Get last user activity
     *
     * @return mixed
     */
    public function getLastActivityAttribute($value)
    {
        return $value ?? Carbon::now()->subMonth()->toDateTimeString();
    }

    public function getLastActivityDiffAttribute()
    {
        $last_activity = \Carbon\Carbon::parse($this->last_activity);
        $activity_diff = \Carbon\Carbon::now()->diff($last_activity);
        return LocalizedCarbon::now()->add($activity_diff)->diffForHumans();
    }

    public function getPositiveReviewsCountAttribute()
    {
        return $this->getReviews()->where('positive', true)->count();
    }

    public function getNegativeReviewsCountAttribute()
    {
        return $this->getReviews()->where('positive', false)->count();
    }

    private function getTasksExecutedBadgeCount()
    {
        $tasks_count = $this->attributes['tasks_executed_count'];
        $filter_data = [500, 400, 300, 200, 100, 50];
        foreach ($filter_data as $filter_count) {
            if ($tasks_count >= $filter_count) return $filter_count;
        }
    }

    private function getTasksExecutedBadge()
    {

        if ($this->attributes['tasks_executed_count'] % 50) {
            return [
                "type" => 1,
                "value" => $this->attributes['tasks_executed_count']
            ];
        }
    }

    public function getBadgesAttribute()
    {
        $test_data = [
            [
                "type" => 1,
                "value" => 50,
            ],
            [
                "type" => 2,
                "value" => 100,
            ],
            [
                "type" => 3,
                "value" => 88.4,
            ],
            [
                "type" => 4,
                "value" => 1,
            ]];
        $test_data = stringify_json($test_data);
        shuffle($test_data);
        for ($i = 0; $i < rand(0, 4); $i++) {
            reset($test_data);
            unset($test_data[rand(key($test_data), count($test_data) - 1)]);
        }
        return collect($test_data)->sortBy(function ($value, $key) {
            return $value['type'];
        })->values()->all();
    }

    private function getAvgMarkValue($reviews, $key)
    {
        return round($reviews->filter(function ($value) use ($key) {
            return $value[$key] != null && $value[$key] != 0;
        })->avg($key));
    }

    private function getAvgData($reviews)
    {
        return [
            'politeness' => $this->getAvgMarkValue($reviews, 'politeness'),
            'punctuality' => $this->getAvgMarkValue($reviews, 'punctuality'),
            'adequacy' => $this->getAvgMarkValue($reviews, 'adequacy'),
            'price' => $this->getAvgMarkValue($reviews, 'price'),
            'quality' => $this->getAvgMarkValue($reviews, 'quality')
        ];
    }

    public function getAvgMarkAttribute()
    {
        $reviews = $this->getReviews();
        $avg_data = $this->getAvgData($reviews);
        $total = collect(array_values($avg_data))->filter(function ($value) {
            return $value != 0;
        })->avg();
        $avg_data['total'] = $total != null ? $total : 0;
        return $avg_data;
    }

    public function getHasAvgMarkAttribute()
    {
        return $this->avg_mark['total'] > 0;
    }

    /**
     * Check if user has at least one verified contact
     *
     * @return bool
     */
    public function hasVerifiedContacts()
    {
        return $this->profile->phone_verified ||
            $this->verified ||
            $this->social_accaounts->isNotEmpty();
    }

    /**
     * Check if user is worker
     *
     * @return bool
     */
    public function is_worker()
    {
        if (!$this->profile)
            return false;

        return $this->profile->is_worker;
    }

    /**
     * Set busy state
     *
     * @param $busy_state
     */
    public function setBusy($busy_state)
    {
        $this->is_busy = (bool)$busy_state;
        $this->save();
    }

    public function newMessageCountLoad()
    {
        $all_participants = $this->participants()->with(['dialogs' => function ($q) {
            $q->withCount('messagesCount')->with(['dialogParticipants' => function ($q) {
                $q->where('user_id', '!=', auth()->id());
            }]);
        }])->get();

        $new_message = [];
        if (count($all_participants))
            foreach ($all_participants as $item) {
                if ($item->dialogs->dialogParticipants->first()) $new_message[] = [
                    'thread_id' => $item->dialog_id,
                    'msg_count' => $item->dialogs->messages_count_count,
                    'user_id' => $item->dialogs->dialogParticipants->first()->user_id
                ];
            }

        return $new_message;
    }

    public function getAllUserByDialogId($dialog_ids, $user_id)
    {

        return $this->whereHas('participants', function ($q) use ($dialog_ids, $user_id) {
            $q->whereIn('dialog_id', $dialog_ids)
                ->where('user_id', '!=', $user_id)
                ->where('user_id', '!=', 1);
        })
            ->with(['participants' => function ($q) use ($dialog_ids, $user_id) {
                $q->whereIn('dialog_id', $dialog_ids)
                    ->where('user_id', '!=', $user_id);
            }])
            ->get();
    }

    public function getThreadWithMessageByLotId($user_id)
    {
        $participants = $this->where('id', $user_id)
            ->whereHas('participants.dialogs')
            ->with('participants')
            ->first();
        return $participants;
    }

    public function newMsgSum()
    {
        $sum = 0;
        $new_msg = $this->newMessageCountLoad();
        foreach ($new_msg as $item) {
            $sum += $item['msg_count'];
        }
        $event_count = [
            'msg_count' => $sum,
            'purchases_count' => 0,
            'sales_count' => 0
        ];
        return json_encode($event_count);
    }

    /*
     * OLD CODE (NEED REFACTOR)
     */

//    private $profile;

    public static function boot()
    {
        parent::boot();

        self::updating(function (User $user) {
            if ($user->getOriginal('password') != $user->password) {
                $user->password = bcrypt($user->password);
            }
        });

        User::observe(new UserActionsObserver);
    }

    /**
     *
     */
    public function createEmptyUserRelated()
    {

        $this->subscribe()->firstOrCreate(['user_id' => $this->id]);
        /** @var Profile $profile */
        $profile = $this->profile()->firstOrCreate(['user_id' => $this->id]);
        $profile->workerProfile()->firstOrCreate(['profile_id' => $profile->id]);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function profile()
    {
        return $this->hasOne(Profile::class, 'user_id');
    }

    public function checkPayment()
    {
        $payment = \App\Payment::where('receiver_id', $this->id)->where('status', 2)->first();
        $profile = $this->load_profile();
        $profile->balance += $payment->amount;
        $profile->save();
        $payment->status = 3;
        $payment->save();
        return $payment;
    }

    public function bill()
    {
        $profile = $this->load_profile();
        if ($profile) {
            return $profile->balance;
        } else {
            return "0.00";
        }
    }

    public function load_profile()
    {
        if (!$this->profile) {
            $this->profile = $this->profile()->first();
        }
        return $this->profile;
    }

    public function avatar()
    {
        $profile = $this->load_profile();
        if ($profile) {
            return $profile->avatar();
        }
    }


    public function get_city()
    {
        $profile = $this->load_profile();
        if ($profile->city_id) {
            return $profile->cities()->first();
        }
        return City::first();
    }

    public function social_accaounts()
    {
        return $this->hasMany('\App\SocialAccount', 'user_id', 'id');
    }

    public function getNameAttribute()
    {
        $profile = $this->load_profile();
        if ($profile && ($profile->name || $profile->surname)) {
            if ($profile->name && $profile->surname) {
                return $profile->name . ' ' . mb_substr($profile->surname, 0, 1) . '.';
            } elseif ($profile->name && !$profile->surname) {
                return $profile->name;
            } elseif (!$profile->name && $profile->surname) {
                return $profile->surname;
            }
        } elseif ($this->attributes['name']) {
            return $this->attributes['name'];
        } else {
            return 'Анон';
        }
    }

    public function getNewEvents()
    {
        $new_msg = user()->newMessageCountLoad();
        $new_messages_total = collect($new_msg)->sum(function ($item) {
            return $item['msg_count'];
        });
        $unreadNotificationsCount = \user()->notifications()->whereNull('read_at')->count();
        return [
            'new_messages_count' => $new_messages_total,
            'unread_notifications_count' => $unreadNotificationsCount
        ];
    }

    public function getMenuData()
    {
        $new_events = $this->getNewEvents();
        $profile = $this->load_profile();

        return [
            'user_id' => $this->id,
            'name' => $this->name,
            'avatar' => $profile->avatar_thumb,
            'balance' => $profile->money ?? 0,
            'is_worker' => $profile->is_worker,
            'has_card' => (bool)$this->card,
            'phone' => $profile->full_phone,
            'new_events' => $new_events,
        ];
    }

    public function name()
    {
        $profile = $this->load_profile();
        if ($profile && ($profile->name || $profile->surname)) {

            if ($profile->name && $profile->surname) {
                return $profile->surname . ' ' . mb_substr($profile->name, 0, 1) . '.';
            } elseif ($profile->name && !$profile->surname) {
                return $profile->name;
            } elseif (!$profile->name && $profile->surname) {
                return $profile->surname;
            }

        } elseif ($this->name) {
            return $this->name;
        } else {
            return 'Анон';
        }
    }

    /**
     * Bind credit card to user
     *
     * @param string $card
     */
    public function bindCard($card)
    {
        $this->card = $card;
        $this->save();
    }

    /**
     * Unbind credit card to user
     */
    public function unbindCard()
    {
        $this->card = '';
        $this->save();
    }

    public function sendPasswordResetNotification($token)
    {

        $this->notify(new ResetPassword($token));
    }

    /**
     * Store OneSignal User Id
     *
     * @param $onesignal_id
     * @return $this
     */
    public function setOnesignalId($onesignal_id)
    {
        $this->onesignal_id = $onesignal_id;
        $this->save();
        return $this;
    }

    /**
     * Route notifications for the OneSignal channel.
     *
     * @return string
     */
    public function routeNotificationForOneSignal()
    {
        return $this->onesignal_id;
    }

    public function scopeExecutorsOnly(Builder $q)
    {
        $q->whereHas('profile', function (Builder $q) {
            $q->where('moderated_worker', true);
        });
    }

    public function scopeWithoutMe(Builder $q)
    {
        if (\Auth::check()) {
            $q->where('id', '!=', user()->id);
        }
    }

    public function getRatingAttribute()
    {
        return $this->avg_mark['total'];
    }

    public static function getExecutorsList(array $sort_data = [])
    {
        /** @var Collection $collection */
        $collection = self::executorsOnly()->withoutMe()
            ->when($sort_data['online'], function (Builder $q) {
                $q->where('last_activity', '>', now()->subMinutes(10));
            })
            ->whereHas('subscribed_categories', function (Builder $q) use ($sort_data) {
                $q->when($sort_data['subcategory'], function (Builder $q) use ($sort_data) {
                    $q->where('subtype_id', '=', $sort_data['subcategory']);
                });
            })
            ->whereHas('profile', function (Builder $q) use ($sort_data) {
                $q->when($sort_data['city'], function (Builder $q) use ($sort_data) {
                    $q->where('city_id', '=', $sort_data['city'])->orWhere('city_id', '=', null);
                });
            })
            ->with('profile.workerProfile')->get();

        //dump($collection, $sort_data);

        if ($sort_data['type'] == 'rate') {
            $collection = $collection->sortByDesc('rating');
        }

        if ($sort_data['type'] == 'new') {
            $collection = $collection->sortByDesc('created_at');
        }

        return self::paginateCollection($collection);
    }

    public static function paginateCollection($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function getTaskCount()
    {
        return UserTasks::where('status', '=', 3)->where('executor_id', '=', $this->id)->count();
    }

    public function getStickerProjectsCount()
    {
        $taskCount = $this->getTaskCount();
        if ($taskCount >= 500) {
            return 500;
        } elseif ($taskCount >= 400) {
            return 400;
        } elseif ($taskCount >= 300) {
            return 300;
        } elseif ($taskCount >= 200) {
            return 200;
        } elseif ($taskCount >= 100) {
            return 100;
        } elseif ($taskCount >= 50) {
            return 50;
        }
        return false;
    }

    public function getPositionByPositiveReviews()
    {
        $userStatistics = self::getQuery()
            ->select([
                'users.id',
                \DB::raw('count(reviews.id) as review_count'),
                \DB::raw('@rownum := @rownum + 1 AS position'),
            ])
            ->join('profiles', 'users.id', '=', 'profiles.user_id', 'left')
            ->join('reviews', 'users.id', '=', 'reviews.user_id', 'left')
            ->where('profiles.is_worker', '=', true)
            ->where('reviews.positive', '=', true)
            ->where('reviews.created_at', '>', \DB::Raw('DATE(DATE_ADD(NOW(), INTERVAL -3 MONTH))'))
            ->groupBy('users.id')
            ->orderBy('review_count', 'DESC')
            ->limit(500)
            ->get();

        if (!$userStatistics) return false;
        $position = array_search($this->id, array_column($userStatistics->toArray(), 'id'));

        if ($position === false) return false;
        if ($position < 50) {
            return 50;
        } elseif ($position < 100) {
            return 100;
        } elseif ($position < 500) {
            return 500;
        }
        return false;
    }

    public function getPositionByPositiveReviewsByCategory($categoryId)
    {
        $userStatistics = self::getQuery()
            ->select([
                'users.id',
                \DB::raw('count(reviews.id) as review_count'),
            ])
            ->join('user_tasks', 'users.id', '=', 'user_tasks.executor_id', 'left')
            ->join('reviews', 'user_tasks.id', '=', 'reviews.task_id', 'left')
            ->where('reviews.positive', '=', true)
            ->where('user_tasks.subcategory_id', '=', $categoryId)
            ->groupBy('users.id')
            ->orderBy('review_count', 'DESC')
            ->get();


        $position = array_search($this->id, array_column($userStatistics->toArray(), 'id'));

        if ($position === false) {
            return false;
        }
        return $position + 1;
    }

    public function getCountTasksByCategory($categoryId)
    {
        $userStatistics = self::getQuery()
            ->select([
                \DB::raw('count(user_tasks.id) as task_count'),
            ])
            ->join('user_tasks', 'users.id', '=', 'user_tasks.executor_id', 'left')
            ->where('user_tasks.subcategory_id', '=', $categoryId)
            ->where('users.id', '=', $this->id)
            ->where('user_tasks.status', '=', UserTasks::STATUS_FINISHED)
            ->first();

        return $userStatistics->task_count;
    }

    public function getCategoriesByTasks()
    {
        $categories = Subcategory::query()
            ->whereHas('tasks', function (Builder $q) {
                $q->where('user_tasks.executor_id', '=', $this->id)
                    ->where('user_tasks.status', '=', UserTasks::STATUS_FINISHED);
            })
            ->get();

        return $categories;
    }

    public function getPercentTasks()
    {
        $tasks = $this->tasks;

        $user = &$this;

        $positive = $tasks->filter(function ($task) use ($user) {
            /** @var UserTasks $task */

            if ($task->executor_id !== $user->id) return false;

            if ($task->getOriginal('status') == 3) {
                return true;
            }
            return false;
        })->count();

        $all = $tasks->filter(function ($task) use ($user) {

            if ($task->executor_id !== $user->id) return false;

            if ($task->getOriginal('status') != 4) {
                return true;
            }
            return false;
        })->count();

        if ($all == 0) return false;

        $percent = ($positive / $all) * 100;
        return round($percent, 1);
    }

    public function cleanerStatus()
    {
        $cleanerCategory = Subcategory::whereSlug('housemaid')->first();

        $userJobCategoryCleanerExist = UserToJobSubtype::where([
            'user_id' => $this->id,
            'subtype_id' => $cleanerCategory->id
        ])->exists();


        if (!$userJobCategoryCleanerExist) return false;
        if ($this->positive_reviews_count <= 10) return false;
        if (!$this->profile->documents_moderated) return false;
        if (!$this->profile->interview_complete) return false;

        return true;
    }

    public function isChatAvailable()
    {
        if (!$currentUser = user()) {
            return false;
        }

        if ($currentUser->role_id == \App\Role::ROLE_ADMIN) {
            return true;
        }

        if ($this->tasks_created()->where('executor_id', '=', $currentUser->id)->count() > 0) {
            return true;
        }

        if ($this->tasks_executed()->where('creator_id', '=', $currentUser->id)->count() > 0) {
            return true;
        }

        return false;
    }
}
