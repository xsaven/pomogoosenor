<?php

namespace App\Observers;

use App\Dialog;
use App\DialogParticipant;
use App\User;

class UserObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  User $user
     * @return void
     */
    public function created(User $user)
    {
        $dialog = new Dialog();
        $dialog->save();

        $admin = User::where('id', '=', env('CHAT_ADMIN_ID'))->first();

        $adminParticipant = new DialogParticipant();
        $adminParticipant->users()->associate($admin);
        $adminParticipant->dialogs()->associate($dialog);
        $adminParticipant->save();

        $userParticipant = new DialogParticipant();
        $userParticipant->users()->associate($user);
        $userParticipant->dialogs()->associate($dialog);
        $userParticipant->save();
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  User $user
     * @return void
     */
    public function deleting(User $user)
    {
        //
    }
}