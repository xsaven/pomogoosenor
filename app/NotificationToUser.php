<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\NotificationToUser
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\NotificationToUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\NotificationToUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\NotificationToUser whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class NotificationToUser extends Model
{
    //
}
