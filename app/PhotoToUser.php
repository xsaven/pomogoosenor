<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PhotoToUser
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PhotoToUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PhotoToUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PhotoToUser whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PhotoToUser extends Model
{
    //
}
