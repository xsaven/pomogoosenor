<?php

namespace App\Decorators\Tasks;

use App\Decorators\Appendable;
use App\Profile;
use App\TaskComments;
use App\TaskOffers;

/**
 * App\Decorators\Tasks\OfferDecorator
 *
 * @property int $id
 * @property int $user_id
 * @property int $task_id
 * @property string $message
 * @property int $other_notification
 * @property \Carbon\Carbon|null $deactivate_date
 * @property float $cost
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int|null $card_id
 * @property-read \App\CreditCard|null $card
 * @property-read mixed $selected
 * @property-read mixed $user_phone
 * @property-read \App\UserTasks $task
 * @property-read \App\Decorators\Tasks\UserDecorator $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskOffers getSorted()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\OfferDecorator whereCardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\OfferDecorator whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\OfferDecorator whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\OfferDecorator whereDeactivateDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\OfferDecorator whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\OfferDecorator whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\OfferDecorator whereOtherNotification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\OfferDecorator whereTaskId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\OfferDecorator whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\OfferDecorator whereUserId($value)
 * @mixin \Eloquent
 */
class OfferDecorator extends TaskOffers
{
    use Appendable;

    protected $with = ['task'];

    protected $table = 'offers_of_performers';

    protected $visible = [
        'id', 'message', 'cost', 'updated_at', 'user', 'user_phone','selected'
    ];

    protected $appends = [
        'user_phone','selected'
    ];



    public function user()
    {
        return $this->hasOne(UserDecorator::class, 'id', 'user_id');
    }

    public function getUserPhoneAttribute()
    {
        return $this->user->profile->full_phone??'+7(341) 314-14-3';
    }

    public function getSelectedAttribute()
    {
        return $this->task->executor_id == $this->user_id;
    }
}