<?php

namespace App\Decorators\Tasks;

use App\Decorators\Appendable;
use App\Subcategory;

/**
 * App\Decorators\Tasks\SubcategoryDecorator
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $question
 * @property string $description
 * @property string $example_title
 * @property string $hint_description
 * @property float $price_from
 * @property float $price_min
 * @property string $order
 * @property bool $default
 * @property int $category_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property array $form_fields
 * @property string|null $marker
 * @property int|null $range_id
 * @property-read \App\Category $category
 * @property-read mixed $fields
 * @property-read \App\PriceRangersMask|null $range
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UserTasks[] $tasks
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\SubcategoryDecorator whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\SubcategoryDecorator whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\SubcategoryDecorator whereDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\SubcategoryDecorator whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\SubcategoryDecorator whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\SubcategoryDecorator whereExampleTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\SubcategoryDecorator whereFormFields($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\SubcategoryDecorator whereHintDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\SubcategoryDecorator whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\SubcategoryDecorator whereMarker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\SubcategoryDecorator whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\SubcategoryDecorator whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\SubcategoryDecorator wherePriceFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\SubcategoryDecorator wherePriceMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\SubcategoryDecorator whereQuestion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\SubcategoryDecorator whereRangeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\SubcategoryDecorator whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\SubcategoryDecorator whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SubcategoryDecorator extends Subcategory {
    use Appendable;

    protected $visible = [
        'id', 'name', 'range', 'price_from','price_min'
    ];

    protected $appends = [
        'range'
    ];



    public function getRangeAttribute($range)
    {
        $range = $this->range()->first()->toArray();
        return $range['formatted_data'];
    }

}