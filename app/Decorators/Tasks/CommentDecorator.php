<?php

namespace App\Decorators\Tasks;

use App\Decorators\Appendable;
use App\Profile;
use App\TaskComments;

/**
 * App\Decorators\Tasks\CommentDecorator
 *
 * @property int $id
 * @property int $user_id
 * @property int $task_id
 * @property int|null $answer_for_id
 * @property string $message
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\TaskComments $answer
 * @property-read \App\TaskComments $answer_for
 * @property-read mixed $is_answer
 * @property mixed $order
 * @property-read \App\UserTasks $task
 * @property-read \App\Decorators\Tasks\UserDecorator $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskComments getSortedList()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\CommentDecorator whereAnswerForId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\CommentDecorator whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\CommentDecorator whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\CommentDecorator whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\CommentDecorator whereTaskId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\CommentDecorator whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\CommentDecorator whereUserId($value)
 * @mixin \Eloquent
 */
class CommentDecorator extends TaskComments
{
    use Appendable;

    protected $table = 'user_tasks_comments';

    protected $hidden = ['answer'];



    public function user()
    {
        return $this->hasOne(UserDecorator::class, 'id', 'user_id');
    }
}