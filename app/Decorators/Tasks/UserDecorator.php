<?php

namespace App\Decorators\Tasks;

use App\Decorators\Appendable;
use App\User;
use App\UserTasks;

/**
 * App\Decorators\Tasks\UserDecorator
 *
 * @property int $id
 * @property int|null $role_id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property bool $verified
 * @property bool $is_busy
 * @property string|null $verification_token
 * @property string|null $remember_token
 * @property string $onesignal_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $card
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CreditCard[] $beneficiary_cards
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Category[] $categories
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CreditCard[] $credit_cards
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DialogMessage[] $dialogMessage
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Dialog[] $dialogs
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $favorite_users
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $followers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Galleries[] $galleries
 * @property-read mixed $active_from_year
 * @property-read mixed $avatar
 * @property-read mixed $avg_mark
 * @property-read mixed $badges
 * @property-read mixed $has_avg_mark
 * @property-read mixed $last_activity
 * @property-read mixed $last_activity_diff
 * @property-read mixed $negative_reviews_count
 * @property-read bool $online
 * @property-read mixed $phone
 * @property-read mixed $positive_reviews_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Decorators\Categories\FlatCategoriesDecorator[] $subscribed_categories
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \App\Decorators\Tasks\OfferDecorator $offer
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DialogParticipant[] $participants
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CreditCard[] $payer_cards
 * @property-read \App\Decorators\Tasks\ProfileDecorator $profile
 * @property-read \App\Profile $profiles
 * @property-read \App\Role|null $role
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SocialAccount[] $socialAccounts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SocialAccount[] $social_accaounts
 * @property-read \App\Subscribe $subscribe
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UserTasks[] $tasks
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UserTasks[] $tasks_created
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UserTasks[] $tasks_executed
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Template[] $templates
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\TransactionsLog[] $transactions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserDecorator whereCard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserDecorator whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserDecorator whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserDecorator whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserDecorator whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserDecorator whereIsBusy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserDecorator whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserDecorator whereOnesignalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserDecorator wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserDecorator whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserDecorator whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserDecorator whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserDecorator whereVerificationToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserDecorator whereVerified($value)
 * @mixin \Eloquent
 */
class UserDecorator extends User
{
    use Appendable;

    protected $table = 'users';

    protected $visible = [
        'id', 'name', 'online',
        'email', 'badges', 'avg_mark',
        'negative_reviews_count', 'positive_reviews_count',
        'has_avg_mark','avatar','active_from_year','phone'
    ];

    public $appends = [
      'avatar','active_from_year','phone'
    ];

    public function profile()
    {
        return $this->hasOne(ProfileDecorator::class, 'user_id', 'id');
    }

    public function offer()
    {
        return $this->hasOne(OfferDecorator::class);
    }

    public function getAvatarAttribute()
    {
        return $this->profile->avatar_thumb??'';
    }

    public function getActiveFromYearAttribute()
    {
        return $this->created_at->year;
    }

    public function getPhoneAttribute()
    {
        $profile = $this->load_profile();
        return $profile->full_phone??'';
    }
}