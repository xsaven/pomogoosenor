<?php

namespace App\Decorators\Tasks;

use App\Profile;

/**
 * App\Decorators\Tasks\ProfileDecorator
 *
 * @property int $id
 * @property float $balance
 * @property string|null $name
 * @property string|null $patronymic
 * @property string|null $surname
 * @property string|null $phone_code
 * @property string|null $phone
 * @property string|null $birthday
 * @property string|null $avatar_full
 * @property bool $is_worker
 * @property bool $phone_verified
 * @property int|null $testing_step
 * @property bool $moderated_worker
 * @property float $money
 * @property int|null $sex
 * @property int $user_id
 * @property int|null $city_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $avatar_thumb
 * @property int|null $quest_step
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Category[] $categories
 * @property-read \App\City|null $cities
 * @property-read \App\ProfileConfidant $confidant
 * @property-read mixed $age
 * @property-read mixed $birth
 * @property-read mixed $city
 * @property-read mixed $full_phone
 * @property-read \App\ProfileConfidant $profileConfidants
 * @property-read \App\User $users
 * @property-read \App\WorkerProfile $workerProfile
 * @property-read \App\WorkerProfile $workerProfiles
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ProfileDecorator whereAvatarFull($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ProfileDecorator whereAvatarThumb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ProfileDecorator whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ProfileDecorator whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ProfileDecorator whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ProfileDecorator whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ProfileDecorator whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ProfileDecorator whereIsWorker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ProfileDecorator whereModeratedWorker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ProfileDecorator whereMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ProfileDecorator whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ProfileDecorator wherePatronymic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ProfileDecorator wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ProfileDecorator wherePhoneCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ProfileDecorator wherePhoneVerified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ProfileDecorator whereQuestStep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ProfileDecorator whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ProfileDecorator whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ProfileDecorator whereTestingStep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ProfileDecorator whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ProfileDecorator whereUserId($value)
 * @mixin \Eloquent
 */
class ProfileDecorator extends Profile
{
    protected $table = 'profiles';

    protected $with = 'cities';

    protected $additional_appends = [
        'city'
    ];
    protected $visible = [
        'id', 'avatar_thumb','avatar_full','age','city','full_phone','phone','phone_code'
    ];

    public function __construct(array $attributes = [])
    {
        $this->setAppends(array_merge($this->appends,$this->additional_appends));
        parent::__construct($attributes);
    }

    public function getCityAttribute()
    {
        return $this->cities->name??'';
    }
}