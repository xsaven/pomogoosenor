<?php

namespace App\Decorators\Tasks;

use App\Decorators\Appendable;
use App\Http\Resources\APICollection;
use App\Http\Resources\Tasks\MyExecutorTasksResource;
use App\PriceRangersMask;
use App\Subcategory;
use App\User;
use App\UserTasks;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * App\Decorators\Tasks\UserTasksDecorator
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string|null $private_description
 * @property array $data
 * @property string|null $cost
 * @property int $email_report
 * @property int $executor_only
 * @property int $commentaries
 * @property int $subcategory_id
 * @property int $creator_id
 * @property int $executor_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $status
 * @property float $exact_cost
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Decorators\Tasks\CommentDecorator[] $comments
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Content[] $content_items
 * @property-read \App\Decorators\Tasks\UserDecorator $creator
 * @property-read \App\Review $creator_review
 * @property-read \App\Decorators\Tasks\UserDecorator $executor
 * @property-read \App\Review $executor_review
 * @property-read mixed $city
 * @property-read mixed $icon
 * @property-read mixed $is_creator
 * @property-read mixed $is_executor
 * @property-read mixed $opened_to
 * @property-read mixed $state
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Decorators\Tasks\OfferDecorator[] $offers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UserTaskReports[] $reports
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Decorators\Tasks\ReviewDecorator[] $reviews
 * @property-read \App\StatusTask $status_task
 * @property-read \App\Decorators\Tasks\SubcategoryDecorator $subcategory
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users_viewed
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UserTaskView[] $views
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserTasksDecorator whereCommentaries($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserTasksDecorator whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserTasksDecorator whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserTasksDecorator whereCreatorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserTasksDecorator whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserTasksDecorator whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserTasksDecorator whereEmailReport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserTasksDecorator whereExactCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserTasksDecorator whereExecutorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserTasksDecorator whereExecutorOnly($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserTasksDecorator whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserTasksDecorator whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserTasksDecorator wherePrivateDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserTasksDecorator whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserTasksDecorator whereSubcategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\UserTasksDecorator whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class UserTasksDecorator extends UserTasks
{

    use Appendable;

    protected $hidden = [
        'creator_id', 'executor_id', "creator_card_id", "executor_card_id"
    ];

    protected $appends = [
        'state', 'icon', 'city'
    ];

    public function subcategory()
    {
        return $this->belongsTo(SubcategoryDecorator::class);
    }

    public function executor()
    {
        return $this->belongsTo(UserDecorator::class, 'executor_id');
    }

    public function creator()
    {
        return $this->belongsTo(UserDecorator::class, 'creator_id');
    }

    public function comments()
    {
        return $this->hasMany(CommentDecorator::class, 'task_id');
    }

    public function offers()
    {
        return $this->hasMany(OfferDecorator::class, 'task_id');
    }

    public function reviews()
    {
        return $this->hasMany(ReviewDecorator::class, 'task_id');
    }

    public function getStateAttribute()
    {
        return $this->creator_id == user()->id ? 'creator' : 'executor';
    }

    public function getIconAttribute()
    {
        return $this->subcategory->category->icon_image ?? '';
    }

    public function getCityAttribute()
    {
        return 'Москва';
    }

    public function loadRelatedData()
    {
        return $this->load(['content_items', 'subcategory', 'creator', 'executor', 'offers', 'reviews' => function ($q) {
            return $q->whereHas('task', function ($q) {
                return $q->where('creator_id', \user()->id)
                    ->orWhere('executor_id', user()->id);
            });
        }, 'reviews.user']);
    }

    public function loadRelatedDataFilteredOffers($offer_filter)
    {
        $task = $this->load(['content_items', 'subcategory',
            'creator', 'executor',
            'offers' => function ($offer) use ($offer_filter) {
                if (!$offer_filter || $offer_filter == 'date')
                    return $offer->orderByDesc('created_at');
            }]);
        $offers = $task->offers;
        $offerss = $offers->sortByDesc(function ($offer) use ($offer_filter) {
            return (int)($offer->user->avg_mark['total'] * 10);
        });
        return $offerss;
        return $task;
//        if ($offer_filter == 'rate')
//            return $offer->where('user.id',78);
//        if ($offer_filter == 'reviews')
//            return $offer->where();
    }

    public static function getTasksStatistic()
    {
        $statistic = self::select('id', 'status', 'creator_id', 'executor_id')
            ->where('creator_id', user()->id)
            ->orWhere('executor_id', user()->id)
            ->orWhereHas('offers', function ($q) {
                return $q->whereHas('task', function ($q) {
                    return $q->where('status', 1);
                })->where('user_id', user()->id);
            })
            ->get()
            ->groupBy('state');
        $status_data = array_fill_keys(UserTasks::$status_data, 0);

        $statistic = collect($statistic)->map(function (Collection $stat) use ($status_data) {
            $total = $stat->count();
            $stat = $stat->groupBy('status')->map(function ($q) {
                return $q->count();
            })->toArray();
            $stat = array_merge($status_data, $stat);
            $stat['total'] = $total;
            return $stat;
        });
        $status_data['total'] = 0;
        $statistic['creator'] = $statistic['creator'] ?? $status_data;
        $statistic['executor'] = $statistic['executor'] ?? $status_data;
        return $statistic;
    }

    public static function getAllTasks($status, $category, $city, $name)
    {
        try {
            $tasks = self::with(['content_items', 'subcategory', 'creator', 'executor', 'offers', 'reviews' => function ($q) {
                return $q->whereHas('task', function ($q) {
                    return $q->where('creator_id', \user()->id)
                        ->orWhere('executor_id', user()->id);
                });
            }, 'reviews.user'])
                ->when($status && $status != 10, function ($q) use ($status) {
                    return $q->where('status', $status);
                })
                ->when(is_array($category) && !(in_array(0, $category)), function ($q) use ($category) {
                    return $q->whereHas('subcategory.category', function ($q) use ($category) {
                        return $q->whereIn('id', $category);
                    });
                })
                ->when($city && $city != 0, function ($q) use ($city) {
                    $city_id = ($city == 1) ? 1 : 173;
//                return $q->where('city_id', $city_id);
                })
                ->when($name, function ($q) use ($name) {
                    return $q->where('name', 'LIKE', "%$name%");
                })
                ->where('status', '!=', UserTasks::STATUS_DRAFT)
                ->where('status', '!=', UserTasks::STATUS_MODERATE)
                ->orderByDesc('created_at')
                ->paginate();
            return $tasks;
        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }
    }

    public static function getMyCreatorTasks($status, $user_type, $category)
    {
        $tasks = self::with(['content_items', 'subcategory', 'creator', 'executor', 'offers', 'reviews' => function ($q) {
            return $q->whereHas('task', function ($q) {
                return $q->where('creator_id', \user()->id)
                    ->orWhere('executor_id', user()->id);
            });
        }, 'reviews.user']);
        if ($user_type == 1) {
            $tasks->where('creator_id', user()->id);
        } else {
            $tasks->where('executor_id', user()->id);
        }
        if ($category) {
            $tasks->whereHas('subcategory.category', function ($q) use ($category) {
                return $q->where('id', $category);
            });
        }
        if ($status != 10) {
            $tasks->where('status', $status);
        }
        if ($status = self::STATUS_FINISHED) {
            $tasks->with(['reviews']);
        }
        $tasks = $tasks->orderByDesc('created_at')->paginate();
        return $tasks;
    }

    public static function filterOffers($tasks)
    {
        $tasks = collect($tasks)->toArray();
        foreach ($tasks['data'] as &$task) {
            if ($task['is_executor']) {
                foreach ($task['offers'] as &$offer) {
                    if ($offer['selected'] == true) {
                        $task['offers'] = [$offer];
                    }
                }
            }
        }
        return $tasks;
    }

    public static function getMyDrafts()
    {
        $tasks = self::with(['content_items', 'subcategory'])
            ->where('status', 0)
            ->where('creator_id', \user()->id);
        $tasks = $tasks->orderByDesc('created_at')->paginate();
        return $tasks;
    }

    public static function getMyExecutorTasks($status, $category)
    {
        $tasks = self::with([
            'content_items',
            'subcategory',
            'creator',
            'executor',
            'offers' => function ($q) {
                return $q->whereHas('task', function ($q) {
                    return $q->where('creator_id', \user()->id)
                        ->orWhere('executor_id', user()->id);
                })->orWhere('user_id', user()->id);
            }
        ]);
        $tasks->where(function ($q) {
            $q->whereHas('offers', function ($q) {
                return $q->where('user_id', user()->id);
            })->orWhere('executor_id', user()->id);
        });
        if ($category) {
            $tasks->whereHas('subcategory.category', function ($q) use ($category) {
                return $q->where('id', $category);
            });
        }
        if ($status != 10) {
            $tasks->where('status', $status);
        }
        if ($status == self::STATUS_FINISHED) {
            $tasks->with(['reviews' => function ($q) {
                return $q->whereHas('task', function ($q) {
                    return $q->where('creator_id', \user()->id)
                        ->orWhere('executor_id', user()->id);
                });
            },
                'reviews.user']);
        }
        $tasks = $tasks->orderByDesc('created_at')->paginate();
        return $tasks;
    }

    public static function getMyTasks($status, $user_type, $category)
    {
        $tasks = self::with(['content_items', 'subcategory', 'creator', 'executor', 'offers', 'reviews' => function ($q) {
            return $q->whereHas('task', function ($q) {
                return $q->where('creator_id', \user()->id)
                    ->orWhere('executor_id', user()->id);
            });
        }, 'reviews.user']);
        if ($user_type == 1) {
            $tasks->where('creator_id', user()->id);
        } else {
            $tasks->where('executor_id', user()->id);
        }
        if ($category) {
            $tasks->whereHas('subcategory.category', function ($q) use ($category) {
                return $q->where('id', $category);
            });
        }
        if ($status != 10) {
            $tasks->where('status', $status);
        }
        if ($status = self::STATUS_FINISHED) {
            $tasks->with(['reviews']);
        }
        $tasks = $tasks->orderByDesc('created_at')->paginate();
        return $tasks;
    }
}