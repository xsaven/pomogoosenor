<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 31 Jan 2018 10:15:46 +0000.
 */

namespace App\Decorators\Tasks;
use App\Decorators\Appendable;
use App\Review;
use App\UserTasks;

/**
 * App\Decorators\Tasks\ReviewDecorator
 *
 * @property int $id
 * @property bool $positive
 * @property int $type
 * @property string $text
 * @property int $politeness
 * @property int $punctuality
 * @property int $adequacy
 * @property int $price
 * @property int $quality
 * @property float|null $final_task_price
 * @property int $user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property int $task_id
 * @property-read \App\Decorators\Tasks\UserTasksDecorator $task
 * @property-read \App\Decorators\Tasks\UserDecorator $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ReviewDecorator whereAdequacy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ReviewDecorator whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ReviewDecorator whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ReviewDecorator whereFinalTaskPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ReviewDecorator whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ReviewDecorator wherePoliteness($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ReviewDecorator wherePositive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ReviewDecorator wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ReviewDecorator wherePunctuality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ReviewDecorator whereQuality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ReviewDecorator whereTaskId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ReviewDecorator whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ReviewDecorator whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ReviewDecorator whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Tasks\ReviewDecorator whereUserId($value)
 * @mixin \Eloquent
 */
class ReviewDecorator extends Review
{
    use Appendable;

    protected $table = 'reviews';

	protected $hidden = [
		'deleted_at',
        'created_at',
        'final_task_price',
        'task_id',
        'user_id',
	];

    public function user()
    {
        return $this->belongsTo(UserDecorator::class,'user_id');
    }

    public function task()
    {
        return $this->belongsTo(UserTasksDecorator::class,'task_id');
    }
}
