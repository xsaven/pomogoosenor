<?php

namespace App\Decorators\Categories;

use App\Category;
use App\Decorators\Appendable;

/**
 * App\Decorators\Categories\FlatCategoriesDecorator
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $icon
 * @property string|null $image
 * @property string|null $icon_image
 * @property string|null $slug
 * @property string|null $order
 * @property int|null $active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property array $marker
 * @property-read mixed $default_subcategory
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Subcategory[] $subcategories
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Categories\FlatCategoriesDecorator whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Categories\FlatCategoriesDecorator whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Categories\FlatCategoriesDecorator whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Categories\FlatCategoriesDecorator whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Categories\FlatCategoriesDecorator whereIconImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Categories\FlatCategoriesDecorator whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Categories\FlatCategoriesDecorator whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Categories\FlatCategoriesDecorator whereMarker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Categories\FlatCategoriesDecorator whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Categories\FlatCategoriesDecorator whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Categories\FlatCategoriesDecorator whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Decorators\Categories\FlatCategoriesDecorator whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FlatCategoriesDecorator extends Category
{

    use Appendable;
    protected $visible = [
        'id', 'name'
    ];
}