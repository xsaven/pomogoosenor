<?php

namespace App\Decorators;

use Reliese\Database\Eloquent\Model;

/**
 * Trait Appendable
 * @package App\Decorators
 * @mixin Model
 */
trait Appendable
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->appends = array_merge((new parent())->appends,$this->appends);
    }
}