<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Category
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $icon
 * @property string $image
 * @property string $icon_image
 * @property string|null $slug
 * @property string|null $order
 * @property array|null $marker
 * @property int|null $active
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read mixed $default_subcategory
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Subcategory[] $subcategories
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Category onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereIconImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Category withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Category withoutTrashed()
 * @mixin \Eloquent
 */
class Category extends BaseModel
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $table = 'categories';

    protected $fillable = [
        'name',
        'slug',
        'order',
        'active',
        'icon',
        'image',
        'icon_image',
        'marker'
    ];

    protected $hidden = [
//        'order',
//        'active',
        'created_at',
        'updated_at',
        'deleted_at',
//        'icon',
//        'slug'
    ];

    protected $casts = [
        'marker' => 'array'
    ];

    protected $appends = [
        'default_subcategory'
    ];

    public static function boot()
    {
        parent::boot();

        self::observe(new UserActionsObserver);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('order', 'asc')->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subcategories()
    {
        return $this->hasMany(Subcategory::class);
    }

    /**
     * Get default subcategory
     *
     * @return Model|null|static
     */
    public function defaultSubcategory()
    {
        return $this->subcategories()
            ->where('default', true)
            ->with(['range'])
            ->orderBy('order', 'asc')
            ->firstOrNew([]);
    }

    public function getDefaultSubcategoryAttribute()
    {
        $default_subcategory = $this->defaultSubcategory();

        if (!$default_subcategory) {
            $default_subcategory = $this->subcategories()->first();
        }

        if ($default_subcategory) {
            $default_subcategory = $default_subcategory->toArray();
            $default_subcategory['range'] = isset($default_subcategory['range']) ? $default_subcategory['range']['formatted_data'] : null;
        }
        return $default_subcategory;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getTree()
    {
        return self::with(['subcategories' => function ($q) {
            $q->orderBy('order', 'asc');
        }])->where('active', '1')
            ->orderBy('order')
            ->get();
    }

    public static function getFormattedTree()
    {
        $category_tree = self::with(['subcategories' => function ($q) {
            $q->with(['range'])->orderBy('order', 'asc');
        }])
            ->where('active', '1')
            ->orderBy('order')
            ->get()->toArray();
        foreach ($category_tree as &$category) {
            foreach ($category['subcategories'] as &$subcategory) {
                $subcategory['range'] = $subcategory['range']['formatted_data'];
            }
        }
        return $category_tree;
    }

    public function getImageAttribute($path)
    {
        return uploads_path($path);
    }

    public function getIconImageAttribute($path)
    {
        return uploads_path($path);
    }

    /**
     * @param array $marker
     */
    public function getMarkerAttribute($marker)
    {
        $marker = json_decode_try($marker);
        if (!empty($marker)) {
            foreach ($marker as &$marker_data) {
                if (isset($marker_data['file'])) {
                    $marker_data['file'] = uploads_path($marker_data['file']);
                } else {
//                    $marker_data['file'] = '';
                }
            }
            return $marker;
        }
        return $marker;
    }
}
