<?php

namespace App\Console\Commands;

use Artisan;
use Illuminate\Console\Command;

class Improve extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'improve';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate PHP-DOC comments for models';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Artisan::call('ide-helper:models', ['--write' => true, '--reset' => true]);
        $this->info('PHP-DOC comments for models has been generated');
    }
}
