<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\DialogMessage
 *
 * @property int $id
 * @property string $text
 * @property bool $seen
 * @property int|null $sender_id
 * @property int $dialog_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Dialog $dialog
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DialogParticipant[] $participants
 * @property-read \App\User|null $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DialogMessage dialogId($dialog_id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DialogMessage unreadForUser($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DialogMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DialogMessage whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DialogMessage whereDialogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DialogMessage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DialogMessage whereSeen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DialogMessage whereSenderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DialogMessage whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DialogMessage whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DialogMessage extends Model
{

    protected $fillable = [
        'sender_id', 'dialog_id', 'seen', 'text'
    ];

    protected $casts = [
        'seen' => 'boolean'
    ];

    protected $hidden = [
        'deleted_at'
    ];

    public function dialog()
    {
        return $this->belongsTo(Dialog::class);
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }

    public function participants()
    {
        return $this->hasMany(DialogParticipant::class, 'dialog_id','dialog_id');
    }


    public function getMsgPerDaysCount($dialog_id,$user_id,$day)
    {
        return $this->where('sender_id',$user_id)
            ->dialogId($dialog_id)
            ->whereDay('created_at', $day)
            ->count();
    }

    public function scopeDialogId($q,$dialog_id)
    {
        if(isset($dialog_id))
            $q->where('dialog_id',$dialog_id);
    }



    /**
     * Returns unread messages given the userId.
     *
     * @param Builder $query
     * @param $userId
     * @return Builder
     */
    public function scopeUnreadForUser(Builder $query, $userId)
    {
        return $query->has('dialog')
            ->where('sender_id', '!=', $userId)
            ->whereHas('participants', function (Builder $query) use ($userId) {
                $query->where('user_id', $userId)
                    ->where(function (Builder $q) {
                        $q->where('last_read', '<', $this->getConnection()->raw($this->getConnection()->getTablePrefix() . $this->getTable() . '.created_at'))
                            ->orWhereNull('last_read');
                    });
            });
    }


    /*
     * Admin
     */

    public function getAllMsgByChatId($id)
    {
        return $this->where('dialog_id', $id)
            ->orderBy('created_at','DESC')
            ->with('users')
            ->paginate('30');

    }


}
