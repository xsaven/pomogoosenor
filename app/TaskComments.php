<?php

namespace App;

use App\Decorators\Tasks\CommentDecorator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Date\DateFormat;

/**
 * App\TaskComments
 *
 * @property int $id
 * @property int $user_id
 * @property int $task_id
 * @property int|null $answer_for_id
 * @property string $message
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\TaskComments $answer
 * @property-read \App\TaskComments $answer_for
 * @property-read mixed $is_answer
 * @property mixed $order
 * @property-read \App\UserTasks $task
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskComments getSortedList()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskComments whereAnswerForId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskComments whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskComments whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskComments whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskComments whereTaskId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskComments whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskComments whereUserId($value)
 * @mixin \Eloquent
 */
class TaskComments extends BaseModel
{
    protected $table = 'user_tasks_comments';

    protected $fillable = [
        'user_id',
        'task_id',
        'answer_for_id',
        'message'
    ];

    protected $appends = [
        'is_answer','order'
    ];


    public function getOrderAttribute()
    {
        return $this->attributes['order']??0;
    }

    public function setOrderAttribute($value)
    {
        $this->attributes['order'] = $value;
    }

    public function answer_for()
    {
        return $this->hasOne(TaskComments::class, 'id', 'answer_for_id');
    }

    public function answer()
    {
        return $this->belongsTo(TaskComments::class, 'id', 'answer_for_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function task()
    {
        return $this->belongsTo(UserTasks::class, 'task_id');
    }

    public function getIsAnswerAttribute()
    {
        return (bool)$this->answer_for_id;
    }

    public function getAnswerForIdAttribute($value)
    {
        return $value ?? '';
    }

    public function getUpdatedAtAttribute($attr)
    {
        return DateFormat::post($attr);
    }

    public function getCreatedAtAttribute($attr)
    {
        return DateFormat::post($attr);
    }

    /**
     * Get sorted comments list
     *
     * @param Builder $query
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function scopeGetSortedList(Builder $query)
    {
        $comments = $query->with('answer')->get();
        $comments->each(function ($item) use ($comments) {
            if ($item->answer) {
                $index = $comments->search(function ($value, $key) use ($item) {
                    return $value == $item;
                });
                $comments->splice($index + 1, 0, [$item->answer]);
            }
        })->transform(function ($item,$key) {
            /**
             * @var CommentDecorator $item
             */
            unset($item->answer);
            $item->order = 1;
            return $item;
        });
        dd($comments->unique()->toArray());
        return $comments->unique()->toArray();
    }

    /**
     * @param UserTasks $task
     * @param array $data
     *
     * @return bool
     */
    public static function store($task, $data)
    {
        $data['user_id'] = user()->id;
        return (bool)$task->comments()->create($data);
    }
}
