<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\StaticMainInfo
 *
 * @property int $id
 * @property string $key
 * @property string|null $comment
 * @property string $value
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StaticMainInfo whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StaticMainInfo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StaticMainInfo whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StaticMainInfo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StaticMainInfo whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StaticMainInfo whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StaticMainInfo whereValue($value)
 * @mixin \Eloquent
 */
class StaticMainInfo extends Model {

   public $timestamps = false;

    protected $table    = 'static_main_info';
    
    protected $fillable = [
          'key',
          'comment',
          'value'
    ];

}