<?php
/**
 * Created by PhpStorm.
 * User: Uer
 * Date: 04.05.2018
 * Time: 18:31
 */

namespace App\Services\WalletOneSafeDeal;


use DateTimeZone;
use Guzzle\Http\Client;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

/**
 * Class WOAPIRequest
 *
 * @package App\Services\WalletOneSafeDeal
 */
class WOWebRequest extends SafeDealSetup
{
    private $url;
    private $data;
    private $timestamp;

    public function setBaseRequestData($url, $data)
    {
        $this->url = $url;
        $data['PlatformId'] = $this->getPlatformId();
        $data['Timestamp'] = now()->setTimezone(new DateTimeZone('UTC'))->format("Y-m-d\TH:i:s");
        $data['Language'] = 'ru';
        $this->data = $data;
        return $this;
    }


    /**
     * @param string $url
     * @param $data
     */
    public function post(string $url, $data)
    {
        $this->client = new Client(['allow_redirects' => ['track_redirects' => true]]);
        $this->setBaseRequestData($url, $data);
        $this->data['Signature'] = $this->getSignature();
        $this->getRedirectResponse()->send();
    }

    public function getRedirectResponse()
    {
            $hiddenFields = '';
            foreach ($this->getRedirectData() as $key => $value) {
                $hiddenFields .= sprintf(
                        '<input type="hidden" name="%1$s" value="%2$s" />',
                        htmlentities($key, ENT_QUOTES, 'UTF-8', false),
                        htmlentities($value, ENT_QUOTES, 'UTF-8', false)
                    )."\n";
            }

            $output = '<!DOCTYPE html>
                        <html>
                            <head>
                                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                                <title>Redirecting...</title>
                            </head>
                            <body onload="document.forms[0].submit();">
                                <form action="%1$s" method="post">
                                    <p>Redirecting to payment page...</p>
                                    <p>
                                        %2$s
                                        <input type="submit" value="Continue" />
                                    </p>
                                </form>
                            </body>
                        </html>';
            $output = sprintf(
                $output,
                htmlentities($this->getRedirectUrl(), ENT_QUOTES, 'UTF-8', false),
                $hiddenFields
            );

            return HttpResponse::create($output);
    }

    /**
     * @return bool
     */
    public function isSuccessful()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function isRedirect()
    {
        return true;
    }

    /**
     * @return string
     */
    public function getRedirectUrl()
    {
        return $this->getRequestUrl();
    }

    /**
     * @return string
     */
    public function getRedirectMethod()
    {
        return 'POST';
    }

    /**
     * @return array
     */
    public function getRedirectData()
    {
        return $this->data;
    }

    protected function stringifyData($data)
    {
        foreach ($data as $name => $val) {
            if (is_array($val)) {
                usort($val, 'strcasecmp');
                $fields[$name] = $val;
            }
        }

        uksort($data, 'strcasecmp');
        $string = '';

        foreach ($data as $value) {
            if (is_array($value)) {
                foreach ($value as $v) {
                    $string .= $v;
                }
            } else {
                $string .= $value;
            }
        }
        return $string;
    }

    protected function getData($data, $is_json)
    {
        if (!$is_json) {
            $string = $this->stringifyData($data);
        } else {
            $string = json_encode($data);
        }
        return $string;
    }

    protected function getRequestUrl()
    {
        return $this->getWebUrl() . $this->url;
    }

    /**
     * @param string $requestBody
     * @return string
     */
    protected function setDataToEncrypt(string $requestBody)
    {
        return $requestBody .
            $this->getSecretKey();
    }

    /**
     * Get encrypted Signature
     *
     * @param bool $is_json
     * @return string
     */
    public function getSignature($is_json = true)
    {
        $requestBody = '';
        if (!empty($this->data)) {
            $requestBody = $this->getData($this->data, false);
        }
        $data_to_encrypt = $this->setDataToEncrypt($requestBody);
        return base64_encode(pack('H*', hash($this->getAlgorithm(), $data_to_encrypt)));
    }

    /**
     * @return mixed
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }
}