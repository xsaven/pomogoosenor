<?php
/**
 * Created by PhpStorm.
 * User: Uer
 * Date: 04.05.2018
 * Time: 18:31
 */

namespace App\Services\WalletOneSafeDeal;


use GuzzleHttp\Exception\ClientException;

/**
 * Class WOAPIRequest
 *
 * @method \Psr\Http\Message\StreamInterface get(string $url, string $data = '')
 * @method \Psr\Http\Message\StreamInterface delete(string $url, string $data = '')
 * @method \Psr\Http\Message\StreamInterface post(string $url, string $data)
 * @method \Psr\Http\Message\StreamInterface put(string $url, string $data = '')
 * @package App\Services\WalletOneSafeDeal
 */
class WOAPIRequest extends SafeDealSetup
{
    private $url;
    private $data;
    private $timestamp;

    const AVAILABLE_METHODS = ['get', 'post', 'put', 'delete'];

    protected function json_decode_try($json) {
        $decoded = json_decode($json,true);
        if(json_last_error() == JSON_ERROR_NONE){
            return $decoded;
        }
        return $json;
    }

    public function __call($name, $arguments)
    {
        if (in_array($name, $this::AVAILABLE_METHODS)) {
            try {
                return call_user_func_array(array(&$this, $name . 'Request'), $arguments);
            } catch (ClientException $exception) {
                return [
                    'success' => false,
                    'message' => $this->getClientExceptionMessage($exception),
                ];
            }
        }
        return call_user_func_array(array(&$this, $name), $arguments);
    }

    private function getClientExceptionMessage(ClientException $exception)
    {
        return json_decode($exception->getResponse()->getBody()->getContents());
    }

    public function setBaseRequestData($url, $data = '')
    {
        $this->url = $url;
        $this->data = $data;
        $this->timestamp = now()->format("Y-m-d\TH:i:s");
        return $this;
    }

    /**
     * @param string $url
     * @param string $data
     * @return \Psr\Http\Message\StreamInterface
     */
    protected function getRequest(string $url, $data = '')
    {
        if (is_array($data)) {
            $url .= '?' . http_build_query($data);
        }
        $this->setBaseRequestData($url);

        return $this->json_decode_try($this->client->get($this->getRequestUrl(), [
            'query' => $data,
            'headers' => $this->getRequestHeaders()
        ])->getBody());
    }

    /**
     * @param string $url
     * @param $data
     * @return \Psr\Http\Message\StreamInterface
     */
    protected function postRequest(string $url, $data)
    {
        $this->setBaseRequestData($url, $data);
        return $this->json_decode_try($this->client->post($this->getRequestUrl(), [
            'json' => $data,
            'headers' => $this->getRequestHeaders()
        ])->getBody());
    }

    /**
     * @param string $url
     * @param $data
     * @return \Psr\Http\Message\StreamInterface
     */
    protected function putRequest(string $url, $data=[])
    {
        $this->setBaseRequestData($url, $data);
        return $this->json_decode_try($this->client->put($this->getRequestUrl(), [
            'form_params'  => $data,
            'headers' => $this->getRequestHeaders()
        ])->getBody());
    }

    /**
     * @param string $url
     * @param string $data
     * @return \Psr\Http\Message\StreamInterface
     */
    protected function deleteRequest(string $url, $data = '')
    {
        if (is_array($data)) {
            $url .= '?' . http_build_query($data);
        }
        $this->setBaseRequestData($url);

        return $this->json_decode_try($this->client->delete($this->getRequestUrl(), [
            'headers' => $this->getRequestHeaders()
        ])->getBody());
    }

    protected function stringifyData($data)
    {
        foreach ($data as $name => $val) {
            if (is_array($val)) {
                usort($val, 'strcasecmp');
                $fields[$name] = $val;
            }
        }

        uksort($data, 'strcasecmp');
        $string = '';

        foreach ($data as $value) {
            if (is_array($value)) {
                foreach ($value as $v) {
                    $string .= $v;
                }
            } else {
                $string .= $value;
            }
        }
        return $string;
    }

    protected function getData($data, $is_json)
    {
        if (!$is_json) {
            $string = $this->stringifyData($data);
        } else {
            $string = json_encode($data);
        }
        return $string;
    }

    protected function getRequestUrl()
    {
        return $this->getApiUrl() . $this->url;
    }

    /**
     * @param string $requestBody
     * @return string
     */
    protected function setDataToEncrypt(string $requestBody)
    {
        return $this->getRequestUrl() .
            $this->getTimestamp() .
            $requestBody .
            $this->getSecretKey();
    }

    /**
     * Get encrypted Signature
     *
     * @param bool $is_json
     * @return string
     */
    public function getSignature($is_json = true)
    {
        $requestBody = '';
        if (!empty($this->data)) {
            $requestBody = $this->getData($this->data, $is_json);
        }
        $data_to_encrypt = $this->setDataToEncrypt($requestBody);
        return base64_encode(pack('H*', hash($this->getAlgorithm(), $data_to_encrypt)));
    }

    public function getRequestHeaders($to_bulk = false)
    {
        $headers = [
            'X-Wallet-PlatformId' => $this->getPlatformId(),
            'X-Wallet-Signature' => $this->getSignature(),
            'X-Wallet-Timestamp' => $this->getTimestamp(),
            'Accept' => 'application/json'
        ];
        if ($to_bulk) {
            foreach ($headers as $header_k => $header_v) {
                echo $header_k . ':' . $header_v . "\n";
            }
            die;
        }
        return $headers;
    }

    /**
     * @return mixed
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }
}