<?php
/**
 * Created by PhpStorm.
 * User: Uer
 * Date: 04.05.2018
 * Time: 20:47
 */

namespace App\Services\WalletOneSafeDeal\Requests;

use App\Services\WalletOneSafeDeal\WOAPIRequest;

class BaseApiRequest
{
    /**
     * @var WOAPIRequest
     */
    protected $request;

    public function __construct()
    {
        $this->request = new WOAPIRequest();
    }

    protected function getClosureParams(&$requestParams, string $closureType)
    {
        if ($requestParams instanceof \Closure){
            $closureClass = new $closureType();
            $requestParams = $requestParams($closureClass)->toArray();
        }
    }

}