<?php
/**
 * Created by PhpStorm.
 * User: Uer
 * Date: 04.05.2018
 * Time: 20:47
 */

namespace App\Services\WalletOneSafeDeal\Requests;

/**
 * Class DealRequests
 *
 * Запросы для работы с сделками
 *
 * @package App\Services\WalletOneSafeDeal\Requests
 */
class DealRequests extends BaseApiRequest
{
    /**
     * Получение статуса сделки.
     * Getting the status of a deal.
     *
     * @param string|integer $platformDealId Идентификатор сделки на стороне площадки
     *
     * @link https://api.dev.walletone.com/p2p/swagger/ui/index#!/P2PApi03/P2PApi03_GetDeal
     * @link https://docs.google.com/document/d/17WBBerMcfyIjKRIXadXBiPlIEbAyKsTdHn0LrCOHVCs/edit#heading=h.dl5scodr5t52
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function getDeal($platformDealId)
    {
        return $this->request->get('/deals/' . $platformDealId);
    }

    /**
     * Регистрация сделки.
     *
     * **$requestParams example value**
     * ```
     * $req->saveDeal(function (\App\Services\WalletOneSafeDeal\Requests\Params\SaveDealParams $params){
     *   $params->PlatformDealId = 1;
     *   $params->PlatformPayerId = 1;
     *   $params->PayerPhoneNumber = '+31413431431';
     *   $params->PayerPaymentToolId = 1;
     *   $params->PlatformBeneficiaryId = 1;
     *   $params->BeneficiaryPaymentToolId = 1;
     *   $params->Amount = '1';
     *   $params->CurrencyId = 1;
     *   $params->ShortDescription = 'adfda';
     *   $params->FullDescription = 'adfadfadfadf';
     *   $params->DeferPayout = true;
     *   return $params;
     *   });
     * ```
     *
     * @param array|\Closure $requestParams Параметры запроса
     *
     * @link https://api.dev.walletone.com/p2p/swagger/ui/index#!/P2PApi03/P2PApi03_GetDeal
     * @link https://docs.google.com/document/d/17WBBerMcfyIjKRIXadXBiPlIEbAyKsTdHn0LrCOHVCs/edit#heading=h.dl5scodr5t52
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function saveDeal($requestParams)
    {
        $this->getClosureParams($requestParams, \App\Services\WalletOneSafeDeal\Requests\Params\SaveDealParams::class);
        return $this->request->post('/deals', $requestParams);
    }

    /**
     * Завершение сделки.
     *
     * @param string|integer $platformDealId Идентификатор сделки на стороне площадки
     *
     * @link https://api.dev.walletone.com/p2p/swagger/ui/index#!/P2PApi03/P2PApi03_CompleteDeal
     * @link https://docs.google.com/document/d/17WBBerMcfyIjKRIXadXBiPlIEbAyKsTdHn0LrCOHVCs/edit#heading=h.ptyvwg958fbj
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function completeDeal($platformDealId)
    {
        return $this->request->put('/deals/'.$platformDealId.'/complete');
    }

    /**
     * Подтверждение сделки.
     *
     * @param string|integer $platformDealId Идентификатор сделки на стороне площадки
     *
     * @link https://api.dev.walletone.com/p2p/swagger/ui/index#!/P2PApi03/P2PApi03_ConfirmDeal
     * @link https://docs.google.com/document/d/17WBBerMcfyIjKRIXadXBiPlIEbAyKsTdHn0LrCOHVCs/edit#heading=h.iuqcaa290ql
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function confirmDeal($platformDealId)
    {
        return $this->request->put('/deals/'.$platformDealId.'/confirm');
    }

    /**
     * Отмена сделки.
     *
     * $requestParams
     *  [
     *      "WithCommission" => true
     *  ]
     *
     * @param string|integer $platformDealId Идентификатор сделки на стороне площадки
     * @param array|\Closure $requestParams Параметры запроса (опционально)
     *
     *
     * @link https://api.dev.walletone.com/p2p/swagger/ui/index#!/P2PApi03/P2PApi03_CancelDeal
     * @link https://docs.google.com/document/d/17WBBerMcfyIjKRIXadXBiPlIEbAyKsTdHn0LrCOHVCs/edit#heading=h.em7lzpfb81zw
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function cancelDeal($platformDealId, $requestParams=[])
    {
        return $this->request->put('/deals/'.$platformDealId.'/cancel', $requestParams);
    }

    /**
     * Изменение инструмента исполнителя по сделке.
     *
     * **$requestParams example value**
     * ```
     *  [
     *      "PaymentToolId" => 1, //Идентификатор инструмента исполнителя
     *      "AutoComplete" => true //Инициировать процедуру завершения сделки после замены инструмента
     *  ]
     * ```
     *
     * @param string|integer $platformDealId Идентификатор сделки на стороне площадки
     * @param array|\Closure $requestParams Параметры запроса
     *
     * @link https://api.dev.walletone.com/p2p/swagger/ui/index#!/P2PApi03/P2PApi03_SetDealBeneficiaryPaymentTool
     * @link https://docs.google.com/document/d/17WBBerMcfyIjKRIXadXBiPlIEbAyKsTdHn0LrCOHVCs/edit#heading=h.frn4b0xc179j
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function setDealBeneficiaryPaymentTool($platformDealId, $requestParams)
    {
        return $this->request->put('/deals/'.$platformDealId.'/beneficiary/tool', $requestParams);
    }

    /**
     * Изменение инструмента заказчика по сделке.
     *
     * **$requestParams example value**
     * ```
     *  [
     *      "PaymentToolId" => 1, //Идентификатор инструмента заказчика
     *  ]
     * ```
     *
     * @param string|integer $platformDealId Идентификатор сделки на стороне площадки
     * @param array|\Closure $requestParams Параметры запроса
     *
     * @link https://api.dev.walletone.com/p2p/swagger/ui/index#!/P2PApi03/P2PApi03_SetDealPayerPaymentTool
     * @link https://docs.google.com/document/d/17WBBerMcfyIjKRIXadXBiPlIEbAyKsTdHn0LrCOHVCs/edit#heading=h.r1muvz6h0g57
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function setDealPayerPaymentTool($platformDealId, $requestParams)
    {
        return $this->request->put('/deals/'.$platformDealId.'/payer/tool', $requestParams);
    }

    /**
     * Массовое завершение сделок.
     *
     * **$requestParams example value**
     * ```
     * [
     *     "PlatformDeals": [ // Идентификаторы сделок, которые необходимо завершить
     *          "Deal1",
     *          "Deal2"
     *     ],
     *     "PaymentToolId": 12345 // необязательный параметр, идентификатор инструмента вывода,
     *                            // который будет установлен для всех указанных сделок.
     *                            // Если не передан, то метод попытается вывести на реквизиты, указанные в сделках
     * ]
     * ```
     *
     * @param string|integer $platformDealId Идентификатор сделки на стороне площадки
     * @param array|\Closure $requestParams Параметры запроса
     *
     * @link https://api.dev.walletone.com/p2p/swagger/ui/index#!/P2PApi03/P2PApi03_CompleteDeals
     * @link https://docs.google.com/document/d/17WBBerMcfyIjKRIXadXBiPlIEbAyKsTdHn0LrCOHVCs/edit#heading=h.adcijuuza161
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function completeDeals($platformDealId, $requestParams)
    {
        return $this->request->put('/deals/complete', $requestParams);
    }


}