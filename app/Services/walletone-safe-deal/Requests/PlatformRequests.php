<?php
/**
 * Created by PhpStorm.
 * User: Uer
 * Date: 04.05.2018
 * Time: 20:47
 */

namespace App\Services\WalletOneSafeDeal\Requests;


/**
 * Class PlatformRequests
 *
 * Запросы для работы с платформой
 *
 * @package App\Services\WalletOneSafeDeal\Requests
 */
class PlatformRequests extends BaseApiRequest
{
    /**
     * Получение списка доступных способов ввода для площадки
     * Getting a list of available input methods for the site
     *
     * @link https://api.dev.walletone.com/p2p/swagger/ui/index#!/P2PApi03/P2PApi03_GetPayinPaymentTypes
     * @link https://docs.google.com/document/d/17WBBerMcfyIjKRIXadXBiPlIEbAyKsTdHn0LrCOHVCs/edit#heading=h.2f8hueu9yqc9
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function getPayinPaymentTypes()
    {
        return $this->request->get('/payin/paymenttypes');
    }

    /**
     * Получение списка доступных способов вывода для площадки
     * Getting a list of available output methods for the site
     *
     * @link https://api.dev.walletone.com/p2p/swagger/ui/index#!/P2PApi03/P2PApi03_GetPayoutPaymentTypes
     * @link https://docs.google.com/document/d/17WBBerMcfyIjKRIXadXBiPlIEbAyKsTdHn0LrCOHVCs/edit#heading=h.lbsa2lv8h32m
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function getPayoutPaymentTypes()
    {
        return $this->request->get('/payout/paymenttypes');
    }
}