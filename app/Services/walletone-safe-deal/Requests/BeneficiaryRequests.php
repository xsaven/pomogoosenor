<?php
/**
 * Created by PhpStorm.
 * User: Uer
 * Date: 04.05.2018
 * Time: 20:47
 */

namespace App\Services\WalletOneSafeDeal\Requests;

/**
 * Class DealRequests
 *
 * Запросы для работы с исполнителями
 *
 * @package App\Services\WalletOneSafeDeal\Requests
 */
class BeneficiaryRequests extends BaseApiRequest
{
    /**
     * Получение списка инструментов исполнителя.
     *
     * @param string|integer $beneficiaryId Идентификатор исполнителя на стороне площадки
     * @param array|\Closure $requestParams Параметры запроса
     *
     * @link https://api.dev.walletone.com/p2p/swagger/ui/index#!/P2PApi03/P2PApi03_GetBeneficiaryPaymentTools
     * @link https://docs.google.com/document/d/17WBBerMcfyIjKRIXadXBiPlIEbAyKsTdHn0LrCOHVCs/edit#heading=h.o40seg9n0322
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function getBeneficiaryPaymentTools($beneficiaryId, $requestParams = null)
    {
        return $this->request->get('/beneficiaries/' . $beneficiaryId . '/tools', $requestParams);
    }

    /**
     * Получение инструмента исполнителя.
     *
     * @param string|integer $beneficiaryId Идентификатор исполнителя на стороне площадки
     * @param string|integer $paymentToolId Идентификатор инструмента исполнителя
     *
     * @link https://api.dev.walletone.com/p2p/swagger/ui/index#!/P2PApi03/P2PApi03_GetBeneficiaryPaymentTool
     * @link https://docs.google.com/document/d/17WBBerMcfyIjKRIXadXBiPlIEbAyKsTdHn0LrCOHVCs/edit#heading=h.xoep3ggwmqei
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function getBeneficiaryPaymentTool($beneficiaryId, $paymentToolId)
    {
        return $this->request->get('/beneficiaries/' . $beneficiaryId . '/tools/' . $paymentToolId);
    }

    /**
     * Удаление привязанного инструмента исполнителя.
     *
     * @param string|integer $beneficiaryId Идентификатор исполнителя на стороне площадки
     * @param string|integer $paymentToolId Идентификатор инструмента исполнителя
     *
     * @link https://api.dev.walletone.com/p2p/swagger/ui/index#!/P2PApi03/P2PApi03_DeleteBeneficiaryPaymentTool
     * @link https://docs.google.com/document/d/17WBBerMcfyIjKRIXadXBiPlIEbAyKsTdHn0LrCOHVCs/edit#heading=h.7qqij827mkko
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function deleteBeneficiaryPaymentTool($beneficiaryId, $paymentToolId)
    {
        return $this->request->delete('/beneficiaries/' . $beneficiaryId . '/tools/' . $paymentToolId);
    }

    /**
     * Получение списка выплат по исполнителю.
     *
     * @param string|integer $beneficiaryId Идентификатор исполнителя на стороне площадки
     * @param array|\Closure $requestParams Параметры запроса
     *
     * @link https://api.dev.walletone.com/p2p/swagger/ui/index#!/P2PApi03/P2PApi03_GetBeneficiaryPayouts
     * @link https://docs.google.com/document/d/17WBBerMcfyIjKRIXadXBiPlIEbAyKsTdHn0LrCOHVCs/edit#heading=h.nj5q7kutimn1
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function getBeneficiaryPayouts($beneficiaryId, $requestParams = null)
    {
        return $this->request->get('/beneficiaries/' . $beneficiaryId . '/payouts/', $requestParams);
    }

    /**
     * Получение списка сделок  по исполнителю.
     *
     * @param string|integer $beneficiaryId Идентификатор исполнителя на стороне площадки
     * @param array|\Closure $requestParams Параметры запроса
     *
     * @link https://api.dev.walletone.com/p2p/swagger/ui/index#!/P2PApi03/P2PApi03_GetBeneficiaryDeals
     * @link https://docs.google.com/document/d/17WBBerMcfyIjKRIXadXBiPlIEbAyKsTdHn0LrCOHVCs/edit#heading=h.slcnnlz5l5ey
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function getBeneficiaryDeals($beneficiaryId, $requestParams = null)
    {
        return $this->request->get('/beneficiaries/' . $beneficiaryId . '/deals/', $requestParams);
    }

}