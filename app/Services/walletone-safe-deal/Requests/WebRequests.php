<?php
/**
 * Created by PhpStorm.
 * User: Uer
 * Date: 04.05.2018
 * Time: 20:47
 */

namespace App\Services\WalletOneSafeDeal\Requests;
use App\Services\WalletOneSafeDeal\WOWebRequest;

/**
 * Class DealRequests
 *
 * Запоросы для оплаты сделки, привязки карт
 * Эти запросы возвращают объект Symfony\Component\HttpFoundation\Response,
 * который генерирует страницу с редиректом на севв
 *
 * @package App\Services\WalletOneSafeDeal\Requests
 */
class WebRequests
{
    /**
     * Добавление (привязка) карты исполнителя.
     *
     * **$requestParams example value:**
     * ```
     * [
     *  "PlatformBeneficiaryId" => 1, // Идентификатор исполнителя на стороне площадки
     *  "PhoneNumber" => '+380666086993', // Номер телефона исполнителя
     *  "Title" => 'Taras L.', // Наименование исполнителя (опционально)
     *  "ReturnUrl" => 'http://pom.laraman.ru/payments', // Урл возврата пользователя
     * ]
     * ```
     *
     * @param $requestParams
     *
     * @link https://docs.google.com/document/d/17WBBerMcfyIjKRIXadXBiPlIEbAyKsTdHn0LrCOHVCs/edit#heading=h.q2w6otqnjchh
     *
     */
    public function bindBeneficiaryCard($requestParams)
    {
        $request = new \App\Services\WalletOneSafeDeal\WOWebRequest();
        return $request->post('/v2/beneficiary',$requestParams);
    }

    /**
     * Добавление (привязка) карты заказчика.
     *
     * **$requestParams example value:**
     * ```
     * [
     *  "PlatformPayerId" => 1, // Идентификатор заказчика на стороне площадки
     *  "PhoneNumber" => '+380666086993', // Номер телефона заказчика
     *  "Title" => 'Taras L.', // Наименование заказчика (опционально)
     *  "ReturnUrl" => 'http://pom.laraman.ru/payments', // Урл возврата пользователя
     * ]
     * ```
     *
     * @param $requestParams
     *
     * @link https://docs.google.com/document/d/17WBBerMcfyIjKRIXadXBiPlIEbAyKsTdHn0LrCOHVCs/edit#heading=h.ixl4sssbxyca
     *
     */
    public function bindPayerCard($requestParams)
    {
        $request = new \App\Services\WalletOneSafeDeal\WOWebRequest();
        return $request->post('/v2/payer',$requestParams);
    }

    /**
     * Оплата сделки.
     *
     * **$requestParams example value:**
     * ```
     * [
     *  "PlatformDealId" => 'c7769d6e-5233-11e8-9c2d-fa7ae01bbebc', // Идентификатор сделки на стороне площадки
     *  "ReturnUrl" => 'http://pom.laraman.ru/payments', // Урл возврата пользователя
     * ]
     * ```
     *
     * @param $requestParams
     *
     * @link https://docs.google.com/document/d/17WBBerMcfyIjKRIXadXBiPlIEbAyKsTdHn0LrCOHVCs/edit#heading=h.uqwabtiivxt3
     *
     */
    public function payDeal($requestParams)
    {
        $request = new \App\Services\WalletOneSafeDeal\WOWebRequest();
//        dd(url('/deal/pay',$requestParams));
        return $request->post('/deal/pay',$requestParams);
    }

    public function requestHasValidSignature()
    {
        if (request()->has('Signature')) {
            $woWebRequest = new WOWebRequest();
            $signature_method = $woWebRequest->getAlgorithm();
            $secret_key = $woWebRequest->getSecretKey();
            $request_data = request()->all();
            foreach ($request_data as $name => $value) {
                if ($name !== "Signature") $params[$name] = $value;
            }
            uksort($params, "strcasecmp");
            $values = "";
            foreach ($params as $name => $value) {
                $values .= $value;
            }

            $signature = base64_encode(pack('H*', hash($signature_method, $values . $secret_key)));

            return request('Signature') == $signature;
        }
        return false;
    }

}