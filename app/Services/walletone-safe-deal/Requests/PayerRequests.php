<?php
/**
 * Created by PhpStorm.
 * User: Uer
 * Date: 04.05.2018
 * Time: 20:47
 */

namespace App\Services\WalletOneSafeDeal\Requests;

/**
 * Class DealRequests
 *
 * Запросы для работы с заказчиками
 *
 * @package App\Services\WalletOneSafeDeal\Requests
 */
class PayerRequests extends BaseApiRequest
{
    /**
     * Получение инструмента заказчика.
     *
     * @param string|integer $payerId Идентификатор заказчика на стороне площадки
     * @param array|\Closure $requestParams Параметры запроса
     *
     * @link https://api.dev.walletone.com/p2p/swagger/ui/index#!/P2PApi03/P2PApi03_GetPayerPaymentTools
     * @link https://docs.google.com/document/d/17WBBerMcfyIjKRIXadXBiPlIEbAyKsTdHn0LrCOHVCs/edit#heading=h.vdd30zisivm6
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function getPayerPaymentTools($payerId, $requestParams = null)
    {
        return $this->request->get('/payers/' . $payerId . '/tools', $requestParams);
    }


    /**
     * Получение инструмента заказчика.
     *
     * @param string|integer $payerId Идентификатор заказчика на стороне площадки
     * @param string|integer $paymentToolId Идентификатор инструмента заказчика
     *
     * @link https://api.dev.walletone.com/p2p/swagger/ui/index#!/P2PApi03/P2PApi03_GetPayerPaymentTool
     * @link https://docs.google.com/document/d/17WBBerMcfyIjKRIXadXBiPlIEbAyKsTdHn0LrCOHVCs/edit#heading=h.vdd30zisivm6
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function getPayerPaymentTool($payerId, $paymentToolId)
    {
        return $this->request->get('/payers/' . $payerId . '/tools/' . $paymentToolId);
    }

    /**
     * Удаление привязанного инструмента заказчика.
     *
     * @param string|integer $payerId Идентификатор заказчика на стороне площадки
     * @param string|integer $paymentToolId Идентификатор инструмента заказчика
     *
     * @link https://api.dev.walletone.com/p2p/swagger/ui/index#!/P2PApi03/P2PApi03_DeletePayerPaymentTool
     * @link https://docs.google.com/document/d/17WBBerMcfyIjKRIXadXBiPlIEbAyKsTdHn0LrCOHVCs/edit#heading=h.o0hwy6uogqnt
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function deletePayerPaymentTool($payerId, $paymentToolId)
    {
        return $this->request->delete('/payers/' . $payerId . '/tools/' . $paymentToolId);
    }

    /**
     * Получение списка возвратов по заказчику.
     *
     * @param string|integer $payerId Идентификатор заказчика на стороне площадки
     * @param array|\Closure $requestParams Параметры запроса
     *
     * @link https://api.dev.walletone.com/p2p/swagger/ui/index#!/P2PApi03/P2PApi03_GetPayerRefunds
     * @link https://docs.google.com/document/d/17WBBerMcfyIjKRIXadXBiPlIEbAyKsTdHn0LrCOHVCs/edit#heading=h.rzapowugmvci
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function getPayerRefunds($payerId, $requestParams = null)
    {
        return $this->request->get('/payers/' . $payerId . '/refunds', $requestParams);
    }


}