<?php
/**
 * Created by PhpStorm.
 * User: Uer
 * Date: 07.05.2018
 * Time: 15:21
 */

namespace App\Services\WalletOneSafeDeal\Requests\Params;


class BaseParams
{
    public function toArray()
    {
        $params = array_filter(get_object_vars($this),function($x) {
            return !is_null($x);
        });
        return $params;
    }
}