<?php
/**
 * Created by PhpStorm.
 * User: Uer
 * Date: 07.05.2018
 * Time: 14:35
 */

namespace App\Services\WalletOneSafeDeal\Requests\Params;

/**
 * Class SaveDealParams
 * @package App\Services\WalletOneSafeDeal\Requests\Params
 */
class SaveDealParams extends BaseParams
{
    /**
     * @var string|integer $PlatformDealId Идентификатор сделки на стороне площадки
     */
    public $PlatformDealId;
    /**
     * @var string|integer $PlatformPayerId Идентификатор заказчика на стороне площадки
     */
    public $PlatformPayerId;
    /**
     * @var string $PayerPhoneNumber Номер телефона заказчика
     */
    public $PayerPhoneNumber;

    /**
     * @var string|integer $PayerPaymentToolId Идентификатор выбранного заказчиком инструмента (ОПЦИОНАЛЬНО)
     */
    public $PayerPaymentToolId;

    /**
     * @var string|integer $PlatformBeneficiaryId Идентификатор исполнителя на стороне площадки
     */
    public $PlatformBeneficiaryId;

    /**
     * @var string|integer $BeneficiaryPaymentToolId Идентификатор выбранного исполнителем инструмента
     */
    public $BeneficiaryPaymentToolId;

    /**
     * @var string|integer $Amount Сумма
     */
    public $Amount;

    /**
     * @var string|integer $CurrencyId ID валюты
     */
    public $CurrencyId;

    /**
     * @var string $ShortDescription Краткое описание
     */
    public $ShortDescription;

    /**
     * @var string $FullDescription Полное описание (ОПЦИОНАЛЬНО)
     */
    public $FullDescription;

    /**
     * @var bool $DeferPayout при значении false создается online (Instant) сделка
     */
    public $DeferPayout;
}