<?php
/**
 * Created by PhpStorm.
 * User: Uer
 * Date: 04.05.2018
 * Time: 17:45
 */

namespace App\Services\WalletOneSafeDeal;


use App\Services\WalletOneSafeDeal\Requests\BeneficiaryRequests;
use App\Services\WalletOneSafeDeal\Requests\DealRequests;
use App\Services\WalletOneSafeDeal\Requests\PayerRequests;
use App\Services\WalletOneSafeDeal\Requests\PlatformRequests;
use App\Services\WalletOneSafeDeal\Requests\WebRequests;


class WOSafeDeal
{
    /**
     * Запоросы для работы с платформой
     *
     * @return PlatformRequests
     */
    public static function platform()
    {
        return new PlatformRequests();
    }

    /**
     * Запросы для работы с сделками
     *
     * @return DealRequests
     */
    public static function deal()
    {
        return new DealRequests();
    }

    /**
     * Запросы для работы с заказчиками
     *
     * @return PayerRequests
     */
    public static function payer()
    {
        return new PayerRequests();
    }

    /**
     * Запросы для работы с исполнителями
     *
     * @return BeneficiaryRequests
     */
    public static function beneficiary()
    {
        return new BeneficiaryRequests();
    }

    /**
     * Запросы для оплаты сделки, привязки карт (редирект на WalletOne)
     *
     * @return WebRequests
     */
    public static function web()
    {
        return new WebRequests();
    }
}