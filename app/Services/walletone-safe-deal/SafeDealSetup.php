<?php
/**
 * Created by PhpStorm.
 * User: Uer
 * Date: 04.05.2018
 * Time: 18:25
 */

namespace App\Services\WalletOneSafeDeal;


use GuzzleHttp\Client;

class SafeDealSetup
{
    protected $secret_key;
    protected $algorithm;
    protected $platform_id;
    protected $test_mode;
    protected $client;
    

    public function __construct()
    {
        $this->secret_key = config('payment.safe_deal.secret_key');
        $this->algorithm = config('payment.safe_deal.signature_method');
        $this->platform_id = config('payment.safe_deal.platform_id');
        $this->test_mode = config('payment.safe_deal.test_mode');
        $this->client = new Client();
    }

    protected function setClient()
    {
        $this->client = new Client([
            'base_uri' => $this->getApiUrl(),
            'allow_redirects' => ['track_redirects' => true]
        ]);
    }

    public function getApiUrl()
    {
        if ($this->test_mode) {
            return 'https://api.dev.walletone.com/p2p/api/v3';
        }
        return 'https://api.w1.ru/p2p/api/v3';
    }

    public function getWebUrl()
    {
        if ($this->test_mode) {
            return 'https://api.dev.walletone.com/p2p';
        }
        return 'https://api.w1.ru/p2p';
    }

    /**
     * Get Wallet One signature algorithm.
     *
     * @return string algorithm
     */
    public function getAlgorithm()
    {
        return $this->algorithm;
    }

    /**
     * Set Wallet One signature algorithm.
     *
     * @param string $value algorithm
     */
    public function setAlgorithm($value)
    {
        $this->algorithm = $value;
    }

    /**
     * Get Wallet One secret key.
     *
     * @return string secretKey
     */
    public function getSecretKey()
    {
        return $this->secret_key;
    }

    /**
     * Set Wallet One secret key.
     *
     * @param string $value secretKey
     */
    public function setSecretKey($value)
    {
        $this->secret_key = $value;
    }

    /**
     * Get Wallet One platform id.
     *
     * @return string
     */
    public function getPlatformId()
    {
        return $this->platform_id;
    }

    /**
     * Set Wallet One platform id.
     *
     * @param string $platform_id
     */
    public function setPlatformId($platform_id)
    {
        $this->platform_id = $platform_id;
    }

    /**
     * @return bool
     */
    public function getTestMode()
    {
        return $this->test_mode;
    }

    /**
     * @param bool $test_mode
     */
    public function setTestMode($test_mode)
    {
        $this->test_mode = $test_mode;
    }

}