<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\DialogParticipant
 *
 * @property int $id
 * @property int|null $user_id
 * @property int $dialog_id
 * @property \Carbon\Carbon|null $last_read
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Dialog $dialogs
 * @property-read \App\User|null $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DialogParticipant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DialogParticipant whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DialogParticipant whereDialogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DialogParticipant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DialogParticipant whereLastRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DialogParticipant whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DialogParticipant whereUserId($value)
 * @mixin \Eloquent
 */
class DialogParticipant extends Model
{
    protected $fillable = [
        'user_id', 'dialog_id', 'last_read'
    ];

    protected $dates = [
        'last_read', 'deleted_at'
    ];

    protected $hidden = [
        'deleted_at'
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function dialogs()
    {
        return $this->belongsTo(Dialog::class, 'dialog_id');
    }


}
