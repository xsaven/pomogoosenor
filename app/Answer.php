<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 07 Feb 2018 16:08:42 +0000.
 */

namespace App;

use Illuminate\Support\Collection;

/**
 * App\Answer
 *
 * @property int $id
 * @property string $title
 * @property bool $is_true
 * @property int $question_id
 * @property-read \App\Question $question
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Answer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Answer whereIsTrue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Answer whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Answer whereTitle($value)
 * @mixin \Eloquent
 */
class Answer extends BaseModel
{
    public $timestamps = false;

    protected $casts = [
        'is_true' => 'bool',
        'question_id' => 'int'
    ];

    protected $fillable = [
        'title',
        'is_true',
        'question_id'
    ];

    protected $hidden = [
        'question_id'
    ];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    /**
     * Delete records by IDs
     *
     * @param Collection $avalieble_ids
     * @param int $question
     */
    public static function deleteUnavailableIds($avalieble_ids, $question_id)
    {
        if ($avalieble_ids->filter()->count() > 0) {
            self::whereNotIn('id', $avalieble_ids)->whereQuestionId($question_id)->delete();
        }
    }

    public static function syncManyToQuestion($answers, Question $question)
    {
        $avalieble_ids = collect($answers)->pluck('id');
        self::deleteUnavailableIds($avalieble_ids, $question->id);
        foreach ($answers as $answer) {
            self::updateOrCreate(['id' => $answer['id'] ?? 0], [
                'title' => $answer['title'],
                'is_true' => $answer['is_true'],
                'question_id' => $question->id
            ]);
        }
    }
}
