<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Posts
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $title
 * @property string|null $slug
 * @property string $preview
 * @property string $titres
 * @property string|null $related
 * @property string|null $subtitle
 * @property string|null $content
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Posts onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Posts whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Posts whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Posts whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Posts whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Posts wherePreview($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Posts whereRelated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Posts whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Posts whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Posts whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Posts whereTitres($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Posts whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Posts whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Posts withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Posts withoutTrashed()
 * @mixin \Eloquent
 * @property-read mixed $subtitle_limited
 */
class Posts extends Model
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $table = 'posts';

    protected $fillable = [
        'user_id',
        'title',
        'subtitle',
        'content',
        'titres',
        'related',
        'preview',
        'slug'
    ];

    protected $appends = [
        'subtitle_limited'
    ];


    public static function boot()
    {
        parent::boot();

        Posts::observe(new UserActionsObserver);
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }


    public function slugify()
    {
        $addFix = 1;
        $first = true;
        do {

            $text = $this->title;
            $text = preg_replace('~[^\pL\d]+~u', '-', $text);
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
            $text = preg_replace('~[^-\w]+~', '', $text);
            $text = trim($text, '-');
            $text = preg_replace('~-+~', '-', $text);
            $text = strtolower($text);

            if (!$first) {
                $text .= '-' . $addFix;
                $addFix++;
            }
            $alraedyExists = \App\Posts::where('slug', $text)->first();
            $first = false;
        } while ($alraedyExists);
        $this->slug = $text;
        $this->save();
    }

    public function getSubtitleLimitedAttribute()
    {
        return str_limit($this->subtitle, 180, '...');
    }


}