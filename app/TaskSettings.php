<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\TaskSettings
 *
 * @property int $id
 * @property string $name
 * @property string $key
 * @property string $value
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\TaskSettings onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskSettings whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskSettings whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskSettings whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskSettings whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskSettings whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskSettings whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskSettings whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\TaskSettings withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\TaskSettings withoutTrashed()
 * @mixin \Eloquent
 */
class TaskSettings extends Model
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $table = 'task_settings';

    protected $fillable = [
        'key',
        'value',
        'name'
    ];


    public static function boot()
    {
        parent::boot();

        self::updated(function (){
            self::cacheModel();
        });

        self::created(function () {
            self::cacheModel();
        });
    }

    public static function cacheModel()
    {
        \Cache::forever('task_settings', self::all());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    public static function getCached()
    {
        $data = cache('task_settings');
        if (!$data) {
            $db_data = self::all();
            \Cache::forever('task_settings', self::all());
            return $db_data;
        }
        return $data;
    }


    /**
     * @param string $key
     * @param null $default
     * @return mixed
     */
    public static function getByKey(string $key, $default = null)
    {
        $setting = self::getCached()->where('key', $key)->first();
        return $setting->value??$default;
    }


}