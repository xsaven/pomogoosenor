<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\WorkerProfile
 *
 * @property int $id
 * @property string|null $about_me
 * @property string|null $min_price
 * @property string|null $about_price
 * @property string $video
 * @property int $profile_id
 * @property-read mixed $about_me_date
 * @property-read \App\Profile $profile
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WorkerProfile whereAboutMe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WorkerProfile whereAboutPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WorkerProfile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WorkerProfile whereMinPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WorkerProfile whereProfileId($value)
 * @mixin \Eloquent
 * @property-read mixed $about_me_html
 * @property-read mixed $about_me_limited
 * @method static \Illuminate\Database\Eloquent\Builder|\App\WorkerProfile whereVideo($value)
 */
class WorkerProfile extends Model
{
    protected $fillable = [
        'about_me', 'min_price', 'about_price','video'
    ];

    protected $appends = ['about_me_date','about_me_html','about_me_limited'];

    protected $hidden = ['profile_id'];

    protected $attributes = [
        'about_me'=>'',
        'min_price'=>'',
        'about_price'=>'',
    ];

    public $timestamps = false;

    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

    public function getAboutMeDateAttribute()
    {
        /** @var Carbon $date */
        $date = $this->profile()->first()->created_at;
        return 'На '. env('APP_NAME'). ' с '. $date->toDateString();
    }

    public function getAboutMeHtmlAttribute()
    {
        return str_replace("\n", "<br>", $this->about_me);
    }

    public function getAboutMeLimitedAttribute()
    {
        return str_limit($this->about_me, 175,'...');
    }
}

