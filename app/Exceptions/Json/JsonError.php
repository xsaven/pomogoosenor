<?php
/**
 * Created by PhpStorm.
 * User: Uer
 * Date: 31.05.2018
 * Time: 14:40
 */

namespace App\Exceptions\Json;



class JsonError
{
    private $message, $headers, $additional_message = [];

    public function __construct($message, array $headers = [])
    {
        $this->message = $message;
        $this->headers = $headers;
    }

    public function with(array $additional_message)
    {
        $this->additional_message = $additional_message;
    }

    public function __destruct()
    {
        if (is_array($this->message)){
            $this->message = json_encode($this->message);
        }
        $this->message = json_encode(array_merge(['error'=>['message'=>$this->message]],$this->additional_message));
        throw new JsonErrorException($this->message, 400);
    }
}