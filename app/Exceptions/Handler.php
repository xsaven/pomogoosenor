<?php

namespace App\Exceptions;

use App\Exceptions\Json\JsonErrorException;
use App\Exceptions\Social\SocialTokenInvalidException;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
            return response()->json(['error' => ['message' => 'Token is Invalid']], 401);
        } else if ($exception instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
            return response()->json(['error' => ['message' => 'Token is Expired']], 401);
        } else if ($exception instanceof FailMessageException) {
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ], 200);
        } else if ($exception instanceof JsonErrorException) {
            return response()->json(json_decode_try($exception->getMessage()), 400);

        } else if ($request->ajax() && $exception instanceof ModelNotFoundException) {
            return response()->json([
                'success' => false,
                'message' => 'Действие невозможно'
            ], 200);
        }
        if ($request->ajax() || $request->wantsJson()) {
            $json = [
//                'success' => false,
                'error' => [
                    'message' => json_decode_try($exception->getMessage()),
                ],
            ];
            return response()->json($json, 400);
        }
        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Illuminate\Auth\AuthenticationException $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest(route('home'))->with('login', true);
    }
}
