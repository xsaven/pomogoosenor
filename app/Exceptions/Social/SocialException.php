<?php

/*
 * This file is part of jwt-auth.
 *
 * (c) Sean Tymon <tymon148@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Exceptions\Social;

class SocialException extends \Exception
{
    /**
     * @var int
     */
    protected $statusCode = 500;

    protected $message = '';

    /**
     * @param string  $message
     * @param int $statusCode
     */
    public function __construct($message = null, $statusCode = null)
    {
        if (! is_null($statusCode)) {
            $this->setStatusCode($statusCode);
        }

        if (is_array($message)) {
            $message = json_encode($message);
        }

        if (! is_null($message)) {
            $this->setMessage($message);
        }
    }

    /**
     * @param int $statusCode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @return int the status code
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }
}
