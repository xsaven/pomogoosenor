<?php

/*
 * This file is part of jwt-auth.
 *
 * (c) Sean Tymon <tymon148@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Exceptions;

class FailMessageException extends \Exception
{
    protected $statusCode = 400;

    protected $message = 'Попробуйте позже';
}