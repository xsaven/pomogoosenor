<?php

namespace App;


class TaskView extends BaseModel
{
    protected $table = 'task_view';

    protected $hidden = ['ip', 'browser', 'profile_id'];

    public function profile()
    {
        return $this->hasOne(Profile::class, 'id', 'profile_id');
    }

    public static function addView($task, $ip, $agent)
    {
        $view = self::query()
            ->where('user_tasks_id', '=', $task->id)
            ->where('ip', '=', $ip)
            ->where('browser', '=', $agent)->first();

        if (!$view) {
            $view = new self();
        }

        $view->user_tasks_id = $task->id;
        $view->ip = $ip;
        $view->browser = $agent;
        $view->save();
    }

}
