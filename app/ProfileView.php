<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ProfileView extends BaseModel
{
    protected $table = 'profile_view';

    protected $hidden = ['ip', 'browser', 'profile_id'];

    public function profile()
    {
        return $this->hasOne(Profile::class, 'id', 'profile_id');
    }

    public static function addView($profile, $ip, $agent)
    {
        $view = self::query()
            ->where('profile_id', '=', $profile->id)
            ->where('ip', '=', $ip)
            ->where('browser', '=', $agent)->first();

        if (!$view) {
            $view = new self();
        }

        $view->profile_id = $profile->id;
        $view->ip = $ip;
        $view->browser = $agent;
        $view->save();
    }

}
