<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Subscribe
 *
 * @property int $id
 * @property bool $system_notification
 * @property bool $sms_new_message
 * @property bool $news
 * @property bool $email
 * @property bool $push
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $user_id
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe whereNews($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe wherePush($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe whereSmsNewMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe whereSystemNotification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscribe whereUserId($value)
 * @mixin \Eloquent
 */
class Subscribe extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'system_notification', 'sms_new_message', 'news', 'email', 'push', 'user_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'user_id', 'id'
    ];

    protected $casts = [
        'system_notification' => 'boolean',
        'sms_new_message' => 'boolean',
        'news' => 'boolean',
        'email' => 'boolean',
        'push' => 'boolean'
    ];

    protected $attributes = [
        'system_notification' => 0,
        'sms_new_message' => 0,
        'news' => 0,
        'email' => 0,
        'push' => 0,
        'user_id' => 0
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
