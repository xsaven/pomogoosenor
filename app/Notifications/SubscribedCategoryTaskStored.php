<?php

namespace App\Notifications;

use App\UserTasks;
use DebugBar\DebugBar;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Log;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalWebButton;

class SubscribedCategoryTaskStored extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var UserTasks
     */
    private $task;

    /**
     * Create a new notification instance.
     *
     * @param UserTasks $task
     */
    public function __construct(UserTasks $task)
    {
        $this->task = $task;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [OneSignalChannel::class, 'database'];
    }


    /**
     * Get the OneSignal representation of the notification.
     *
     * @param $notifiable
     * @return OneSignalMessage|boolean
     */
    public function toOneSignal($notifiable)
    {
        $task_cost = empty($this->task->cost) ? $this->task->exact_cost : $this->task->exact_cost;
        if (isset($this->task->id)) {
            $task_url = env('APP_URL') . '/' . route('task.review', $this->task, false);
            return OneSignalMessage::create()
                ->subject("Новое задание")
                ->body($this->task->name . ' | ' . $task_cost . '₽')
                ->url($task_url)
                ->icon('http://www.iconsearch.ru/uploads/icons/starwars/128x128/darthvader.png')
                ->webButton(
                    OneSignalWebButton::create('link-1')
                        ->text('Перейти к заданию')
                        ->icon('')
                        ->url($task_url)
                );
        }
        return true;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'task_id' => $this->task->id,
            'task_name' => $this->task->name,
            'task_url' => env('APP_URL') . route('task.review', $this->task, false),
            'message'=> 'Новое задание'
        ];
    }
}
