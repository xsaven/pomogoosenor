<?php

namespace App\Notifications;

use App\Http\Resources\Notifications\NotificationResource;
use App\TaskOffers;
use App\UserTasks;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalWebButton;

class FinishTask extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var boolean
     */
    protected $deal_payed;

    /**
     * @var UserTasks
     */
    protected $task;

    protected $status;

    protected $exec = 0;


    /**
     * Create a new notification instance.
     *
     * @param UserTasks $task
     * @param int $execut
     */
    public function __construct($task, $execut = 0)
    {
        $this->exec = $execut;

        $this->status = 'Задание '.(!$execut ? "закрыто" : (!$task->executor_review->positive ? "не " : "")."выполнено исполнителем");
        $this->task = $task;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast', OneSignalChannel::class];
    }

    /**
     * Get the OneSignal representation of the notification.
     *
     * @param $notifiable
     * @return OneSignalMessage|boolean
     */
    public function toOneSignal($notifiable)
    {
        if (isset($this->task->id)) {
            $task_url = env('APP_URL') . '/' . route('task.review', $this->task, false);
            return OneSignalMessage::create()
                ->subject('Задание '.(!$this->exec ? "закрыто" : (!$this->task->executor_review->positive ? "не " : "")."выполнено исполнителем"))
                ->body($this->status)
                ->url($task_url)
                ->icon('http://www.iconsearch.ru/uploads/icons/starwars/128x128/darthvader.png')
                ->webButton(
                    OneSignalWebButton::create('link-1')
                        ->text('Перейти к заданию')
                        ->icon('')
                        ->url($task_url)
                );
        }
        return true;
    }

    /**
     * Get the database representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'task_id' => $this->task->id,
            'task_name' => $this->task->name,
            'task_url' => env('APP_URL') . route('task.review', $this->task, false),
            'message'=>$this->status
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'api_type' => 'system',
            'data' => [
                'task_id' => $this->task->id,
                'message'=>$this->status,
                'task_name' => $this->task->name,
                'task_url' => env('APP_URL') . route('task.review', $this->task, false),
            ],
            'created_at' => \LocalizedCarbon::parse(now())->formatLocalized('%d %f'),
            'timestamp' => now()->timestamp
        ];
    }

}
