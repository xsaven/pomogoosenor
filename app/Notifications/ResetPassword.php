<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Auth\Notifications\ResetPassword as OldResetPassword;

class ResetPassword extends OldResetPassword
{
    use Queueable;

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('Вы получаете это электронное письмо, потому что мы получили запрос на сброс пароля для вашей учетной записи.')
            ->action('Сбросить пароль', url(config('app.url') . route('password.reset', $this->token, false)) . '?email=' . $notifiable->email)
            ->line('Если вы не запросили сброс пароля, никаких дополнительных действий не требуется.');
    }

}
