<?php

namespace App\Notifications;

use App\UserNotification;
use Debugbar;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserNotificationCreated extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var UserNotification
     */
    private $notification;

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast'];
    }

    /**
     * Create a new notification instance.
     *
     * @param UserNotification $notification
     */
    public function __construct( $notification)
    {
        $this->notification = $notification;
    }

    public function toArray($notifiable)
    {
        return $this->notification;
    }
}
