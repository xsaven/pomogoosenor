<?php

namespace App\Notifications;

use App\TaskComments;
use App\TaskOffers;
use App\User;
use App\UserTasks;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalWebButton;

class CommentCreated extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var TaskComments
     */
    protected $comment;

    /**
     * @var UserTasks
     */
    protected $task;


    /**
     * Create a new notification instance.
     *
     * @param TaskComments $comment
     */
    public function __construct(TaskComments $comment)
    {
        $this->comment = $comment;
        $this->task = $comment->task;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast', OneSignalChannel::class];
    }

    /**
     * Get the OneSignal representation of the notification.
     *
     * @param $notifiable
     * @return OneSignalMessage|boolean
     */
    public function toOneSignal($notifiable)
    {
        $task_cost = empty($this->task->cost) ? $this->task->exact_cost : $this->task->exact_cost;
        if (isset($this->task->id)) {
            $task_url = env('APP_URL') . '/' . route('task.review', $this->task, false);
            return OneSignalMessage::create()
                ->subject('Новое предложение')
                ->body($this->task->name)
                ->url($task_url)
                ->icon('http://www.iconsearch.ru/uploads/icons/starwars/128x128/darthvader.png')
                ->webButton(
                    OneSignalWebButton::create('link-1')
                        ->text('Перейти к заданию')
                        ->icon('')
                        ->url($task_url)
                );
        }
        return true;
    }

    /**
     * Get the database representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'task_id' => $this->task->id,
            'task_name' => $this->task->name,
            'task_url' => env('APP_URL') . route('task.review', $this->task, false),
            'comment_message' => $this->comment->message,
            'user' => [
                'id'=>$notifiable->id,
                'name'=>$notifiable->name,
                'avatar'=>$notifiable->avatar()
            ],
            'message'=>'Новый комментарий'
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'data' => [
                'task_id' => $this->task->id,
                'task_name' => $this->task->name,
                'task_url' => env('APP_URL') . route('task.review', $this->task, false),
                'comment_message' => $this->comment->message,
                'user' => [
                    'id'=>$notifiable->id,
                    'name'=>$notifiable->name,
                    'avatar'=>$notifiable->avatar()
                ],
                'message'=>'Новый комментарий'
            ],
            'created_at' => \LocalizedCarbon::parse(now())->formatLocalized('%d %f'),
            'timestamp' => now()->timestamp
        ];
    }

}
