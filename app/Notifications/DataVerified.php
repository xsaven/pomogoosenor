<?php

namespace App\Notifications;

use App\TaskOffers;
use App\UserTasks;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;

class DataVerified extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast', OneSignalChannel::class];
    }
    public function toDatabase($notifiable)
    {
        return [
            'message'=>'Ваши данные подтверждены'
        ];
    }


    /**
     * Get the OneSignal representation of the notification.
     *
     * @param $notifiable
     * @return OneSignalMessage|boolean
     */
    public function toOneSignal($notifiable)
    {
        return OneSignalMessage::create()
            ->subject("Поздравляем")
            ->body('Ваши данные подтверждены')
            ->icon('http://www.iconsearch.ru/uploads/icons/starwars/128x128/darthvader.png');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'created_at' => \LocalizedCarbon::parse(now())->formatLocalized('%d %f'),
            'timestamp' => now()->timestamp
        ];
    }

}
