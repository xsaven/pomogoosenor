<?php

namespace App\Notifications\Broadcast;

use App\TaskOffers;
use App\UserTasks;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TaskOfferCreated extends Notification implements ShouldQueue
{
    use Queueable;

    protected $offer;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(TaskOffers $offer)
    {
        $this->offer = $offer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the database representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'task_id' => $this->offer->task->id,
            'task_name' => $this->offer->task->name,
            'task_url'=> env('APP_URL'). route('task.review', $this->offer->task, false)
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'data' => [
                'task_id' => $this->offer->task->id,
                'task_name' => $this->offer->task->name,
                'task_url'=> env('APP_URL') . route('task.review', $this->offer->task, false),
            ],
            'created_at'=> \LocalizedCarbon::parse(now())->formatLocalized('%d %f'),
            'timestamp'=> now()->timestamp
        ];
    }

}
