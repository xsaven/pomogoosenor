<?php

namespace App\Notifications;

use App\TaskOffers;
use App\UserTasks;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalWebButton;

class TaskOfferCreated extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var TaskOffers
     */
    protected $offer;

    /**
     * @var UserTasks
     */
    protected $task;


    /**
     * Create a new notification instance.
     *
     * @param TaskOffers $offer
     */
    public function __construct(TaskOffers $offer)
    {
        $this->offer = $offer;
        $this->task = $offer->task;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast', OneSignalChannel::class];
    }

    /**
     * Get the OneSignal representation of the notification.
     *
     * @param $notifiable
     * @return OneSignalMessage|boolean
     */
    public function toOneSignal($notifiable)
    {
        $task_cost = empty($this->task->cost) ? $this->task->exact_cost : $this->task->exact_cost;
        if (isset($this->task->id)) {
            $task_url = env('APP_URL') . '/' . route('task.review', $this->task, false);
            return OneSignalMessage::create()
                ->subject('Новое предложение')
                ->body($this->task->name)
                ->url($task_url)
                ->icon('http://www.iconsearch.ru/uploads/icons/starwars/128x128/darthvader.png')
                ->webButton(
                    OneSignalWebButton::create('link-1')
                        ->text('Перейти к заданию')
                        ->icon('')
                        ->url($task_url)
                );
        }
        return true;
    }

    /**
     * Get the database representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'task_id' => $this->task->id,
            'task_name' => $this->task->name,
            'task_url' => env('APP_URL') . route('task.review', $this->task, false),
            'message'=>'У Вас есть предложение к заданию'
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'data' => [
                'task_id' => $this->task->id,
                'task_name' => $this->task->name,
                'task_url' => env('APP_URL') . route('task.review', $this->task, false),
            ],
            'created_at' => \LocalizedCarbon::parse(now())->formatLocalized('%d %f'),
            'timestamp' => now()->timestamp
        ];
    }

}
