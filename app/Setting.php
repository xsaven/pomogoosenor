<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Setting
 *
 * @property int $id
 * @property string $key
 * @property string $type
 * @property string $value
 * @property string $label
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereValue($value)
 * @mixin \Eloquent
 */
class Setting extends Model
{
    protected $table = 'settings';

    protected $guarded = [];

    public static function keys() {
        return [
            'fb_url' => 'Facebook',
            'vk_url' => 'Vkontakte',
            'tw_url' => 'Twitter',
            'yt_url' => 'YouTube',
            'mailchimp_key' => 'MailChimp API ключ',
            'mailchimp_list_id'=>'MailChimp List ID'
        ];
    }
}
