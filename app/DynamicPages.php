<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DynamicPages extends Model
{

    protected $fillable = [
        'id',
        'name',
        'data',
        'slug',
    ];

    protected $casts = [
        'data' => 'array'
    ];

}
