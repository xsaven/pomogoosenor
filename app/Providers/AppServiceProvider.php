<?php

namespace App\Providers;

use App\Observers\UserObserver;
use App\User;
use Config;
use DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Resources\Json\Resource;



class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        Resource::withoutWrapping();
        Schema::defaultStringLength(191);
        if(($autoAuthId = env("XSAVEN_AUTH", false)) && \Auth::guest()){
            \Auth::loginUsingId($autoAuthId, true);
            return redirect('/');
        }
        config(['queries.all' => []]);
        DB::listen(function($query) {
            \Config::push('queries.all', [$query->sql,$query->bindings]);
        });

        User::observe(UserObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() == 'local') {
            $this->app->register(\Reliese\Coders\CodersServiceProvider::class);
//            if (request()->ip() == '194.44.114.223'||request()->ip() == '127.0.0.1') {
                $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
//            }
        }
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Way\Generators\GeneratorsServiceProvider::class);
            $this->app->register(\Xethron\MigrationsGenerator\MigrationsGeneratorServiceProvider::class);
        }
    }
}
