<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\TaskViewed' => [
            'App\Listeners\UpdateTaskViewCount',
        ],
        'App\Events\TaskCreated' => [
            'App\Listeners\SendTaskCreatedNotifications',
        ],
        'App\Events\ChangedSubscribedCategories' => [
            'App\Listeners\EditOneSignalSubCatTags',
        ],
        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            \JhaoDa\SocialiteProviders\MailRu\MailRuExtendSocialite::class,
            'SocialiteProviders\VKontakte\VKontakteExtendSocialite@handle',
        ],
        'App\Events\Payments\TaskPaymentHandled' => [
            'App\Listeners\Payments\UpdateDealParameters',
            'App\Listeners\Payments\UpdateTask',
        ],
        'App\Events\ReviewStored' => [
            'App\Events\SetTaskState'
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
