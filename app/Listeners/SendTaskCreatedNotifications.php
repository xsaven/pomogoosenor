<?php

namespace App\Listeners;

use App\Events\TaskCreated;
use App\Notifications\SubscribedCategoryTaskStored;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Notification;

class SendTaskCreatedNotifications
{

    /**
     * Handle the event.
     *
     * @param  TaskCreated $event
     * @return void
     */
    public function handle(TaskCreated $event)
    {
        $users = User::with('categories')->withoutMe()
            ->whereHas('categories', function (Builder $q) use ($event) {
                $q->where('subtype_id', '=', $event->task->subcategory_id);
            })
            ->get();

        Notification::send($users, new SubscribedCategoryTaskStored($event->task));
    }
}
