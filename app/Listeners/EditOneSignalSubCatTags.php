<?php

namespace App\Listeners;

use App\Events\ChangedSubscribedCategories;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EditOneSignalSubCatTags
{
    /**
     * Handle the event.
     *
     * @param  ChangedSubscribedCategories  $event
     * @return void
     */
    public function handle(ChangedSubscribedCategories $event)
    {
        if (!empty($event->user->onesignal_id)) {
            \OneSignal::editPlayer([
                'id' => $event->user->onesignal_id,
                'tags' => [
                    'categories_sunscribed' => [
                        1, 5, 9, 2
                    ]
                ]
            ]);
        }
    }
}
