<?php

namespace App\Listeners;

use App\Events\TaskViewed;
use App\UserTasks;
use App\UserTaskView;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateTaskViewCount
{

    /**
     * Handle the event.
     *
     * @param  TaskViewed  $event
     * @return void
     */
    public function handle(TaskViewed $event)
    {
        if (\Auth::check()){
            $event->task->users_viewed()->syncWithoutDetaching(user()->id);
        }
    }
}
