<?php

namespace App\Listeners\Payments;

use App\Events\Payments\TaskPaymentHandled;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateDealParameters
{

    /**
     * Handle the event.
     *
     * @param  TaskPaymentHandled  $event
     * @return void
     */
    public function handle(TaskPaymentHandled $event)
    {
        $event->task->saveOrUpdateDeal($event->deal_parameters);
    }
}
