<?php

namespace App\Listeners\Payments;

use App\Events\Payments\TaskPaymentHandled;
use App\Http\Controllers\Payment\SBRController;
use App\Mail\Verification;
use App\Notifications\PaymentStatusChanged;
use App\Notifications\SetExecutor;
use App\Services\WalletOneSafeDeal\WOSafeDeal;
use App\TransactionsLog;
use App\User;
use App\UserTasks;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class UpdateTask
{


    /**
     * Handle the event.
     *
     * @param  TaskPaymentHandled $event
     * @return mixed
     */
    public function handle(TaskPaymentHandled $event)
    {

        switch ($event->deal_parameters['DealStateId']) {
            case 'PaymentHold':
                WOSafeDeal::deal()->confirmDeal($event->deal->id);
                break;
            case 'PaymentProcessError':
                $event->task->setAttribute('locked', false)->save();
                $this->notify($event->task, false);
                break;
            case 'Paid':
                if ($event->task->getOriginal('status') !== UserTasks::STATUS_IN_PROGRESS) $this->notify($event->task, true);
                $event->task->setExecutor($event->deal_parameters['PlatformBeneficiaryId']);
                break;
            case 'Completed':
                $event->task->status = UserTasks::STATUS_FINISHED;
                $event->task->save();
                break;
            case 'Canceled':
                $event->task->status = UserTasks::STATUS_NOT_FINISHED;
                $event->task->save();
                break;
        }

        $orderLog = new Logger('payments');
        $orderLog->pushHandler(new StreamHandler(storage_path('logs/payments.log')), Logger::INFO);
        $orderLog->info('OrderLog', $event->deal_parameters);

        $transactionsLog = new TransactionsLog();
        $transactionsLog->type = isset($event->deal_parameters['DealStateId']) ? TransactionsLog::PAYMENT_STATUS[$event->deal_parameters['DealStateId']] : 0;
        $transactionsLog->amount = isset($event->deal_parameters['Amount']) ? $event->deal_parameters['Amount'] : 0;
        $transactionsLog->recipient_id = isset($event->deal_parameters['PlatformBeneficiaryId']) ? $event->deal_parameters['PlatformBeneficiaryId'] : 0;
        $transactionsLog->sender_id = isset($event->deal_parameters['PlatformPayerId']) ? $event->deal_parameters['PlatformPayerId'] : 0;
        $transactionsLog->content = json_encode($event->deal_parameters);

        $transactionsLog->save();

    }

    private function notify(UserTasks $task, $success_payment)
    {
        User::find($task->creator_id)->notify(new PaymentStatusChanged($task, $success_payment));

        if ($task->executor_id) {
            User::find($task->executor_id)->notify(new SetExecutor($task, $success_payment));
            Mail::send(new \App\Mail\SetExecutor($task->executor->email));
        }


    }
}
