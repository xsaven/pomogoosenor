<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\StaticPages
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string|null $description
 * @property string|null $keywords
 * @property string $content
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\StaticPages onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StaticPages whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StaticPages whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StaticPages whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StaticPages whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StaticPages whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StaticPages whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StaticPages whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StaticPages whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StaticPages whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\StaticPages withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\StaticPages withoutTrashed()
 * @mixin \Eloquent
 */
class StaticPages extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'staticpages';
    
    protected $fillable = [
          'title',
          'slug',
          'description',
          'keywords',
          'content'
    ];


    public static function boot()
    {
        parent::boot();

        self::updating(function ($model){
            $model->slug = trim($model->slug,'/');
        });

        self::creating(function ($model){
            $model->slug = trim($model->slug,'/');
        });

        self::saved(function ($model){
            /**
             * Cache staticpages table data to use it in middleware
             */
           \Cache::forever('static_pages',\DB::table('staticpages')->get());
        });

        StaticPages::observe(new UserActionsObserver);
    }
    
}