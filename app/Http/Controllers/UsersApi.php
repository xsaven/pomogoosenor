<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UsersApi extends Controller
{
    public function postFindUser(Request $request) {

        if ($request->has('query')) {

            $users = User::where('name','LIKE','%'.$request->input('query').'%')->take(10)->get();
            if (!$users->isEmpty()) {
                $dataArray = [];
                foreach ($users as $user) {
                    $dataArray[] = array(
                        'id' => $user->id,
                        'name' => $user->name.' #'.$user->id,
                    );
                }
                return response()->json($dataArray,200);
            }

        }

    }
}
