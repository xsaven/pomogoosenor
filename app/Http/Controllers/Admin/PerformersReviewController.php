<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\PerformersReview;
use App\Http\Requests\CreatePerformersReviewRequest;
use App\Http\Requests\UpdatePerformersReviewRequest;
use Illuminate\Http\Request;

use App\User;


class PerformersReviewController extends Controller
{

    /**
     * Display a listing of performersreview
     *
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $performersreview = PerformersReview::with("user")->get();

        return view('admin.performersreview.index', compact('performersreview'));
    }

    /**
     * Show the form for creating a new performersreview
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $user = User::all()->keyBy('id')->map(function ($user) {
            return $user->name;
        });
        return view('admin.performersreview.create', compact("user"));
    }

    /**
     * Store a newly created performersreview in storage.
     *
     * @param CreatePerformersReviewRequest|Request $request
     */
    public function store(CreatePerformersReviewRequest $request)
    {

        PerformersReview::create($request->all());

        return redirect()->route(config('quickadmin.route') . '.performersreview.index');
    }

    /**
     * Show the form for editing the specified performersreview.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $performersreview = PerformersReview::find($id);
        $user = User::all()->keyBy('id')->map(function ($user) {
            return $user->name;
        });

        return view('admin.performersreview.edit', compact('performersreview', "user"));
    }

    /**
     * Update the specified performersreview in storage.
     * @param UpdatePerformersReviewRequest|Request $request
     *
     * @param  int $id
     */
    public function update($id, UpdatePerformersReviewRequest $request)
    {
        $performersreview = PerformersReview::findOrFail($id);


        $performersreview->update($request->all());

        return redirect()->route(config('quickadmin.route') . '.performersreview.index');
    }

    /**
     * Remove the specified performersreview from storage.
     *
     * @param  int $id
     */
    public function destroy($id)
    {
        PerformersReview::destroy($id);

        return redirect()->route(config('quickadmin.route') . '.performersreview.index');
    }

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            PerformersReview::destroy($toDelete);
        } else {
            PerformersReview::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route') . '.performersreview.index');
    }

}
