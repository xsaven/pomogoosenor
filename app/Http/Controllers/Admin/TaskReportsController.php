<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\UserTasks;
use Redirect;
use Schema;
use App\UserTaskReports;
use App\Http\Requests\CreateTaskReportsRequest;
use App\Http\Requests\UpdateTaskReportsRequest;
use Illuminate\Http\Request;

use App\User;
use App\Category;


class TaskReportsController extends Controller
{

    /**
     * Display a listing of taskreports
     *
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $taskreports = UserTaskReports::with("user")->with('task')->get();

        return view('admin.taskreports.index', compact('taskreports'));
    }

    /**
     * Show the form for creating a new taskreports
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $user = User::pluck("name", "id")->prepend('Please select', null);
        $reason = UserTaskReports::$reason;

        return view('admin.taskreports.create', compact("user", "reason"));
    }

    /**
     * Store a newly created taskreports in storage.
     *
     * @param CreateTaskReportsRequest|Request $request
     */
    public function store(CreateTaskReportsRequest $request)
    {
        UserTaskReports::create($request->all());
        return redirect()->route(config('quickadmin.route') . '.taskreports.index');
    }

    /**
     * Show the form for editing the specified taskreports.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $taskreports = UserTaskReports::find($id);
        $user = User::pluck("name", "id")->prepend('Please select', null);
        $reason = UserTaskReports::$reason;

        return view('admin.taskreports.edit', compact('taskreports', "user", "reason"));
    }

    /**
     * Update the specified taskreports in storage.
     * @param UpdateTaskReportsRequest|Request $request
     *
     * @param  int $id
     */
    public function update($id, UpdateTaskReportsRequest $request)
    {
        $taskreports = UserTaskReports::findOrFail($id);
        $taskreports->update($request->all());

        return redirect()->route(config('quickadmin.route') . '.taskreports.index');
    }

    /**
     * Remove the specified taskreports from storage.
     *
     * @param  int $id
     */
    public function destroy($id)
    {
        UserTaskReports::destroy($id);

        return redirect()->route(config('quickadmin.route') . '.taskreports.index');
    }

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            UserTaskReports::destroy($toDelete);
        } else {
            UserTaskReports::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route') . '.taskreports.index');
    }

    public function deleteTask($id)
    {
        $task = UserTasks::whereId($id)->first();
        $task->delete();
        return back();
    }

}
