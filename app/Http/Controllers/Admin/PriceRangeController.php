<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\PriceRangersMask;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;



class PriceRangeController extends Controller {

    /**
     * Display a listing of categories
     *
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $ranges = PriceRangersMask::all();

        return view('admin.categories.price_range', compact('ranges'));
    }

    /**
     * Show the form for creating a new categories
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.categories.price_range_create');
    }

    /**
     * Store a newly created categories in storage.
     *
     * @param CreateCategoriesRequest|Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'data' => 'required|array'
        ]);

        PriceRangersMask::create($request->all());
        return redirect()->route(config('quickadmin.route').'.pricerange.index');
    }

    /**
     * Show the form for editing the specified categories.
     *
     * @param  Category $category
     * @return \Illuminate\View\View
     */
    public function edit($pricerange, PriceRangersMask $priceRangersMask)
    {
        $priceRangersMask = $priceRangersMask->find($pricerange);
        //dd($priceRangersMask);
        return view('admin.categories.pricerange_edit', ['priceRangersMask' => $priceRangersMask]);
    }

    /**
     * Update the specified categories in storage.
     *
     * @param  Category $category
     * @param UpdateCategoriesRequest|Request $request
     * @return RedirectResponse
     */
    public function update($pricerange, PriceRangersMask $priceRangersMask, Request $request)
    {
        $priceRangersMask->find($pricerange)->update($request->all());
        return redirect()->route(config('quickadmin.route').'.pricerange.index');
    }

    /**
     * Remove the specified categories from storage.
     *
     * @param  int  $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        PriceRangersMask::destroy($id);
        return redirect()->route(config('quickadmin.route').'.pricerange.index');
    }

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            PriceRangersMask::destroy($toDelete);
        } else {
            PriceRangersMask::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.pricerange.index');
    }

}
