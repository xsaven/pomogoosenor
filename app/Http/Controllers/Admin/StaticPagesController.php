<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\StaticPages;
use App\Http\Requests\CreateStaticPagesRequest;
use App\Http\Requests\UpdateStaticPagesRequest;
use Illuminate\Http\Request;



class StaticPagesController extends Controller {

	/**
	 * Display a listing of staticpages
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $staticpages = StaticPages::all();

		return view('admin.staticpages.index', compact('staticpages'));
	}

	/**
	 * Show the form for creating a new staticpages
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    return view('admin.staticpages.create');
	}

	/**
	 * Store a newly created staticpages in storage.
	 *
     * @param CreateStaticPagesRequest|Request $request
	 */
	public function store(CreateStaticPagesRequest $request)
	{
	    
		StaticPages::create($request->all());

		return redirect()->route(config('quickadmin.route').'.staticpages.index');
	}

	/**
	 * Show the form for editing the specified staticpages.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$staticpages = StaticPages::find($id);
	    
	    
		return view('admin.staticpages.edit', compact('staticpages'));
	}

	/**
	 * Update the specified staticpages in storage.
     * @param UpdateStaticPagesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateStaticPagesRequest $request)
	{
		$staticpages = StaticPages::findOrFail($id);

        

		$staticpages->update($request->all());

		return redirect()->route(config('quickadmin.route').'.staticpages.index');
	}


    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
	{
		StaticPages::destroy($id);

		return redirect()->route(config('quickadmin.route').'.staticpages.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            StaticPages::destroy($toDelete);
        } else {
            StaticPages::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.staticpages.index');
    }

}
