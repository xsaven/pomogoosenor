<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Notifications\ApproveTask;
use App\Notifications\DeleteTask;
use App\Notifications\UnApproveTask;
use App\UserTasks;
use Illuminate\Support\Facades\Mail;
use Redirect;
use Schema;
use App\UserTaskReports;
use App\Http\Requests\CreateTaskReportsRequest;
use App\Http\Requests\UpdateTaskReportsRequest;
use Illuminate\Http\Request;

use App\User;
use App\Category;


class TaskController extends Controller
{

    /**
     * Display a listing of taskreports
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $taskCollection = UserTasks::orderByDesc('id')->get();

        return view('admin.task.index', compact('taskCollection'));
    }

    /**
     * @param UserTasks $task
     * @return \Illuminate\Http\RedirectResponse
     */
    public function approve(UserTasks $task)
    {
        $task->status = UserTasks::STATUS_OPENED;
        $task->save();
        $task->creator->notify(new ApproveTask($task));
        if ($task->creator && $task->creator->email)
            Mail::send(new \App\Mail\ApproveTask($task));
        return back();
    }

    /**
     * @param UserTasks $task
     * @return \Illuminate\Http\RedirectResponse
     */
    public function unapprove(UserTasks $task)
    {
        $task->status = UserTasks::STATUS_MODERATE;
        $task->save();
        $task->creator->notify(new UnApproveTask($task));
        if ($task->creator && $task->creator->email)
            Mail::send(new \App\Mail\UnApproveTask($task));
        return back();
    }

    public function delete(UserTasks $task)
    {
        $task->delete();
        $task->creator->notify(new DeleteTask($task));
        if ($task->creator && $task->creator->email)
            Mail::send(new \App\Mail\DeleteTask($task));
        return back();
    }




}
