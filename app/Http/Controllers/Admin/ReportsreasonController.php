<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Reportsreason;
use App\Http\Requests\CreateReportsreasonRequest;
use App\Http\Requests\UpdateReportsreasonRequest;
use Illuminate\Http\Request;



class ReportsreasonController extends Controller {

	/**
	 * Display a listing of reportsreason
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $reportsreason = Reportsreason::all();

		return view('admin.reportsreason.index', compact('reportsreason'));
	}

	/**
	 * Show the form for creating a new reportsreason
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    
	    
	    return view('admin.reportsreason.create');
	}

	/**
	 * Store a newly created reportsreason in storage.
	 *
     * @param CreateReportsreasonRequest|Request $request
	 */
	public function store(CreateReportsreasonRequest $request)
	{
	    
		Reportsreason::create($request->all());

		return redirect()->route(config('quickadmin.route').'.reportsreason.index');
	}

	/**
	 * Show the form for editing the specified reportsreason.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$reportsreason = Reportsreason::find($id);
	    
	    
		return view('admin.reportsreason.edit', compact('reportsreason'));
	}

	/**
	 * Update the specified reportsreason in storage.
     * @param UpdateReportsreasonRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateReportsreasonRequest $request)
	{
		$reportsreason = Reportsreason::findOrFail($id);

        

		$reportsreason->update($request->all());

		return redirect()->route(config('quickadmin.route').'.reportsreason.index');
	}

	/**
	 * Remove the specified reportsreason from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Reportsreason::destroy($id);

		return redirect()->route(config('quickadmin.route').'.reportsreason.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Reportsreason::destroy($toDelete);
        } else {
            Reportsreason::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.reportsreason.index');
    }

}
