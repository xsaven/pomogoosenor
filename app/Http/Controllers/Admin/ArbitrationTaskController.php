<?php

namespace App\Http\Controllers\Admin;

use App\DialogMessage;
use App\DialogParticipant;
use App\Http\Controllers\Controller;
use App\Notifications\ApproveTask;
use App\Notifications\ArbitrationApproveTask;
use App\Notifications\ArbitrationUnApproveTask;
use App\Notifications\DeleteTask;
use App\Notifications\UnApproveTask;
use App\Services\WalletOneSafeDeal\WOSafeDeal;
use App\UserTasks;
use Illuminate\Support\Facades\Mail;
use Redirect;
use Schema;
use App\UserTaskReports;
use App\Http\Requests\CreateTaskReportsRequest;
use App\Http\Requests\UpdateTaskReportsRequest;
use Illuminate\Http\Request;

use App\User;
use App\Category;


class ArbitrationTaskController extends Controller
{

    /**
     * Display a listing of taskreports
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $taskCollection = UserTasks::whereStatus(UserTasks::STATUS_IN_ARBITRATION)->get();

        return view('admin.task.arbitration', compact('taskCollection'));
    }


    public function review($id)
    {
        if (!$task = UserTasks::whereId($id)->first()) {
            return redirect(route('admin.tasks.arbitration'));
        }

        if ((int)$task->getOriginal('status') !== UserTasks::STATUS_IN_ARBITRATION) {
            return redirect(route('admin.tasks.arbitration'));
        }

        $usersDialogId = DialogParticipant::query()
            ->select(['dialog_participants.dialog_id'])
            ->leftJoin('dialog_participants as second', 'dialog_participants.dialog_id', '=', 'second.dialog_id')
            ->where('dialog_participants.user_id', '!=', 'second.user_id')
            ->where('dialog_participants.user_id', '=', $task->creator_id)
            ->where('second.user_id', '=', $task->executor_id)
            ->first();

        $usersDialogId = isset($usersDialogId->dialog_id) ? $usersDialogId->dialog_id : null;

        $messages = DialogMessage::where('dialog_id', '=', $usersDialogId)->get();

        return view('admin.task.arbitration.review', compact('task', 'usersDialogId', 'messages'));
    }

    public function confirm($id)
    {
        if (!$task = UserTasks::whereId($id)->first()) {
            return redirect(route('admin.tasks.arbitration'));
        }

        if ((int)$task->getOriginal('status') !== UserTasks::STATUS_IN_ARBITRATION) {
            return redirect(route('admin.tasks.arbitration'));
        }

        $task->status = UserTasks::STATUS_FINISHED;
        $task->save();

        WOSafeDeal::deal()->completeDeal($task->deal->id);

        $task->creator->notify(new ArbitrationApproveTask($task));
        $task->executor->notify(new ArbitrationApproveTask($task));

        return redirect(route('admin.tasks.arbitration'));
    }

    public function cancel($id)
    {
        if (!$task = UserTasks::whereId($id)->first()) {
            return redirect(route('admin.tasks.arbitration'));
        }

        if ((int)$task->getOriginal('status') !== UserTasks::STATUS_IN_ARBITRATION) {
            return redirect(route('admin.tasks.arbitration'));
        }

        $task->status = UserTasks::STATUS_NOT_FINISHED;
        $task->save();

        WOSafeDeal::deal()->cancelDeal($task->deal->id);

        $task->creator->notify(new ArbitrationUnApproveTask($task));
        $task->executor->notify(new ArbitrationUnApproveTask($task));

        return redirect(route('admin.tasks.arbitration'));
    }


}
