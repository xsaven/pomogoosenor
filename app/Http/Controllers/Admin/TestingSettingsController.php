<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\TestingSettings;
use App\Http\Requests\CreateTestingSettingsRequest;
use App\Http\Requests\UpdateTestingSettingsRequest;
use Illuminate\Http\Request;



class TestingSettingsController extends Controller {

	/**
	 * Display a listing of testingsettings
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $testingsettings = TestingSettings::all();

		return view('admin.testingsettings.index', compact('testingsettings'));
	}

	/**
	 * Show the form for creating a new testingsettings
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    
	    
	    return view('admin.testingsettings.create');
	}

	/**
	 * Store a newly created testingsettings in storage.
	 *
     * @param CreateTestingSettingsRequest|Request $request
	 */
	public function store(CreateTestingSettingsRequest $request)
	{
	    
		TestingSettings::create($request->all());

		return redirect()->route(config('quickadmin.route').'.testingsettings.index');
	}

	/**
	 * Show the form for editing the specified testingsettings.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$testingsettings = TestingSettings::find($id);
	    
	    
		return view('admin.testingsettings.edit', compact('testingsettings'));
	}

	/**
	 * Update the specified testingsettings in storage.
     * @param UpdateTestingSettingsRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateTestingSettingsRequest $request)
	{
		$testingsettings = TestingSettings::findOrFail($id);

        

		$testingsettings->update($request->all());

		return redirect()->route(config('quickadmin.route').'.testingsettings.index');
	}

	/**
	 * Remove the specified testingsettings from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		TestingSettings::destroy($id);

		return redirect()->route(config('quickadmin.route').'.testingsettings.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            TestingSettings::destroy($toDelete);
        } else {
            TestingSettings::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.testingsettings.index');
    }

}
