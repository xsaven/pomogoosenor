<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateFooterRequest;
use App\PageConfig;

class FooterController extends Controller
{

    /**
     * Index page
     *
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        if (!$footerConfig = PageConfig::where('key', '=', 'footer')->first()) {
            $footerConfig = new PageConfig();
            $footerConfig->key = 'footer';
            $footerConfig->save();
        }

        return view('admin.footer.index', ['data' => $footerConfig->value]);
    }


    public function update(UpdateFooterRequest $request)
    {
        $request = $this->saveFiles($request);
        $data = $request->all();

        $footerConfig = PageConfig::where('key', '=', 'footer')->first();

        for ($i = 1; $i <= 5; $i++) {
            if (!isset($data['image_' . $i])) $data['image_' . $i] = isset($footerConfig->value['image_' . $i]) ? $footerConfig->value['image_' . $i] : null;
        }

        $footerConfig->value = $data;
        $footerConfig->save();

        return back();
    }
}