<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\CreateVacancyRequest;
use App\Http\Requests\UpdateVacancyRequest;
use App\Vacancies;

class VacancyController extends Controller
{
    public function index()
    {
        $vacancies = Vacancies::all();

        return view('admin.vacancies.index', ['vacancies' => $vacancies]);
    }

    public function add()
    {
        return view('admin.vacancies.edit');
    }

    public function create(CreateVacancyRequest $request)
    {
        if (Vacancies::create($request->all())) {
            return redirect(url('admin/vacancy'));
        }
        return view('admin.vacancies.create');
    }

    public function delete($id)
    {
        if (!$vacancy = Vacancies::whereId($id)->first()) {
            return redirect(url('admin/vacancy'));
        }
        $vacancy->delete();
        return redirect(url('admin/vacancy'));
    }

    public function edit($id)
    {
        if (!$vacancy = Vacancies::whereId($id)->first()) {
            return redirect(url('admin/vacancy'));
        }

        return view('admin.vacancies.edit', ['vacancy' => $vacancy]);
    }

    public function update(UpdateVacancyRequest $request)
    {
        if (!$vacancy = Vacancies::whereId($request->id)->first()) {
            return redirect(url('admin/vacancy'));
        }

        $vacancy->update($request->all());

        return redirect(url('admin/vacancy/edit', ['id' => $request->id]));
    }

}
