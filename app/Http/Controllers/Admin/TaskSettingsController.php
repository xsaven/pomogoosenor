<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\TaskSettings;
use App\Http\Requests\CreateTaskSettingsRequest;
use App\Http\Requests\UpdateTaskSettingsRequest;
use Illuminate\Http\Request;



class TaskSettingsController extends Controller {

	/**
	 * Display a listing of tasksettings
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $tasksettings = TaskSettings::all();

		return view('admin.tasksettings.index', compact('tasksettings'));
	}

	/**
	 * Show the form for creating a new tasksettings
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    
	    
	    return view('admin.tasksettings.create');
	}

	/**
	 * Store a newly created tasksettings in storage.
	 *
     * @param CreateTaskSettingsRequest|Request $request
	 */
	public function store(CreateTaskSettingsRequest $request)
	{
	    
		TaskSettings::create($request->all());

		return redirect()->route(config('quickadmin.route').'.tasksettings.index');
	}

	/**
	 * Show the form for editing the specified tasksettings.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$tasksettings = TaskSettings::find($id);
	    
	    
		return view('admin.tasksettings.edit', compact('tasksettings'));
	}

	/**
	 * Update the specified tasksettings in storage.
     * @param UpdateTaskSettingsRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateTaskSettingsRequest $request)
	{
		$tasksettings = TaskSettings::findOrFail($id);

        

		$tasksettings->update($request->all());

		return redirect()->route(config('quickadmin.route').'.tasksettings.index');
	}

	/**
	 * Remove the specified tasksettings from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		TaskSettings::destroy($id);

		return redirect()->route(config('quickadmin.route').'.tasksettings.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            TaskSettings::destroy($toDelete);
        } else {
            TaskSettings::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.tasksettings.index');
    }

}
