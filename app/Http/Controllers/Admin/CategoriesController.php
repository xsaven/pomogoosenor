<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\CreateCategoriesRequest;
use App\Http\Requests\UpdateCategoriesRequest;
use Illuminate\Http\Request;



class CategoriesController extends Controller {

	/**
	 * Display a listing of categories
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $categories = Category::all();

		return view('admin.categories.index', compact('categories'));
	}

	/**
	 * Show the form for creating a new categories
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    return view('admin.categories.create');
	}

	/**
	 * Store a newly created categories in storage.
	 *
     * @param CreateCategoriesRequest|Request $request
     * @return RedirectResponse
	 */
	public function store(CreateCategoriesRequest $request)
	{
        $request = $this->saveFiles($request);
        Category::create($request->all());
		return redirect()->route(config('quickadmin.route').'.categories.index');
	}

    /**
	 * Show the form for editing the specified categories.
	 *
	 * @param  Category $category
     * @return \Illuminate\View\View
	 */
	public function edit(Category $category)
	{
		return view('admin.categories.edit', compact('category'));
	}

    /**
     * Update the specified categories in storage.
     *
     * @param  Category $category
     * @param UpdateCategoriesRequest|Request $request
     * @return RedirectResponse
     */
    public function update(Category $category, UpdateCategoriesRequest $request)
    {
        $request = $this->saveFiles($request);
        $category->update($request->all());
        return redirect()->route(config('quickadmin.route').'.categories.index');
    }

    /**
	 * Remove the specified categories from storage.
	 *
	 * @param  int  $id
     * @return RedirectResponse
	 */
	public function destroy($id)
	{
		Category::destroy($id);
		return redirect()->route(config('quickadmin.route').'.categories.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Category::destroy($toDelete);
        } else {
            Category::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.categories.index');
    }

}
