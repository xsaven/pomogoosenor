<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Doctrine\DBAL\Schema\AbstractAsset;
use Redirect;
use Schema;
use App\FAQ;
use App\Http\Requests\CreateFAQRequest;
use App\Http\Requests\UpdateFAQRequest;
use Illuminate\Http\Request;


class FAQController extends Controller
{

    /**
     * Display a listing of faq
     *
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $faq = FAQ::all();

        return view('admin.faq.index', compact('faq'));
    }

    /**
     * Show the form for creating a new faq
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {


        return view('admin.faq.create');
    }

    /**
     * Store a newly created faq in storage.
     *
     * @param CreateFAQRequest|Request $request
     */
    public function store(CreateFAQRequest $request)
    {

        FAQ::create($request->all());

        return redirect()->route(config('quickadmin.route') . '.faq.index');
    }

    /**
     * Show the form for editing the specified faq.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $faq = FAQ::find($id);


        return view('admin.faq.edit', compact('faq'));
    }

    /**
     * Update the specified faq in storage.
     * @param UpdateFAQRequest|Request $request
     *
     * @param  int $id
     */
    public function update($id, UpdateFAQRequest $request)
    {
        $faq = FAQ::findOrFail($id);

        $faq->update($request->all());

        return redirect()->route(config('quickadmin.route') . '.faq.index');
    }

    /**
     * Remove the specified faq from storage.
     *
     * @param  int $id
     */
    public function destroy($id)
    {
        FAQ::destroy($id);

        return redirect()->route(config('quickadmin.route') . '.faq.index');
    }

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            FAQ::destroy($toDelete);
        } else {
            FAQ::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route') . '.faq.index');
    }

}
