<?php

namespace App\Http\Controllers\Admin;

use App\Answer;
use App\Http\Controllers\Controller;
use App\Question;
use Redirect;
use Schema;
use App\Pattern;
use App\Http\Requests\CreatePatternsRequest;
use App\Http\Requests\UpdatePatternsRequest;
use Illuminate\Http\Request;


/**
 * Class PatternsController
 * @package App\Http\Controllers\Admin
 */
class PatternsController extends Controller {

	/**
	 * Display a listing of patterns
	 *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
        $patterns = Pattern::all();
		return view('admin.patterns.index', compact('patterns'));
	}

	/**
	 * Show the form for creating a new patterns
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    return view('admin.patterns.create');
	}

    /**
     * Store a newly created patterns in storage.
     *
     * @param CreatePatternsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreatePatternsRequest $request)
	{
        $questions = json_decode_try($request->questions);
        $pattern = Pattern::create($request->only(['name','active','order']));
        foreach (Question::syncManyToPattern($questions, $pattern) as $index=>$question){
            Answer::syncManyToQuestion($questions[$index]['answers'], $question);
        }

		return redirect()->route(config('quickadmin.route').'.patterns.index');
	}

    /**
     * Show the form for editing the specified patterns.
     *
     * @param Pattern $pattern
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Pattern $pattern)
	{
        $pattern->load(['questions.answers']);
		return view('admin.patterns.edit', compact('pattern'));
	}


    /**
     * Update the specified patterns in storage.
     *
     * @param Pattern $pattern
     * @param UpdatePatternsRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(Pattern $pattern, UpdatePatternsRequest $request)
	{
        $questions = json_decode_try($request->questions);
        $pattern->update($request->only(['name','active','order']));
        foreach (Question::syncManyToPattern($questions, $pattern) as $index=>$question){
            Answer::syncManyToQuestion($questions[$index]['answers'], $question);
        }
        return redirect()->route(config('quickadmin.route').'.patterns.index');
	}

    /**
     * Remove the specified patterns from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
	{
		Pattern::destroy($id);

		return redirect()->route(config('quickadmin.route').'.patterns.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Pattern::destroy($toDelete);
        } else {
            Pattern::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.patterns.index');
    }

}
