<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Subcategory;
use App\Http\Requests\CreateSubcategoriesRequest;
use App\Http\Requests\UpdateSubcategoriesRequest;
use Illuminate\Http\Request;

use App\Categories;


class SubcategoriesController extends Controller
{

    /**
     * Display a listing of subcategories
     *
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $subcategories = Subcategory::with("category")->get();
        return view('admin.subcategories.index', compact('subcategories'));
    }

    /**
     * Show the form for creating a new subcategories
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $categories = Category::all()->keyBy('id')->map(function ($category) {
            return $category->name;
        });

        return view('admin.subcategories.create', compact("categories"));
    }

    /**
     * Store a newly created subcategories in storage.
     *
     * @param CreateSubcategoriesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateSubcategoriesRequest $request)
    {
        Subcategory::create($request->all());
        return redirect()->route(config('quickadmin.route') . '.subcategories.index');
    }

    /**
     * Show the form for editing the specified subcategories.
     *
     * @param  Subcategory $subcategory
     * @return \Illuminate\View\View
     */
    public function edit(Subcategory $subcategory)
    {
        $categories = Category::all()->keyBy('id')->map(function ($category) {
            return $category->name;
        });

        return view('admin.subcategories.edit', compact('subcategory', "categories"));
    }

    /**
     * Update the specified subcategories in storage.
     *
     * @param Subcategory $subcategory
     * @param UpdateSubcategoriesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Subcategory $subcategory, UpdateSubcategoriesRequest $request)
    {
        $subcategory->update($request->all());
        return redirect()->route(config('quickadmin.route') . '.subcategories.index');
    }

    /**
     * Remove the specified subcategories from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        Subcategory::destroy($id);

        return redirect()->route(config('quickadmin.route') . '.subcategories.index');
    }

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Subcategory::destroy($toDelete);
        } else {
            Subcategory::whereNotNull('id')->delete();
        }
        return redirect()->route(config('quickadmin.route') . '.subcategories.index');
    }

}
