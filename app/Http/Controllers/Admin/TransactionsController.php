<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\TransactionsLog;
use Redirect;
use Schema;

use Illuminate\Http\Request;


class TransactionsController extends Controller
{

    /**
     * Display a listing of transactions
     *
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $transactions = TransactionsLog::all();

        return view('admin.transactions.index', compact('transactions'));
    }


    /**
     * Show the form for editing the specified transactions.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $transaction = TransactionsLog::find($id);

        return view('admin.transactions.edit', compact('transaction'));
    }

}
