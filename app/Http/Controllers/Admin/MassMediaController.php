<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\MassMedia;
use App\Http\Requests\CreateMassMediaRequest;
use App\Http\Requests\UpdateMassMediaRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\FileUploadTrait;


class MassMediaController extends Controller {

	/**
	 * Display a listing of massmedia
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $massmedia = MassMedia::all();

		return view('admin.massmedia.index', compact('massmedia'));
	}

	/**
	 * Show the form for creating a new massmedia
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    
	    
	    return view('admin.massmedia.create');
	}

	/**
	 * Store a newly created massmedia in storage.
	 *
     * @param CreateMassMediaRequest|Request $request
	 */
	public function store(CreateMassMediaRequest $request)
	{
	    $request = $this->saveFiles($request);
		MassMedia::create($request->all());

		return redirect()->route(config('quickadmin.route').'.massmedia.index');
	}

	/**
	 * Show the form for editing the specified massmedia.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$massmedia = MassMedia::find($id);
	    
	    
		return view('admin.massmedia.edit', compact('massmedia'));
	}

	/**
	 * Update the specified massmedia in storage.
     * @param UpdateMassMediaRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateMassMediaRequest $request)
	{
		$massmedia = MassMedia::findOrFail($id);

        $request = $this->saveFiles($request);

		$massmedia->update($request->all());

		return redirect()->route(config('quickadmin.route').'.massmedia.index');
	}

	/**
	 * Remove the specified massmedia from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		MassMedia::destroy($id);

		return redirect()->route(config('quickadmin.route').'.massmedia.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            MassMedia::destroy($toDelete);
        } else {
            MassMedia::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.massmedia.index');
    }

}
