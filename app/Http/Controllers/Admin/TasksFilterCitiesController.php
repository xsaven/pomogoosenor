<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\TasksFilterCities;
use App\Http\Requests\CreateTasksFilterCitiesRequest;
use App\Http\Requests\UpdateTasksFilterCitiesRequest;
use Illuminate\Http\Request;


class TasksFilterCitiesController extends Controller {

	/**
	 * Display a listing of tasksfiltercities
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $tasksfiltercities = TasksFilterCities::with("city")->get();

		return view('admin.tasksfiltercities.index', compact('tasksfiltercities'));
	}

	/**
	 * Show the form for creating a new tasksfiltercities
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
        $city =  City::pluck("name", "id")->prepend('Please select', null);

	    return view('admin.tasksfiltercities.create', compact("city"));
	}

	/**
	 * Store a newly created tasksfiltercities in storage.
	 *
     * @param CreateTasksFilterCitiesRequest|Request $request
	 */
	public function store(CreateTasksFilterCitiesRequest $request)
	{
	    
		TasksFilterCities::create($request->all());

		return redirect()->route(config('quickadmin.route').'.tasksfiltercities.index');
	}

	/**
	 * Show the form for editing the specified tasksfiltercities.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$tasksfiltercities = TasksFilterCities::find($id);
	    $city =  City::pluck("name", "id")->prepend('Please select', null);

		return view('admin.tasksfiltercities.edit', compact('tasksfiltercities', "city"));
	}

	/**
	 * Update the specified tasksfiltercities in storage.
     * @param UpdateTasksFilterCitiesRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateTasksFilterCitiesRequest $request)
	{
		$tasksfiltercities = TasksFilterCities::findOrFail($id);
		$tasksfiltercities->update($request->all());

		return redirect()->route(config('quickadmin.route').'.tasksfiltercities.index');
	}

	/**
	 * Remove the specified tasksfiltercities from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		TasksFilterCities::destroy($id);

		return redirect()->route(config('quickadmin.route').'.tasksfiltercities.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            TasksFilterCities::destroy($toDelete);
        } else {
            TasksFilterCities::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.tasksfiltercities.index');
    }

}
