<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\CustomerReview;
use App\Http\Requests\CreateCustomerReviewRequest;
use App\Http\Requests\UpdateCustomerReviewRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\FileUploadTrait;


class CustomerReviewController extends Controller {

	/**
	 * Display a listing of customerreview
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $customerreview = CustomerReview::all();

		return view('admin.customerreview.index', compact('customerreview'));
	}

	/**
	 * Show the form for creating a new customerreview
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    
	    
	    return view('admin.customerreview.create');
	}

	/**
	 * Store a newly created customerreview in storage.
	 *
     * @param CreateCustomerReviewRequest|Request $request
	 */
	public function store(CreateCustomerReviewRequest $request)
	{
	    $request = $this->saveFiles($request);
		CustomerReview::create($request->all());

		return redirect()->route(config('quickadmin.route').'.customerreview.index');
	}

	/**
	 * Show the form for editing the specified customerreview.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$customerreview = CustomerReview::find($id);
	    
	    
		return view('admin.customerreview.edit', compact('customerreview'));
	}

	/**
	 * Update the specified customerreview in storage.
     * @param UpdateCustomerReviewRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateCustomerReviewRequest $request)
	{
		$customerreview = CustomerReview::findOrFail($id);

        $request = $this->saveFiles($request);

		$customerreview->update($request->all());

		return redirect()->route(config('quickadmin.route').'.customerreview.index');
	}

	/**
	 * Remove the specified customerreview from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		CustomerReview::destroy($id);

		return redirect()->route(config('quickadmin.route').'.customerreview.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            CustomerReview::destroy($toDelete);
        } else {
            CustomerReview::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.customerreview.index');
    }

}
