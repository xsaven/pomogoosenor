<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Classes\SMS;
use App\Notifications\WorkerVerified;
use App\Profile;
use App\ProfileConfidant;
use App\User;
use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request as HttpRequest;

class WorkerRequestController extends Controller
{

    /**
     * Index page
     *
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */

    private $SMS;

    public function __construct(SMS $sms)
    {
        $this->SMS = $sms;
    }

    public function update(HttpRequest $request)
    {
        $profile = Profile::where('user_id', $request->input('user_id'))->first();
        if ($request->has('reason')) {
            $profile->testing_step = 0;
            $profile->quest_step = 0;
            $profile->is_worker = 0;
        } else {
            $profile->users->notify(new WorkerVerified());
            $profile->is_worker = 1;
            $profile->moderated_worker = 1;
        }
        $profile->save();
        return redirect('/admin/workerrequest');
    }

    public function index()
    {
        $moderatableUsers = User::whereHas('profile', function (Builder $query) {
            $query->where('is_worker', 1)
                ->where(function (Builder $q) {
                    $q->where('moderated_worker', '=', 0)
                        ->orWhereNull('moderated_worker');
                });
        })->get();

        return view('admin.workerrequest.index', compact('moderatableUsers'));
    }

    public function show($user_id)
    {
        $user = User::find($user_id);
        $profile = $user->profile()->first();
        $confidant = ProfileConfidant::where('profile_id', $profile->id)->first();
        $cats = $user->categories()->get();

        return view('admin.workerrequest.edit', compact('user', 'profile', 'confidant', 'cats'));
    }

}