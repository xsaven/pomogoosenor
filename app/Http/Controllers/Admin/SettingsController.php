<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Setting;

class SettingsController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
        $settings = array();

        foreach (Setting::keys() as $key => $label) {
            $settings[] = Setting::firstOrCreate([
                'key' =>  $key,
                'label' => $label
            ]);
        }

		return view('admin.settings.index',compact('settings'));
	}

	public function update(Request $request) {
        $data = $request->all();
        foreach ($data['setting'] as $key => $value) {
            $setting = Setting::where('key',$key)->first();
            $setting->value = $value;
            $setting->save();
        }
        return redirect()->back();
    }

}