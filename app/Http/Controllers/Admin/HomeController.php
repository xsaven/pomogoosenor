<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateHomeRequest;
use App\PageConfig;

class HomeController extends Controller
{

    public function index()
    {
        if (!$homeConfig = PageConfig::where('key', '=', 'home')->first()) {
            $homeConfig = new PageConfig();
            $homeConfig->key = 'home';
            $homeConfig->save();
        }

        return view('admin.home.index', ['data' => $homeConfig->value]);
    }


    public function update(UpdateHomeRequest $request)
    {
        $data = $request->all();

        $homeConfig = PageConfig::where('key', '=', 'home')->first();

        $homeConfig->value = $data;
        $homeConfig->save();

        return back();
    }
}