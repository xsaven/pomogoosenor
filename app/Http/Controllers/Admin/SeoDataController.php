<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\SeoData;
use App\Http\Requests\CreateSeoDataRequest;
use App\Http\Requests\UpdateSeoDataRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\FileUploadTrait;


class SeoDataController extends Controller {

	/**
	 * Display a listing of seodata
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $seodata = SeoData::all();

		return view('admin.seodata.index', compact('seodata'));
	}

	/**
	 * Show the form for creating a new seodata
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
        $twitter_card = SeoData::$twitter_card;

	    return view('admin.seodata.create', compact("twitter_card"));
	}

    /**
     * Store a newly created seodata in storage.
     *
     * @param CreateSeoDataRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateSeoDataRequest $request)
	{
        SeoData::create($request->all());

        return redirect()->route(config('quickadmin.route').'.seodata.index');
	}

	/**
	 * Show the form for editing the specified seodata.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
        $seodata = SeoData::find($id);
        $twitter_card = SeoData::$twitter_card;

		return view('admin.seodata.edit', compact('seodata', "twitter_card"));
	}

    /**
     * Update the specified seodata in storage.
     *
     * @param $id
     * @param UpdateSeoDataRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, UpdateSeoDataRequest $request)
	{
		$seodata = SeoData::findOrFail($id);
        $request->request->set('social_data', array_filter($request->social_data));
        $seodata->update($request->all());

		return redirect()->route(config('quickadmin.route').'.seodata.index');
	}

    /**
     * Remove the specified seodata from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
	{
		SeoData::destroy($id);

		return redirect()->route(config('quickadmin.route').'.seodata.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            SeoData::destroy($toDelete);
        } else {
            SeoData::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.seodata.index');
    }

}
