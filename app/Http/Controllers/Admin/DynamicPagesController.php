<?php

namespace App\Http\Controllers\Admin;

use App\DynamicPages;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdatePageRequest;
use Redirect;
use Schema;
use App\StaticPages;
use App\Http\Requests\CreateStaticPagesRequest;
use App\Http\Requests\UpdateStaticPagesRequest;
use Illuminate\Http\Request;


class DynamicPagesController extends Controller
{

    /**
     * @param $slug
     */
    public function edit($slug)
    {
        $templateData = DynamicPages::where(['slug' => $slug])->first();

        return view('admin.dynamic_pages.html',
            [
                'data' => $templateData->data,
                'slug' => $templateData->slug,
                'name' => $templateData->name,
            ]
        );

    }

    public function update(UpdatePageRequest $request)
    {
        $template = DynamicPages::where(['slug' => $request->slug])->first();

        if (!$template) {
            fail_message('test');
        }

        $template->update($request->all());
        return back();
    }

}
