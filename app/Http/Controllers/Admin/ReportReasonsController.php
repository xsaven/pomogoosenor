<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\ReportReasons;
use App\Http\Requests\CreateReportReasonsRequest;
use App\Http\Requests\UpdateReportReasonsRequest;
use Illuminate\Http\Request;



class ReportReasonsController extends Controller {

	/**
	 * Display a listing of reportreasons
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $reportreasons = ReportReasons::all();

		return view('admin.reportreasons.index', compact('reportreasons'));
	}

	/**
	 * Show the form for creating a new reportreasons
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    
	    
	    return view('admin.reportreasons.create');
	}

	/**
	 * Store a newly created reportreasons in storage.
	 *
     * @param CreateReportReasonsRequest|Request $request
	 */
	public function store(CreateReportReasonsRequest $request)
	{
	    
		ReportReasons::create($request->all());

		return redirect()->route(config('quickadmin.route').'.reportreasons.index');
	}

	/**
	 * Show the form for editing the specified reportreasons.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$reportreasons = ReportReasons::find($id);
	    
	    
		return view('admin.reportreasons.edit', compact('reportreasons'));
	}

	/**
	 * Update the specified reportreasons in storage.
     * @param UpdateReportReasonsRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateReportReasonsRequest $request)
	{
		$reportreasons = ReportReasons::findOrFail($id);

        

		$reportreasons->update($request->all());

		return redirect()->route(config('quickadmin.route').'.reportreasons.index');
	}

	/**
	 * Remove the specified reportreasons from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		ReportReasons::destroy($id);

		return redirect()->route(config('quickadmin.route').'.reportreasons.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            ReportReasons::destroy($toDelete);
        } else {
            ReportReasons::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.reportreasons.index');
    }

}
