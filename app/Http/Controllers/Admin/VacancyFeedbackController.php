<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\VacancyFeedback;
use App\Http\Requests\CreateVacancyFeedbackRequest;
use App\Http\Requests\UpdateVacancyFeedbackRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\FileUploadTrait;


class VacancyFeedbackController extends Controller {

	/**
	 * Display a listing of vacancyfeedback
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $vacancyfeedback = VacancyFeedback::all();

		return view('admin.vacancyfeedback.index', compact('vacancyfeedback'));
	}

	/**
	 * Show the form for creating a new vacancyfeedback
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    
	    
	    return view('admin.vacancyfeedback.create');
	}

	/**
	 * Store a newly created vacancyfeedback in storage.
	 *
     * @param CreateVacancyFeedbackRequest|Request $request
	 */
	public function store(CreateVacancyFeedbackRequest $request)
	{
	    $request = $this->saveFiles($request);
		VacancyFeedback::create($request->all());

		return redirect()->route(config('quickadmin.route').'.vacancyfeedback.index');
	}

	/**
	 * Show the form for editing the specified vacancyfeedback.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$vacancyfeedback = VacancyFeedback::find($id);
	    
	    
		return view('admin.vacancyfeedback.edit', compact('vacancyfeedback'));
	}

	/**
	 * Update the specified vacancyfeedback in storage.
     * @param UpdateVacancyFeedbackRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateVacancyFeedbackRequest $request)
	{
		$vacancyfeedback = VacancyFeedback::findOrFail($id);

        $request = $this->saveFiles($request);

		$vacancyfeedback->update($request->all());

		return redirect()->route(config('quickadmin.route').'.vacancyfeedback.index');
	}

	/**
	 * Remove the specified vacancyfeedback from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		VacancyFeedback::destroy($id);

		return redirect()->route(config('quickadmin.route').'.vacancyfeedback.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            VacancyFeedback::destroy($toDelete);
        } else {
            VacancyFeedback::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.vacancyfeedback.index');
    }

}
