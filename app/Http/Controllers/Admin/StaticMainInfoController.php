<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\StaticMainInfo;
use App\Http\Requests\CreateStaticMainInfoRequest;
use App\Http\Requests\UpdateStaticMainInfoRequest;
use Illuminate\Http\Request;



class StaticMainInfoController extends Controller {

	/**
	 * Display a listing of static_main_info
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $static_main_info = StaticMainInfo::all();
		return view('admin.static_main_info.index', compact('static_main_info'));
	}

	/**
	 * Show the form for creating a new static_main_info
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    return view('admin.static_main_info.create');
	}


    /**
     * Store a newly created static_main_info in storage.
     *
     * @param CreateStaticMainInfoRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateStaticMainInfoRequest $request)
	{
		StaticMainInfo::create($request->all());
		return redirect()->route(config('quickadmin.route').'.staticmaininfo.index');
	}

	/**
	 * Show the form for editing the specified static_main_info.
	 *
	 * @param  int $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
        $static_main_info = StaticMainInfo::find($id);
		return view('admin.static_main_info.edit', compact('static_main_info'));
	}


    /**
     * Update the specified static_main_info in storage.
     *
     * @param int $id
     * @param UpdateStaticMainInfoRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, UpdateStaticMainInfoRequest $request)
	{
        $static_main_info = StaticMainInfo::find($id);
        $static_main_info->update($request->all());
		return redirect()->route(config('quickadmin.route').'.staticmaininfo.index');
	}


    /**
     * Remove the specified static_main_info from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
	{
        StaticMainInfo::destroy($id);
		return redirect()->route(config('quickadmin.route').'.staticmaininfo.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            StaticMainInfo::destroy(json_decode($request->toDelete));
        } else {
            StaticMainInfo::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.staticmaininfo.index');
    }

}
