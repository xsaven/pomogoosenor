<?php

namespace App\Http\Controllers;

use App\Category;
use App\City;
use App\Galleries;
use App\Http\Controllers\API\VerificationController;
use App\Payment;
use App\ProfileView;
use App\Review;
use App\Subscribe;
use App\Task;
use App\UserTasks;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use App\Profile;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Hash;
use Validator;
use Image;
use App\ProfileConfidant;

class ProfileController extends Controller
{

    private $avatarsFolder = '/uploads/users/';


    public function index(User $users, $tab = null)
    {
        if (!$user = \user()->getUserWithProfile())
            return abort('404');

        $galleries = Galleries::getListByUser($user);

        $all_transactions = Payment::where('receiver_id', '=', $user->id)
            ->orWhere('sender_id', '=', $user->id)->get();

        $received_transactions = Payment::whereReceiverId($user->id)->get();
        $sent_transactions = Payment::whereSenderId($user->id)->get();
        $cat_tree = Category::getTree();


        $reviews = Review::query()->whereIn('task_id', array_column($user->tasks()->get('id')->toArray(), 'id'))
            ->where('user_id', '!=', $user->id)
            ->paginate(5);

        return view('profile.profile', compact('user', 'tab', 'galleries', 'received_transactions', 'sent_transactions', 'all_transactions', 'cat_tree', 'reviews'));
    }

    public function transactions($sort)
    {
        $now = new \Date();
        $ranged = (new \Date())->modify('-1 ' . $sort);

        if (!$user = \user()->getUserWithProfile())
            return abort('404');

        $all_transactions = Payment::whereBetween('created_at', [$ranged, $now])
            ->where('receiver_id', '=', $user->id)
            ->orWhere('sender_id', '=', $user->id)
            ->get();

        $received_transactions = Payment::where('receiver_id', '=', $user->id)
            ->whereBetween('created_at', [$ranged, $now])
            ->get();

        $sent_transactions = Payment::where('sender_id', '=', $user->id)
            ->whereBetween('created_at', [$ranged, $now])
            ->get();


        return view('profile.tabs.transactions.transactions', compact('received_transactions', 'sent_transactions', 'all_transactions'));
    }

    public function show(User $user, Request $request)
    {
        $galleries = Galleries::getListByUser($user);

        if ($user->is_worker()) {

            $user_tasks = UserTasks::where("executor_id", $user->id)->get("id")->map(function ($item) { return $item->id; })->toArray();

            $reviews = Review::whereIn('task_id', $user_tasks)
                ->where('user_id', '!=', $user->id)
                ->where("type", "1")
                ->paginate(5);

        } else {

            $user_tasks = $user->tasks()->get("id")->map(function ($item) { return $item->id; })->toArray();

            $reviews = Review::whereIn('task_id', $user_tasks)
                ->where('user_id', '!=', $user->id)
                ->paginate(5);

        }



        if (\Auth::check() && $user->id == user()->id) {
            return view('profile.profile', compact('user', 'galleries', 'reviews'))->with('tab', null);
        }


        ProfileView::addView($user->profile, $request->ip(), $request->userAgent());


        return view('profile.public.profile_public', compact('user', 'galleries', 'reviews'));
    }

    /*
     * Main info update validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'surname' => 'nullable|string|max:255',
            'city' => 'required|string|max:50',
            'sex' => 'required|boolean',
            'phone_code' => 'required|string|max:5',
            'phone' => 'required|string|max:15',
            'birthday' => 'required|date',
            'email' => 'required|string|email|max:255|unique:users,email,' . auth()->id(),
        ]);
    }

    public function update(City $city, Request $request)
    {
        $date = Carbon::createFromDate($request->year, $request->month, $request->day);
        $request->request->add(['birthday' => $date]);
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 421);
        }
        $city_id = '1';
        $city = $city->findCityByName($request->city);
        if (!empty($city))
            $city_id = $city->id;
        $request->request->add(['city_id' => $city_id]);
        $user = auth()->user();
        if ($request->email != $user->email)
            $request->request->add(['verified' => '']);
        $user->update($request->only('name', 'email', 'verified'));
        $profile = $user->profiles();
        $profile->update($request->only('name', 'surname', 'sex', 'city_id', 'phone_code', 'phone', 'birthday'));
        return response()->json(['status' => 'success', 'message' => 'Данные обновлены'], 200);
    }

    /*
     * Change password ajax
     */
    public function changePassword(Request $request)
    {
        $inputs = $request->all();

        $rules = array(
            'password' => 'required|confirmed|min:6',
        );

        $validation = Validator::make($inputs, $rules);

        if ($validation->fails()) {
            return response()->json($validation->errors()->getMessages(), 421);
        }
        $user = auth()->user();
        $user->password = $request->password;
        $user->save();
        return response()->json(['status' => 'success', 'message' => 'Пароль обновлён'], 200);
    }

    /*
     * Ajax validate
     */
    public function ajaxValidate(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 421);
        } else {
            return response()->json([], 200);
        }
    }

    /*
     * Remove user
     */
    public function destroy()
    {
        User::destroy(auth()->id());
        return redirect()->route('home');
    }


    /*
     * OLD CODE
     */

//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    protected function create_profile_validator($data)
    {
        return Validator::make($data, [
            'name' => 'required|min:6|max:25',
            'tel' => 'required|numeric',
            'city_id' => 'required|exists:city,id',
            'avatar' => 'image|dimensions:min_width=200,min_height=200',
        ]);
    }

    public function ajaxLoadAvatar(Request $request)
    {
        if ($request->has('avatar')) {
            $profile = auth()->user()->profiles;
            $avatar = $this->loadAvatar($request->input('avatar'), true);

            $profile->avatar_full = $avatar['full'];
            $profile->avatar_thumb = $avatar['thumb'];
            $profile->save();

        }
        return response()->json(['status' => 'success', 'message' => 'Аватар успешно загружен'], 200);
    }

    public function ajaxDeleteAvatar(Request $request)
    {
        $profile = auth()->user()->profiles;
        $profile->avatar_full = '/photos/defaults/avatar.png';
        $profile->avatar_thumb = '/photos/defaults/avatar.png';
        $profile->save();

        return response()->json(['status' => 'success', 'message' => 'Аватар успешно удалён'], 200);
    }

    protected
    function saveVideo(UploadedFile $file)
    {
        $dir = 'uploads/videos/' . \user()->id;
        $extension = $file->getClientOriginalExtension();
        $filename = $filename = uniqid() . '_' . time() . '_' . date('Ymd') . '.' . $extension;
        $file->move($dir, $filename);
        return url('/') . '/' . $dir . '/' . $filename;
    }

    public
    function uploadVideo(Request $request)
    {
        if ($request->files->has('video')) {
            if (!$request->file('video')->getSize()) {
                return response()->json(['error' => true, 'error' => 'Файл слишком большой']);
            }
            $path = $this->saveVideo($request->file('video'));
            $workerProfile = \user()->profile->workerProfile;
            $workerProfile->video = $path;
            $workerProfile->save();
        }
        return response()->json(['path' => $path ?? '']);
    }


    public
    function subscribe(Request $request)
    {
        if ($request->has('news') && ((bool)$request->news != user()->subscribe->news)) {
            (new \App\Http\Controllers\API\SubscribeController())->mailChimpSubscribtion(user()->email, $request->news);
        }
        $subscribe_data = [
            'system_notification' => $request->system_notification ?? 0,
            'sms_new_message' => $request->sms_new_message ?? 0,
            'news' => $request->news ?? 0
        ];
        $subscription = \user()->subscribe();
        if ($subscription->first()) {
            $subscription->update($subscribe_data);
        } else {
            $subscription->create($subscribe_data);
        }
        return redirect()->back();
    }


    protected
    function loadAvatar($file, $base64 = false)
    {
//        dd($file);
        if ($base64) {
            $ext = 'jpg';
        } else {
            $ext = $file->getClientOriginalExtension();
        }

        $name = 'user_' . auth()->id();
        $nameWithPathFull = $this->avatarsFolder . $name . '.' . $ext;
        $nameWithPathThumb = $this->avatarsFolder . $name . '_thumb.' . $ext;

        $fullImage = Image::make($file);
        $thumbImage = Image::make($file);


        $fullImage->save(public_path($nameWithPathFull));
        $thumbImage->fit(300)->save(public_path($nameWithPathThumb));

        return array(
            'full' => $nameWithPathFull,
            'thumb' => $nameWithPathThumb
        );
    }

    protected
    function fill_profile_validator($data)
    {
        return Validator::make($data, [
            'name' => 'required|min:2|max:25',
            'surname' => 'required|min:2|max:25',
            'phone_verified' => 'required',
            'phone_code' => 'required',
            'phone_number' => 'required',
            'city' => 'required|exists:city,name',
            'avatar' => 'image|dimensions:min_width=200,min_height=200',
            'day' => 'required|numeric|between:1,31',
            'month' => 'required|numeric|between:1,12',
            'year' => 'required|numeric',
            'selected_cat' => 'array',
            //'selected_cat' => 'array|max:3',


            'confirm_1' => 'required',
            'confirm_2' => 'required',
            'confirm_3' => 'required',

            'confidant_name' => 'required|max:50',
            'confidant_surname' => 'required|min:2|max:50',
            'confidant_status' => 'required|min:2|max:50',
            'confidant_phone' => 'required|min:2|max:50',

        ], [
            'confirm_1.required' => 'Вы должны подтвердить что проходите верификацию на нашем сервисе впервые',
            'confirm_2.required' => 'Вы должны подтвердить что указали свои личные данные достоверно',
            'confirm_3.required' => 'Вы должны подтвердить что согласны с соглашением на обработку персональных данных',
            'phone_verified.required' => 'Вы должны подтвердить свой телефон'
        ]);
    }

    public
    function postFillInfo(Request $request)
    {
        $validator = $this->fill_profile_validator($request->all());
        if ($validator->fails()) {
            return redirect()->back()->with('validation_errors', $validator->errors()->getMessages())->withInput($request->all());
        }

        $user = auth()->user();

        $profile = $user->profiles;


        if (
            $request->has('confidant_name') ||
            $request->has('confidant_surname') ||
            $request->has('confidant_status') ||
            $request->has('confidant_phone')
        ) {
            $profile->profileConfidants()->create([
                'name' => $request->confidant_name,
                'surname' => $request->confidant_surname,
                'status' => $request->confidant_status,
                'phone' => $request->confidant_phone,

            ]);

        }

        $city_id = '1';
        $city = (new City())->findCityByName($request->city);
        if (!empty($city)) {
            $city_id = $city->id;
        }

        $profile->name = $request->input('name');
        $profile->surname = $request->input('surname');
        $profile->city_id = $city_id;
        $profile->phone_code = $request->input('phone_code');
        $profile->phone = $request->input('phone_number');

        $profile->birthday = $request->input('year') . '-' . sprintf("%02d", $request->input('month')) . '-' . sprintf("%02d", $request->input('day'));

        if (!empty($request->input('selected_cat')))
            $user->categories()->attach($request->input('selected_cat'));


        $profile->save();

        $profile->switchTestingStep(1);

        return redirect('/testing/start');

    }

    public
    function saveSettings(Request $request)
    {
        $user = auth()->user();

        foreach ($user->categories as &$category) {
            if (!in_array($category->id, $request->input('selected_cat', []))) {
                $user->categories()->detach($category->id);
            }
        }

        foreach ($request->input('selected_cat', []) as $item) {
            if (!in_array($item, array_column($user->categories->toArray(), 'id'))) {
                $user->categories()->attach($item);
            }
        }

        $user->subscribe_push = $request->input('subscribe')['push'];
        $user->subscribe_email = $request->input('subscribe')['email'];
        $user->save();
        return response()->json(['success' => true]);


    }

    public
    function sendPhoneVerification()
    {
        $phone_number = preg_replace('/\D/', '', \request('phone_number'));
        $phone = \request('phone_code') . $phone_number;
        \request()->request->set('phone', $phone);
        return (new VerificationController())->sendPhoneVerification();
    }

    public
    function checkPhoneVerification(Request $request)
    {
        return (new VerificationController())->checkPhoneVerification($request);
    }

    public
    function postCreateProfile(Request $request)
    {
        $validator = $this->create_profile_validator($request->all());
        if ($validator->fails()) {
            return redirect()->back();
        }


        \Auth::user()->name = $request->input('name');
        \Auth::user()->save();

        $profile = Profile::firstOrCreate([
            'user_id' => \Auth::user()->id
        ]);

        if ($request->hasFile('avatar')) {
            $avatar_data = $this->loadAvatar($request->file('avatar'));
            $profile->avatar_full = $avatar_data['full'];
            $profile->avatar_thumb = $avatar_data['thumb'];
        }
        $profile->phone = $request->input('tel');
        $profile->city_id = $request->input('city_id');
        $new_profile_id = $profile->save();

        return redirect('/')->with('success', ['Добро пожаловать, ' . $request->input('name')]);
    }

    public
    function postPrevalidate(Request $request)
    {
        $validator = $this->create_profile_validator($request->all());
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 421);
        } else {
            return response()->json([], 200);
        }
    }

    public
    function changeBusyState(Request $request)
    {
        \user()->setBusy($request->is_busy);
        return redirect()->back();
    }

    public
    function updateAboutMe(Request $request)
    {
        if ($request->about_me) {
            $workerProfile = \user()->profile->workerProfile;
            $workerProfile->about_me = $request->about_me;
            if (!$workerProfile->save()) {
                return response()->json(['success' => false]);
            }
        }
        return response()->json(['success' => true]);
    }

    public
    function updateMinPrice(Request $request)
    {
        if ($request->min_price && $request->about_price) {
            $workerProfile = \user()->profile->workerProfile;
            $workerProfile->min_price = $request->min_price;
            $workerProfile->about_price = $request->about_price;
            if (!$workerProfile->save()) {
                return response()->json(['success' => false]);
            }
        }
        return response()->json(['success' => true]);
    }


}
