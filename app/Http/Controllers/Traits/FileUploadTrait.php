<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;

trait FileUploadTrait
{

    private function mergeUploadedFilesToRequest(&$request, $filename, $key, $main_key = null)
    {
        $request_data = $request->all();
        if ($main_key){
            array_set($request_data,"$main_key.$key",$filename);
        }else{
            array_set($request_data,"$key",$filename);
        }
        $request = new Request($request_data);
    }

    private function saveFilesByKey(Request &$request, $key, $main_key = null)
    {
        if (\request()->has($key . '_w') && \request()->has($key . '_h')) {
            // Check file width
            $filename = md5(uniqid()) . '.' . \request()->file($key)->getClientOriginalExtension();
            $file = \request()->file($key);
            $image = Image::make($file);
            Image::make($file)->resize(50, 50)->save(public_path('uploads/thumb') . '/' . $filename);
            $width = $image->width();
            $height = $image->height();
            if ($width > \request()->{$key . '_w'} && $height > \request()->{$key . '_h'}) {
                $image->resize(\request()->{$key . '_w'}, \request()->{$key . '_h'});
            } elseif ($width > \request()->{$key . '_w'}) {
                $image->resize(\request()->{$key . '_w'}, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            } elseif ($height > \request()->{$key . '_w'}) {
                $image->resize(null, \request()->{$key . '_h'}, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            $image->save(public_path('uploads') . '/' . $filename);
            $request = \request();
            $this->mergeUploadedFilesToRequest($request, $filename, $key, $main_key);
        } else {
            if (is_array(request()->file($key))) {
                foreach (request()->file($key) as $f_key=>$file) {
                    if (is_array($file)) {
                        foreach ($file as $s_f_key=>$s_file) {
                            $filename = md5(uniqid()) . '.' . $s_file->getClientOriginalExtension();
                            dump($filename);
                            $s_file->move(public_path('uploads'), $filename);
                            $this->mergeUploadedFilesToRequest($request, $filename, $s_f_key, "$key.$f_key");
                        }
                    }else {
                        $filename = md5(uniqid()) . '.' . $file->getClientOriginalExtension();
                        $file->move(public_path('uploads'), $filename);
                        $this->mergeUploadedFilesToRequest($request, $filename, $f_key, $key);
                    }
                }
            } else {
                $filename = md5(uniqid()) . '.' . \request()->file($key)->getClientOriginalExtension();
                \request()->file($key)->move(public_path('uploads'), $filename);
                $this->mergeUploadedFilesToRequest($request, $filename, $key, $main_key);
            }
        }
    }

    /**
     * File upload trait used in controllers to upload files
     */
    public function saveFiles(Request $request)
    {
        if (!file_exists(public_path('uploads'))) {
            mkdir(public_path('uploads'), 0777);
            mkdir(public_path('uploads/thumb'), 0777);
        }
        $request_data = array_dot($request->all());
        $req = $request;
        foreach ($request_data as $key => $value) {
            if ($request->hasFile($key)) {
                    $this->saveFilesByKey($req, $key);
            }
        }

        return $req;
    }

    private function getMimeType($file)
    {
        $type = explode('/',$file->getClientMimeType())[0];
        switch ($type){
            case 'image':
                return $type;
                break;
            case 'video':
                return $type;
                break;
            default:
                return 'document';
        }
    }

    /**
     * @param UploadedFile $file File
     * @param $path Path
     * @return string
     */
    public function storeFile($file, $path)
    {
        $filename = time() . '-' . $file->getClientOriginalName();
        $file->move(public_path($path), $filename);
        return rtrim($path, '/').'/'.$filename;
    }

    /**
     * @param string $path
     * @return \Generator
     */
    function storeAllFiles($path = 'uploads'){
        $files = request()->files->all();
        $files_data = [];
        array_walk_recursive($files,function($file, $key) use ($path, &$files_data){
            $filepath = $this->storeFile($file, $path);
            $files_data[] = ['file' => $filepath, 'type' => $this->getMimeType($file)];
        });
        return $files_data;
    }
}