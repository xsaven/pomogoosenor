<?php

namespace App\Http\Controllers;

use App\Category;
use App\Content;
use App\Galleries;
use App\Http\Requests\CreateGalleriesRequest;
use App\Http\Requests\UpdateGalleriesRequest;
use App\Posts;
use Illuminate\Http\Request;

class GalleryController extends Controller
{

    /**
     * Display a listing of galleries
     *
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $galleries = Galleries::whereUserId($request->user_id)
            ->with('content_items')->get();
        return response()->json($galleries);
    }

    public function show(Galleries $gallery, Request $request)
    {
        $currentUser = user();

        return view('galleries.show', compact('gallery', 'currentUser'));
    }


    public function getExamples(Request $request)
    {
        $examples = Content::getUserGalleries($request->user_id)->pluck('path');
        return response()->json(compact('examples'));
    }

    public function create()
    {
        $user = user();
        return view('galleries.create', compact('user'));
    }

    /**
     * Store a newly created galleries in storage.
     *
     * @param CreateGalleriesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateGalleriesRequest $request)
    {

        $gallery = user()->galleries()->create($request->all());
        foreach ($this->storeAllFiles('uploads/gallery') as $key => $file_data) {
            $item = $gallery->content_items()->create($file_data);
            if ($request->has('preview_new') && $key === (int)$request->get('preview_new')) {
                $item->gallery_content->is_avatar = true;
                $item->gallery_content->save();
            }
        }
        return response()->json($gallery->load('content_items'));
    }

    public function edit(Galleries $gallery, Request $request)
    {
        if ($gallery->user->id !== user()->id) {
            return abort('404');
        }

        $user = user();
        return view('galleries.edit', compact('gallery', 'user'));
    }

    /**
     * Update the specified galleries in storage
     *
     * @param Galleries $gallery
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateGalleriesRequest $request)
    {
        $gallery = Galleries::whereId($request->get('id'))->first();

        if ($gallery->user->id !== user()->id) {
            return abort('404');
        }

        $gallery->update($request->only('title', 'description'));

        foreach ($gallery->content_items as $contentItem) {
            if ($request->has('preview')) {
                $contentItem->gallery_content->is_avatar = false;
                $contentItem->gallery_content->save();
                if ($contentItem->id === (int)$request->get('preview')) {
                    $contentItem->gallery_content->is_avatar = true;
                    $contentItem->gallery_content->save();
                }
            }
        }


        foreach ($this->storeAllFiles('uploads/gallery') as $key => $file_data) {
            $item = $gallery->content_items()->create($file_data);

            if ($request->has('preview_new')) {
                foreach ($gallery->content_items as $contentItem) {
                    $contentItem->gallery_content->is_avatar = false;
                    $contentItem->gallery_content->save();
                }

                if ($key === (int)$request->get('preview_new')) {
                    $item->gallery_content->is_avatar = true;
                    $item->gallery_content->save();
                }
            }
        }

        foreach ($gallery->content_items as $contentItem) {
            if (!in_array($contentItem->id, $request->get('images', []))) {
                $contentItem->delete();

            }
        }


        return response()->json($gallery->load('content_items'));
    }

    /**
     * Remove the specified galleries from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Galleries $gallery)
    {
        if ($gallery->user->id !== user()->id) {
            return abort('404');
        }

        $gallery->delete();
        return redirect(url('profile'));
    }
}
