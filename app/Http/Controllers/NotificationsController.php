<?php

namespace App\Http\Controllers;

use App\Http\Resources\APICollection;
use App\Http\Resources\Notifications\NotificationResource;
use Illuminate\Http\Request;
use App\User;

class NotificationsController extends Controller
{
    public function index(Request $request) {
        $notifications = \user()->notifications()->paginate(5);
        $unreadNotificationsCount = \user()->notifications()->whereNull('read_at')->count();
        return (new APICollection($notifications,NotificationResource::class))->additional(compact('unreadNotificationsCount'));
    }

    public function readAll()
    {
        \user()->notifications()->update(['read_at'=>now()]);
        return response()->json(['success'=>true]);
    }
}
