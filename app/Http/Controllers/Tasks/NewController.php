<?php

namespace App\Http\Controllers\Tasks;

use App\Category;
use App\City;
use App\FAQ;
use App\Http\Controllers\Auth\RegisterController;
use App\Providers\AuthUser;
use App\Subcategory;
use App\User;
use App\UserTasks;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class NewController extends Controller
{
    public function copy_new(UserTasks $userTasks, Request $request)
    {
        if (Auth::check() && $userTasks->creator_id != user()->id ?? false) abort(404);
        return redirect()->route('task.new', ['category' => $userTasks->subcategory->category->slug, 'subcategory' => $userTasks->subcategory->slug])
            ->withInput($userTasks->toArray());
    }

    public function index($category, $subcategory = false, Request $request, $validate = false)
    {
        $category = \App\Category::where('slug', $category)->first();
        if (!$category) abort(404);
        $defaultSubCategory = \App\Subcategory::where('default', 1)->where('category_id', $category->id)->first();
        if (!$defaultSubCategory) abort(404);
        $subcategory = $subcategory ?
            \App\Subcategory::where('slug', $subcategory)->first() :
            $defaultSubCategory;
        if (!$category || !$subcategory || $category->id != $subcategory->category_id) abort(404);

        if ($validate) return [$category, $subcategory];

        $user = user();

        $fields = [];
        foreach (\App\FormFields::all() as $field) $fields[$field->id] = $field;

        $sessionName = $request->query_search ?? Session::get('query_search', '');

        Session::forget('query_search');

        $faq = FAQ::where('show_on_menu', '=', true)->get();

        return view($request->ajax() ? 'tasks.forms.new' : 'tasks.new', [
            'categories' => \App\Category::where('active', 1)->get(),
            'subcategories' => \App\Subcategory::where('category_id', $category->id)->get(),
            'selectCategory' => $category,
            'selectSubcategory' => $subcategory,
            'defaultSubCategory' => $defaultSubCategory,
            'fields' => $fields,
            'user' => $user,
            'sessionName' => $sessionName,
            'faq' => $faq,
        ]);


    }

    public function create($category, $subcategory = false, Request $request)
    {

        $subcategory = Subcategory::whereSlug($subcategory)->first();
        $messages = [
            'agree_with_rules.required' => 'Вы должны принять правила использования даного сервиса',
            'cost.required' => 'Необходимо указать бюджет',
            'name.required' => 'Вы должны сообщить нам что вам нужно',
            'description.required' => 'Вы должны сообщить нам детали вашего пожелания, чтобы исполнители лучше оценили вашу задачу',
            'validAddress.required' => 'Необходимо указать правильные адреса',
        ];
        $validation_rules = [
            'agree_with_rules' => 'required',
            'cost' => 'required',
            'name' => 'required|string',
            'description' => 'required|string',
            'data' => 'array',
            'email_report' => 'int',
            'executor_only' => 'int',
            'commentaries' => 'int',
            'photo' => 'file',
        ];
        $form_fileds = array_flatten($subcategory->form_fields ?? []);

        if (in_array('Адрес', $form_fileds)) {
            $validation_rules['validAddress'] = 'required';
        }
        $this->validate($request, $validation_rules, $messages);

        if (!isset(Auth::user()->id)) {
            $this->validate($request, [
                'user_name' => 'required|string|max:255',
                'user_email' => 'required|email|max:255',
                'user_phone_code' => 'required|string|max:5',
                'user_phone' => 'required|string|max:20'
            ]);

            $requestFromUserRegister = new Request([
                'name' => $request->user_name,
                'email' => $request->user_email,
                'phone_code' => $request->user_phone_code,
                'phone' => $request->user_phone,
                'city' => $request->city
            ]);
            $regResult = (new RegisterController)->autoAuth()->register($requestFromUserRegister);
        }

        if (!is_null($request->photo)) {
            $request->photo = $request->photo->store('/task_files');
            Storage::disk('task_files')->put($request->photo, file_get_contents(storage_path('app/' . $request->photo)));
        }
        $values = $request->all();

        if ($city = City::whereName($request->city)->first()) {
            $values['city_id'] = $city->id;
        }

        $values['need_documents'] = isset($request->need_documents);
        $values['subcategory_id'] = $subcategory->id;
        $values['creator_id'] = Auth::user()->id;
        $values['executor_id'] = 0;
        $values['photo'] = !is_null($request->photo) ? $request->photo : '';
        $values['private_description'] = !is_null($request->private_description) ? $request->private_description : '';
        $values['status'] = UserTasks::STATUS_MODERATE;
        $task = UserTasks::create($values);

        return redirect()->route('task.review', $task);
    }

    public function search(Request $request)
    {

        $this->validate($request, [
            'q' => 'required|string|max:255'
        ]);

        $searchResult = Subcategory::orderBy('created_at', 'asc');

        $this->dump_words = [];

        $subCategory = Subcategory::query()->with('category')
            ->where(function (Builder $query) use ($request) {
                $words = explode(' ', $request->q);
                foreach ($words as $word) {
                    $word = preg_replace("/[^ a-zа-яё\d]/ui", "", $word);
                    if ((strlen($word)) < 5) continue;
                    //dump(strlen($word), $word);
                    $query->orWhere('tags', 'LIKE', '%' . $word . '%');
                    //$query->orWhere('description', 'LIKE', '%' . $word . '%');
                    $query->orWhere('name', 'LIKE', '%' . $word . '%');
                    $this->dump_words[] = $word;
                }
            })->first();

        //dd($this->dump_words, $subCategory->toArray());

        if (!$subCategory) {
            $subCategory = Subcategory::query()->first();
        }


        Session::put('query_search', $request->q);
        return redirect()->route('task.new', ['category' => $subCategory->category->slug, 'subcategory' => $subCategory->slug, 'select' => true]);
    }

    public function storeOldData(Request $request)
    {
        Session::put('_old_input', $request->all());

        return response()->json(['status' => 'success']);
    }
}
