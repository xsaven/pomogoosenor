<?php

namespace App\Http\Controllers\Tasks;

use App\City;
use App\Services\WalletOneSafeDeal\Requests\Params\SaveDealParams;
use App\Services\WalletOneSafeDeal\WOSafeDeal;
use App\Subcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserTasks;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\StatusTask;

class EditController extends Controller
{
    public function index($category, $subcategory=false, UserTasks $userTasks, Request $request, $validate=false)
    {
        if($userTasks->creator_id!=user()->id) abort(404);

        $category = \App\Category::where('slug', $category)->first();
        if(!$category) abort(404);
        $defaultSubCategory = \App\Subcategory::where('default', 1)->where('category_id',$category->id)->first();
        if(!$defaultSubCategory) abort(404);
        $subcategory = $subcategory ?
            \App\Subcategory::where('slug', $subcategory)->first() :
            $defaultSubCategory;
        if(!$category || !$subcategory || $category->id != $subcategory->category_id) abort(404);

        if($validate) return [$category, $subcategory];

        $user = user();

        $fields = [];
        foreach (\App\FormFields::all() as $field) $fields[$field->id] = $field;

        return view($request->ajax() ? 'tasks.forms.new' : 'tasks.new', [
            'categories' => \App\Category::where('active', 1)->get(),
            'subcategories' => \App\Subcategory::where('category_id', $category->id)->get(),
            'selectCategory' => $category,
            'selectSubcategory' => $subcategory,
            'defaultSubCategory' => $defaultSubCategory,
            'fields' => $fields,
            'task' => $userTasks,
            'user' => $user
        ]);
    }

    public function edit($category, $subcategory=false, UserTasks $userTasks, Request $request)
    {
        if($userTasks->creator_id!=user()->id) abort(404);

        $subcategory = Subcategory::whereSlug($subcategory)->first();

        $messages = [
            'agree_with_rules.required' => 'Вы должны принять правила использования даного сервиса',
            'cost.required' => 'Необходимо указать бюджет',
            'name.required' => 'Вы должны сообщить нам что вам нужно',
            'description.required' => 'Вы должны сообщить нам детали вашего пожелания, чтобы исполнители лучше оценили вашу задачу',
            'validAddress.required' => 'Необходимо указать правильные адреса',
        ];
        $validation_rules = [
            'agree_with_rules' => 'required',
            'cost' => 'required',
            'name' => 'required|string',
            'description' => 'required|string',
            'data' => 'array',
            'email_report' => 'int',
            'executor_only' => 'int',
            'commentaries' => 'int',
            'photo' => 'file',
        ];
        $form_fileds = array_flatten($subcategory->form_fields);
        if (in_array('Адрес', $form_fileds)) {
            $validation_rules['validAddress'] = 'required';
        }
        $this->validate($request,$validation_rules , $messages);

        if(!is_null($request->photo)) {
            $request->photo = $request->photo->store('/task_files');
            Storage::disk('task_files')->put($request->photo, file_get_contents(storage_path('app/'.$request->photo)));
        }
        $values = $request->all();

        if ($city = City::whereName($request->city)->first()) {
            $values['city_id'] = $city->id;
        }

        $values['status'] = UserTasks::STATUS_MODERATE;
        $values['subcategory_id'] = $subcategory->id;
        $values['creator_id'] = Auth::user()->id;
        $values['executor_id'] = 0;
        $values['need_documents'] = isset($request->need_documents);
        if($request->photo) $values['photo'] = $request->photo;
        $values['private_description'] = !is_null($request->private_description) ? $request->private_description : '';

        $userTasks->update($values);
        return redirect()->route('task.review', ['userTasks' => $userTasks->id])->with(['status' => 'New Task Created']);
    }

    public function field_edit(UserTasks $userTasks, $field, Request $request)
    {
        if($userTasks->creator_id!=user()->id) abort(404);
        if($field=='status'){

            $messages = [
                'cancel.required' => 'Укажите причину отмены'
            ];
            $this->validate($request, [
                'cancel' => 'required'
            ], $messages);

            $userTasks->status = 4;
            $userTasks->save();

            StatusTask::create([
                'task_id' => $userTasks->id,
                'user_id' => user()->id,
                'reason' => $request->cancel,
                'reason_description' => $request->another_reason_description
            ]);

            return back()->with(['status' => 'success', 'message' => 'Статус задания изменён!']);
        }else if($field=='cost'){

            $messages = [
                'cost.required' => 'Необходимо указать бюджет',
            ];
            $this->validate($request, [
                'cost' => 'required',
            ], $messages);
            $userTasks->cost = $request->cost;
            $userTasks->exact_cost = $request->exact_cost;
            $userTasks->save();
            return back()->with(['status' => 'success', 'message' => 'Бюджет был успешно изменен']);
        }
        else abort(404);

    }

    /**
     * Select task executor
     *
     * @param UserTasks $task
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function selectExecutor(UserTasks $task, Request $request)
    {
        try {


            dd($request->all());
            $task->setExecutor($request->user_id);
            return back();
        }catch (\Exception $exception){
            return back()->withErrors($exception->getMessage());
        }
    }
}
