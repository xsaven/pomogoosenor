<?php
/**
 * Created by PhpStorm.
 * User: Uer
 * Date: 26.03.2018
 * Time: 16:30
 */

namespace App\Http\Controllers\Tasks;


use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Tasks\StoreOfferRequest;
use App\TaskOffers;
use App\User;
use App\UserTasks;
use Illuminate\Support\Facades\Mail;

class OffersController extends Controller
{

    public function store($task_id, StoreOfferRequest $request)
    {
        $task = UserTasks::find($task_id);
        if ($task && $offer = TaskOffers::storeTwo($task, $request->all())) {
            if ($task->email_report) {
                Mail::send(new \App\Mail\NotifyCreatorNewOffer($task));
            }

            User::find($task->creator_id)->notify(new \App\Notifications\TaskOfferCreated($offer));

            return redirect()->back()->with(['status' => 'success', 'message' => 'Предложение добавлено']);
        }
        return redirect()->back()->with(['status' => 'error', 'message' => 'Невозможно создать предложение']);
    }

    public function update($offer_id, StoreOfferRequest $request)
    {

        $offer = TaskOffers::where('id', $offer_id)->where('user_id', user()->id)->first();
        $request->request->set('other_notification', $request->other_notification ?? 0);
        if ($offer && $offer->update($request->all())) {
            return redirect()->back()->with(['status' => 'success', 'message' => 'Предложение отредактировано']);
        }
        return redirect()->back()->with(['status' => 'error', 'message' => 'Невозможно отредактировать предложение']);
    }

    public function remove($offer_id)
    {
        $offer = TaskOffers::where('id', $offer_id)->where('user_id', user()->id)->first();
        if ($offer && $offer->delete()) {
            return redirect()->back()->with(['status' => 'success', 'message' => 'Предложение удалено']);
        }
        return redirect()->back()->with(['status' => 'error', 'message' => 'Невозможно удалить предложение']);
    }

}