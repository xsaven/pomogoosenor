<?php

namespace App\Http\Controllers\Tasks;

use App\Category;
use App\Http\Controllers\Controller;
use App\TasksFilterCities;
use Illuminate\Http\Request;
use App\UserTasks;

class ListController extends Controller
{

    public function index(Request $request)
    {
        if ($request->has('type')) {
            return $this->{'getMy' . lcfirst($request->type) . 'Tasks'}();
        }
        $creator_tasks = UserTasks::withMyCreatorTasks();
        $executor_tasks = UserTasks::withMyExecutorTasks();
        $categories = Category::getTree();
        $sort_data = (array)(cache('user.' . user()->id . '.sort-data') ?? $this->getBaseSortData());

        $tasksMapDataCreators = $this->getMapsData($creator_tasks);
        $tasksMapDataExecutors = $this->getMapsData($executor_tasks);

        return view(
            'tasks.list',
            compact('creator_tasks',
                'executor_tasks',
                'categories',
                'sort_data',
                'tasksMapDataCreators',
                'tasksMapDataExecutors'
            )
        );
    }

    private function fixTaskData()
    {
        $tasks = UserTasks::all();
        foreach ($tasks as $task) {
            if ($task->cost && $task->cost != '0' && $task->exact_cost != 0) {
                dump($task->cost, $task->exact_cost);
                $task->exact_cost = 0;
                $task->save();
            }
        }
        die;
    }

    private function getBaseSortData()
    {
        return [
            'status' => UserTasks::STATUS_OPENED,
            'no_offers' => false,
            'q' => '',
            'subcategories' => [],
            'city' => TasksFilterCities::first()->id
        ];
    }

    private function getSortData()
    {
        $old_sort_data = $this->getCachedSortData();
        $sort_data = request()->all();
        $sort_data['no_offers'] = isset($sort_data['no_offers']);

        if (isset($sort_data['subcategories'])) {
            $sort_data['subcategories'] = array_filter(explode(',', $sort_data['subcategories']));
            if (in_array('all', $sort_data['subcategories'])) {
                $sort_data['subcategories'] = [];
            }
        }

        return array_merge($old_sort_data, $sort_data);
    }

    public function sort(Request $request)
    {
        $sort_data = $this->getSortData();


        $tasks = UserTasks::getAll($sort_data);

        $tasksMapData = $this->getMapsData($tasks);

        $sortDataCache = $sort_data;
        unset($sortDataCache['map']);

        if (\Auth::check()) {
            \Cache::forever('user.' . user()->id . '.sort-data', $sortDataCache);
        } else {
            \Session::put('user.sort-data', $sortDataCache);
        }

        return view('tasks.list_blocks.all_tasks_items', compact('tasks', 'tasksMapData', 'sort_data'));
    }

    public function getMapsData($tastCollection)
    {
        $tasksMapData = [];
        /** @var UserTasks $task */
        foreach ($tastCollection as $task) {
            if (!isset($task->data['address'])) continue;

            $taskMap = [
                'title' => $task->name,
                'balloon' => [
                    'name' => $task->name,
                    'description' => $task->description,
                    'price' => $task->exact_cost
                ]
            ];

            if (isset($task->data['address'][0])) {
                $taskMap['coords'] = [
                    'lat' => $task->data['address'][0]['lat'],
                    'lng' => $task->data['address'][0]['lon']
                ];
            }

            switch ($task->getOriginal('status')) {
                case UserTasks::STATUS_OPENED:
                    if (isset($task->subcategory->category->marker['opened']['file'])) {
                        $taskMap['icon_src'] = $task->subcategory->category->marker['opened']['file'];
                    }
                    break;
                case UserTasks::STATUS_FINISHED:
                    if (isset($task->subcategory->category->marker['finished']['file'])) {
                        $taskMap['icon_src'] = $task->subcategory->category->marker['finished']['file'];
                    }
                    break;
                case UserTasks::STATUS_IN_PROGRESS:
                    if (isset($task->subcategory->category->marker['in_progress']['file'])) {
                        $taskMap['icon_src'] = $task->subcategory->category->marker['in_progress']['file'];
                    }
                    break;
            }

            $tasksMapData[$task->id] = $taskMap;
        }

        return $tasksMapData = json_encode($tasksMapData);
    }

    public function all(Request $request)
    {
        $sort_data = $this->getCachedSortData();
        $tasks = UserTasks::getAll($sort_data);

        /*$a = UserTasks::all()->last();
        $b = UserTasks::whereName("уборка квартиры")->first();

        $b = collect($b)->except(['subcategory', 'data']);
        $a = collect($a)->except(['subcategory', 'data'])->diff($b);

        dump($a);*/

        $categories = Category::getTree();

        $tasksMapData = $this->getMapsData($tasks);

        return view('tasks.all', compact('tasks', 'categories', 'sort_data', 'tasksMapData'));
    }

    public function getMyCreatorTasks()
    {
        $tasks = UserTasks::withMyCreatorTasks();
        return view('tasks.list_blocks._task_items', compact('tasks'))->with('type', 'creator');
    }

    public function getMyExecutorTasks()
    {
        $tasks = UserTasks::withMyExecutorTasks();
        return view('tasks.list_blocks._task_items', compact('tasks'))->with('type', 'executor');
    }

    private function getCachedSortData()
    {
        if (\Auth::check()) {
            return (array)(cache('user.' . user()->id . '.sort-data') ?? $this->getBaseSortData());
        }
        return (array)(\Session::get('user.sort-data') ?? $this->getBaseSortData());
    }
}