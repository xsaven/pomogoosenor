<?php

namespace App\Http\Controllers\Tasks;

use App\FAQ;
use App\ReportReasons;
use App\Subcategory;
use App\TaskOffers;
use App\TaskView;
use App\UserTaskReports;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserTasks;
use App\TaskComments;

class PreviewController extends Controller
{
    public function index(UserTasks $userTasks, Request $request)
    {
//        $cost_range = \App\PriceRangersMask::find($userTasks->subcategory->range_id);
//        $cost = explode(' ', $userTasks->getAttributes()['cost']);
//        dd($cost_range->toArray());
        $userTasks = UserTasks::where('id', $userTasks->id)->with(['creator', 'executor', 'offers' => function ($q) {
            $q->orderByDesc('created_at');
        }])->first();

        $faq = FAQ::where('show_on_menu', '=', true)->get();

        $reportReasons = ReportReasons::all();

        if (\Auth::check() && $userTasks->creator_id == user()->id) {
            return view('tasks.preview', ['task' => $userTasks, 'faq' => $faq, 'reportReasons' => $reportReasons]);
        }

        TaskView::addView($userTasks, $request->ip(), $request->userAgent());

        return view('tasks.public.preview', ['task' => $userTasks, 'faq' => $faq, 'reportReasons' => $reportReasons]);
    }


    public function getTaskReviews(UserTasks $task, Request $request)
    {
        $offers = $task->offers()->getSorted();
        return view('tasks.list_blocks._task_offers', compact('offers', 'task'));
    }

    public function comment(UserTasks $userTasks, Request $request)
    {
        $messages = [
            'message.required' => 'Сообщение не может быть пустым',
            'message.min' => 'Сообщение должно быть не меньше 10-ти символов!'
        ];
        $this->validate($request, [
            'message' => 'required|min:10'
        ], $messages);

        $result = TaskComments::create([
            'user_id' => user()->id,
            'task_id' => $userTasks->id,
            'answer_for_id' =>
                $request->answer &&
                is_numeric($request->answer) &&
                $userTasks->creator_id == user()->id &&
                TaskComments::find($request->answer)
                    ? $request->answer : null,
            'message' => $request->message
        ]);

        return redirect()->back()->with(['status' => 'success', 'message' => 'Ваше сообщение успешно опубликовано!']);
    }

    public function report(Request $request, $id)
    {
        $report = new UserTaskReports();
        $report->user_id = user()->id;
        $report->user_task_id = (int)$id;
        $report->reason = (int)$request->input('reason');
        $report->description = $request->input('description');

        $report->save();

        return back()->with('status', 'success');
    }
}
