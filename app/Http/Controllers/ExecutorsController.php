<?php

namespace App\Http\Controllers;

use App\Category;
use App\Notifications\JobOffered;
use App\Posts;
use App\Subcategory;
use App\TasksFilterCities;
use App\User;
use App\UserTasks;
use Cache;
use Illuminate\Http\Request;

class ExecutorsController extends Controller
{

    public function index()
    {
//        Cache::forget('user.' . user()->id . '.executors-sort-data');
        $sort_data = $this->getCachedSortData();
        $categories = Category::getTree();
        $executors = User::getExecutorsList($sort_data);

        return view('executors.index', compact('categories', 'executors','sort_data'));
    }

    public function sort()
    {
        $sort_data = $this->getSortData();
        if (\Auth::check()) {
            \Cache::forever('user.' . user()->id . '.executors-sort-data', $sort_data);
        } else {
            \Session::put('user.executors-sort-data', $sort_data);
        }
        $executors = User::getExecutorsList($sort_data);
        return view('executors.blocks.executors_list', compact('executors'));
    }

    public function offerJob(Request $request)
    {
//        dd($request->all());
        if ($request->has('executor_id')){
            $task = UserTasks::find($request->task_id);
            User::find($request->executor_id)->notify(new JobOffered($task));
        }
        return redirect()->back();
    }

    private function getCachedSortData()
    {
        if (\Auth::check()) {
            return (array)(cache('user.' . user()->id . '.executors-sort-data') ?? $this->getBaseSortData());
        }
        return (array)(\Session::get('user.executors-sort-data') ?? $this->getBaseSortData());
    }

    private function getBaseSortData()
    {
        return [
            'type' => 'rate',
            'online' => false,
            'category'=>Category::getTree()->first()->id,
            'subcategory' => Category::getTree()->first()->default_subcategory['id'],
            'city'=> TasksFilterCities::first()->id
        ];
    }

    private function getSortData()
    {
        $old_sort_data = $this->getCachedSortData();
        $sort_data = request()->all();
        $sort_data['online'] = isset($sort_data['online']);
        $sort_data = array_merge($old_sort_data, $sort_data);
        $sort_data['category'] = $sort_data['subcategory'] ? Subcategory::find($sort_data['subcategory'])->category_id : null;
        return $sort_data;
    }
}
