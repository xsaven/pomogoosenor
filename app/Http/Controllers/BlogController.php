<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;

class BlogController extends Controller
{
    public function show($slug)
    {
        $post = Posts::with('user.profile')->where('slug', $slug)->first();
        if (!$post) {
            \App::abort(404);
        }

        $related = false;
        if ($post->related) {
            $ids = explode(',', $post->related);
            $related = Posts::whereIn('id', $ids)->get();
        }

        return view('blog.single', compact('post', 'related'));
    }

    public function index()
    {
        $posts = Posts::orderBy('created_at', 'DESC')->paginate(10);
        return view('blog.index', compact('posts'));
    }

    public function findByName(Request $request)
    {
        if ($request->has('query')) {

            $users = Posts::where('title', 'LIKE', '%' . $request->input('query') . '%')->take(10)->get();
            if (!$users->isEmpty()) {
                $dataArray = [];
                foreach ($users as $user) {
                    $dataArray[] = array(
                        'id' => $user->id,
                        'name' => $user->title . ' #' . $user->id,
                    );
                }
                return response()->json($dataArray, 200);
            }

        }
    }
}
