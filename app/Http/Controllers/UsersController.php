<?php

namespace App\Http\Controllers;

use App\City;
use App\Profile;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Show a list of users
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $users = User::all();

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show a page of user creation
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $roles = Role::pluck('title', 'id');

        return view('admin.users.create', compact('roles'));
    }

    /**
     * Insert new user into the system
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);

        return redirect()->route('users.index')->withMessage(trans('quickadmin::admin.users-controller-successfully_created'));
    }

    /**
     * Show a user edit page
     *
     * @param $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        $roles = Role::pluck('title', 'id');

        $cityCollection = array_map(
            function ($item) {
                return $item['name'];
            }, City::all()->keyBy('id')->toArray()
        );

        $transactions = $user->received_transactions;

        return view('admin.users.edit', compact('user', 'roles', 'cityCollection', 'transactions'));
    }

    /**
     * Update our user information
     *
     * @param Request $request
     * @param         $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        try {
            \DB::beginTransaction();
            $user = User::findOrFail($id);
            $input = $request->all();
            $user->update($input);
            $user->role_id = $request->role_id;
            $user->save();

            $profile = Profile::where('user_id', '=', $user->id)->first();

            $profile->documents_moderated = $request->documents_moderated;
            $profile->interview_complete = $request->interview_complete;

            $data = $request->get('profile');

            $profile->documents_moderated = isset($data['documents_moderated']) ? true : false;
            $profile->interview_complete = isset($data['interview_complete']) ? true : false;
            $profile->moderated_worker = isset($data['moderated_worker']) ? true : false;
            $profile->is_worker = isset($data['is_worker']) ? true : false;
            $profile->update($data);
//dd($data);
            \DB::commit();
//            return redirect()->route('users.index')->withMessage(trans('quickadmin::admin.users-controller-successfully_updated'));
            return back();
        } catch (\Exception $exception) {
            \DB::rollBack();
//            return redirect()->route('users.index')->withMessage(trans('quickadmin::admin.users-controller-error'));
        }

    }

    /**
     * Destroy specific user
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        User::destroy($id);

        return redirect()->route('users.index')->withMessage(trans('quickadmin::admin.users-controller-successfully_deleted'));
    }

}