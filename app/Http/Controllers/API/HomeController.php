<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Http\Controllers\Controller;
use App\PageConfig;
use App\Posts;

class HomeController extends Controller
{
    public function index()
    {
        $catTree = Category::getTree();
        $lastBlog = Posts::orderby('created_at', 'DESC')->take(4)->get();

        return response()->json(['categories' => $catTree, 'posts' => $lastBlog]);
    }
}
