<?php

namespace App\Http\Controllers\API\Payment;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Omnipay\Omnipay;
use App\Payment;

class PaymentsController extends Controller
{

    public function bindCard(Request $request)
    {
        user()->bindCard($request->card);
        return response()->json(['success' => true]);
    }

    public function unbindCard(Request $request)
    {
        user()->unbindCard();
        return response()->json(['success' => true]);
    }

    public function pay(Request $request)
    {
        $this->validate($request, [
            'amount' => 'required|numeric'
        ]);

        $payment = new Payment();
        $payment->order = time();
        $payment->receiver_id = Auth::user()->id;
        $payment->amount = $request->amount;
        $payment->description = 'Пополнение счёта пользователем ' . Auth::user()->name;
        $payment->hash = [$payment->order, $payment->receiver_id, $payment->amount];
        $payment->save();

        return response()->json(['hash' => $payment->hash]);
    }

    public function in_request(Request $request)
    {
        $pay = Payment::where('order', $request->WMI_PAYMENT_NO)->first();
        if ($pay->status != 3) {
            $pay->status = $request->WMI_ORDER_STATE == 'Accepted' ? 2 : -1;
            $pay->save();
        }
        if ($pay->status == 2) {
            User::find($pay->receiver_id)->checkPayment();
        }
        //$this->in($pay->hash, $request);
    }

    public function redirect(Request $request)
    {
        $hash = $request->hash;
        if (!$payment = Payment::where('hash', $hash)->where('receiver_id', \user()->id)->first()) {
            fail_message('Incorrect hash');
        }

        $gateway = Omnipay::create('WalletOne');
        $gateway->setMerchantId(config('payment.MerchantId'));
        $gateway->setSecretKey(config('payment.SecretKey'));

        $settings = [
            'amount' => $payment->amount,
            'currency' => 'UAH',
            'transactionId' => $payment->order,
            'description' => $payment->description,
            'returnUrl' => route('payment.test', ['hash' => $hash, 'status' => 'ok']),
            'cancelUrl' => route('payment.test', ['hash' => $hash, 'status' => 'ok']),
            'expirationDate' => new \DateTime(date('Y-m-d H:i:s', time() + 86400), new \DateTimeZone(config('app.timezone')))
        ];

        if ($payment->status == 1) {
            $response = $gateway->completePurchase()->send();
            $payment->payment_response = $response->getData();
            $payment->save();
        } else {
            $response = $gateway->purchase($settings)->send();
        }

        if ($response->isSuccessful()) {
            $payment->status = 2;
            $payment->save();
            \Auth::user()->checkPayment();
            die('WMI_RESULT=OK');
        } elseif ($response->isRedirect()) {
            $payment->status = 1;
            $payment->save();
            $response->redirect();
        } else {
            $payment->status = -1;
            $payment->error_message = $response->getMessage();
            $payment->save();
            fail_message($response->getMessage());
        }
    }

    public function test(Request $request)
    {
        return redirect()->route('profile_main');
    }

}