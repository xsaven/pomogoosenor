<?php
/**
 * Created by PhpStorm.
 * User: Uer
 * Date: 26.03.2018
 * Time: 16:30
 */

namespace App\Http\Controllers\API\Tasks;


use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Tasks\StoreOfferRequest;
use App\TaskComments;
use App\TaskOffers;
use App\UserTasks;
use Illuminate\Http\Request;

class CommentsController extends Controller
{

    public function store($task_id, Request $request)
    {
        $task = UserTasks::find($task_id);
        //&& TaskComments::store($task, $request->all())
        if ($task ) {
            $comments = $task->comments()->getSortedList();
            return response()->json($comments);
        }
        fail_message('Невозможно создать комментарий');
    }

    /**
     *
     * @param UserTasks $task
     * @param TaskComments $comment
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(UserTasks $task, TaskComments $comment)
    {
        if ($comment->answer){
            TaskComments::destroy($comment->answer->id);
        }
        TaskComments::destroy($comment->id);
        $comments = $task->comments()->getSortedList();
        return response()->json($comments);
    }

}