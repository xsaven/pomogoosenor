<?php

namespace App\Http\Controllers\API\Tasks;

use App\Http\Controllers\Controller;
use App\UserTaskReports;
use App\Http\Requests\CreateTaskReportsRequest;
use App\Http\Requests\UpdateTaskReportsRequest;
use App\UserTasks;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\User;


class ReportsController extends Controller
{
    /**
     * Store a newly created taskreports in storage.
     *
     * @param UserTasks $task
     * @param Request $request
     * @return JsonResponse
     */
    public function store(UserTasks $task, Request $request)
    {
        $request->request->set('user_id', \user()->id);
        $task->reports()->create($request->all());
        return response()->json(['success' => true]);
    }
}
