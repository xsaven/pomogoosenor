<?php
/**
 * Created by PhpStorm.
 * User: Uer
 * Date: 26.03.2018
 * Time: 16:30
 */

namespace App\Http\Controllers\API\Tasks;


use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Tasks\StoreOfferRequest;
use App\TaskOffers;
use App\UserTasks;

class OffersController extends Controller
{

    public function store($task_id, StoreOfferRequest $request)
    {
        $task = UserTasks::find($task_id);
        if ($task && TaskOffers::store($task, $request->all())) {
            return response()->json(['success' => true]);
        }
        fail_message('Невозможно создать предложение');
    }

}