<?php

namespace App\Http\Controllers\API\Tasks;

use App\Category;
use App\Decorators\Tasks\UserTasksDecorator;
use App\Http\Requests\Api\Tasks\StoreTaskRequest;
use App\Http\Requests\Api\Tasks\UpdateTaskRequest;
use App\Http\Resources\APICollection;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Notifications\SubscribedCategoryTaskStored;
use App\User;
use App\UserTasks;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class TasksController extends Controller
{

    /**
     * Create task
     *
     * @param StoreTaskRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreTaskRequest $request)
    {

        $task_data = $request->all();

        $task_data['status'] = UserTasks::STATUS_MODERATE;

        unset($task_data['photo']);

        $task = UserTasks::store($task_data);

        foreach ($this->storeAllFiles('uploads/tasks') as $file_data) {
            $task->content_items()->create($file_data);
        }
        $task = UserTasksDecorator::find($task->id)->loadRelatedData();

        return response()->json(['success' => true, 'task' => $task]);

    }

    /**
     * Create draft
     *
     * @param StoreTaskRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeDraft(StoreTaskRequest $request)
    {
        $request->request->set('status', 0);
        $this->store($request);
        return response()->json(['success' => true]);
    }

    /**
     * Check if user have permissions to edit task
     *
     * @param $task_id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     */
    private function checkIfCanEdit($task_id)
    {
        $task = UserTasks::where('id', $task_id)
            ->where('creator_id', user()->id)->first();
        if (!$task) {
            error('Вы не можете редактировать это задание');
        }
        return $task;
    }

    /**
     * Update task
     *
     * @param $task_id
     * @param UpdateTaskRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($task_id, UpdateTaskRequest $request)
    {
        /** @var UserTasks $task */
        $task = $this->checkIfCanEdit($task_id);
        $task_data = $request->all();
        $task->content_items()->detach();
        foreach ($this->storeAllFiles('uploads/tasks') as $file_data) {
            $task->content_items()->create($file_data);
        }
        $task->edit($task_data);
        $task = UserTasksDecorator::find($task->id)->loadRelatedData();
        return response()->json(['success' => true, 'task' => $task]);
    }

    /**
     * Get list of drafts
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDrafts()
    {
        $drafts = UserTasksDecorator::getMyDrafts();
        return response()->json($drafts);
    }

    /**
     * Get list of "My tasks"
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function myTasks(Request $request)
    {
        if ($request->user_type == UserTasks::USER_TYPE_CREATOR) {
            $my_tasks = UserTasksDecorator::getMyCreatorTasks($request->status, $request->user_type, $request->category);
        } else {
            $my_tasks = UserTasksDecorator::getMyExecutorTasks($request->status, $request->category);
        }
        return response()->json($my_tasks);
    }

    /**
     * Show task
     *
     * @param UserTasksDecorator $task
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(UserTasksDecorator $task, Request $request)
    {
        $task = $task->loadRelatedData();
        return response()->json($task);
    }

    /**
     * Get tasks from "All tasks screen"
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function all(Request $request)
    {
        $tasks = UserTasksDecorator::getAllTasks($request->status, $request->category, $request->city_id, $request->name);

        return response()->json($tasks);
    }

    public function cancel(UserTasks $task, Request $request)
    {
        if ($task->status == 'opened') {
            $request->request->set('user_id', user()->id);
            $task->status_task()->create($request->all());
            $task->status = UserTasks::STATUS_CANCELED;
            $task->save();
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false]);
    }

    /**
     * Get users's tasks statistic
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStatistic()
    {
        $my_tasks = UserTasksDecorator::getTasksStatistic();
        return response()->json($my_tasks);
    }

    /**
     * Get list of tasks history and popular tasks
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSearchData()
    {
        $history = UserTasksDecorator::getHistory();
        $popular = UserTasksDecorator::getPopular();
        return response()->json(compact('history', 'popular'));
    }

    /**
     * Get tasks list by suggestion query
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchSuggest(Request $request)
    {
        return \user()->getCategoriesRate();
        $searchResult = UserTasksDecorator::searchSuggest($request->get('query'));
        return response()->json($searchResult);
    }

    /**
     * Select task executor
     *
     * @param UserTasks $task
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function selectExecutor(UserTasks $task, Request $request)
    {

        $task->setExecutor($request->user_id);
        return response()->json(['success' => true]);
    }
}
