<?php
/**
 * Created by PhpStorm.
 * User: Uer
 * Date: 26.03.2018
 * Time: 16:30
 */

namespace App\Http\Controllers\API\Tasks;


use App\Decorators\Tasks\ReviewDecorator;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Tasks\StoreReviewRequest;
use App\Http\Resources\APICollection;
use App\Http\Resources\Profile\UserReviewsResource;
use App\Review;
use App\User;
use App\UserTasks;
use Illuminate\Http\Request;

class ReviewsController extends Controller
{

    public function store($task_id, StoreReviewRequest $request)
    {
        $task = UserTasks::find($task_id);
        if ($task && Review::store($task, $request->all())) {
            return response()->json(['success' => true]);
        }
        error('Невозможно создать отзыв');
    }

    public function update($task_id, $review_id, StoreReviewRequest $request)
    {
        $task = UserTasks::find($task_id);
        $review = Review::find($review_id);
        if ($task && $review && $review->edit($task, $request->all())) {
            return response()->json(['success' => true]);
        }
        error('Невозможно редактировать отзыв');
    }

    public function index(User $user, Request $request)
    {
        $reviews = Review::getListForUser($user->id, $request->positive);
        return new APICollection($reviews, UserReviewsResource::class);
    }

}