<?php

namespace App\Http\Controllers\API;

use App\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LocationController extends Controller
{
    public function findCity(Request $request)
    {
        $data = [];
        if ($request->has('query')) {
            $city_name = $request->get('query');
            $data = City::where('name', 'LIKE', "%$city_name%")
                ->take(10)
                ->select('id','name')
                ->get();
        }
        return response()->json($data, 200);
    }

    public function getAllCities()
    {
        $data = City::select('id','name')->get();
        return response()->json($data, 200);
    }
}
