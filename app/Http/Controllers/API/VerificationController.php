<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Http\Controllers\Controller;
use App\Notifications\VerifyPhone;

use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use NotificationChannels\SmscRu\Exceptions\CouldNotSendNotification;

class VerificationController extends Controller
{

    private static function getUID()
    {
        return md5(user()->id);
    }

    private function getPhoneVerificationCache()
    {
        return Cache::get("user." . self::getUID() . ".verification.phone");
    }

    /**
     * @return array
     */
    private function getVerificationData()
    {
        return [
            'code' => rand(10000, 99999),
            'phone' => '+' . preg_replace('/\D/', '', \request('phone')),
            'attempts' => 5
        ];
    }

    private function cacheVerificationData()
    {
        $verification_data = $this->getVerificationData();
        Cache::put("user." . self::getUID() . ".verification.phone", $verification_data, 3600);

        return $verification_data;
    }

    public function sendPhoneVerification()
    {
        try {
            $verification_data = $this->cacheVerificationData();
            user()->profile->phone = $verification_data['phone'];
//            if ($verification_data['phone'] == "+74532453245") {
//                user()->notify(new VerifyPhone(1234));
//            } else

            user()->notify(new VerifyPhone($verification_data['code']));

        } catch (CouldNotSendNotification $exception) {
            error('На этот номер невозможно отправить SMS. Попробуйте еще раз через 30 минут')
                ->with(['exception' => $exception->getMessage()]);
        }
        return response()->json(['success' => true, 'code' => $verification_data['code']]);
    }

    /**
     * @return mixed
     */
    private function checkAttemptsCount()
    {
        $verification_data = $this->getPhoneVerificationCache();
        if ($verification_data && $verification_data['attempts'] > 1) {
            $verification_data['attempts'] -= 1;
            Cache::put("user." . self::getUID() . ".verification.phone", $verification_data, 60);
            return $verification_data;
        }
        error('Превышен лимит попыток ввода кода. Подтвердите ваш телефон снова');
    }

    public function checkPhoneVerification(Request $request)
    {
        $verification_data = $this->checkAttemptsCount();
        if ($verification_data['code'] == $request->code) {
            user()->profile->setPhoneVerified(true);
            return response()->json(['success' => true]);
        }
        error('Неверный код подтверждения. Осталось попыток: ' . $verification_data['attempts']);
    }
}
