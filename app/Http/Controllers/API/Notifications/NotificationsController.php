<?php
/**
 * Created by PhpStorm.
 * User: Uer
 * Date: 16.03.2018
 * Time: 13:22
 */

namespace App\Http\Controllers\API\Notifications;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class NotificationsController extends Controller
{
    public function setOneSignalUser(Request $request)
    {
        if ($request->has('onesignal_id')){
            user()->setOnesignalId($request->onesignal_id);
        }
    }
}