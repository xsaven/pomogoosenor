<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\SubscribeEmailRequest;
use App\Subscribe;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use ZfrMailChimp\Client\MailChimpClient as Client;

/**
 * Class SubscribeController
 *
 *
 * @package App\Http\Controllers\API
 * @mixin User
 */
class SubscribeController extends Controller
{
    private $client;

    public function __construct() {
        $this->client = new Client(get_setting('mailchimp_key'));
    }

    private function getMailChimpData($email)
    {
        return [
            'id' => get_setting('mailchimp_list_id'),
            'email' => [
                'email' => $email,
                'euid'  => md5(strtolower($email))
            ]
        ];
    }

    public function mailChimpSubscribtion($email, $subscribed)
    {
        try {
            if ($subscribed) {
                return $this->client->subscribe($this->getMailChimpData($email));
            }
            return $this->client->unsubscribe($this->getMailChimpData($email));
        } catch (\Exception $e) {
            abort(422,'Данный почтовый адрес '.$email.' не может быть использован');
        }
    }
    /**
     * Subscribe to the mailing list
     *
     * @param SubscribeEmailRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function subscribeEmail(SubscribeEmailRequest $request) {
        $this->mailChimpSubscribtion($request->email, true);
        return response()->json(['status'=>'success'],200);
    }

    public function subscribe(Request $request)
    {
        if ($request->has('news') && ((bool)$request->news != Auth::user()->subscribe->news) ){
            $this->mailChimpSubscribtion(Auth::user()->email, $request->news);
        }
        $subscribe_data = getRequestFillable(Subscribe::class);
        $subscription = \user()->subscribe();
        if ($subscription->first()){
            $subscription->update($subscribe_data);
        }else{
            $subscription->create($subscribe_data);
        }
        return $subscription->first();
    }


    public function subscribeCategory($category, Request $request)
    {
        user()->categories()->syncWithoutDetaching($category);
        return response()->json(['success'=>true]);
    }

    public function unsubscribeCategory($category, Request $request)
    {
        user()->categories()->detach($category);
        return response()->json(['success'=>true]);
    }
}
