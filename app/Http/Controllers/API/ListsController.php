<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\CreditCard;
use App\Http\Controllers\Controller;
use App\Http\Resources\APICollection;
use App\Http\Resources\Lists\FlatCategoriesResource;
use App\Services\WalletOneSafeDeal\WOSafeDeal;
use App\User;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\Request;

class ListsController extends Controller
{
    public function getCategories()
    {
        $cat_tree = Category::getFormattedTree();
        $new_events = user()->getNewEvents();
        return response()->json(['categories' => $cat_tree, 'new_events' => $new_events]);
    }

    public function getFlatCategories()
    {
        $categories = Category::all();
        return new APICollection($categories, FlatCategoriesResource::class);
    }

    private function saveUserCards(User $user, $e_cards, $c_cards)
    {
        $executor_cards = [];
        $creator_cards = [];
        foreach ($e_cards as $card) {
            $executor_cards[] = [
                'id' => $card['PaymentToolId'],
                'user_id' => $user->id,
                'type' => CreditCard::BENEFICIARY_TYPE,
                'mask' => $card['Mask']
            ];
        }
        foreach ($c_cards as $card) {
            $creator_cards[] = [
                'id' => $card['PaymentToolId'],
                'user_id' => $user->id,
                'type' => CreditCard::PAYER_TYPE,
                'mask' => $card['Mask']
            ];
        }
        CreditCard::insert(array_merge($creator_cards, $executor_cards));
        return compact('executor_cards', 'creator_cards');
    }

    private function updateUserCards(User $user)
    {
        $user->credit_cards()->delete();
        $executor_cards = WOSafeDeal::beneficiary()->getBeneficiaryPaymentTools($user->id)['PaymentTools'];
        $creator_cards = WOSafeDeal::payer()->getPayerPaymentTools($user->id)['PaymentTools'];
        return $this->saveUserCards($user, $executor_cards, $creator_cards);
    }

    /**
     * Get WalletOne cards
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getWOCards(Request $request)
    {
        $cards = $this->updateUserCards(\user());
        foreach ($cards as &$card_type){
            foreach ($card_type as &$card){
                unset($card['type']);
                unset($card['user_id']);
            }
        }
        if (isset($request->type) && in_array($request->type,['executor','creator'])){
            $cards = $cards[$request->type . '_cards'];
        }
        return response()->json($cards);
    }
}
