<?php

namespace App\Http\Controllers\API;

use App\Content;
use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Galleries;
use App\Http\Requests\CreateGalleriesRequest;
use App\Http\Requests\UpdateGalleriesRequest;
use Illuminate\Http\Request;

use App\User;


class GalleryController extends Controller
{

    /**
     * Display a listing of galleries
     *
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $galleries = Galleries::whereUserId($request->user_id)
            ->with('content_items')->get();
        return response()->json($galleries);
    }

    public function getExamples(Request $request)
    {
        $examples = Content::getUserGalleries($request->user_id)->pluck('path');
        return response()->json(compact('examples'));
    }

    /**
     * Store a newly created galleries in storage.
     *
     * @param CreateGalleriesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateGalleriesRequest $request)
    {
        $gallery = user()->galleries()->create($request->all());
        foreach ($this->storeAllFiles('uploads/gallery') as $file_data) {
            $gallery->content_items()->create($file_data);
        }
        return response()->json($gallery->load('content_items'));
    }

    /**
     * Update the specified galleries in storage
     *
     * @param Galleries $gallery
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Galleries $gallery, Request $request)
    {
        $gallery->update($request->only('title', 'description'));
        foreach ($this->storeAllFiles('uploads/gallery') as $file_data) {
            $gallery->content_items()->create($file_data);
        }
        return response()->json($gallery->load('content_items'));
    }

    /**
     * Remove the specified galleries from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        Galleries::destroy($id);
        return response()->json(['success' => true]);
    }


}
