<?php

namespace App\Http\Controllers\API;

use App\Content;
use App\Http\Controllers\Controller;
use App\TransactionsLog;
use Illuminate\Database\Eloquent\Builder;
use Redirect;
use Schema;
use App\Galleries;
use App\Http\Requests\CreateGalleriesRequest;
use App\Http\Requests\UpdateGalleriesRequest;
use Illuminate\Http\Request;

use App\User;


class TransactionsController extends Controller
{

    /**
     * Display a listing of galleries
     *
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        /** @var TransactionsLog $transactions */
        $transactions = \user()
//            ->withoutAppends()
            ->transactions()
            ->filterByType($request->type)
            ->filterByDate($request->from, $request->to)
            ->with(['sender', 'recipient'])
            ->get()
            ->toArray();
        return response()->json(compact('transactions'));
    }

}
