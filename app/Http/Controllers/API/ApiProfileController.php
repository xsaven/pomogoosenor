<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Profile\ChangeAuthCredentialsRequest;
use App\Http\Requests\Api\Profile\FillInfoRequest;
use App\Http\Requests\Api\Profile\LoadAvatarRequest;
use App\Http\Requests\Api\Profile\SaveConfidantRequest;
use App\Http\Requests\Api\Profile\UpdateAboutMeRequest;
use App\Http\Requests\Api\Profile\UpdateProfileRequest;
use App\Profile;
use App\User;
use App\WorkerProfile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Exception\NotReadableException;
use JWTAuth;
use Image;
use phpDocumentor\Reflection\Types\Integer;

class ApiProfileController extends Controller
{
    private $avatarsFolder = '/uploads/users/';


    public function index(Request $request)
    {
        if ($request->is_worker == 'true') {
            return \Auth::user()->getUserWithProfile();
        }
        return \Auth::user()->getUserWithClientProfile();
    }

    public function show(Request $request, $userId)
    {
        if(!$user = (new User())->find($userId)) {
            return response()->json(['error' => ['message' => 'Record not found']], 404);
        }

        $user_profile = $user->getUserWithProfile();
        return response()->json($user_profile);
    }

    public function getMenuInfo()
    {
        if (\request()->has('onesignal_id')){
            user()->setOnesignalId(request('onesignal_id'));
        }
        return response()->json(user()->getMenuData());
    }

    public function apiLoadAvatar(LoadAvatarRequest $request)
    {
        $profile = auth()->user()->profile;
        $avatar = $this->loadAvatar($request->file('avatar'));

        $profile->avatar_full = $avatar['full'];
        $profile->avatar_thumb = $avatar['thumb'];
        $profile->save();
        return auth()->user()->getUserWithProfile();
    }


    public function loadAvatar($file, $base64 = false)
    {
        if ($base64) {
            $ext = 'jpg';
        } else {
            $ext = $file->getClientOriginalExtension();
        }

        $name = 'user_' . auth()->id();
        $nameWithPathFull = $this->avatarsFolder . $name . '.' . $ext;
        $nameWithPathThumb = $this->avatarsFolder . $name . '_thumb.' . $ext;
        try {
            $fullImage = Image::make($file);
            $thumbImage = Image::make($file);
        }catch (NotReadableException $exception){
            abort(400,'Невозможно загрузить файл. Возможно, он был поврежден');
        }
        $fullImage->save(public_path($nameWithPathFull));
        $thumbImage->fit(300)->save(public_path($nameWithPathThumb));

        return array(
            'full' => $nameWithPathFull,
            'thumb' => $nameWithPathThumb
        );
    }

    // user()->categories()->sync($request->selected_cat);
    public function postFillInfo(FillInfoRequest $request)
    {
        $profile = user()->load_profile();
        $profile->profileConfidants()->updateOrCreate($request->confidant);
        $profile->update($request->only('name', 'surname', 'patronymic', 'birthday','phone_code','phone'));
        user()->update($request->only('email'));
        if ($request->hasFile('avatar')){
            $this->apiLoadAvatar(app()->make(LoadAvatarRequest::class));
        }

        $profile->save();
        $profile->switchTestingStep(1);

        return response()->json(['success' => true]);
    }

    protected function create_profile_validator($data)
    {
        return Validator::make($data, [
            'name' => 'required|min:6|max:25',
            'tel' => 'required|numeric',
            'city_id' => 'required|exists:city,id',
            'avatar' => 'image|dimensions:min_width=200,min_height=200',
        ]);
    }


    public function postCreateProfile(Request $request)
    {
        $validator = $this->create_profile_validator($request->all());
        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => $validator->errors()], 422);
        }
        $user = JWTAuth::parseToken()->authenticate();
        $user->name = $request->input('name');
        $user->save();

        $profile = Profile::firstOrCreate([
            'user_id' => $user->id
        ]);

        if ($request->hasFile('avatar')) {
            $avatar_data = $this->loadAvatar($request->file('avatar'));
            $profile->avatar_full = $avatar_data['full'];
            $profile->avatar_thumb = $avatar_data['thumb'];
        }
        $profile->phone = $request->input('tel');
        $profile->city_id = $request->input('city_id');
        $profile->save();
        return response()->json(['success' => true], 200);
    }

    /**
     * Change User's 'email' and 'password'
     *
     * @param ChangeAuthCredentialsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeAuthCredentials(ChangeAuthCredentialsRequest $request)
    {
        \Auth::user()->update(array_filter($request->only(['password', 'email'])));
        return response()->json(\user()->getUserWithProfile(), 200);
    }

    public function postPrevalidate(Request $request)
    {
        $validator = $this->create_profile_validator($request->all());
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 421);
        } else {
            return response()->json([], 200);
        }
    }

    public function update(UpdateProfileRequest $request)
    {
        $date = Carbon::parse($request->birthday);
        $request->request->add([
            'birthday' => $date->toDateString()
        ]);
        $user = auth()->user();
        $user->update($request->only('name'));
        $profile = $user->profiles();
        $profile->update($request->only('name','surname', 'sex', 'city_id', 'birthday'));
        return response()->json($user->getUserWithProfile(), 200);
    }

    public function updateAboutMe(UpdateAboutMeRequest $request)
    {
        /** @var WorkerProfile $user */
        $worker_profile = \Auth::user()->profile->workerProfiles();
        $worker_profile->update($request->only( 'about_me'));
        return response()->json(\Auth::user()->getUserWithProfile(), 200);
    }
}
