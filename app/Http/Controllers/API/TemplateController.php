<?php

namespace App\Http\Controllers\API;

use App\Content;
use App\Http\Controllers\Controller;
use App\Template;
use Redirect;
use Schema;
use App\Galleries;
use App\Http\Requests\CreateGalleriesRequest;
use App\Http\Requests\UpdateGalleriesRequest;
use Illuminate\Http\Request;

use App\User;


class TemplateController extends Controller {

	/**
	 * Display a listing of templates
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $templates = user()->templates()->get();
        return response()->json(compact('templates'));
	}

    /**
     * Store a newly created templates in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
	{
        $template = user()->templates()->create($request->only(['title', 'text']));
        return response()->json(['success'=>true]);
	}

    /**
     * Update the specified templates in storage
     *
     * @param Template $template
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Template $template, Request $request)
	{
        $template->update($request->only('title','text'));
        return response()->json(['success'=>true]);
    }

    /**
     * Remove the specified templates from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
	{
        Template::destroy($id);
        return response()->json(['success'=>true]);
	}

}
