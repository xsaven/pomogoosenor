<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;

class FavoritesController extends Controller
{
    public function followers()
    {
        return user()->followers()->with('profile')->get();
    }

    public function userFollowers($userId)
    {
        if(!$user = (new User())->find($userId)) {
            return response()->json(['error' => ['message' => 'Record not found']], 404);
        }

        return $user->followers()->with('profile')->get();
    }

    public function store($userId)
    {
        if(!$user = (new User())->find($userId)) {
            return response()->json(['error' => ['message' => 'Record not found']], 404);
        }

        $user->followers()->syncWithoutDetaching(user()->id);
        return response()->json(['success' => true,'status'=>'followed']);
    }

    public function remove($userId)
    {
        if(!$user = (new User())->find($userId)) {
            return response()->json(['error' => ['message' => 'Record not found']], 404);
        }


        $user->followers()->detach(user()->id);
        return response()->json(['success' => true,'status'=>'unfollowed']);
    }
}
