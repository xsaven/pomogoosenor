<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pattern;

class TestingController extends Controller
{

    public function submitStep(Request $request)
    {
        if(user() && user()->profile) {
            user()->profile->makeWorker();
            return response()->json(['success' => true]);
        } else {
            return fail_message('Invalid user');
        }
    }

    public function getAllSteps()
    {
        $patterns = Pattern::loadQuestions();
        return response()->json([
            'steps' => $patterns,
        ]);
    }
}
