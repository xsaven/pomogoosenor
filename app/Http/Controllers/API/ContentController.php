<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;
use Redirect;
use Schema;
use App\Content;
use App\Http\Requests\CreateContentRequest;
use App\Http\Requests\UpdateContentRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\FileUploadTrait;


class ContentController extends Controller {

    /**
     * Store a newly created content in storage.
     *
     * @param CreateContentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
	{

        $request = $this->saveFiles($request);
		$content = Content::create($request->all());

		return response()->json(['success'=>true]);
	}

	/**
	 * Show the form for editing the specified content.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$content = Content::find($id);
	    
	    
        $type = Content::$type;

		return view('admin.content.edit', compact('content', "type"));
	}

	/**
	 * Update the specified content in storage.
     * @param UpdateContentRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateContentRequest $request)
	{
		$content = Content::findOrFail($id);

        $request = $this->saveFiles($request);

		$content->update($request->all());

		return redirect()->route(config('quickadmin.route').'.content.index');
	}

	/**
	 * Remove the specified content from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Content::destroy($id);

		return redirect()->route(config('quickadmin.route').'.content.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Content::destroy($toDelete);
        } else {
            Content::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.content.index');
    }

}
