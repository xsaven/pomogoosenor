<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\RegisterRequest;
use App\Mail\Verification;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;

class ApiRegisterController extends Controller
{

    private $pass;

    /**
     * API Register
     *
     * @param RegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        $this->pass = $this->generatePassword(8);
        event(new Registered($user = User::create([
            'email' => $request['email'],
            'password' => bcrypt($this->pass),
            'name' => $request['name'],
            'verification_token' => Str::random(60)
        ])));

        $user->profiles()->create([
            'name' => $request['name'],
        ]);


        $user->createEmptyUserRelated();
        $this->sendUserVerificationEmail($user);
        $token = JWTAuth::fromUser($user);
        $user = collect($user)->merge(['password' => $this->pass]);

        return \response()->json(['success' => true, 'user' => $user, 'token' => $token]);
    }

    private function sendUserVerificationEmail($user)
    {
        Mail::send(new Verification($user->email, $this->pass,
            route('email_verified', [
                'token' => $user->verification_token,
                'email' => $user->email
            ])
        ));
    }

    protected function generatePassword($lengh = 7)
    {
        return str_random($lengh);
    }
}