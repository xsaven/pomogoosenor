<?php

namespace App\Http\Controllers\API\Auth;

use App\Exceptions\Social\SocialTokenInvalidException;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Http\Requests\Api\Auth\RegisterSocialRequest;
use App\Mail\Verification;
use App\User;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Image;

class ApiLoginController extends Controller
{

    use AuthenticatesUsers;

    /**
     * @var string
     */
    private $save_path = '/uploads/users/';

    private $provider;

    private $pass;

    public function login(LoginRequest $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => ['message' => 'Недействительные учетные данные. Пожалуйста, убедитесь, что вы указали правильную информацию, и вы подтвердили свой адрес электронной почты.']], 401);
            } else {
                return $this->sendLoginResponse($request, $token);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => ['message' => 'could_not_create_token']], 500);
        }
    }

    public function refresh(Request $request)
    {
        if (!$request->token) {
            return fail_message('invalid token');
        }

        return response()->json([
            'success' => true,
            'token' => JWTAuth::refresh($request->token)
        ]);
    }


    protected function sendLoginResponse(Request $request, $token)
    {
        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user()->getAuthIdentifier(), $token);
    }

    protected function authenticated(Request $request, $user_id, $token)
    {
        return response()->json([
            'token' => $token,
            'user' => User::with('profile')->find($user_id)
        ]);
    }


    protected function sendFailedLoginResponse(Request $request)
    {
        return response()->json([
            'message' => Lang::get('auth.failed'),
        ], 401);
    }

    /**
     * Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to relogin to get a new token
     *
     * @param Request $request
     */
    public function logout(Request $request)
    {
        $this->validate($request, ['token' => 'required']);
        try {
            JWTAuth::invalidate($request->input('token'));
            return response()->json(['success' => true]);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
        }
    }

    /**
     * Clear driver service params to use it for API
     *
     * @param $provider
     */
    private function prepareApiDriver($provider)
    {
        $driver_conf = app('config')['services.' . $provider];
        abort_unless($driver_conf, 400, 'Unavailable social service');
        $prepared = array_fill_keys(array_keys($driver_conf), null);
        app('config')['services.' . $provider] = $prepared;
    }

    /**
     * Get Socialite user by driver name
     *
     * @return \Laravel\Socialite\Two\User|null
     * @throws SocialTokenInvalidException
     */
    public function getUserBySocialDriver()
    {
        $fields = app('config')['services.' . $this->provider . '.fields'];

        try {
            $this->prepareApiDriver($this->provider);
            return Socialite::driver($this->provider)->fields($fields)
                ->userFromToken(request('token'));
        } catch (RequestException $e) {
            throw new SocialTokenInvalidException();
        }
    }

    /**
     * Upload image
     *
     * @param User $user
     * @param string $url Image URL
     * @param string $suffix Suffix (for example '_thumb')
     * @return string Path to image
     */
    private function saveUserImage($user, $url, $suffix = '')
    {
        /** @var array $sslOptions SSL params */
        $sslOptions = ["ssl" => ["verify_peer" => false, "verify_peer_name" => false]];
        $image_path = $this->save_path . 'user_' . $user->id . $suffix . '.jpg';
        $image = @file_get_contents($url, false, stream_context_create($sslOptions));
        Image::make($image)->save(public_path($image_path));
        return $image_path;
    }

    private function retrieveFacebookProfile($data)
    {
        return [
            'sex' => $data->user['gender'] == 'male' ? 0 : 1,
            'name' => explode(" ", $data->name)[0],
            'surname' => explode(" ", $data->name)[1],
            'avatar' => $data->avatar,
            'avatar_full' => $data->avatar_original,
        ];
    }

    private function retrieveVkontakteProfile($data)
    {
        return [
            'sex' => $data->user['sex'] == 2 ? 0 : 1,
            'name' => $data->user['first_name'],
            'surname' => $data->user['last_name'],
            'avatar' => $data->user['photo_100'],
            'avatar_full' => $data->user['photo_max'],
        ];
    }

    /**
     * Format user profile data from Socialite
     *
     * @param string $provider
     * @param mixed $data
     * @return mixed
     */
    private function retrieveSocialProfile($provider, $data)
    {
        return $this->{'retrieve' . lcfirst($provider) . 'Profile'}($data);;
    }

    private function prepareProfileData($social_profile)
    {
        $basic_attributes = ['sex', 'name', 'surname'];
        return array_intersect_key($social_profile, array_flip($basic_attributes));
    }

    private function createUserProfile($user, $social_profile)
    {
        $social_data = $this->prepareProfileData($social_profile);
        $social_data['avatar_thumb'] = $this->saveUserImage($user, $social_profile['avatar'], '_thumb');
        $social_data['avatar_full'] = $this->saveUserImage($user, $social_profile['avatar_full']);
        $user->profiles()->firstOrCreate($social_data);
    }

    private function sendUserVerificationEmail($user)
    {
        Mail::send(new Verification($user->email, $this->pass,
            route('email_verified', [
                'token' => $user->verification_token,
                'email' => $user->email
            ])
        ));
    }

    private function createUserFromSocial($email, $social_user)
    {
        $this->generatePassword(8);
        $user = User::create([
            'email' => $email,
            'name' => $social_user->name,
            'password' => bcrypt($this->pass),
            'verification_token' => Str::random(60)
        ]);
        $this->sendUserVerificationEmail($user);
        $social_profile = $this->retrieveSocialProfile($this->provider, $social_user);
        $this->createUserProfile($user, $social_profile);
        $user->createEmptyUserRelated();
        return User::with('profile')->find($user->id);
    }

    /**
     * Accept access_token, email
     *
     * @param RegisterSocialRequest $request
     * @param  string $provider
     * @return \Illuminate\Http\JsonResponse
     * @throws SocialTokenInvalidException
     */
    public function authSocial(RegisterSocialRequest $request, $provider)
    {

        $this->provider = $provider;
        $social_user = $this->getUserBySocialDriver();

        $user = User::findByEmail($request->email);
        if (!$user) {

            $user = $this->createUserFromSocial($request->email, $social_user);
        }


        $this->attachSocial($user, $provider !== 'vkontakte' ? $social_user->id : $social_user['id']);
        $token = JWTAuth::fromUser($user);
        $user = collect($user)->merge(['password' => $this->pass]);
        return \response()->json(['user' => $user, 'token' => $token]);
    }

    private function attachSocial($user, $social_id)
    {
        $user->socialAccounts()->firstOrCreate([
            'social_type' => $this->provider,
            'social_id' => $social_id
        ]);
    }

    private function generatePassword($lengh)
    {
        $this->pass = str_random($lengh);
    }

    /*
     * OLD CODE
     */

    public function redirectToFacebookProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleFacebookProviderCallback()
    {
        $facebook_user = Socialite::driver('facebook')->user();

        $user = \App\User::where('email', $facebook_user->user['email'])->first();
        if (!$user) {
            $user = new \App\User;
            $user->email = $facebook_user->user['email'];
            $user->verified = 1;
            $user->password = null;
            $user->save();
        }
        $this->attachSocial($user->id, 'facebook', $facebook_user->id);

        $token = JWTAuth::fromUser($user);

        return response()->json(['success' => true, 'token' => $token]);

    }

    public function redirectToGoogleProvider()
    {
        return Socialite::driver('google')->stateless()->redirect();
    }

    public function handleGoogleProviderCallback()
    {
        $google_user = Socialite::driver('google')->user();

        $user = \App\User::where('email', $google_user->email)->first();

        if (!$user) {
            $user = new \App\User;
            $user->email = $google_user->email;
            $user->verified = 1;
            $user->password = null;
            $user->save();
        }
        $token = JWTAuth::fromUser($user);

        $this->attachSocial($user->id, 'google', $google_user->id);

        return response()->json(['success' => true, 'token' => $token]);
    }
}
