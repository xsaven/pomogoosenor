<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Posts;

class BlogController extends Controller
{
    public function show($slug)
    {

        $post = Posts::with('user.profile')->where('slug', $slug)->first();
        if (!$post) {
            return response()->json(['message' => 'Record not found', 404]);
        }

        $related = false;
        if ($post->related) {
            $ids = explode(',', $post->related);
            $related = Posts::whereIn('id', $ids)->get();
        }

        return response()->json(['post' => $post, 'related' => $related]);
    }

    public function index()
    {
        $posts = Posts::orderBy('created_at', 'DESC')->paginate(10);
        return response()->json(['posts' => $posts]);
    }


}
