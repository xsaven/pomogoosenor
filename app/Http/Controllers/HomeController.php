<?php

namespace App\Http\Controllers;

use App\Category;
use App\PageConfig;
use App\Posts;
use App\UserTasks;

class HomeController extends Controller
{

    public function index()
    {
        $tasks = UserTasks::where('status','=',UserTasks::STATUS_OPENED)->limit(6)->orderByDesc('created_at')->get();

        $homeInfo = PageConfig::where('key', '=', 'home')->first()->value;
        $catTree = Category::getTree();
        $lastBlog = Posts::orderby('created_at', 'DESC')->take(4)->get();
        return view('home', compact('catTree', 'lastBlog', 'homeInfo', 'tasks'));
    }
}
