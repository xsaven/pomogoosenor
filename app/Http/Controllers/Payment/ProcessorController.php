<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Omnipay\Omnipay;
use App\Payment;

class ProcessorController extends Controller{

    public function pay(Request $request)
    {
        $this->validate($request, [
            'amount' => 'required|numeric'
        ]);

        $payment = new Payment();
        $payment->order = time();
        $payment->receiver_id = Auth::user()->id;
        $payment->amount = $request->amount;
        $payment->description = 'Пополнение счёта пользователем '.Auth::user()->name;
        $payment->hash = [$payment->order, $payment->receiver_id, $payment->amount];
        $payment->save();

        return $this->in($payment->hash,$request);
    }

    public function in_request(Request $request)
    {
        $pay = Payment::where('order', $request->WMI_PAYMENT_NO)->first();
        if ($pay->status != 3) {
            $pay->status = $request->WMI_ORDER_STATE == 'Accepted' ? 2 : -1;
            $pay->save();
        }
        if ($pay->status == 2) {
            User::find($pay->receiver_id)->checkPayment();
        }
        //$this->in($pay->hash, $request);
    }

    public function in($hash, Request $request)
    {

        $payment = Payment::where('hash', $hash)->first();

        $gateway = Omnipay::create('WalletOne');
        $gateway->setMerchantId(config('payment.MerchantId'));
        $gateway->setSecretKey(config('payment.SecretKey'));

        $settings = [
            'amount' => $payment->amount,
            'currency' => 'UAH',
            'transactionId' => $payment->order,
            'description' => $payment->description,
            'returnUrl' => route('payment.test', ['hash' => $hash, 'status' => 'ok']),
            'cancelUrl' => route('payment.test', ['hash' => $hash, 'status' => 'ok']),
            'expirationDate' => new \DateTime(date('Y-m-d H:i:s', time()+86400), new \DateTimeZone(config('app.timezone')))
        ];

        if($payment->status==1) {
            $response = $gateway->completePurchase()->send();
            $payment->payment_response = $response->getData();
            $payment->save();
        }else{
            $response = $gateway->purchase($settings)->send();
        }

        if ($response->isSuccessful())
        {
            $payment->status = 2;
            $payment->save();
            \Auth::user()->checkPayment();
            return response('WMI_RESULT=OK&WMI_DESCRIPTION=Order successfully processed');
            //return redirect()->route('profile_main')->with('status', "Your account has been successfully replenished by {$payment->amount} RUB!");
        }
        elseif ($response->isRedirect())
        {
            $payment->status = 1;
            $payment->save();
            $response->redirect();
        }
        else
        {
            $payment->status = -1;
            $payment->error_message = $response->getMessage();
            $payment->save();
            return response('WMI_RESULT=RETRY&WMI_DESCRIPTION=Payment error');
//            return redirect()->route('profile_main')->withErrors(['Payment error: '.$response->getMessage()]);
        }
    }

    public function test(Request $request)
    {
        return redirect()->route('profile_main');
    }

}