<?php

namespace App\Http\Controllers\Payment;

use App\CreditCard;
use App\Events\Payments\TaskPaymentHandled;
use App\Helpers\SVGViewer\SvgViewer;
use App\Http\Controllers\Controller;
use App\Notifications\SetExecutor;
use App\Services\WalletOneSafeDeal\Extra\Currencies;
use App\Services\WalletOneSafeDeal\Requests\Params\SaveDealParams;
use App\Services\WalletOneSafeDeal\SafeDealSetup;
use App\Services\WalletOneSafeDeal\WOSafeDeal;
use App\Services\WalletOneSafeDeal\WOWebRequest;
use App\Task;
use App\TaskOffers;
use App\User;
use App\UserTaskDeal;
use App\UserTasks;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Mail;

class SBRController extends Controller
{

    public static function log($message)
    {
        $orderLog = new Logger('payments');
        $orderLog->pushHandler(new StreamHandler(storage_path('logs/payments.log')), Logger::INFO);
        $orderLog->info('OrderLog', $message);
    }

    public function bindPayerCard()
    {
        session(['return_url' => url()->previous()]);
        return WOSafeDeal::web()->bindPayerCard([
            "PlatformPayerId" => user()->id, // Идентификатор заказчика на стороне площадки
            "PhoneNumber" => user()->profile ? user()->profile->getDecimalPhone() : null, // Номер телефона заказчика
            "Title" => '#' . user()->id . ' ' . user()->name, // Наименование заказчика (опционально)
            "ReturnUrl" => route('payment.sbr.handle-bind-payer-card') . '#closeWV', // Урл возврата пользователя
        ]);
    }

    public function handleBindPayerCard(Request $request)
    {
        $user_id = $request['PlatformPayerId'];
        if (WOSafeDeal::web()->requestHasValidSignature()) {
            $cards = WOSafeDeal::payer()->getPayerPaymentTools($user_id)['PaymentTools'];
            $this->updateUserCards($user_id, $cards, CreditCard::PAYER_TYPE);
        }
        return redirect(session('return_url'));
    }

    public function bindBeneficiaryCard()
    {
        session(['return_url' => url()->previous()]);
        return WOSafeDeal::web()->bindBeneficiaryCard([
            "PlatformBeneficiaryId" => user()->id, // Идентификатор исполнителя на стороне площадки
            "PhoneNumber" => user()->profile ? user()->profile->getDecimalPhone() : null, // Номер телефона исполнителя
            "Title" => '#' . user()->id . ' ' . user()->name, // Наименование исполнителя (опционально)
            "ReturnUrl" => route('payment.sbr.handle-bind-beneficiary-card') . '#closeWV', // Урл возврата пользователя
        ]);
    }

    public function handleBindBeneficiaryCard(Request $request)
    {
        $user_id = $request['PlatformBeneficiaryId'];
        if (WOSafeDeal::web()->requestHasValidSignature()) {
            self::log(WOSafeDeal::beneficiary()->getBeneficiaryPaymentTools($user_id));
            $cards = WOSafeDeal::beneficiary()->getBeneficiaryPaymentTools($user_id)['PaymentTools'];
            $this->updateUserCards($user_id, $cards, CreditCard::BENEFICIARY_TYPE);
        }
        return redirect(session('return_url'));
    }

    private function testPay()
    {
        return WOSafeDeal::web()->payDeal([
            "PlatformDealId" => '59113ed6-a95d-4e9b-a742-5c2ca20cad2f', // Идентификатор сделки на стороне площадки
            "ReturnUrl" => route('payment.sbr.handle-pay'), // Урл возврата пользователя
        ]);
    }

    private function getUnpayedDealId(UserTasks $task, $user_id, $cost = null)
    {
        $task_deal = $task->deal;

        if ($task_deal) {
            if ($task_deal->status == UserTaskDeal::DEAL_STATUS['PaymentHold']) {
                return false;
            }

            $dealCost = WOSafeDeal::deal()->getDeal($task_deal->id);
            if (isset($dealCost['Amount']) && $cost == $dealCost['Amount']) {
                return $task_deal->id;
            }
        }
        return $this->registerDeal($task, $user_id, $cost)['PlatformDealId'] ?? false;
    }


    public function pay(UserTasks $task, Request $request)
    {

        session(['return_url' => url()->previous()]);
        session(['task_id' => $task->id]);
        session(['executor_id' => $request->user_id]);
        if ($request->has('card_id')) {
            $task->creator_card_id = $request->card_id;
        }

        if (!is_numeric($request->get('cost', null))) {
            return back()->withErrors(['Неправильная цена.']);
        }

        $deal_id = $this->getUnpayedDealId($task, $request->user_id ?? false, (double)$request->get('cost', null));

        if ($deal_id) {
            return WOSafeDeal::web()->payDeal([
                "PlatformDealId" => $deal_id, // Идентификатор сделки на стороне площадки
                "ReturnUrl" => route('payment.sbr.handle-pay', []) . '#closeWV', // Урл возврата пользователя
            ]);
        }

        return redirect(session('return_url'))->with(['status' => 'success', 'message' => 'Оплата уже была произведена']);
    }

    public function handlePay(Request $request)
    {
        if ($request->has('DealStateId')) {
            $task = UserTasks::whereId($request->session()->get('task_id'))->first();
            $task->setExecutor(session("executor_id"));

            /*$this->executor_id = session("executor_id");
            $this->status = UserTasks::STATUS_IN_PROGRESS;
            $this->locked = true;*/

            /*$task->locked = true;
            $task->status = 2;*/

            User::find(session("executor_id"))->notify(new SetExecutor($task, []));
            Mail::send(new \App\Mail\SetExecutor($task->executor->email));

            $task->save();
        }

        return redirect(session('return_url'));
    }

    protected function registerDeal(UserTasks $task, $user_id, $cost = null)
    {
        $deal_params = WOSafeDeal::deal()->saveDeal(function (SaveDealParams $params) use ($task, $user_id, $cost) {
            /** @var TaskOffers $offer */

            if (!$offer = $task->offers()->where('user_id', $user_id)->first()) {
                fail_message('Incorrect user id');
            };

            $params->PlatformDealId = Uuid::uuid4()->toString();
            $params->PlatformPayerId = $task->creator->id;
            $params->PayerPhoneNumber = $task->creator->profile->getDecimalPhone() ?? '';
            if ($task->creator_card) {
                $params->PayerPaymentToolId = $task->creator_card->id;
            }
            $params->PlatformBeneficiaryId = $user_id;
            $params->BeneficiaryPaymentToolId = $offer->card_id;
            $params->Amount = $cost ?? $offer->cost;
            $params->CurrencyId = Currencies::getCurrencyId('RUB');
            $params->ShortDescription = 'Оплата задания №' . $task->id;
            $params->FullDescription = 'Оплата задания №' . $task->id . ' ' . $task->name;
            $params->DeferPayout = true;
            return $params;
        });


        if (isset($deal_params['success']) && $deal_params['success'] == false) {
            return false;
        }
        $task->saveOrUpdateDeal($deal_params);
        return $deal_params;
    }

    protected function updateUserCards($user_id, $wo_cards, $type)
    {
        User::find($user_id)->credit_cards()->where('type', $type)->delete();
        $cards = [];
        foreach ($wo_cards as $card) {
            $cards[] = [
                'id' => $card['PaymentToolId'],
                'user_id' => $user_id,
                'type' => $type,
                'mask' => $card['Mask']
            ];
        }
        CreditCard::insert($cards);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        dump(user()->credit_cards->toArray());
        echo '<a href="' . route('payment.sbr.pay', UserTasks::find(182)) . '">GOTO</a>';
        die;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function handleWalletOneRequest(Request $request)
    {
        $orderLog = new Logger('payments');
        $orderLog->pushHandler(new StreamHandler(storage_path('logs/111.log')), Logger::INFO);
        $orderLog->info('OrderLog', ['test' => 'in']);
        try {
            if (WOSafeDeal::web()->requestHasValidSignature()) {
                $deal_params = WOSafeDeal::deal()->getDeal($request['PlatformDealId']);
                $deal = UserTaskDeal::find($request['PlatformDealId']);

                event(new TaskPaymentHandled($deal, $deal_params));
            }

            return response('WMI_RESULT=OK&WMI_DESCRIPTION=Order successfully processed');
        } catch (\Exception $exception) {

            $orderLog = new Logger('payments');
            $orderLog->pushHandler(new StreamHandler(storage_path('logs/error.log')), Logger::INFO);
            $orderLog->info('OrderLog', $exception->getMessage());

            return response('WMI_RESULT=RETRY&WMI_DESCRIPTION=' . urlencode($exception->getMessage()));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function print_answer($result)
    {
        print "WMI_RESULT=" . strtoupper($result);
        exit();
    }
}
