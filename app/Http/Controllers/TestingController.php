<?php

namespace App\Http\Controllers;

use App\Category;
use App\TestingSettings;
use App\User;
use Illuminate\Http\Request;
use App\Pattern;

class TestingController extends Controller
{
    private $profile;

    private $max_step_index;

//    public function __construct()
//    {
//        $this->middleware('auth');
//
//    }

    public function step_info(User $users)
    {
        $user = \user()->getUserWithProfile();

        if ($user->profiles->testing_step != '0') {
            return redirect()->back()->with('warning', ['Вы уже прошли этот шаг']);
        }

        $cat_tree = Category::getTree();

        return view('testing.info', compact('cat_tree', 'user'));
    }

    public function startSteps()
    {
        $this->profile = \Auth::user()->profiles;
        if ($this->profile->testing_step != '1') {
            return redirect()->back()->with('warning', ['Сначала заполните личную информацию']);
        }
        $this->profile->switchTestingStep(2);
        return redirect('/testing/start');
    }

    public function step_testing()
    {
        $this->profile = \Auth::user()->profiles;
        if ($this->profile->testing_step != '1') {
            return redirect()->back()->with('warning', ['Вы уже прошли этот шаг']);
        }
        $test_links = TestingSettings::all();
        return view('testing.steps',compact('test_links'));
    }

    private function loadPatterns()
    {
        $patterns = Pattern::with('right_answers')->where([
            ['active', '1'],
        ])->orderBy('order', 'ASC')->get();
        $this->max_step_index = (count($patterns) - 1);
        return $patterns;
    }


    private function checkUnswers($pattern, $answers)
    { //return array Invalid unswers OR false if all is correct
        $right_answers = $pattern->right_answers->pluck('id')->toArray();
        return array_diff(array_flatten($answers),$right_answers);
    }

    private function checkIfAllUnswered($pattern, $answers)
    {
        return count(array_flatten($answers)) >= $pattern->right_answers->count();
    }

    public function submitStep(Request $request)
    {
        $this->profile = \Auth::user()->profiles;
        $patterns = $this->loadPatterns();
        $checking_pattern = $patterns[$this->profile->quest_step];

        if (!$request->has('questions') ||
            !$this->checkIfAllUnswered($checking_pattern, $request->questions)) {
            return redirect()->back()->with('formatted_unswers', $checking_pattern->right_answers)
                ->with('danger', ['Сначала дайте ответ на все вопросы']);
        }

        $tasting_errors = $this->checkUnswers($checking_pattern, $request->questions);

        if (count($tasting_errors) == 0) {

            //next step switch
            $new_step = $this->profile->quest_step + 1;
            if ($new_step <= $this->max_step_index) {
                $this->profile->quest_step = $new_step;
                $this->profile->save();

            } else {
                $this->profile->switchTestingStep(3);
            }
            return $this->maybeStartTesting();

        } else {
            return redirect()
                ->back()
                ->with('testing_errors', $tasting_errors)
                ->with('formatted_unswers', $request->questions)
                ->with('danger', ['Неправильных ответов: ' . count($tasting_errors)]);
        }

    }

    public function step_all()
    {
        $this->profile = \Auth::user()->profiles;
        if ($this->profile->testing_step != '2') {
            return redirect()->back()->with('warning', ['Вы уже прошли этот шаг']);
        }
        $active_pattern = $this->profile->quest_step;
        $patterns = $this->loadPatterns();

        return view('testing.steps_all', compact('patterns', 'active_pattern'));
    }

    public function confirmPage(User $users)
    {
        $user = user()->getUserWithProfile();
        $user->profiles->is_worker = '1';
        $user->profiles->save();
        $cat_tree = Category::getTree();
        return view('profile.profile_confirm', compact('user', 'cat_tree'));
    }

    public function maybeStartTesting()
    {
        $this->profile = auth()->user()->profiles;

        if ($this->profile->is_worker && !$this->profile->moderated_worker) {
            return redirect()->back()->with('warning', ['Ваш статус исполнителя находится на проверке']);
        } elseif ($this->profile->is_worker && $this->profile->moderated_worker) {
            return redirect()->back()->with('warning', ['Вы уже являетесь проверенным исполнителем']);
        } else {
            switch ($this->profile->testing_step) {
                case 0:
                    return redirect('testing/info');
                    break;
                case 1:
                    return redirect('testing/steps');
                    break;
                case 2:
                    return redirect('testing/steps_all');
                    break;
                case 3:
                    return redirect('testing/confirm');
                    break;
            }
        }
    }


}
