<?php

namespace App\Http\Controllers\Auth;

use App\City;
use App\Mail\Verification;
use App\Profile;
use App\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Validator;
use Image;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

/**
 * Class RegisterController
 * @package App\Http\Controllers\Auth
 */
class RegisterController extends Controller
{

    use RegistersUsers;

    /**
     * @var string
     */
    protected $redirectTo = '/';


    /**
     * @var string
     */
    private $pass = '';

    /**
     * @var string
     */
    protected $redirectAfterVerification = '/';

    private $autoAuth = false;

    /**
     * @param array $data
     * @return mixed
     */
    protected function emailValidate(array $data)
    {
        $messages = [
            'confirm_rules.required' => 'Вы должны принять правила использования даного сервиса'
        ];
        return Validator::make($data, [
            'email' => 'required|email|max:255|unique:users',
            'confirm_rules' => 'required',
        ], $messages);
    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function validator(array $data)
    {
        if (auth()->check())
            return Validator::make($data, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email,' . auth()->id(),
                'phone_code' => 'required|string|max:5',
                'phone' => 'required|string|max:20|unique:profiles,phone',
                'city' => 'required|max:50',
                'avatar' => 'image|dimensions:max_width=2000,max_height=2000',

            ], [
                'city.required' => 'Пожалуйста выберите город из списка',
            ]);
        else
            return Validator::make($data, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email',
                'phone_code' => 'required|string|max:5',
                'phone' => 'required|string|max:20|unique:profiles,phone',
                'city' => 'required|max:50',
                'avatar' => 'image|dimensions:max_width=2000,max_height=2000',

            ], [
                'city.required' => 'Пожалуйста выберите город из списка',
            ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getEmail(Request $request)
    {
        $this->emailValidate($request->all())->validate();
        session()->put('email', $request->email);
        return redirect()->route('finish_register');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function autoAuth()
    {
        $this->autoAuth = true;
        return $this;
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $city_id = '1';

        if (!is_null($request->city)) {

            $city = (new City())->findCityByName($request->city);

            if (!empty($city))
                $city_id = $city->id;

            $request->request->add(['city' => $city_id]);
        }

        event(new Registered($user = $this->create($request->all() + ['city_id' => $city_id])));

        $this->guard()->login($user);

        if ($request->hasFile('avatar')) {
            $avatar_data = $this->loadAvatar($request->file('avatar'));
            $user->profiles()->update([
                'avatar_full' => $avatar_data['full'],
                'avatar_thumb' => $avatar_data['thumb']
            ]);
        }

        session()->put('first_login', $this->pass);


        $user->verification_token = Str::random(60);
        $user->save();

        Mail::send(new Verification($request->email, $this->pass, route('email_verified', ['token' => $user->verification_token, 'email' => $request->email])));

//        UserVerification::generate($user);

//        UserVerification::send($user, 'Регистрация на сайте Pomogoo');

        if ($this->autoAuth) {
            \Auth::login($user, true);
        }

        return $this->registered($user)
            ?: redirect()->route('home');
    }

    /**
     * @param $file
     * @param bool $base64
     * @return array
     */
    protected function loadAvatar($file, $base64 = false)
    {
        $root = public_path();
        $path = '/uploads/users/';
        if ($base64) {
            $ext = 'jpg';
        } else {
            $ext = $file->getClientOriginalExtension();
        }

        $name = 'user_' . \Auth::user()->id;
        $nameWithPathFull = $root . $path . $name . '.' . $ext;
        $nameWithPathThumb = $root . $path . $name . '_thumb.' . $ext;

        if ($base64) {
            $fullImage = Image::make($file);
            $thumbImage = Image::make($file);
        } else {
            $fullImage = Image::make($file);
            $thumbImage = Image::make($file);
        }


        $fullImage->save($nameWithPathFull);
        $thumbImage->fit(300)->save($nameWithPathThumb);

        $nameWithPathFull = $path . $name . '_thumb.' . $ext;
        $nameWithPathThumb = $path . $name . '_thumb.' . $ext;

        return array(
            'full' => $nameWithPathFull,
            'thumb' => $nameWithPathThumb
        );
    }

    /**
     * @param int $lengh
     * @return string
     */
    protected
    function generatePassword($lengh = 7)
    {
        return str_random($lengh);
    }

    /**
     * @param Request $request
     * @param $user
     * @return \Illuminate\Http\JsonResponse
     */
    protected
    function registered($user)
    {
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public
    function endRegister()
    {
        $email = old('email') ?? session()->get('email');
        return view('auth.registration-end', compact('email'));
    }

    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Model
     */
    protected function create(array $data)
    {
        $this->pass = $this->generatePassword();
        $user = User::create([
            'email' => $data['email'],
            'password' => bcrypt($this->pass),
            'name' => $data['name'],
        ]);

        preg_match_all('/\S+/', $user->getOriginal('name'), $matches);
        list($first_name, $last_name) = array_pad($matches[0], 2, '');
        $user->profiles()->create([
            'phone_code' => $data['phone_code'],
            'phone' => $data['phone'],
            'city_id' => $data['city_id'],
            'testing_step' => '0',
            'quest_step' => '0',
            'name' => $first_name,
            'surname' => $last_name
        ]);

        $user->subscribe()->create([
            'system_notification' => '1',
            'sms_new_message' => '1',
            'news' => '1',
            'subscribe_type' => '1',
            'user_id' => '1',
        ]);

        return $user;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public
    function sendEmail(Request $request)
    {
        if (!empty($user = auth()->user())) {
            $user->verification_token = Str::random(60);
            $user->save();

            $email = $request->email ?? $user->email;
            Mail::send(new Verification($request->email ?? $user->email, $this->pass, route('email_verified', [
                'token' => $user->verification_token,
                'email' => $email,

            ])));

            return response()->json(['status' => 'success', 'message' => 'Письмо было отправлено на почту: ' . $email]);

        } else {
            return back()->with('errors', 'Что-то пошло не так, обратитесь к администратору');
        }
    }

    /**
     * @param User $users
     * @param $token
     * @param $email
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function checkToken(User $users, $token, $email)
    {
        if (!empty($user = $users->getUserByToken($token, $email))) {
            $user->verified = '1';
            $user->verification_token = '';
            $user->save();
            user()->notify(new \App\Notifications\DataVerified());
            return redirect()->route('congratulation');
        }
        return abort('403');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public
    function congratulation()
    {
        return view('pages.congratulation');
    }

    /*
     * Ajax validate
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public
    function ajaxValidate(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 421);
        } else {
            return response()->json([], 200);
        }
    }


}