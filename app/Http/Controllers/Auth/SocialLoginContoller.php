<?php

namespace App\Http\Controllers\Auth;

use App\City;
use App\Http\Controllers\AttachSocialController;
use App\Mail\Verification;
use App\SocialAccount;
use App\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use function Couchbase\defaultDecoder;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Socialite;
use Image;
use Validator;

/**
 * Class SocialLoginContoller
 * @package App\Http\Controllers\Auth
 */
class SocialLoginContoller extends Controller
{

    private $pass;
    private $random_email;

    public function __construct()
    {
        $this->random_email = 'example.' . str_random(8) . '@gmail.com';
    }

    /**
     * @var string
     */
    private $save_path = '/uploads/users/';

    public function callAction($method, $parameters)
    {
        if (str_is('handle*ProviderCallback', $method) && \Auth::check()) {
            preg_match('/handle(.*)ProviderCallback/', $method, $match);
            $provider = strtolower($match[1]);
            $provider = $provider == 'vk' ? 'vkontakte' : $provider;
            $provider = $provider == 'mail' ? 'mailru' : $provider;
            return (new AttachSocialController())->handleProviderCallback($provider);
        }
        return parent::callAction($method, $parameters);
    }

    /*
     * Social redirect
     */
    /**
     * @return mixed
     */
    public function redirectToFacebookProvider()
    {
        return Socialite::with('facebook')->redirect();
    }

    /**
     * @return mixed
     */
    public function redirectToGoogleProvider()
    {
        return Socialite::with('google')->redirect();
    }

    /**
     * @return mixed
     */
    public function redirectToVkProvider()
    {
        return Socialite::with('vkontakte')->redirect();
    }

    /**
     * @return mixed
     */
    public function redirectToMailProvider()
    {
        return Socialite::with('mailru')->redirect();
    }


    /*
     * CallBack
     */
    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handleFacebookProviderCallback()
    {
        $social = Socialite::driver('facebook')->user();

        $social->email = null;
        $user = $social->email != null ? User::where('email', $social->email)->first() : null;

        if (!$user) {
            $user = new User;
            $user->email = $social->email;
            $user->name = $social->name;
            $user->password = $this->generatePassword('8');
            $user->save();

            /*
             * SSL  param
             */
            $arrContextOptions = array(
                "ssl" => array(
                    "verify_peer" => false,
                    "verify_peer_name" => false,
                ),
            );

            /*
             * Thumb
             */
            $avatar_thumb = $this->save_path . 'user_' . $user->id . '_thumb.jpg';
            $avatar_thumb_pic = @file_get_contents($social->avatar, false, stream_context_create($arrContextOptions));
            Image::make($avatar_thumb_pic)->save(public_path($avatar_thumb));

            /*
             * Full
             */
            $avatar_full = $this->save_path . 'user_' . $user->id . '.jpg';
            $avatar_full_pic = @file_get_contents($social->avatar_original, false, stream_context_create($arrContextOptions));
            Image::make($avatar_full_pic)->save(public_path($avatar_full));


            $user->profiles()->firstOrCreate([
                'avatar_thumb' => $avatar_thumb,
                'avatar_full' => $avatar_full,
                'testing_step' => '0',
                'quest_step' => '0',
            ]);

            $user->socialAccounts()->firstOrCreate([
                'social_type' => 'facebook',
                'social_id' => $social->id
            ]);

            auth()->login($user, true);

            return redirect()->route('finish_register_social');
        }
        $this->checkSocialExists($user, $social->id, 'facebook');

        auth()->login($user, true);

        return redirect()->route('profile_main');
    }


    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handleGoogleProviderCallback()
    {
        $social = Socialite::driver('google')->user();

        $user = User::where('email', $social->email)->first();

        if (!$user) {
            $user = new User;
            $user->email = $social->email;
            $user->name = $social->name;
            $user->password = $this->generatePassword('8');
            $user->save();

            /*
              * SSL  param
              */
            $arrContextOptions = array(
                "ssl" => array(
                    "verify_peer" => false,
                    "verify_peer_name" => false,
                ),
            );

            /*
             * Thumb
             */
            $avatar_thumb = $this->save_path . 'user_' . $user->id . '_thumb.jpg';
            $avatar_thumb_pic = @file_get_contents($social->avatar, false, stream_context_create($arrContextOptions));
            Image::make($avatar_thumb_pic)->save(public_path($avatar_thumb));

            /*
             * Full
             */
            $avatar_full = $this->save_path . 'user_' . $user->id . '.jpg';
            $avatar_full_pic = @file_get_contents($social->avatar, false, stream_context_create($arrContextOptions));
            Image::make($avatar_full_pic)->save(public_path($avatar_full));

            $user->profiles()->firstOrCreate([
                'avatar_thumb' => $avatar_thumb,
                'avatar_full' => $avatar_full,
                'testing_step' => '0',
                'quest_step' => '0',
            ]);

            $user->socialAccounts()->firstOrCreate([
                'social_type' => 'google',
                'social_id' => $social->id
            ]);

            auth()->login($user, true);

            return redirect()->route('finish_register_social');
        }

        $this->checkSocialExists($user, $social->id, 'google');

        auth()->login($user, true);

        return redirect()->route('profile_main');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handleMailProviderCallback()
    {
        $social = Socialite::driver('mailru')->user();

        $user = User::where('email', $social->email)->first();

        if (!$user) {
            $user = new User;
            $user->email = $social->email;
            $user->name = $social->name;
            $user->password = $this->generatePassword('8');
            $user->save();

            $avatar_thumb = '';
            $avatar_full = '';

            if ($social->user['has_pic'] == '1') {
                $path_thumb = $social->user['pic_128'];
                $avatar_thumb = $this->save_path . 'thumb_' . $user->id . '_' . basename($path_thumb);
                Image::make($path_thumb)->save(public_path($avatar_thumb));

                $path_full = $social->user['pic'];
                $avatar_full = $this->save_path . $user->id . '_' . basename($path_full);
                Image::make($path_full)->save(public_path($avatar_thumb));
            }

            $user->profiles()->create([
                'surname' => $social->user['last_name'],
                'birthday' => Carbon::parse($social->user['birthday']),
                'avatar_thumb' => $avatar_thumb,
                'avatar_full' => $avatar_full,
                'testing_step' => '0',
                'quest_step' => '0',
            ]);

            $user->socialAccounts()->firstOrCreate([
                'social_type' => 'mailru',
                'social_id' => $social->id
            ]);

            auth()->login($user, true);

            return redirect()->route('finish_register_social');
        }

        $this->checkSocialExists($user, $social->id, 'mailru');

        auth()->login($user, true);

        return redirect()->route('profile_main');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public
    function handleVkProviderCallback()
    {
        $social = Socialite::driver('vkontakte')->user();
        $user = SocialAccount::whereSocialId($social->user['id'])
                ->whereSocialType('vkontakte')
                ->whereHasNotTrashedUser()
                ->first()
                ->user ?? null;
        if (!$user) {
            $user = new User;
            $user->email = $social->email ?? $this->random_email;
            $user->name = $social->name;
            $user->password = $this->generatePassword('8');
            $user->save();

            $path = $social->user['photo'];
            $avatar_thumb = $this->save_path . 'thumb_' . $user->id . '_' . basename($path);
            $avatar_thumb = str_replace('?ava=1', '', $avatar_thumb);

            Image::make($path)->save(public_path($avatar_thumb));

            $avatar_full = $this->save_path . $user->id . '_' . basename($path);
            $avatar_full = str_replace('?ava=1', '', $avatar_full);

            Image::make($path)->save(public_path($avatar_full));

            $user->profiles()->firstOrCreate([
                'surname' => $social->user['last_name'],
                'avatar_thumb' => $avatar_thumb,
                'avatar_full' => $avatar_full,
                'testing_step' => '0',
                'quest_step' => '0',
            ]);

            $user->socialAccounts()->firstOrCreate([
                'social_type' => 'vkontakte',
                'social_id' => $social->user['id']
            ]);
            auth()->login($user, true);

            return redirect()->route('finish_register_social');
        }
        $this->checkSocialExists($user, $social->user['id'], 'vkontakte');

        auth()->login($user, true);

        return redirect()->route('profile_main');
    }


    /**
     * @param $lengh
     * @return string
     */
    private function generatePassword($lengh)
    {
        $this->pass = str_random($lengh);
        return bcrypt($this->pass);
    }

    /**
     * @param User $user
     * @param integer $social_id
     * @param string $social_type
     */
    private function checkSocialExists(User $user, $social_id, $social_type)
    {
        if (!$user->socialAccounts()->where('social_type', $social_type)->where('social_id', $social_id)->exists())
            $user->socialAccounts()->firstOrCreate([
                'social_type' => $social_type,
                'social_id' => $social_id
            ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendEmail()
    {
        if (!empty($user = auth()->user())) {
            $user->verification_token = Str::random(60);
            $user->save();
            Mail::send(new Verification($user->email, $this->pass, route('email_verified', ['token' => $user->verification_token, 'email' => $user->email])));

        } else {
            return back()->with('errors', 'Что-то пошло не так, обратитесь к администратору');
        }
    }

    public function endRegisterSocial(User $users)
    {
        session()->forget('register_status');
        if (!$user = \user()->getUserWithProfile())
            return abort('403');
        return view('auth.registration-end-social', compact('user'));
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,' . auth()->id(),
            'phone_code' => 'required|string|max:5',
            'phone' => 'required|string|max:20',
            'city' => 'required|string|max:50',
            'avatar' => 'image|dimensions:min_width=200,min_height=200',

        ]);
    }

    public function finishRegister(Request $request, City $city)
    {
        $this->validator($request->all())->validate();
        $city_id = '1';
        $city = $city->findCityByName($request->city);
        if (!empty($city))
            $city_id = $city->id;
        $request->request->add(['city_id' => $city_id]);

        $user = auth()->user();
        $this->pass = str_random(8);
        $user->update([
            'email' => $request->email,
            'password' => $this->pass
        ]);

        $user->profiles()->update($request->only('phone_code', 'phone', 'city_id'));

        session()->put('first_login', $this->pass);

        $user->verification_token = Str::random(60);
        $user->save();

        $this->sendEmail();

        return redirect()->route('profile_main');

    }
}
