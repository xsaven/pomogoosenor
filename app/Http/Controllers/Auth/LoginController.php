<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Socialite;
use Validator;
use Jrean\UserVerification\Traits\VerifiesUsers;
use Jrean\UserVerification\Facades\UserVerification;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, VerifiesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->redirectTo = config('quickadmin.route');
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function logout(Request $request)
    {
        if (auth()->check()) {
            auth()->logout();
        }
        if ($request->ajax()) {
            return response()->json([], 200);
        }
        return redirect()->route('home');
    }

    protected function authenticated(Request $request, $user)
    {
        if ($request->ajax()) {
            return response()->json(['success' => 'AUTHENTICATED'], 200);
        } else {
            return redirect()->back();
        }
    }

    protected function sendLoginResponse(Request $request)
    {
        $this->clearLoginAttempts($request);
        return $this->authenticated($request, $this->guard()->user());

    }
    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => [trans('auth.failed')]];
        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }
        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

    protected function sendLockoutResponse(Request $request)
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );
        $errors = [$this->username() => [trans('auth.throttle',compact('seconds'))]];
        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }
        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
        return response()->json(array('email' => array('Слишком много попыток входа. Подождите ' . $seconds . ' секунд и попробуйте снова')), 422);
    }


    public function pickeEmail()
    {
        return view('auth.pick-email');

    }


}