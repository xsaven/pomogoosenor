<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;

class LocationController extends Controller
{


    public function postFindCity(Request $request)
    {
        $data = [];
        if ($request->has('query')) {

            $searchName = $request->get('query');
            $collection = City::with('country')
                ->where('name', 'LIKE', '%' . $searchName . '%')
                ->take(10)
                ->get();

            if (!$collection->isEmpty()) {
                foreach ($collection as $city) {
                    $data[] = array(
                        'id' => $city->id,
                        'name' => $city->name,
                    );
                }
            }

        }
        return response()->json($data, 200);
    }
}
