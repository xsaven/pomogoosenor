<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ZfrMailChimp\Client\MailChimpClient as Client;
use Validator;

class SubscribeController extends Controller
{
    private $client;

    public function __construct() {
        $this->client = new Client(get_setting('mailchimp_key'));
    }

    protected function validator ($data) {
        return Validator::make($data,[
            'email' => 'required|email'
        ]);
    }

    public function subscribe (Request $request) {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(),422);
        }
        try {
            $this->client->subscribe(array(
                'id' => 'b691db3ede',
                'email' => array(
                    'email' => $request->input('email'),
                    'euid'  => '1545d'
                )
            ));
        } catch (\Exception $e) {

            return response()->json([
                'email' => ['Внетренняя ошибка']
            ],422);
        }

        return response()->json([],200);
    }
}
