<?php

namespace App\Http\Controllers\Page;


use App\Http\Controllers\Controller;
use App\Vacancies;
use App\VacancyFeedback;

class VacanciesController extends Controller
{
    public function index()
    {
        $vacancies = Vacancies::all();

        return view('pages.vacancies', ['vacancies' => $vacancies]);
    }

    public function vacancy($id)
    {
        $vacancy = (new Vacancies())->where('id', '=', $id)->first();

        return view('pages.vacancy', ['vacancy' => $vacancy]);
    }

    public function save(\App\Http\Requests\VacancyFeedback $feedback)
    {
        $data = $feedback->all();
        if ($feedback->hasFile('cv')) {
            $file = $this->storeFile($feedback->file('cv'), 'uploads/cv');
            $data['cv'] = $file;
        }

        VacancyFeedback::create($data);
        return back()->with('status', true)->with('message_title', 'Резюме сохранено');
    }
}