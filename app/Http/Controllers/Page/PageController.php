<?php

namespace App\Http\Controllers\Page;

use App\Category;
use App\CustomerReview;
use App\FAQ;
use App\MassMedia;
use App\PerformersReview;
use App\StaticPages;
use App\Subcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\View\Compilers\BladeCompiler;

class PageController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        return view('pages.how-it-works', ['page' => StaticPages::whereSlug('how-it-works')->first()]);
    }

    public function customerReview()
    {
        return view('pages.customer-reviews', [
            'reviews' => CustomerReview::all(),
            'page' => StaticPages::whereSlug('customer-reviews')->first()
        ]);
    }

    public function performersReview()
    {
        return view('pages.performers-reviews', [
            'reviews' => PerformersReview::all(),
            'page' => StaticPages::whereSlug('performers-review')->first()
        ]);
    }

    public function rewards()
    {
        return view('pages.rewards', ['page' => StaticPages::whereSlug('rewards')->first()]);
    }

    public function howPerformer()
    {
        return view('pages.how-performer', ['page' => StaticPages::whereSlug('how-performer')->first()]);
    }


    public function noRisk()
    {
        return view('pages.no-risk', [
            'page' => StaticPages::whereSlug('no-risk')->first(),
            'questions' => FAQ::query()->limit(4)->get()
        ]);
    }

    public function security()
    {
        return view('pages.security', ['page' => StaticPages::whereSlug('security')->first()]);
    }

    public function massMedia()
    {
        return view('pages.mass-media', ['collection' => MassMedia::all()]);
    }

    public function contacts()
    {
        return view('pages.contacts', ['page' => StaticPages::whereSlug('contacts')->first()]);
    }

    public function rules()
    {
        return view('pages.rules', ['page' => StaticPages::whereSlug('rules')->first()]);
    }

    public function receipt()
    {
        return view('pages.receipt', ['page' => StaticPages::whereSlug('receipt')->first()]);
    }

    public function faq()
    {
        $faq = FAQ::all();

        return view('pages.faq', [
            'collection' => $faq,
            'title' => StaticPages::whereSlug('no-risk')->first()->title
        ]);
    }

    public function task_categories()
    {
        return view('pages.task_category')
            ->with('cat_tree', Category::getTree())
            ->with('defaultSlug', Subcategory::where('default', '=', 1)->first()->slug);
    }

    public function getPage()
    {
        return view('layouts.static', compact('static_page'));
    }
}
