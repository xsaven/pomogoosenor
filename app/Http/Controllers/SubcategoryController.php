<?php

namespace App\Http\Controllers;

use App\Subcategory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\City;

class SubcategoryController extends Controller
{


    public function findSubcategory(Request $request)
    {
        $data = [];
        if ($request->has('query')) {

            $search = $request->get('query');
            $collection = Subcategory::query()
                ->select(['id', 'name'])
                ->where(function (Builder $query) use ($search) {
                    $words = explode(' ', $search);
                    foreach ($words as $word) {
                        $query->orWhere('tags', 'LIKE', '%' . $word . '%');
                        $query->orWhere('name', 'LIKE', '%' . $word . '%');
                    }
                })
                ->take(10)
                ->get();


            foreach ($collection as $item) {
                $categoryItem['name'] = $item->name;
                $categoryItem['id'] = $item->id;
                $categoryItem['url'] = $item->CreateTaskUrlWithQuery($item->name);
                $data[] = $categoryItem;
            }

            return response()->json($data, 200);
        }
        return response()->json([], 400);
    }
}
