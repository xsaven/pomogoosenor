<?php

namespace App\Http\Controllers;

use App\DialogParticipant;
use App\Events\ChatGlobalMessage;
use App\Events\NewEventCount;
use App\ChatSetting;
use App\Dialog;
use App\DialogMessage;
use App\Http\Resources\APICollection;
use App\Http\Resources\Chat\ChatUserResource;
use App\Http\Resources\UserResource;
use App\Message;
use App\Traits\ChatTrait;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;

/**
 * Class ChatController
 * @package App\Http\Controllers
 */
class ChatController extends Controller
{
    use ChatTrait;

    protected $take = 15;

    private function formatMessageText($text)
    {
        $text = strip_tags($text);
        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        if (preg_match($reg_exUrl, $text, $url)) {
            return preg_replace($reg_exUrl, "<a href=" . $url[0] . ">{$url[0]}</a> ", $text);
        } else {
            return $text;
        }
    }

    /**
     * Send message to dialog
     *
     * @param Dialog $dialog
     * @param DialogMessage $messages
     * @param ChatSetting $chatSetting
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendMessage(Dialog $dialog, DialogMessage $messages, ChatSetting $chatSetting, Request $request)
    {

        $request->request->set('text', $this->formatMessageText($request->text));

        $response = [];
        $user = auth()->user();
        $dialog_id = null;
        $new_dialog = null;

        $msg_per_day = $chatSetting->getByKeyWord('msg_per_day');
        $current_user_msg_count = $messages->getMsgPerDaysCount($dialog_id, $user->id, date('d'));

        if (isset($msg_per_day) && $msg_per_day->value <= $current_user_msg_count)
            return response()->json(['msg' => 'Антиспам защита, вы исчерпали лимит сообщений на день. Лимит: ' . $msg_per_day->value], 403);

        if ($request->text != '' && $user != '') {

            if ($request->thread_id != '') {
                $dialog_id = $request->thread_id;

                $dialogs = Dialog::find($dialog_id);
                $message = $dialogs->messages()->create([
                    'sender_id' => $user->id,
                    'seen' => '0',
                    'text' => $request->text,
                ]);
                $dialogs->dialogParticipants()->touch();
            } else
                return response()->json($response);


            $messages = [
                'id' => $message->id,
                'body' => $message->text,
                'name' => 'You',
                'avatar' => $user->profile->avatar_thumb != '' ? $user->profile->avatar_thumb : 'no-image.jpg',
                'status' => $user->online,
                'time' => $message->created_at->setTimezone(new \DateTimeZone('Europe/Moscow'))->format('d.m.y H:i:s'),
            ];
            $from_messages = $messages;
            $from_messages['name'] = $user->name;

            broadcast(new ChatGlobalMessage($user, $dialogs, $from_messages))->toOthers();

            $users = $dialogs->dialogParticipants()
                ->where('user_id', '!=', $user->id)
                ->where('user_id', '!=', 1)
                ->get();

            $other_user_id = 0;

            foreach ($users as $item) {
                $other_user = $user->where('id', $item->user_id)->first();
                $event = [
                    'user_id' => $user->id,
                    'thread_id' => $dialogs->id,
                    'new_user_id' => $new_dialog,
                    'msg' => true,
                ];
                $other_user_id = $other_user->id;
                broadcast(new NewEventCount($other_user, $event));
            }

            $response = array(
                'success' => true,
                'message' => $messages,
                'thread_id' => $dialogs->id,
                'to_user_id' => $other_user_id,
                '$current_user_msg_count' => $current_user_msg_count,
                '$msg_per_day' => $msg_per_day

            );
        }
        return response()->json($response);
    }

    /**
     * Get All User Thread
     *
     * @param User $userModel
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function initializeChat(User $userModel)
    {
        $user = \user();

        $new_msg = $user->newMessageCountLoad();

        $new_messages_total = collect($new_msg)->sum(function ($item) {
            return $item['msg_count'];
        });

        $active_trade_users_ids = [];

        $new_chat_event = [
            'new_msg' => $new_msg,
            'active_trade' => $active_trade_users_ids,
            'new_msg_total' => $new_messages_total
        ];

        $user_dialog_thread = $this->getDialogIdsArray($user->id);

        $threads = DialogParticipant::where('user_id', $user->id)->get();
        if ($threads->isNotEmpty()) {
            $threads = $threads->pluck('dialog_id');
        }

        //dd($userModel->getAllUserByDialogId($user_dialog_thread, $user->id));

        /** @var APICollection $users */
        $users = new APICollection($userModel->getAllUserByDialogId($user_dialog_thread, $user->id), ChatUserResource::class);

        if (request()->q) {
            $users = collect($users)->filter(function ($user) {
                return stripos($user['name'], request()->q) !== false;
            });
        }

        $auth_user = new ChatUserResource(user());

        //dd(compact('users', 'new_chat_event', 'auth_user', 'threads'));

        return response()->json(compact('users', 'new_chat_event', 'auth_user', 'threads'));
    }

    public function getNewUserToThread(User $user, Request $request)
    {
        /** @var User $users */
        $users = Dialog::find($request->dialog_id)->dialogParticipants()->where('user_id', '<>', \user()->id)->first()->users;
        return response()->json(['user' => new ChatUserResource($users)]);

    }


    /**
     * Get Message Thread
     * @param Dialog $dialog
     * @param User $users
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getThreadMessage(Dialog $dialog, Request $request)
    {
        $user = auth()->user();

        if (!$request->has('skip'))
            $skip = 0;
        else
            $skip = $request->skip;

        if (isset($request->thread_id)) {
            $dialogs = $dialog->getChatThreadWithMessage($request->thread_id, $this->take, $skip);
        } elseif (isset($request->user_id)) {
            $dialogs = $dialog->getThreadMessagesByUserId($request->user_id, $this->take, $skip);
        } else {
            return response()->json();
        }


        if (count($dialogs)) {

            $other_user_id = $dialog->getParticipantsId($dialogs->id, $user->id)->dialogParticipants->where('user_id', '!=', $user->id)->first()->user_id;


            $other_user = User::find($other_user_id);

            $dialogs->dialogParticipants()->where('user_id', auth()->id())->update([
                'last_read' => Carbon::now()
            ]);

            $dialogs->messages()->where('sender_id', '!=', auth()->id())->update([
                'seen' => '1'
            ]);
            $admin = User::find('1');
            $messages = [];
            foreach ($dialogs->messages as $message) {
                $system = false;
                if ($message->sender_id != $user->id) {
                    if ($message->sender_id == 1) {
                        $name = $admin->name;
                        $avatar = $admin->avatar != '' ? $admin->avatar : 'images/useful/svg/default-avatar.svg';
                        $status = true;
                        $system = true;

                    } else {
                        $name = $other_user->name;
                        $avatar = $other_user->profile->avatar_thumb != '' ? $other_user->profile->avatar_thumb : 'images/useful/svg/default-avatar.svg';
                        $status = $other_user->online;
                    }
                } else {
                    $name = 'You';
                    $avatar = $user->profile->avatar_thumb != '' ? $user->profile->avatar_thumb : 'images/useful/svg/default-avatar.svg';
                    $status = $user->online;
                }


                $messages[] = [
                    'id' => $message->id,
                    'body' => $message->text,
                    'name' => $name,
                    'avatar' => $avatar,
                    'status' => $status,
                    'is_system' => $system,
                    'user_id' => $message->sender_id,
                    'time' => $message->created_at->setTimezone(new \DateTimeZone('Europe/Moscow'))->format('d.m.y H:i:s'),
                ];

            }
            if ($request->get('token')) {
                $messages = array_reverse($messages);
            }

            $response = [
                'success' => true,
                'messages' => $messages,
                'user' => $other_user,
                'thread_id' => $dialogs->id,
                'messages_count' => $dialogs->messages_count,
                'current_msg_index' => $skip + $this->take,
                'per_page' => $this->take

            ];

            return response()->json($response);
        }
        return response()->json();
    }

    public function createNewChat(Request $request)
    {
        $participants = [\user()->id, $request->user_id];
        $dialog = \App\Dialog::getDialogByParticipants($participants)->first();
        if (!$dialog) {
            $dialog = \App\Dialog::createDialogByParticipants($participants);
        }
        return response()->json(['dialog_id' => $dialog->id]);
    }


    public function markAsRead(Request $request)
    {
        $dialogs = Dialog::find($request->data['thread_id']);
        $dialogs->messages()->where('sender_id', \user()->id)->update([
            'seen' => '1'
        ]);
        return response()->json();
    }
}
