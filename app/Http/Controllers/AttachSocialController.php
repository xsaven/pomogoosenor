<?php

namespace App\Http\Controllers;

use App\SocialAccount;
use App\User;
use Illuminate\Http\Request;
use Socialite;

class AttachSocialController extends Controller
{

    /**
     * Attach new social account
     *
     * @param string $provider
     * @return \Illuminate\Http\Response
     */
    public function attach($provider)
    {
        session_flash('social_redirect', \URL::previous());
        return Socialite::with($provider)->redirect();
    }

    /**
     * Handle callback from social provider
     *
     * @param string $provider
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function handleProviderCallback($provider)
    {
        $social = Socialite::driver($provider)->user();
        $social = $provider == 'vkontakte' ? $social->user : $social;
        user()->socialAccounts()->firstOrCreate([
            'social_type' => $provider,
            'social_id' => $provider != 'vkontakte' ? $social->id : $social['id'],
        ]);
        if (session('social_redirect')) {
            return redirect(session('social_redirect') . '#');
        }
        return redirect(route('profile_main'));
    }

    /**
     * Detach social account
     *
     * @param  string $provider
     * @return \Illuminate\Http\Response
     */
    public function detach($provider)
    {
        SocialAccount::where('social_type', $provider)
            ->where('user_id', \user()->id)
            ->delete();
        return redirect()->back();
    }
}
