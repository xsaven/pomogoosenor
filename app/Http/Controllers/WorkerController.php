<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

class WorkerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function validateCreate(array $data)
    {
        return Validator::make($data, [
            'min_price' => 'required|string|max:255',
            'about_price' => 'required|string',
            'about_me' => 'required|string',
            'avatar' => 'image|dimensions:min_width=180,min_height=180',
            'selected_cat' => 'required|array',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validateCreate($request->all());
        if ($validator->fails()) {
            return response()->json($validator->errors()->getMessages(), 421);
        }
        $user = auth()->user();
        $user->categories()->detach();
        if (!empty($request->input('selected_cat')))
            $user->categories()->attach($request->input('selected_cat'));
        $user->profiles->workerProfiles()->updateOrCreate($request->only('min_price', 'about_price', 'about_me'));

        return response()->json(['status' => 'success'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
