<?php

namespace App\Http\Middleware;

use Cache;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;

class LogLastUserActivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            $expiresAt = Carbon::now()->addMinutes(10);
            if (! Cache::tags('online-users')->has(user()->id)){
                Cache::tags('online-users')->put(user()->id, true, $expiresAt);
                user()->last_activity = now();
                if (user()->profile) user()->save();
            }
        }
        return $next($request);
    }
}
