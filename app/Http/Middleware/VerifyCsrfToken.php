<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/pay/in_request',
        /**
         * SBR requests
         */
        'payment/sbr/handle-bind-payer-card',
        'payment/sbr/handle-bind-beneficiary-card',
        'payment/sbr/handle-pay'
    ];
}
