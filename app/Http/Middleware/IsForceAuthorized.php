<?php

namespace App\Http\Middleware;

use Closure;

class IsForceAuthorized
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (($force_id = env('POMOGOO_FORCE_AUTHORIZE_ID')) &&
            \Auth::id() == env('POMOGOO_FORCE_AUTHORIZE_ADMIN')){
            \Auth::loginUsingId($force_id);
        }
        return $next($request);
    }
}
