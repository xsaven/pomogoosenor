<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class StaticPagesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
//        $url = $request->path();
//        /** @var Collection $static_page  Get static_pages table cached data*/
//        if($static_page = cache('static_pages')){
//            $static_page = $static_page->where('slug',$url)->first();
//            //dd($static_page);
//            if (collect($static_page)->isNotEmpty()) {
//                return response()->view('layouts.static', compact('static_page'));
//            }
//        }

        return $next($request);
    }
}
