<?php

namespace App\Http\Middleware;

use Closure;


class CheckRegistration
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    protected $except = [
        'finish-registration',
        'profile',
        'location',
        'logout'
    ];

    public function handle($request, Closure $next)
    {
        if (!in_array($request->segment(1),$this->except) ) {
            if (\Auth::check() && is_null(\Auth::user()->name) ) {
                return redirect('finish-registration');
            }
        }

        return $next($request);
    }
}
