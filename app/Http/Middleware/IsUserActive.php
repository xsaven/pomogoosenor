<?php

namespace App\Http\Middleware;

use Closure;

class IsUserActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check())
            if (!auth()->user()->verified) {
                if (!session()->has('register_status'))
                    session()->put('register_status', '0');
            } else
                if (session()->has('register_status'))
                    session()->forget('register_status');
        return $next($request);
    }
}
