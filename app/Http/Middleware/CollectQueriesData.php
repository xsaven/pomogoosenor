<?php

namespace App\Http\Middleware;

use Closure;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class CollectQueriesData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $orderLog = new Logger('api');
        $orderLog->pushHandler(new StreamHandler(storage_path('logs/api.log')), Logger::INFO);
        $orderLog->info('Request', [
            'url' => $request->getUri(),
            'params' => $request->toArray()
        ]);


        $response = $next($request);
        $response->header('queries_data', $this->getQueriesData());


        $orderLog->info('Response', [
            'response' => $response
        ]);

        return $response;
    }

    private function getQueriesData()
    {
        $queries = config('queries.all');
        $all_queries_count = count($queries);
        $unique_queries = count(collect($queries)->unique());
        $queries_data = [
            'all_count' => $all_queries_count,
            'unique' => $unique_queries,
            'duplicated' => $all_queries_count - $unique_queries
        ];
        return json_encode($queries_data);
    }
}
