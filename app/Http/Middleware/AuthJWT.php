<?php

namespace App\Http\Middleware;

use JWTAuth;
use Closure;

class AuthJWT
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        if ($request->headers->has('X-CSRF-TOKEN')) {
            return app(\App\Http\Middleware\EncryptCookies::class)->handle($request, function ($request) use ($next) {
                return $next($request);
            });
        } else {
            if (!isset($request->token)) {
                abort(401, 'Token is not provided');
            }
            if (isset($request->token)) {
                request()->query->set('token', $request->token);
                JWTAuth::setToken($request->input('token'));
                if (!$user = JWTAuth::authenticate()) {
                    abort(401, 'Пользователь с таким токеном не найден');
                }
                $response = $next($request);
                $response->headers->set('user', $user);
                return $response;
            }
        }

        return $next($request);
    }
}
