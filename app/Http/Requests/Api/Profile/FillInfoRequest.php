<?php

namespace App\Http\Requests\Api\Profile;


use App\Http\Requests\Api\ApiFormRequest;

class FillInfoRequest extends ApiFormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:25',
            'surname' => 'required|max:50',
            'email' => 'required|max:50',
            'patronymic' => 'required|max:50',
            'birthday' => 'required|date',
            'avatar' => 'image|dimensions:min_width=200,min_height=200',
            'phone' => 'required',


            'confirm_1' => 'required',
            'confirm_2' => 'required',
            'confirm_3' => 'required',

            'confidant.name' => 'required|max:50',
            'confidant.surname' => 'required|max:50',
            'confidant.status' => 'required|max:50',
            'confidant.phone' => 'required|max:50',
        ];
    }
}
