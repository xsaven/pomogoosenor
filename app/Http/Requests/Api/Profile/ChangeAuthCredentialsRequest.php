<?php

namespace App\Http\Requests\Api\Profile;


use App\Http\Requests\Api\ApiFormRequest;

class ChangeAuthCredentialsRequest extends ApiFormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'email' => 'email|unique:users,email,'.auth()->user()->id,
            'password' => 'confirmed|min:6',
		];
	}
}
