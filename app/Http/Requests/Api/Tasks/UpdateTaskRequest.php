<?php

namespace App\Http\Requests\Api\Tasks;


use App\Http\Requests\Api\ApiFormRequest;

class UpdateTaskRequest extends ApiFormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string',
            'description' => 'string',
            'data' => 'array',
            'email_report' => 'int',
            'executor_only' => 'int',
            'commentaries' => 'int',
        ];
    }
}
