<?php

namespace App\Http\Requests\Api\Tasks;


use App\Http\Requests\Api\ApiFormRequest;

class StoreOfferRequest extends ApiFormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'required|string',
            'cost' => 'required'
        ];
    }
}
