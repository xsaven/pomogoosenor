<?php

namespace App\Http\Requests\Api\Tasks;


use App\Http\Requests\Api\ApiFormRequest;

class StoreTaskRequest extends ApiFormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'cost' => 'required',
            'name' => 'required|string',
            'description' => 'required|string',
            'data' => 'array',
            'email_report' => 'int',
            'executor_only' => 'int',
            'commentaries' => 'int',
        ];
    }

    public function messages()
    {
        return [
            'agree_with_rules.required' => 'Вы должны принять правила использования даного сервиса',
            'cost.required' => 'Необходимо указать бюджет',
            'name.required' => 'Вы должны сообщить нам что вам нужно',
            'description.required' => 'Вы должны сообщить нам детали вашего пожелания, чтобы исполнители лучше оценили вашу задачу',
        ];
    }
}
