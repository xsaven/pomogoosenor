<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateHomeRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'sub-title' => 'required',
            'bottom-block-title' => 'required',
            'blog-title' => 'required',
            'search-placeholder' => 'required',
            'search-button' => 'required',
            'search-example' => 'required',
            'categories-title' => 'required',
            'all-categories' => 'required',
            'main-info-block' => 'required',
            'story-block-title' => 'required',
            'add-task-text' => 'required',
            'executor-text' => 'required',
            'right-bottom-block-title-1' => 'required',
            'right-bottom-block-title-2' => 'required',
            'right-bottom-block-title-3' => 'required',
            'all-tasks-button' => 'required',
        ];
    }
}
