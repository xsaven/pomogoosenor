<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateSubcategoriesRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'name' => 'required', 
            'slug' => 'required|unique:subcategories,slug,'.$this->subcategories, 
            'question' => 'required', 
            'description' => 'required', 
            'example_title' => 'required', 
            'hint_description',
            'price_from' => 'numeric|required', 
            'price_min' => 'numeric|required', 
            'order' => 'required', 
            'category_id' => 'required',
            'tags',
            'top_text' => 'required',
		];
	}
}
