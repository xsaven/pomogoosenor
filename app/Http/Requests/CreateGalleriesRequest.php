<?php

namespace App\Http\Requests;

use App\Http\Requests\Api\ApiFormRequest;

class CreateGalleriesRequest extends ApiFormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'title' => 'required', 
            'description' => 'required',
            'file'=>'required'
		];
	}

	public function messages()
    {
        return [
            'file.required' => 'Загрузите изображения для альбома'
        ];
    }

    public function attributes()
    {
        return [
            'title'=>'Название',
            'description'=>'Описание',
        ];
    }
}
