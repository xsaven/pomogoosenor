<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Pagination\LengthAwarePaginator;


class BaseCollection extends ResourceCollection
{
    public function resolve($request = null)
    {
        $base_response =  parent::resolve($request);
        if ($this->resource instanceof LengthAwarePaginator){
            $response = [
                'current_page' => $this->resource->currentPage(),
                'data' => $base_response,
                'from' => $this->resource->firstItem(),
                'last_page' => $this->resource->lastPage(),
                'next_page_url' => $this->resource->nextPageUrl(),
                'per_page' => $this->resource->perPage(),
                'prev_page_url' => $this->resource->previousPageUrl(),
                'to' => $this->resource->lastItem(),
                'total' => $this->resource->total(),
                ];
            if ($this->additional){
                $response = array_merge($response, $this->additional);
            }
            return $response;
        }
        return $base_response;
    }

}