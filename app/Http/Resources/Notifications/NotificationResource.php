<?php

namespace App\Http\Resources\Notifications;

use App\Category;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class UserResource
 * @package App\Http\Resources
 *
 */
class NotificationResource extends Resource
{
    const API_TYPES = [
        'App\Notifications\TaskOfferCreated' => 'offer',
        'App\Notifications\FinishTask' => 'offer',
        'App\Notifications\SubscribedCategoryTaskStored' => 'offer',
        'App\Notifications\ApproveTask' => 'system',
        'App\Notifications\UnApproveTask' => 'system',
        'App\Notifications\DeleteTask' => 'system',
        'App\Notifications\JobOffered' => 'offer',
        'App\Notifications\PaymentStatusChanged' => 'system',
        'App\Notifications\SetExecutor' => 'system',
        'App\Notifications\DataVerified' => 'system',
        'App\Notifications\WorkerVerified' => 'system',
        'App\Notifications\CommentCreated' => 'comment',
        'App\Notifications\ArbitrationTask' => 'system',
        'App\Notifications\ArbitrationUnApproveTask' => 'system',
        'App\Notifications\ArbitrationApproveTask' => 'system'
    ];

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'api_type' => self::API_TYPES[$this->type],
            'data' => $this->data,
            'read_at' => $this->read_at,
            'created_at' => \LocalizedCarbon::parse($this->created_at)->formatLocalized('%d %f'),
            'timestamp' => $this->created_at->timestamp
        ];
    }
}