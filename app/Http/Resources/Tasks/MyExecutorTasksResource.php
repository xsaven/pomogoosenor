<?php

namespace App\Http\Resources\Tasks;

use App\User;
use App\UserTasks;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class UserResource
 * @package App\Http\Resources
 * @mixin UserTasks
 */
class MyExecutorTasksResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'content_items'=>$this->content_items,
            'subcategory'=>$this->subcategory,
            'creator'=>$this->creator,
            'executor'=>$this->executor,
            'comments'=>$this->comments,
            'offers'=>$this->offers,
            'reviews'=>$this->when($this->relationLoaded('reviews'),function (){
                return $this->reviews;
            })
        ];
    }
}