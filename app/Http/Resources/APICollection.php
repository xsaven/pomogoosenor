<?php

namespace App\Http\Resources;

class APICollection extends BaseCollection
{

    public $resourceClassName;
    public function __construct($resource, string $resourceClassName)
    {
        $this->resourceClassName = $resourceClassName;
        parent::__construct($resource);
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return $this->resourceClassName::collection($this->collection);
    }
}