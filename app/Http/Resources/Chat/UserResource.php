<?php

namespace App\Http\Resources\Chat;

use App\Dialog;
use App\DialogMessage;
use App\Message;
use App\User;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class UserResource
 * @package App\Http\Resources
 * @mixin User
 */
class ChatUserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $last_message = [];
        if ($this->id != \user()->id) {
            /** @var DialogMessage $last_message */
            $last_message = DialogMessage::whereHas('participants', function ($q) {
                $q->where('user_id', user()->id);
            })->whereHas('participants', function ($q) {
                $q->where('user_id', $this->id);
            })->orderByDesc('created_at')->first();
        }

        //dump($this->resource, $this->check_locked);

        return [
            'id' => $this->id,
            'name' => $this->name,
            'online' => $this->online,
            'participants' => $this->participants,
            'profile' => [
                'avatar_thumb' => $this->profile ? $this->profile->avatar_thumb : asset("photos/defaults/avatar.png"),
                'phone' => $this->when($this->id != \user()->id, function () {
                    return $this->profile ? $this->profile->full_phone : "";
                }),
                'relation' => $this->when($this->id != \user()->id, function () {
                    return $this->getChatRelatedData(\user());
                })
            ],
            'last_activity_diff' => $this->last_activity_diff,
            'last_message' => $this->when($this->id != \user()->id, function () use ($last_message) {
                return $last_message??'';
            }),
            'last_read' => $this->when($this->id != \user()->id, function () use ($last_message) {
                if (!$last_message || $last_message->sender_id == \user()->id){
                    return true;
                }
                return $last_message->seen;
            })
        ];
    }
}