<?php

namespace App\Http\Resources\Profile;

use App\Category;
use App\Review;
use App\User;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class UserResource
 * @package App\Http\Resources
 * @mixin Category
 */
class CategoriesRateResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'category_name' => $this->name,
            'rate' => $this->subcategories,
            'tasks_count' => $this->id,
        ];
    }
}