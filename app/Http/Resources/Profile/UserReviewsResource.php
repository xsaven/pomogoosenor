<?php

namespace App\Http\Resources\Profile;

use App\Review;
use App\User;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class UserResource
 * @package App\Http\Resources
 *
 */
class UserReviewsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request = null)
    {
        return [
            'id' => $this->id,
            'positive' => $this->positive,
            'type' => $this->type,
            'text' => $this->text,
            'politeness' => $this->politeness,
            'punctuality' => $this->punctuality,
            'adequacy' => $this->adequacy,
            'price' => $this->price,
            'quality' => $this->quality,
            'created_at' => $this->created_at->toDateTimeString(),
            'user' => [
                'id'=>$this->user->id,
                'name'=>$this->user->name,
                'avatar'=>$this->user->profile->avatar_thumb,
            ],
            'task'=> [
                'id'=>$this->task->id,
                'name'=>$this->task->name
            ]
        ];
    }
}