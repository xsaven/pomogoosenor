<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\FormFields
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $shape
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FormFields whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FormFields whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FormFields whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FormFields whereShape($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FormFields whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FormFields whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FormFields extends BaseModel
{
    protected $table = 'form_fields';

    protected $hidden = [
        'id',
        'slug',
        'created_at',
        'updated_at'
    ];


}
