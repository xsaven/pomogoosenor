<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\UserTaskReports
 *
 * @property int $id
 * @property string $reason
 * @property string|null $description
 * @property int $user_id
 * @property int $user_task_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read mixed $reason_full
 * @property-read \App\UserTasks $task
 * @property-read \App\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\UserTaskReports onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTaskReports whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTaskReports whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTaskReports whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTaskReports whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTaskReports whereReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTaskReports whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTaskReports whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTaskReports whereUserTaskId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\UserTaskReports withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\UserTaskReports withoutTrashed()
 * @mixin \Eloquent
 */
class UserTaskReports extends Model
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $table = 'user_task_reports';

    protected $fillable = [
        'reason',
        'description',
        'user_id',
        'user_task_id'
    ];

    public static function boot()
    {
        parent::boot();

        UserTaskReports::observe(new UserActionsObserver);
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function task()
    {
        return $this->hasOne(UserTasks::class,'id','user_task_id');
    }

    public function reasons()
    {
        return $this->hasOne(ReportReasons::class,'id','reason');
    }

}