<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\VideoToUser
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VideoToUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VideoToUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VideoToUser whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class VideoToUser extends Model
{
    //
}
