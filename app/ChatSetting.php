<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ChatSetting
 *
 * @property int $id
 * @property string|null $title
 * @property string $keyword
 * @property int $is_active
 * @property int $value
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ChatSetting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ChatSetting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ChatSetting whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ChatSetting whereKeyword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ChatSetting whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ChatSetting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ChatSetting whereValue($value)
 * @mixin \Eloquent
 */
class ChatSetting extends Model
{
    protected $fillable = [
        'title', 'keyword', 'is_active', 'value'
    ];

    protected $casts = [
        'value' => 'integer'
    ];

    public function getByKeyWord($keyword)
    {
        return $this->where('keyword', $keyword)
            ->where('is_active', 1)
            ->first();
    }

    public function getAllP($paginate)
    {
        return $this->paginate($paginate);
    }


}
