<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Mar 2018 12:51:17 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * App\ContentTask
 *
 * @property int $id
 * @property int $task_id
 * @property int $content_id
 * @property-read \App\Content $content
 * @property-read \App\UserTasks $user_task
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContentTask whereContentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContentTask whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContentTask whereTaskId($value)
 * @mixin \Eloquent
 */
class ContentTask extends Eloquent
{
	protected $table = 'content_task';
	public $timestamps = false;

	protected $casts = [
		'task_id' => 'int',
		'content_id' => 'int'
	];

	protected $fillable = [
		'task_id',
		'content_id'
	];

	public function content()
	{
		return $this->belongsTo(Content::class);
	}

	public function user_task()
	{
		return $this->belongsTo(UserTasks::class, 'task_id');
	}
}
