<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class TestingSettings extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'testingsettings';
    
    protected $fillable = [
          'link',
          'text'
    ];
    

    public static function boot()
    {
        parent::boot();

        TestingSettings::observe(new UserActionsObserver);
    }
    
    
    
    
}