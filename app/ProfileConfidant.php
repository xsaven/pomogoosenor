<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ProfileConfidant
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $surname
 * @property string|null $status
 * @property string|null $phone
 * @property int $profile_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Profile $profiles
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfileConfidant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfileConfidant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfileConfidant whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfileConfidant wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfileConfidant whereProfileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfileConfidant whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfileConfidant whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProfileConfidant whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ProfileConfidant extends Model
{
    protected $fillable = [
        'name', 'surname', 'status', 'phone', 'profile_id',
    ];

    public function profiles()
    {
        return $this->belongsTo('App\Profile');
    }
}
