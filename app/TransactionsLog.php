<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 14 Feb 2018 15:05:07 +0000.
 */

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * App\TransactionsLog
 *
 * @property int $id
 * @property int $type
 * @property int $sender_id
 * @property int $recipient_id
 * @property int $amount
 * @property int $content
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\User|null $recipient
 * @property-read \App\User|null $sender
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionsLog filterByDate($from, $to)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionsLog filterByType($type)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionsLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionsLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionsLog whereRecipientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionsLog whereSenderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionsLog whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionsLog whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TransactionsLog extends Eloquent
{
    const PAYMENT_STATUS = [
        'Created' => 1,
        'PaymentProcessing' => 2,
        'PaymentProcessError' => 3,
        'Paid' => 4,
        'PayoutProcessing' => 5,
        'PayoutProcessError' => 6,
        'Completed' => 7,
        'Canceling' => 8,
        'CancelError' => 9,
        'Canceled' => 10,
        'PaymentHold' => 11,
        'PaymentHoldProcessing' => 12
    ];

    protected $table = 'transactions_log';

    protected $casts = [
        'type' => 'int',
        'amount' => 'int',
        'sender_id' => 'int',
        'recipient_id' => 'int',
    ];

    protected $fillable = [
        'type',
        'sender_id',
        'amount',
        'recipient_id'
    ];

    protected $hidden = [
        'sender_id',
        'recipient_id',
        'updated_at'
    ];

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id')
            ->select('id', 'name');
    }

    public function recipient()
    {
        return $this->belongsTo(User::class, 'recipient_id')
            ->select('id', 'name');
    }

    public function getTypeLabel()
    {
        return array_search($this->type, self::PAYMENT_STATUS);
    }

    /**
     * @param Builder $query
     * @param $type
     * @return Builder
     */
    public function scopeFilterByType(Builder $query, $type)
    {
        if ($type && $type != 0) {
            $query->where('type', $type);
        }
    }


    /**
     * @param Builder $query
     * @param $from
     * @param $to
     * @return Builder
     */
    public function scopeFilterByDate(Builder $query, $from, $to)
    {
        if ($from && $to) {
            $from = Carbon::parse($from)->startOfDay()->toDateTimeString();
            $to = Carbon::parse($to)->endOfDay()->toDateTimeString();
            $query->whereBetween('created_at', [$from, $to]);
        }
    }
}
