<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;

/**
 * App\Galleries
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Content[] $content_items
 * @property-read mixed $gallery_avatar
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Galleries whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Galleries whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Galleries whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Galleries whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Galleries whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Galleries whereUserId($value)
 * @mixin \Eloquent
 */
class Galleries extends Model {

    protected $table    = 'galleries';
    
    protected $fillable = [
          'title',
          'description',
          'user_id'
    ];

    protected $hidden = [
        'user_id',
        'created_at',
        'updated_at',
    ];

    protected $appends = [
        'gallery_avatar'
    ];
    

    public static function boot()
    {
        parent::boot();

        Galleries::observe(new UserActionsObserver);
    }
    
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function content_items()
    {
        return $this->belongsToMany(Content::class,'content_gallery','gallery_id','content_id');
    }

    public function getContentItemWithAvatar()
    {
        return $this->content_items()->wherePivot('is_avatar',true)->first();
    }

    public function getGalleryAvatarAttribute()
    {
        if ($this->getContentItemWithAvatar()) {
            return $this->getContentItemWithAvatar()->path;
        }
        return $this->content_items()->first()->path??'';
    }

    /**
     * @param User $user
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getListByUser(User $user)
    {
        return self::whereUserId($user->id)
            ->with('content_items')
            ->withCount('content_items')
            ->whereHas('content_items')
            ->get();
    }
    
}