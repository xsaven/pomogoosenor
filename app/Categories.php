<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Categories
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $icon
 * @property string|null $image
 * @property string|null $icon_image
 * @property string|null $slug
 * @property string|null $order
 * @property int|null $active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property string|null $marker
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Categories[] $children
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Categories onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereIconImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereMarker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Categories whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Categories withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Categories withoutTrashed()
 * @mixin \Eloquent
 */
class Categories extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'categories';
    
    protected $fillable = [
          'name',
          'slug',
          'order',
          'active',
          'parent_id',
          'icon',
          'featured',
    ];
    

    public static function boot()
    {
        parent::boot();

        Categories::observe(new UserActionsObserver);
    }
    
    public function children(){
        return $this->hasMany('\App\Categories','parent_id','id')->orderBy('order');
    }
    
    
}