<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 14 May 2018 12:15:49 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * App\UserTaskDeal
 *
 * @property string $id
 * @property array $deal_data
 * @property int $status
 * @property int $task_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\UserTasks $user_task
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTaskDeal whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTaskDeal whereDealData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTaskDeal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTaskDeal whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTaskDeal whereTaskId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTaskDeal whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class UserTaskDeal extends Eloquent
{
	public $incrementing = false;

	protected $casts = [
		'status' => 'int',
		'task_id' => 'int',
        'deal_data' => 'array'
	];

    protected $fillable = [
        'id',
		'deal_data',
		'status',
		'task_id'
	];

    const DEAL_STATUS = [
        'Created' => 0, // Сделка зарегистрирована в системе
        'PaymentProcessing' => 1, // Производится оплата сделки заказчиком
        'PaymentProcessError' => 2, // Ошибка в процессе оплаты сделки
        'Paid' => 3, // Сделка успешно оплачена заказчиком
        'PayoutProcessing' => 4, // Производится выплата исполнителю
        'PayoutProcessError' => 5, // Ошибка в процессе выплаты исполнителю
        'Completed' => 6, // Сумма сделки успешно выплачена исполнителю
        'Canceling' => 7, // Сделка отменяется (возврат заказчику)
        'CancelError' => 8, // Ошибка в процессе отмены сделки
        'Canceled' => 9, // Сделка успешно отменена
        'PaymentHold' => 10, // Средства захолдированы на карте
        'PaymentHoldProcessing' => 11, // Сделка в процессе завершения оплаты либо отмены
    ];

	public function user_task()
	{
		return $this->belongsTo(UserTasks::class, 'task_id');
	}
}
