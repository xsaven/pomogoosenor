<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Slides
 *
 * @property int $id
 * @property string $image
 * @property int $active
 * @property string|null $order
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Slides onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slides whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slides whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slides whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slides whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slides whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slides whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slides whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Slides withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Slides withoutTrashed()
 * @mixin \Eloquent
 */
class Slides extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'slides';
    
    protected $fillable = [
          'image',
          'active',
          'order'
    ];
    

    public static function boot()
    {
        parent::boot();

        Slides::observe(new UserActionsObserver);
    }
    
    
    
    
}