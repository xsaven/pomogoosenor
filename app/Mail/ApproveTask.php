<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApproveTask extends Mailable
{
    use Queueable, SerializesModels;

    public $title = 'Задание подтверждено модератором';
    public $task;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($task)
    {
        $this->task = $task;
    }

    public function build()
    {
        return $this->view('emails.approve')->with('task', $this->task)->subject($this->title)->to($this->task->creator->email);
    }

}
