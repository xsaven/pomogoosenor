<?php

namespace App\Mail;

use App\UserTasks;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyCreatorNewOffer extends Mailable
{
    use Queueable, SerializesModels;

    public $title = 'У Вас новое предложение';
    public $task;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(UserTasks $task)
    {
        $this->task = $task;
    }

    public function build()
    {
        return $this->view('emails.notifycreatornewoffer', ['task' => $this->task])->subject($this->title)->to($this->task->creator->email);
    }

}
