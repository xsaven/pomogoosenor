<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Verification extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
    public $password;
    public $url;
    public $title = 'Подтвердите почтовый адрес';
    public $url_text = 'Для подтверждения почты';


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($email, $password, $url)
    {
        $this->email = $email;
        $this->password = $password;
        $this->url = $url;
    }

    public function build()
    {
        return $this->view('emails.verification')
            ->with([
                'email' => $this->email,
                'password' => $this->password,
                'url' => $this->url,
                'title' => $this->title,
                'url_text' => $this->url_text,
            ])
            ->to($this->email);
    }

}
