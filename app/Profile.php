<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

/**
 * App\Profile
 *
 * @property int $id
 * @property float $balance
 * @property string|null $surname
 * @property string|null $birthday
 * @property int|null $sex
 * @property string|null $phone_code
 * @property string|null $phone
 * @property string|null $avatar_thumb
 * @property string|null $avatar_full
 * @property bool $is_worker
 * @property bool $moderated_worker
 * @property int|null $testing_step
 * @property int|null $quest_step
 * @property float $money
 * @property string|null $name
 * @property int $user_id
 * @property int|null $city_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $patronymic
 * @property bool $phone_verified
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Category[] $categories
 * @property-read \App\City|null $cities
 * @property-read \App\ProfileConfidant $confidant
 * @property-read mixed $age
 * @property-read mixed $birth
 * @property-read mixed $full_phone
 * @property-read \App\ProfileConfidant $profileConfidants
 * @property-read \App\User $users
 * @property-read \App\WorkerProfile $workerProfile
 * @property-read \App\WorkerProfile $workerProfiles
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereAvatarFull($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereAvatarThumb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereIsWorker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereModeratedWorker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile wherePatronymic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile wherePhoneCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile wherePhoneVerified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereQuestStep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereTestingStep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereUserId($value)
 * @mixin \Eloquent
 */
class Profile extends BaseModel
{

    const DEFAULT_SEX = 0;
    const DEFAULT_CITY = 1;
    const DEFAULT_BIRTHDAY = null;
    const DEFAULT_AVATAR = '/photos/defaults/avatar.png';

    protected $fillable = [
        'surname', 'birthday', 'sex', 'phone_code', 'phone', 'avatar_thumb', 'avatar_full',
        'is_worker', 'testing_step', 'quest_step', 'user_id', 'city_id', 'name', 'patronymic', 'balance',
        'phone_verified'
    ];

    protected $appends = [
        'birth',
        'age',
        'full_phone'
    ];

    protected $hidden = [
        'city_id',
        'user_id',
        'birthday',
    ];

    protected $casts = [
        'documents_moderated' => 'boolean',
        'interview_complete' => 'boolean',
        'is_worker' => 'boolean',
        'phone_verified' => 'boolean',
        'moderated_worker' => 'boolean',
        'money' => 'float',
    ];

    protected $attributes = [
        'birthday' => self::DEFAULT_BIRTHDAY,
        'testing_step' => 0,
        'quest_step' => 0,
        'sex' => self::DEFAULT_SEX,
        'phone_code' => '',
        'phone' => '',
        'moderated_worker' => false,
        'is_worker' => false,
        'money' => 0,
        'city_id' => self::DEFAULT_CITY,
        'avatar_thumb' => self::DEFAULT_AVATAR,
        'avatar_full' => self::DEFAULT_AVATAR,
        'name' => '',
        'surname' => '',
        'patronymic' => ''
    ];


    protected static function boot()
    {
        parent::boot();

        static::saving(function (Profile $profile) {
            $profile->phone_code = $profile->phone_code ?? "+7";
            $profile->city_id = $profile->city_id ?? self::DEFAULT_CITY;
        });
    }

    public function getBirthdayAttribute($date)
    {
        return Carbon::parse($date);
    }

    public function setBirthdayAttribute($value)
    {
        $this->attributes['birthday'] = Carbon::parse($value)->toDateString();
    }

    public function getBirthAttribute($date)
    {
        return $this->birthday->toDateString();
    }

    public function getAgeAttribute($date)
    {
        /** @var Carbon $birthday */
        $birthday = $this->birthday;
        return $birthday->age;
    }

    public function getSexAttribute($sex)
    {
        return ($sex == 1) ? 'Женщина' : 'Мужчина';
    }

    public function getFullPhoneAttribute()
    {
        return $this->phone_code . $this->phone;
    }

    public function getDecimalPhone()
    {
        return preg_replace('/\D/', '', $this->full_phone);
    }

    /*
     * RELATION
     */
    public function users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function cities()
    {
        return $this->belongsTo('App\City', 'city_id');
    }

    public function views()
    {
        return $this->hasMany(ProfileView::class, 'profile_id');
    }

    public function profileConfidants()
    {
        return $this->hasOne('App\ProfileConfidant', 'profile_id');
    }

    public function workerProfiles()
    {
        return $this->hasOne('App\WorkerProfile', 'profile_id')->withDefault(true);
    }

    public function workerProfile()
    {
        return $this->hasOne('App\WorkerProfile', 'profile_id')->withDefault(true);
    }


    public function getAvatarThumbAttribute($data)
    {
        return url('/') . $data;
    }

    public function getAvatarFullAttribute($data)
    {
        return url('/') . $data;
    }

    /**
     * Throw exception if testing step is not equals to $value var
     *
     * @param integer $value
     */
    public function abortIfStepNotEquals($value)
    {
        if ($this->testing_step != $value) {
            error('Вы не можете приступить к этому шагу');
        }
    }

    /**
     * Throw exception if testing step is not equals to $value var
     *
     * @param integer $value
     */
    public function abortIfQuestionStepNotEquals($value)
    {
        if ($this->quest_step != $value) {
            error('Вы не можете приступить к этому шагу');
        }
    }

    /**
     *  Make this user worker
     */
    public function makeWorker()
    {
        $this->is_worker = true;
        $this->save();
    }

    /**
     * Set phone_verified value
     *
     * @param bool $value
     */
    public function setPhoneVerified($value)
    {
        $this->phone_verified = $value;
        $this->save();
    }

    /*
     * MODEL GET
     */


    /*
     * OLD CODE (NEED REFACTOR)
     */
    public function switchTestingStep($new_step)
    {
        $this->testing_step = $new_step;
        $this->save();
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function confidant()
    {
        return $this->hasOne('\App\ProfileConfidant', 'profile_id', 'id');
    }

    public function registeredFrom()
    {
        $date = new Date($this->created_at);
        return $date->format('j F Y');
    }

    public function avatar($size = 'thumb')
    {
        $property = 'avatar_' . $size;
        if ($this->$property) {
            return url($this->$property);
        } else {
            return url('public/photos/defaults/avatar.png');
        }
    }

    public function getViews() {
        return $this->views()->count();
    }
}

