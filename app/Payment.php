<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Payment
 *
 * @property int $id
 * @property int $order
 * @property int $status
 * @property int $sender_id
 * @property int $receiver_id
 * @property float $amount
 * @property string $description
 * @property string $hash
 * @property string|null $error_message
 * @property string|null $payment_response
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereErrorMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment wherePaymentResponse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereReceiverId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereSenderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Payment extends Model
{

    public function setHashAttribute($array)
    {
        list($order, $receiver_id, $amount) = $array;
        $this->attributes['hash'] = md5("|{$order}|{$receiver_id}|{$amount}|");
    }

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }

    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiver_id');
    }

}
