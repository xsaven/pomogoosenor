<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\City
 *
 * @property int $id
 * @property int $id_region
 * @property int $id_country
 * @property string $name
 * @property int|null $priority
 * @property string|null $name_url
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Country $country
 * @property-read \App\Region $region
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereIdCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereIdRegion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereNameUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class City extends BaseModel
{
    protected $table = 'city';

    protected $hidden = ['created_at', 'updated_at', 'id_country', 'priority', 'name_url', 'id_region'];

    public function country()
    {
        return $this->hasOne('App\Country', 'id', 'id_country');
    }

    public function region()
    {
        return $this->hasOne(Region::class, 'id', 'id_region');
    }

    public function findCityByName($name)
    {
        return $this->where('name', '=', $name)->first();
    }
}
