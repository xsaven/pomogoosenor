<?php
/**
 * Created by PhpStorm.
 * User: Uer
 * Date: 28.02.2018
 * Time: 17:55
 */

namespace App\Helpers\SVGViewer;


use View;

class SvgViewer
{
    private $file_path = null;

    public function __construct($file_path = false)
    {
        if ($file_path) {
            $this->file_path = $file_path;
        }
        View::addNamespace('SVG', __DIR__	);
    }

    public function setFile($file_path)
    {
        $this->file_path = $file_path;
        return $this;
    }

    public static function getIconsFromSvg($file_path)
    {
        $file_data = file_get_contents($file_path);
        $matches = [];
        preg_match_all('/<symbol id="(.*)" /', $file_data, $matches);
        return $matches[1];
    }

    public function rengerSvg()
    {
        $file_path = $this->file_path;
        $icons = self::getIconsFromSvg($file_path);
        $html_data = '';
        return view('SVG::list', compact('icons','file_path'));
    }


}