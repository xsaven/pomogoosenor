<?php

if (!function_exists('symbolSvg')) {
    function symbolSvg($name)
    {
        return url('/s/images/useful/svg/theme/symbol-defs.svg#' . $name);
//            return url('require/dist/s/images/useful/svg/theme/symbol-defs.svg#'.$name);
    }
}

if (!function_exists('get_setting')) {
    function get_setting($key)
    {
        $setting = \App\Setting::where('key', $key)->first();
        return isset($setting->value)?$setting->value:'';
    }
}

if (!function_exists('get_static_info')) {
    function get_static_info($key)
    {
        $setting = \App\StaticMainInfo::where('key', $key)->first();
        return isset($setting->value)?$setting->value:'';
    }
}

if (!function_exists('user')) {
    function user()
    {
        return Auth::user();
    }
}


if (!function_exists('error')) {
    function error($message, array $headers = [])
    {
        return new \App\Exceptions\Json\JsonError($message ,$headers);
    }
}

if (!function_exists('fail_message')) {
    function fail_message($message, $code = 400, array $headers = [])
    {
        throw new \App\Exceptions\FailMessageException($message);
    }
}

if (!function_exists('now')) {
    /**
     * Get a Carbon instance for the current date and time.
     *
     * @return \Carbon\Carbon
     */
    function now()
    {
        return \Carbon\Carbon::now();
    }
}

if (!function_exists('json_decode_try')) {
    /**
     * @param $json
     * @return mixed
     */
    function json_decode_try($json) {
        $decoded = json_decode($json,true);
        if(json_last_error() == JSON_ERROR_NONE){
            return $decoded;
        }
        return $json;
    }
}

if (!function_exists('stringify_json')) {
    function stringify_json($json) {
        array_walk_recursive($json,function (&$item){
            $item = strval($item);
        });
        return $json;
    }
}


if (!function_exists('uploads_path')) {
    function uploads_path($path) {
        return url('/').'/uploads/'.$path;
    }
}

if (!function_exists('mb_ucfirst')) {
    function mb_ucfirst($string, $encoding = 'UTF-8')
    {
        $strlen = mb_strlen($string, $encoding);
        $firstChar = mb_substr($string, 0, 1, $encoding);
        $then = mb_substr($string, 1, $strlen - 1, $encoding);
        return mb_strtoupper($firstChar, $encoding) . $then;
    }
}



if (!function_exists('getRequestFillable')) {
    /**
     * Get 'request' input filtered by '$fillable' variable from model
     *
     * @param string $molel Class name of the model
     * @return mixed
     */
    function getRequestFillable($molel)
    {
        /** @var Illuminate\Database\Eloquent\Model $model */
        $model = (new $molel());
        $fillable = $model->getFillable();
        $request_keys = array_keys(request()->all());
        return request()->only(array_intersect($fillable, $request_keys));
    }
}

if (!function_exists('session_flash')) {
    /**
     * Flash a piece of data to the session.
     *
     * @param  string|array  $key
     * @param  mixed  $value
     */
    function session_flash($key, $value = null)
    {
        $key = is_array($key) ? $key : [$key => $value];

        foreach ($key as $k => $v) {
            request()->session()->flash($k, $v);
        }
    }
}