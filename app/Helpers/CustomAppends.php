<?php

namespace App\Helpers;

use Illuminate\Database\Eloquent\Model;

/**
 * Trait CustomAppends
 * @package App\Helpers
 * @mixin Model
 */
trait CustomAppends{

    /**
     * @var bool
     */
    public static $withoutAppends = false;

    /**
     * Check if $withoutAppends is enabled.
     *
     * @return array
     */
    protected function getArrayableAppends()
    {
        if(self::$withoutAppends){
            return [];
        }
        return parent::getArrayableAppends();
    }

    /**
     * Ignore appends mutators
     *
     * @return $this
     */
    public function withoutAppends()
    {
        self::$withoutAppends = true;
        return $this;
    }
}