<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 18.09.18
 * Time: 17:20
 */

namespace App\Helpers;


class DateTime
{
    public static function formatted($int, $expr)
    {
        $count = $int % 100;
        if ($count >= 5 && $count <= 20) {
            $result = $int . " " . $expr['2'];
        } else {
            $count = $count % 10;
            if ($count == 1) {
                $result = $int . " " . $expr['0'];
            } elseif ($count >= 2 && $count <= 4) {
                $result = $int . " " . $expr['1'];
            } else {
                $result = $int . " " . $expr['2'];
            }
        }
        return $result;
    }

}