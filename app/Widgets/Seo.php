<?php

namespace App\Widgets;

use App\SeoData;
use Arrilot\Widgets\AbstractWidget;
use Cache;

class Seo extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * @return \Illuminate\Support\Collection
     */
    protected function getCachedSeo()
    {
        if(!($seo = cache('seo_data'))){
            $seo = Cache::forever('seo_data',SeoData::all());
        }
        return collect($seo);
    }

    /**
     * @return bool
     */
    protected function urlMatchPattern($mask)
    {
        return str_is(ltrim($mask,'/'), request()->path());
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $seo_data = [];
        $this->getCachedSeo()->each(function ($item) use (&$seo_data){
            if($this->urlMatchPattern($item->url_mask)){
                $seo_data = $item;
            }
        });
        return view('widgets.seo')->withSeoData($seo_data);
    }
}
