<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Slides;

class Slider extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = ['show' => 4];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run() {
        $slides = Slides::where('active','1')->orderBy('order','ASC')->take($this->config['show'])->get();
        return view('widgets.slider', compact('slides'));
    }
}
