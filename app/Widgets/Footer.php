<?php

namespace App\Widgets;

use App\PageConfig;
use Arrilot\Widgets\AbstractWidget;

class Footer extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $footer = PageConfig::where('key', '=', 'footer')->first();

        return view('widgets.footer', [
            'data' => $footer->value,
            'config' => $this->config,
        ]);
    }
}
