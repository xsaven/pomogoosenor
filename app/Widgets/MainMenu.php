<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Category;

class MainMenu extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'classes' => '',
        'display' => 'all'
    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        return view('widgets.main_menu', [
            'config' => $this->config,
            'logged_in' => \Auth::check(),
            'cat_tree' => Category::getTree(),
            'defaultSlug' => \App\Subcategory::where('default','=',1)->first()->slug
        ]);
    }
}
