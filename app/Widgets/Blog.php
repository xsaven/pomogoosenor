<?php

namespace App\Widgets;

use App\Http\Controllers\BlogController;
use App\Posts;
use Arrilot\Widgets\AbstractWidget;

class Blog extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'postsCount'=>4
    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $posts = Posts::orderBy('created_at', 'DESC')->limit($this->config['postsCount'])->get();
        $mainPost = $posts->first();
        if ($mainPost){
            $posts = $posts->slice(1, $posts->count());
        }
        return view('widgets.blog', compact('posts','mainPost'));
    }
}
