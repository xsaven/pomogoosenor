<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

class SocialShare extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'message'=>'',
        'url'=>'',
        'flat'=>false
    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        if ($this->config['url'] == ''){
            $this->config['url'] = url('/');
        }
        return view('widgets.social_share', [
            'config' => $this->config,
        ]);
    }
}
