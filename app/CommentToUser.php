<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\CommentToUser
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CommentToUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CommentToUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CommentToUser whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CommentToUser extends Model
{
    //
}
