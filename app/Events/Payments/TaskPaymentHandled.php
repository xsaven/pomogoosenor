<?php

namespace App\Events\Payments;

use App\UserTaskDeal;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TaskPaymentHandled
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var UserTaskDeal $deal
     */
    public $deal;

    public $task;
    public $deal_parameters;

    /**
     * Create a new event instance.
     *
     * @param UserTaskDeal $deal
     */
    public function __construct(UserTaskDeal $deal, $deal_parameters)
    {
        $this->deal = $deal;
        $this->task = $deal->user_task;
        $this->deal_parameters = $deal_parameters;
    }
}
