<?php

namespace App\Events;

use App\UserTasks;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class TaskViewed
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $task;

    /**
     * Create a new event instance.
     *
     * @param $task UserTasks
     *
     */
    public function __construct(UserTasks $task)
    {
        $this->task = $task;
    }
}
