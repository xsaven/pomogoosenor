<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ChangedSubscribedCategories
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var \App\User|null
     */
    public $user;

    /**
     * List of subscribed categories IDs
     *
     * @var array
     */
    public $categories;

    /**
     * Create a new event instance.
     *
     * @param array $categories
     */
    public function __construct($categories)
    {
        $this->user = user();
        $this->categories = $categories;
    }
}
