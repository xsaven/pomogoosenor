<?php

namespace App\Events;

use App\Events\ReviewStored;
use App\Http\Controllers\Payment\SBRController;
use App\Notifications\ArbitrationTask;
use App\Notifications\ArbitrationUnApproveTask;
use App\Review;
use App\Services\WalletOneSafeDeal\WOSafeDeal;
use App\UserTasks;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SetTaskState
{


    /**
     * Handle the event.
     *
     * @param  ReviewStored $event
     */
    public function handle(ReviewStored $event)
    {
        $task = $event->task->load('executor_review', 'creator_review');
        $this->check($task);
    }

    protected function check(UserTasks $task)
    {

        if ((int)$task->getOriginal('status') == UserTasks::STATUS_IN_PROGRESS) {

            if ($task->creator_review && $task->creator_review->positive) {
                return $this->submitTask($task);
            }
            if ($task->executor_review && !$task->executor_review->positive) {
                return $this->cancelTask($task);
            }
            if ($task->creator_review && !$task->creator_review->positive && $task->creator_review->average_mark < 3) {
                return $this->sendToArbitration($task);
            }
            if ($task->executor_review && $task->creator_review) {
                if ($task->executor_review->positive && !$task->creator_review->positive) {
                    return $this->sendToArbitration($task);
                }
            }

        }

        if ($task->getOriginal('status') == UserTasks::STATUS_IN_ARBITRATION && !$task->executor_review->positive) {
            $task->status = UserTasks::STATUS_CANCELED;
            $task->save();

            $task->creator->notify(new ArbitrationUnApproveTask($task));
            $task->executor->notify(new ArbitrationUnApproveTask($task));

            return $this->cancelTask($task);
        }
    }

    protected function sendToArbitration(UserTasks $task)
    {
        if ($task->executor) $task->executor->notify(new ArbitrationTask($task));
        $task->creator->notify(new ArbitrationTask($task));
        $task->status = UserTasks::STATUS_IN_ARBITRATION;
        $task->save();
        return true;
    }

    protected function cancelTask(UserTasks $task)
    {
        WOSafeDeal::deal()->cancelDeal($task->deal->id);

        $task->status = UserTasks::STATUS_CANCELED;
        $task->save();

        return true;
    }

    protected function submitTask(UserTasks $task)
    {
        try {
            WOSafeDeal::deal()->completeDeal($task->deal->id);

            $task->status = UserTasks::STATUS_FINISHED;
            $task->save();

        } catch (\Exception $exception) {
            SBRController::log(['error' => $exception->getMessage(), 'file' => $exception->getFile(), 'line' => $exception->getLine()]);
        }

        return true;
    }
}
