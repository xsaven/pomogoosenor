<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Pattern
 *
 * @property int $id
 * @property string $name
 * @property int|null $active
 * @property string|null $order
 * @property array $tasks
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Question[] $questions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Answer[] $right_answers
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Pattern onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pattern whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pattern whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pattern whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pattern whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pattern whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pattern whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pattern whereTasks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pattern whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Pattern withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Pattern withoutTrashed()
 * @mixin \Eloquent
 */
class Pattern extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'patterns';
    
    protected $fillable = [
          'name',
          'active',
          'order',
          'tasks',
    ];

    protected $hidden = [
        'id',
        'tasks',
        'active',
        'order',
        'created_at',
        'updated_at',
        'deleted_at',
        ];

    protected $casts = [
        'tasks'=>'array'
    ];
    

    public static function boot()
    {
        parent::boot();

        Pattern::observe(new UserActionsObserver);
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function right_answers()
    {
        return $this->hasManyThrough(
            Answer::class,
            Question::class,
            'pattern_id',
            'question_id',
            'id',
            'id'
        )->where('is_true',true);
    }

    public static function loadQuestions()
    {
        return self::with('questions.answers')
            ->whereActive(1)
            ->orderBy('order', 'ASC')
            ->get();
    }

}