<?php
/**
 * Created by PhpStorm.
 * User: Uer
 * Date: 26.03.2018
 * Time: 22:51
 */

namespace App;


use App\Http\Resources\APICollection;
use App\libraries\ModelCache\Traits\Cachable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\BaseModel
 *
 * @mixin \Eloquent
 */
class BaseModel extends Model
{
//    use Cachable;

    /**
     * Return APICollection representation
     *
     * Usage:
     *
     * $model_instance->toApiCollection(ModelResource::class)
     *
     * @param $resourceClassName
     * @return APICollection
     */
    public function toApiCollection($resourceClassName)
    {
        return new APICollection($this, $resourceClassName);
    }

}