<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Message
 *
 * @property int $id
 * @property string $from
 * @property string $to
 * @property string|null $message
 * @property \Carbon\Carbon $time
 * @property int|null $is_read
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\User $fromUsers
 * @property-read \App\User $toUsers
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereIsRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Message extends Model
{
    protected $fillable = [
        'from', 'to', 'message', 'is_read', 'time'
    ];

    protected $dates = ['time'];

    public function fromUsers()
    {
        return $this->belongsTo(User::class, 'from');
    }

    public function toUsers()
    {
        return $this->belongsTo(User::class, 'to');
    }



}
