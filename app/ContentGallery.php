<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 13 Feb 2018 12:22:26 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * App\ContentGallery
 *
 * @property int $id
 * @property int $gallery_id
 * @property int $content_id
 * @property bool $is_avatar
 * @property-read \App\Content $content
 * @property-read \App\Galleries $gallery
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContentGallery whereContentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContentGallery whereGalleryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContentGallery whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContentGallery whereIsAvatar($value)
 * @mixin \Eloquent
 */
class ContentGallery extends Eloquent
{
	protected $table = 'content_gallery';
	public $timestamps = false;

	protected $casts = [
		'gallery_id' => 'int',
		'content_id' => 'int',
		'is_avatar' => 'bool'
	];

	protected $fillable = [
		'gallery_id',
		'content_id',
		'is_avatar'
	];

	public function content()
	{
		return $this->belongsTo(Content::class);
	}

	public function gallery()
	{
		return $this->belongsTo(Galleries::class);
	}
}
