<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class PerformersReview extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'performersreview';

    protected $fillable = [
          'user_id',
          'text',
          'source'
    ];


    public static function boot()
    {
        parent::boot();

        PerformersReview::observe(new UserActionsObserver);
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }





}