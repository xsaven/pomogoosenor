<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;

/**
 * App\Task
 *
 * @property int $id
 * @property string $name
 * @property int|null $price
 * @property int $status
 * @property string|null $address
 * @property float|null $latitude
 * @property float|null $longitude
 * @property int $is_sbr
 * @property int $is_regular
 * @property int|null $price_range_id
 * @property int $subcategory_id
 * @property int $creator_id
 * @property int|null $executor_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\User $creator
 * @property-read \App\Review $creator_review
 * @property-read \App\User|null $executor
 * @property-read \App\Review $executor_review
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Review[] $reviews
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Subcategory[] $subcategory
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Task onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereCreatorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereExecutorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereIsRegular($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereIsSbr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task wherePriceRangeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereSubcategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Task withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Task withoutTrashed()
 * @mixin \Eloquent
 */
class Task extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $table = 'tasks';

    protected $fillable = [
        'name',
        'price',
        'status',
        'address',
        'latitude',
        'longitude',
        'is_sbr',
        'is_regular',
        'price_range_id',
        'subcategory_id',
        'creator_id',
        'executor_id',
    ];

    public static function boot()
    {
        parent::boot();

        self::observe(new UserActionsObserver);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subcategory()
    {
        return $this->hasMany(Subcategory::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function executor_review()
    {
        return $this->hasOne(Review::class)->where('type',0);
    }

    public function creator_review()
    {
        return $this->hasOne(Review::class)->where('type',1);
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id');
    }

    public function executor()
    {
        return $this->belongsTo(User::class, 'executor_id');
    }
}
