<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class TasksFilterCities extends Model
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $table = 'tasks_filter_cities';

    protected $fillable = [
        'name',
        'google_name',
        'type',
        'active'
    ];


    public static function boot()
    {
        parent::boot();

        TasksFilterCities::observe(new UserActionsObserver);
    }

    public function city()
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }


}