<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 14 Mar 2018 14:17:28 +0000.
 */

namespace App;

use Carbon\Carbon;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * App\TaskOffers
 *
 * @property int $id
 * @property int $user_id
 * @property int $task_id
 * @property string $message
 * @property int $other_notification
 * @property \Carbon\Carbon|null $deactivate_date
 * @property float $cost
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\UserTasks $task
 * @property-read \App\User $user
 * @property-read string $cost_with_fee
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskOffers whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskOffers whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskOffers whereDeactivateDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskOffers whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskOffers whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskOffers whereOtherNotification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskOffers whereTaskId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskOffers whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskOffers whereUserId($value)
 * @mixin \Eloquent
 */
class TaskOffers extends BaseModel
{
    protected $table = 'offers_of_performers';

    protected $casts = [
        'user_id' => 'int',
        'task_id' => 'int',
        'other_notification' => 'int',
        'cost' => 'float'
    ];

    protected $dates = [
        'deactivate_date'
    ];

    protected $fillable = [
        'user_id',
        'task_id',
        'card_id',
        'message',
        'other_notification',
        'deactivate_date',
        'cost'
    ];

    protected $appends = ['cost_with_fee'];

    protected static function boot()
    {
        parent::boot();

        /*self::created(function ($offer) {
            User::find($offer->task->creator_id)->notify(new \App\Notifications\TaskOfferCreated($offer));
        });*/

        self::addGlobalScope(function ($q) {

            $q->where(function ($q) {
                $q->whereHas('task', function ($q) {
                    $q->where('deactivate_date', '>', now());
                    $q->orWhere('user_tasks.status', '=', UserTasks::STATUS_IN_PROGRESS);
                });

            });
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function task()
    {
        return $this->belongsTo(UserTasks::class, 'task_id');
    }

    public function card()
    {
        return $this->belongsTo(CreditCard::class, 'card_id');
    }

    private static function isUnique($task_id)
    {
        return self::where('user_id', user()->id)
            ->where('task_id', $task_id)
            ->get()
            ->isEmpty();
    }

    public static function store(UserTasks $task, $data)
    {
        if (!$task->is_creator) {
            if (!self::isUnique($task->id)) {
                fail_message('Вы уже оставили предложение к этому заданию');
            }
            if ($task->original['status'] == $task::STATUS_OPENED) {
                $data['task_id'] = $task->id;
                $data['user_id'] = user()->id;
                if ($data['deactivate_hours']) {
                    $data['deactivate_date'] = Carbon::now()
                        ->addHour($data['deactivate_hours'])
                        ->toDateTimeString();
                } else {
                    $data['deactivate_date'] = Carbon::now()->toDateTimeString();
                }
                return (boolean)self::create($data);
            }
        }
        return false;
    }

    public static function storeTwo(UserTasks $task, $data)
    {
        if (!$task->is_creator) {
            if (!self::isUnique($task->id)) {
                fail_message('Вы уже оставили предложение к этому заданию');
            }
            if ($task->original['status'] == $task::STATUS_OPENED) {
                $data['task_id'] = $task->id;
                $data['user_id'] = user()->id;
                if ($data['deactivate_hours']) {
                    $data['deactivate_date'] = Carbon::now()
                        ->addHour($data['deactivate_hours'])
                        ->toDateTimeString();
                } else {
                    $data['deactivate_date'] = Carbon::now()->toDateTimeString();
                }
                return self::create($data);
            }
        }
        return false;
    }

    public function scopeGetSorted($query)
    {
        if (request('sort') == 'date') {
            $offers = $query->orderByDesc('created_at')->get();
        }
        if (request('sort') == 'reviews') {
            $offers = $query->with('user')->get()->sortByDesc(function (TaskOffers $offer) {
                return $offer->user->positive_reviews_count;
            });
        }
        if (request('sort') == 'rate') {
            $offers = $query->with('user')->get()->sortByDesc(function (TaskOffers $offer) {
                return $offer->user->avg_mark['total'];
            });
        }
        return $offers;
    }

    public function getCostWithFeeAttribute()
    {
        $fee = TaskSettings::getByKey('fee', 10);
        return $this->cost + $this->cost * ($fee / 100);
    }
}
