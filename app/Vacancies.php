<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacancies extends Model {

	protected $fillable = [
	    'id',
	    'name',
        'salary',
        'experience',
        'city',
        'title',
        'content',
        'seo_description',
        'seo_keywords'
    ];

    protected $hidden = [
        ''
    ];
}
