<?php

namespace App;

use Cache;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Subcategory
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $question
 * @property string $description
 * @property string $example_title
 * @property string $hint_description
 * @property float $price_from
 * @property float $price_min
 * @property string $order
 * @property bool $default
 * @property int $category_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property array $form_fields
 * @property int $range_id
 * @property string|null $marker
 * @property-read \App\Category $category
 * @property-read mixed $fields
 * @property-read \App\PriceRangersMask $range
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Subcategory onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subcategory whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subcategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subcategory whereDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subcategory whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subcategory whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subcategory whereExampleTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subcategory whereFormFields($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subcategory whereHintDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subcategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subcategory whereMarker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subcategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subcategory whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subcategory wherePriceFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subcategory wherePriceMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subcategory whereQuestion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subcategory whereRangeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subcategory whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subcategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Subcategory withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Subcategory withoutTrashed()
 * @mixin \Eloquent
 */
class Subcategory extends BaseModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $casts = [
        'form_fields' => 'array',
        'default' => 'bool'
    ];

    protected $table = 'subcategories';

    protected $fillable = [
        'name',
        'slug',
        'question',
        'description',
        'example_title',
        'hint_description',
        'price_from',
        'price_min',
        'order',
        'default',
        'category_id',
        'form_fields',
        'range_id',
        'tags',
        'top_text',
    ];

    protected $hidden = [
        'order',
        'form_fields',
        'hint_description',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $appends = [
        'fields',
    ];

    public function CreateTaskUrlWithQuery($query)
    {
        $this->refresh();
        return route('task.new', [
            'category' => $this->category->slug,
            'subcategory' => $this->slug,
            'select' => true,
            'query_search' => $query
        ]);
    }

    public static function boot()
    {
        parent::boot();

        Subcategory::observe(new UserActionsObserver);
    }

    public function tasks()
    {
        return $this->hasMany(UserTasks::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getCategory()
    {
        return $this->hasOne('App\Category', 'id', 'category_id');
    }


    public function range()
    {
        return $this->belongsTo(PriceRangersMask::class, 'range_id');
    }

    public function getFieldsAttribute()
    {
        $fields_ids = collect($this->form_fields)->map(function ($item, $key) {
            return array_first(array_keys($item));
        })->toArray();
        $form_fields = Cache::remember('form_fields', Carbon::now()->addMinutes(5), function () {
            return FormFields::all();
        });
        return $form_fields->map(function ($field) use ($fields_ids) {
            $field->active = in_array($field->id, $fields_ids);
            return $field;
        });
    }

    protected function asJson($value)
    {
        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }

}