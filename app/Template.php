<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 13 Feb 2018 17:37:40 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * App\Template
 *
 * @property int $id
 * @property string $title
 * @property string $text
 * @property int $user_id
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Template whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Template whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Template whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Template whereUserId($value)
 * @mixin \Eloquent
 */
class Template extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'user_id' => 'int'
	];

	protected $fillable = [
		'title',
		'text',
		'user_id'
	];

	protected $hidden = [
	    'user_id'
    ];

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
