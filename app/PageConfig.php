<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class PageConfig extends Model
{

    protected $table = 'page_config';

    protected $fillable = [
        'key',
        'value'
    ];

    protected $casts = [
        'value' => 'array'
    ];
}
