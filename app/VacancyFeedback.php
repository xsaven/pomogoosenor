<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class VacancyFeedback extends Model
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $table = 'vacancyfeedback';

    protected $fillable = [
        'name',
        'second_name',
        'email',
        'cv',
        'vacancy_id'
    ];


    public static function boot()
    {
        parent::boot();

        VacancyFeedback::observe(new UserActionsObserver);
    }

    public function vacancy()
    {
        return $this->belongsTo(Vacancies::class, 'vacancy_id');
    }


}