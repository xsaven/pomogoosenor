<?php

namespace App;

use App\Decorators\Tasks\SubcategoryDecorator;
use App\Events\TaskCreated;
use App\Notifications\SetExecutor;
use App\Notifications\SubscribedCategoryTaskStored;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use LocalizedCarbon;
use function PHPSTORM_META\override;
use Date\DateFormat;

/**
 * App\UserTasks
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string|null $private_description
 * @property array $data
 * @property string|null $cost
 * @property int $email_report
 * @property int $executor_only
 * @property int $commentaries
 * @property int $subcategory_id
 * @property int $creator_id
 * @property int $executor_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $status
 * @property float $exact_cost
 * @property $base_cost
 * @property $photo
 * @property boolean $need_documents
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\TaskComments[] $comments
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Content[] $content_items
 * @property-read \App\User $creator
 * @property-read \App\Review $creator_review
 * @property-read \App\User $executor
 * @property-read \App\Review $executor_review
 * @property-read \App\CreditCard $executor_card
 * @property-read \App\CreditCard $creator_card
 * @property-read mixed $is_creator
 * @property-read mixed $is_executor
 * @property-read mixed $opened_to
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\TaskOffers[] $offers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UserTaskReports[] $reports
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Review[] $reviews
 * @property-read \App\StatusTask $status_task
 * @property-read \App\Subcategory $subcategory
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users_viewed
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UserTaskView[] $views
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTasks whereCommentaries($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTasks whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTasks whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTasks whereCreatorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTasks whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTasks whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTasks whereEmailReport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTasks whereExactCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTasks whereExecutorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTasks whereExecutorOnly($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTasks whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTasks whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTasks wherePrivateDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTasks whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTasks whereSubcategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTasks whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $creator_card_id
 * @property int|null $executor_card_id
 * @property int|null $city_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\TaskOffers[] $active_offers
 * @property-read \App\TasksFilterCities|null $city
 * @property-read \App\UserTaskDeal $deal
 * @property-read mixed $marker
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTasks whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTasks whereCreatorCardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTasks whereExecutorCardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTasks whereNeedDocuments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserTasks wherePhoto($value)
 */
class UserTasks extends BaseModel
{

    use SoftDeletes;

    protected $table = 'user_tasks';

    protected $perPage = 15;

    protected $fillable = ['name', 'description', 'private_description', 'data',
        'cost', 'exact_cost', 'email_report', 'executor_only', 'commentaries', 'subcategory_id',
        'creator_id', 'executor_id', 'status', 'creator_card_id', 'executor_card_id', 'photo', 'need_documents', 'city_id'];

    protected $casts = [
        'data' => 'array'
    ];

    protected $attributes = [
        'private_description' => '',
        'executor_id' => 0,
        'exact_cost' => 0,
        'status' => 1
    ];

    protected $withCount = [
        'views',
//        'comments'
    ];

    protected $appends = [
        'opened_to',
        'is_creator',
        'is_executor',
        'base_cost',
        'marker',
    ];

    protected $events = [
        'created' => TaskCreated::class,
    ];

    const USER_TYPE_EXECUTOR = 0;
    const USER_TYPE_CREATOR = 1;

    const STATUS_DRAFT = 0;
    const STATUS_OPENED = 1;
    const STATUS_IN_PROGRESS = 2;
    const STATUS_FINISHED = 3;
    const STATUS_CANCELED = 4;
    const STATUS_IN_ARBITRATION = 5;
    const STATUS_NOT_FINISHED = 6;
    const STATUS_MODERATE = 7;

    public static $status_data = [
        self::STATUS_DRAFT => 'draft',
        self::STATUS_OPENED => 'opened',
        self::STATUS_IN_PROGRESS => 'in-progress',
        self::STATUS_FINISHED => 'finished',
        self::STATUS_CANCELED => 'canceled',
        self::STATUS_IN_ARBITRATION => 'in-arbitration',
        self::STATUS_NOT_FINISHED => 'not-finished',
        self::STATUS_MODERATE => 'not-moderated',
    ];

    public function setDataAttribute($data)
    {
        if (isset($data['address'])) {
            if (is_string($data['address'])) {
                $data['address'] = json_decode_try($data['address']);
            }
        }
        $this->attributes['data'] = \GuzzleHttp\json_encode($data);
    }


    public function views()
    {
        return $this->hasMany(TaskView::class);
    }

    private static function setCostValue(UserTasks &$task)
    {
        if ($task->exact_cost) {
            $task->cost = 0;
        } else {
            if (!strpos($task->cost, '-') && $task->cost) {
                $ranges = PriceRangersMask::find($task->subcategory->range_id)->toArray()['formatted_data'];
                foreach ($ranges as $range) {
                    if (mb_ucfirst($range['value']) == mb_ucfirst($task->cost)) {
                        $task->cost = $range['name'] . ' - ' . $range['value'];
                    }
                }
            }

        }
    }

    private static function setCityValue(UserTasks $task)
    {
        if (isset($task->data['address'][0]['lat'])) {
            $address = $task->data['address'][0];
            if (is_numeric($address['lon']) && is_numeric($address['lat'])) {
                $city = self::getCityByLatLng($address['lat'], $address['lon']);
                if ($city) {
                    file_put_contents(public_path('/logs.txt'), serialize($city->id));

                    $task->flushEventListeners();
                    $task->city_id = $city->id;
                    $task->save();
                    file_put_contents(public_path('/logs.txt'), serialize($task->city->name));
                }
            }
        }
    }

    protected static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub
        self::addGlobalScope(function (Builder $q) {
            $q->addSelect(DB::raw("(select count(*) from `offers_of_performers` 
           where `user_tasks`.`id` = `offers_of_performers`.`task_id` 
           and (`deactivate_date` > '" . now()->toDateTimeString() . "' or user_tasks.status = " . UserTasks::STATUS_IN_PROGRESS . " )) as `offers_count`"));
        });
        self::creating(function (UserTasks $task) {
            self::setCostValue($task);
        });
        self::updating(function (UserTasks $task) {
            self::setCostValue($task);
        });

        self::updated(function (UserTasks $task) {
            self::setCityValue($task);
        });

        self::created(function (UserTasks $task) {
            self::setCityValue($task);
        });
    }

    /**
     * Store task to DB
     *
     * @param array $values
     * @return $this|Model
     */
    public static function store($values)
    {
        if ($values['subcategory_id'] == 0) {
            $values['subcategory_id'] = Category::first()->defaultSubcategory()->id;
        }
        $values['creator_id'] = user()->id;
        $values['status'] = $values['status'] ?? self::STATUS_OPENED;

        return self::create($values);
    }

    /**
     * Edit task
     *
     * @param array $values
     */
    public function edit($values)
    {
        $this->update($values);
    }

    private static function getTaskHistoryData()
    {
        return self::where('creator_id', user()->id)
            ->select('subcategory_id', 'name')
            ->orderByDesc('created_at')
            ->get();
    }

    private static function getTaskPopularData()
    {
        return self::where('created_at', '>', Carbon::now()->subDays(30))
            ->select('subcategory_id', 'name', DB::raw('count(*) as total'))
            ->groupBy('subcategory_id', 'name')
            ->orderByDesc('total')
            ->take(10)
            ->get();
    }

    private static function getTaskSearchData($query_string)
    {
        $partian_query = explode(' ', $query_string);
        $results = self::select('subcategory_id', 'name');

        foreach ($partian_query as $query) {
            $results->orWhere('name', 'like', '%' . $query . '%');
        }
        return $results->groupBy('subcategory_id', 'name')
            ->take(10)
            ->get();
    }

    /**
     * Set title attribute to subcategories collections
     *
     * @param Collection $subcategories
     * @param Collection $task_data
     * @return array
     */
    private static function setTitleToSubcategories($subcategories, $task_data)
    {
        foreach ($task_data as $task) {
            $formatted = $subcategories->where('id', $task->subcategory_id)->first()->toArray();
            $formatted['title'] = $task->name;
            $data[] = $formatted;
        }
        return $data ?? [];
    }

    private static function formatSubcategoriesRange($subcategories)
    {
        foreach ($subcategories as &$subcategory) {
            $subcategory['range'] = $subcategory['range']['formatted_data'];
        }
        return $subcategories;
    }

    /**
     * Get tasks history
     *
     * @return array
     */
    public static function getHistory()
    {
        $task_history_data = self::getTaskHistoryData();
        $subcategory_ids = $task_history_data->pluck('subcategory_id');
        $subcategories = Subcategory::with('range')->whereIn('id', $subcategory_ids)->get();
        $sub_data = self::setTitleToSubcategories($subcategories, $task_history_data);
        return self::formatSubcategoriesRange($sub_data);
    }

    /**
     * Get tasks popular search requests
     *
     * @return array
     */
    public static function getPopular()
    {
        $task_popular_data = self::getTaskPopularData();
        $subcategory_ids = $task_popular_data->pluck('subcategory_id');
        $subcategories = Subcategory::with('range')->whereIn('id', $subcategory_ids)->get();
        $sub_data = self::setTitleToSubcategories($subcategories, $task_popular_data);
        return self::formatSubcategoriesRange($sub_data);
    }

    /**
     * Get tasks popular search requests
     *
     * @return array
     */
    public static function searchSuggest($query)
    {
        $task_search_data = self::getTaskSearchData($query);
        $subcategory_ids = $task_search_data->pluck('subcategory_id');
        $subcategories = Subcategory::with('range')->whereIn('id', $subcategory_ids)->get();
        $sub_data = self::setTitleToSubcategories($subcategories, $task_search_data);
        return self::formatSubcategoriesRange($sub_data);
    }

    public function getStatusAttribute($status)
    {
        return self::$status_data[$status];
    }

    public function setExactCostAttribute($value)
    {
        $this->attributes['exact_cost'] = $value ?? 0;
    }


    public function subcategory()
    {
        return $this->belongsTo(Subcategory::class);
    }

    public function executor()
    {
        return $this->belongsTo(User::class, 'executor_id');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id');
    }

    public function users_viewed()
    {
        return $this->belongsToMany(User::class, 'user_tasks_view', 'user_tasks_id', 'user_id');
    }

    public function reports()
    {
        return $this->hasMany(UserTaskReports::class, 'user_task_id');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'task_id');
    }

    public function executor_review()
    {
        return $this->hasOne(Review::class, 'task_id')->where('type', 0);
    }

    public function creator_review()
    {
        return $this->hasOne(Review::class, 'task_id')->where('type', 1);
    }

    public function executor_card()
    {
        return $this->belongsTo(CreditCard::class, 'executor_card_id');
    }

    public function creator_card()
    {
        return $this->belongsTo(CreditCard::class, 'creator_card_id');
    }


    public function deal()
    {
        return $this->hasOne(UserTaskDeal::class, 'task_id');
    }

    public function status_task()
    {
        return $this->hasOne(StatusTask::class, 'task_id');
    }

    public function getIsCreatorAttribute()
    {
        return $this->creator_id == (user()->id ?? false);
    }

    public function getIsExecutorAttribute()
    {
        return $this->executor_id == (user()->id ?? false);
    }

    public function getOpenedToAttribute()
    {
        if (!isset($this->data['date-and-time'])) return null;
        $date = $this->data['date-and-time'];
        if (isset($date['to_date']) && $date['to_date'] != '0') {
            return $date['to_date'] . ' ' . $date['to_time'];
        } elseif (isset($date['from_date']) && $date['from_date'] != '0') {
            return $date['from_date'] . ' ' . $date['from_time'];
        }
        $this->status = self::STATUS_NOT_FINISHED;
        $this->save();
        return now()->subDays(2)->toDateTimeString();
    }

    /**
     * Retrieve task marker from category marker attribute
     *
     * @param array $marker
     * @return string
     */
    private function retrieveMarker($marker)
    {
        switch ($this->getOriginal('status')) {
            case self::STATUS_OPENED:
                return $marker['opened'];
            case self::STATUS_IN_PROGRESS:
                return $marker['in_progress'];
            case self::STATUS_FINISHED:
                return $marker['finished'];
            default:
                return $marker['finished'];
        }
    }

    public function markerImage()
    {
        $marker = $this->subcategory->category->marker;
        return $this->retrieveMarker($marker)['file'];
    }

    public function getMarkerAttribute()
    {
        $marker = $this->subcategory->category->marker;
        return $this->retrieveMarker($marker)['slug'];
    }

    public function content_items()
    {
        return $this->belongsToMany(Content::class, 'content_task', 'task_id', 'content_id');
    }

    public function getBaseCostAttribute()
    {
        if ($this->cost) {
            return explode('-', $this->cost)[1] ?? $this->cost;
        }
        return $this->exact_cost;
    }

    public function getExactCostAttribute($val)
    {
        return (int)$val;
    }

    public function getCreatedAtAttribute($attr)
    {
        return DateFormat::post($attr);
    }

    public function status()
    {
        return "<span style='color: " . trans('statuses.' . $this->status . '.color') . "'>" . trans('statuses.' . $this->status . '.title') . "</span>";
    }

    public function status_color()
    {
        return trans('statuses.' . $this->status . '.color');
    }

    public function comments()
    {
        return $this->hasMany(TaskComments::class, 'task_id');
    }

    public function offers()
    {
        return $this->hasMany(TaskOffers::class, 'task_id');
    }

    public function active_offers()
    {
        return $this->hasMany(TaskOffers::class, 'task_id')->where('deactivate_date', '>', now());
    }

    public function city()
    {
        return $this->belongsTo(TasksFilterCities::class, 'city_id', 'id');
    }

    public static function getAll($sort_data)
    {
        $executor = false;
        if ($user = user()) {
            $executor = $user->is_worker();
        }

        $filtersCityColllection = TasksFilterCities::all('city_id');

        //dd($sort_data['city']);

        $result = self::with(['subcategory.category', 'creator'])
            ->orderBy('created_at', 'desc')
            ->when($sort_data['no_offers'], function (Builder $q) {
                $q->whereDoesntHave('offers');
            })
            ->when($sort_data['q'], function (Builder $q) use ($sort_data) {
                $q->where('name', 'like', "%" . $sort_data['q'] . "%");
            })
            ->when($sort_data['subcategories'], function (Builder $q) use ($sort_data) {
                $q->whereIn('subcategory_id', $sort_data['subcategories']);
            })
            ->when($sort_data['city'], function (Builder $q) use ($sort_data) {
                $q->where(function ($q) use ($sort_data) {
                    $q->where('city_id', $sort_data['city'])->orWhere('city_id', '=', null);
                });
            })
            ->when(!$executor, function (Builder $q) {
                $q->where('executor_only', '<>', true);
            })
            ->when($sort_data['status'], function (Builder $q) use ($sort_data) {
                $q->where('status', '=', $sort_data['status']);
            })
            ->when(isset($sort_data['map']), function (Builder $q) use ($sort_data) {
                $q->whereIn('id', explode(',', $sort_data['map']));
            })
//            ->where('status', '<>', self::STATUS_DRAFT)
            ->paginate();

        return $result;

    }

    public static function withMyCreatorTasks()
    {
        return self::with('subcategory.category')
            ->where('creator_id', user()->id)
            ->orderBy('created_at', 'desc')
            ->paginate();
    }

    /**
     * @return Collection|static[]
     */
    public static function getTasksToOffer()
    {
        if (user()) {
            return self::where('creator_id', user()->id)
                ->orderBy('created_at', 'desc')
                ->whereStatus(self::STATUS_OPENED)
                ->get();
        }
    }

    public static function withMyExecutorTasks()
    {
        $tasks = self::with([
            'content_items',
            'subcategory',
            'creator',
            'executor',
            'offers'
        ]);
        $tasks->whereHas('offers', function ($q) {
            $q->where('user_id', user()->id);
        })->orWhere('executor_id', user()->id);

//        if ($category??false) {
//            $tasks->whereHas('subcategory.category', function ($q) use ($category) {
//                return $q->where('id', $category);
//            });
//        }
//        if ($status != 10) {
//            $tasks->where('status', $status);
//        }
//        if ($status == self::STATUS_FINISHED) {
//            $tasks->with(['reviews' => function ($q) {
//                return $q->whereHas('task', function ($q) {
//                    return $q->where('creator_id', \user()->id)
//                        ->orWhere('executor_id', user()->id);
//                });
//            },
//                'reviews.user']);
//        }
        return $tasks->orderByDesc('created_at')->paginate();
    }

    public function checkIfCreator()
    {
        if (!$this->is_creator) {
            fail_message('Вы не можете редактировать это задание');
        }
    }

    public function hasUserOffers($user_id)
    {
        return $this->offers()->where('user_id', $user_id)->get()->isNotEmpty();
    }


    /**
     * @param array $deal_params
     * @return $this|bool|\Illuminate\Database\Eloquent\Model
     */
    public function saveOrUpdateDeal($deal_params)
    {
        $data = [
            'id' => $deal_params['PlatformDealId'],
            'deal_data' => $deal_params,
            'status' => UserTaskDeal::DEAL_STATUS[$deal_params['DealStateId']],
        ];
        if ($this->deal) {
            return $this->deal->update($data);
        }
        return $this->deal()->create($data);
    }

    public function setExecutor($user_id)
    {
        $executor = User::find($user_id);
        if ($this->original['status'] == $this::STATUS_OPENED && $executor) {
            $this->executor_id = $executor->id;
            $this->status = $this::STATUS_IN_PROGRESS;
            $this->locked = false;
            $this->save();

            return true;
        }
    }

    /**
     * Get city by latitude and longitude
     *
     * @param string $latitude Latitude
     * @param string $longitude Longitude
     * @return mixed
     */
    public static function getCityByLatLng(string $latitude, string $longitude)
    {
        $response = \GoogleMaps::load('geocoding')
            ->setParam([
                'latlng' => $latitude . ',' . $longitude,
                "language" => "en",
            ])->get();
        $results = json_decode_try($response)['results'];
        if (isset($results[0])) {
            $address_components = (isset($results[2]) ? $results[2] : $results[0])['address_components'] ?? false;
            if ($address_components) {
                $filter_cities = TasksFilterCities::query()->orderByDesc('type')->get();
                foreach ($filter_cities as $filter_city) {
                    $administrative_area_level = 'administrative_area_level_' . $filter_city->type;
                    $address_component = collect($address_components)->filter(function ($component) use ($administrative_area_level) {
                        return in_array($administrative_area_level, $component['types']);
                    })->first();
                    if ($address_component && $address_component['long_name'] == $filter_city->google_name) {
                        return $filter_city;
                    }
                };
            }
        }
    }

    /**
     * Get location info by latitude and longitude
     *
     * @param string $latitude Latitude
     * @param string $longitude Longitude
     * @param int $area_level Administrative area level
     * @return string|null
     */
    public static function getInfoByLatLng(string $latitude, string $longitude, int $area_level = 1)
    {
        $administrative_area_level = 'administrative_area_level_' . $area_level;
        $response = \GoogleMaps::load('geocoding')
            ->setParam([
                'latlng' => $latitude . ',' . $longitude,
                "language" => "en",
            ])->get();
        $results = json_decode_try($response)['results'];
        if (isset($results[0])) {
            $address_components = (isset($results[2]) ? $results[2] : $results[0])['address_components'] ?? false;
            if ($address_components) {
                $address_components = collect($address_components)->filter(function ($component) use ($administrative_area_level) {
                    return in_array($administrative_area_level, $component['types']);
                });
                $city = $address_components->first()['long_name'];
                if ($city) {
                    return $city;
                }
            }
        }
    }
}




















