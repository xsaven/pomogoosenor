<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Slide
 *
 * @property int $id
 * @property string $image
 * @property int $active
 * @property string|null $order
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slide whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slide whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slide whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slide whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slide whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slide whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Slide whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Slide extends Model
{
    //
}
