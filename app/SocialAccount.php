<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\SocialAccount
 *
 * @property int $id
 * @property string $social_type
 * @property string $social_id
 * @property int $user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read mixed $caption
 * @property-read string $profile_url
 * @property-read mixed $social_type_short
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SocialAccount whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SocialAccount whereHasNotTrashedUser()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SocialAccount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SocialAccount whereSocialId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SocialAccount whereSocialType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SocialAccount whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SocialAccount whereUserId($value)
 * @mixin \Eloquent
 */
class SocialAccount extends Model
{
    protected $fillable = [
        'social_type', 'social_id'
    ];

    protected $hidden = ['created_at','updated_at','user_id'];

    protected $appends = [
        'profile_url',
        'social_type_short',
        'caption'
    ];

    /**
     * Short keys for social types
     *
     * @var array
     */
    public static $social_types_short = [
      'facebook'=>'fb',
      'vkontakte'=>'vk',
      'mailru'=>'ml',
      'google'=>'google',
      'twitter'=>'tw'
    ];

    /**
     * Captions for social types
     *
     * @var array
     */
    public static $social_types_captions = [
        'facebook'=>'Facebook',
        'vkontakte'=>'Вконтакте',
        'mailru'=>'Mail.ru',
        'google'=>'Google',
        'twitter'=>'Twitter'
    ];

    /*
     * Relation
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Check if has user that's not deleted
     *
     * @param Builder $q
     * @return Builder|static
     */
    public function scopeWhereHasNotTrashedUser(Builder $q)
    {
        return $q->whereHas('user',function (Builder $q){
            $q->whereNull('deleted_at');
        });
    }

    /**
     * Get social type short key
     *
     * @return mixed
     */
    public function getSocialTypeShortAttribute()
    {
        return self::$social_types_short[$this->social_type];
    }

    /**
     * Get social type caption
     *
     * @return mixed
     */
    public function getCaptionAttribute()
    {
        return self::$social_types_captions[$this->social_type];
    }

    /**
     * Get URL to social account
     *
     * @return string
     */
    public function getProfileUrlAttribute()
    {
        if ($this->social_type='facebook')
            return 'https://www.facebook.com/app_scoped_user_id/'.$this->social_id;
        if ($this->social_type='vkontakte')
            return 'https://vk.com/id'.$this->social_id;
        if ($this->social_type='google')
            return 'https://plus.google.com/u/0/'.$this->social_id;
    }
}
