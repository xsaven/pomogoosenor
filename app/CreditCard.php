<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 08 May 2018 11:21:22 +0000.
 */

namespace App;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * App\CreditCard
 *
 * @property int $id
 * @property string $mask
 * @property int $type
 * @property int $user_id
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CreditCard beneficiaryOnly()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CreditCard payerOnly()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CreditCard whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CreditCard whereMask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CreditCard whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CreditCard whereUserId($value)
 * @mixin \Eloquent
 */
class CreditCard extends Eloquent
{
    protected $table = 'user_credit_cards';
    public $incrementing = false;
    public $timestamps = false;

    /**
     * Исполнитель
     */
    const BENEFICIARY_TYPE = 0;

    /**
     * Заказчик
     */
    const PAYER_TYPE = 1;

	protected $casts = [
		'id' => 'int',
		'type' => 'int',
		'user_id' => 'int'
	];

	protected $fillable = [
        'id',
		'mask',
		'type',
		'user_id'
	];

    public function getWalletOnePaymentToolKey()
    {
        if ($this->type == self::BENEFICIARY_TYPE){
            return 'BeneficiaryPaymentToolId';
        }
        return 'PayerPaymentToolId';
	}

    public function isBeneficiary()
    {
        return $this->type == self::BENEFICIARY_TYPE;
	}

    public function isPayer()
    {
        return $this->type == self::PAYER_TYPE;
    }

	public function user()
	{
		return $this->belongsTo(User::class);
	}

    public function scopeBeneficiaryOnly($query)
    {
        return $query->whereType(self::BENEFICIARY_TYPE)->get();
	}

    public function scopePayerOnly($query)
    {
        return $query->whereType(self::PAYER_TYPE)->get();
    }
}
