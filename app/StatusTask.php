<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\StatusTask
 *
 * @property int $id
 * @property int $task_id
 * @property int $user_id
 * @property string $reason
 * @property string|null $reason_description
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StatusTask whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StatusTask whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StatusTask whereReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StatusTask whereReasonDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StatusTask whereTaskId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StatusTask whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\StatusTask whereUserId($value)
 * @mixin \Eloquent
 */
class StatusTask extends Model
{
    protected $table = 'ch_status_tasks';

        protected $fillable = ['task_id', 'user_id', 'reason', 'reason_description', 'updated_at'];

        /**
         * no_proposals - Нет подходящих предложений
         * not_actual - Задание уже не актуально
         * other_executor - Исполнитель найден на другом ресурсе
         * self_made - Решил проблему самостоятельно
         * expensive - Дорого
         * other - Другая причина
         */
}
