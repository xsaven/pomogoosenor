$(document).on('click','#add_quest',function () {
   var UI = $('#patternsUI');
   var blank = UI.find('#blank_quest').clone().html();
   $('#pattern_content').append(blank);
});

$(document).on('click','.add_unswer',function () {
    var th = $(this);
    var UI = $('#patternsUI');
    var blank = UI.find('#blank_unswer').clone().html();
    th.parent().find('.unsers_wrapper').append(blank);
});

$('#form-with-validation').on('submit',function () {
    var form = $(this);
    // console.log(form.collectPattern());
    var input = $('<input name="questions" type="hidden">');
    input.val(JSON.stringify(form.collectPattern()));
    form.append(input);
    // return false;
})

$(document).on('click','.remove_block',function () {
    $(this).closest('.content_item').remove();
});

$.fn.extend({
    collectPattern: function () {
        var form = $(this);
        var wrapper = form.find('#pattern_content');
        var data = [];

        wrapper.find('.content_item').each(function () {
            var questBlock = $(this);
            var quest_title = questBlock.find('input.quest').val();
            var quest_id = questBlock.find('input.quest').attr('data-id');
            var unswersWrapper = questBlock.find('.unsers_wrapper');
            var answers = [];
            unswersWrapper.find('.unswer-collection').each(function () {
                var collection = $(this);
                var is_true = collection.find('input[type="checkbox"]').prop('checked');
                var answer_text = collection.find('input[type="text"]').val();
                var answer_id = collection.find('input[type="text"]').attr('data-id');
                answers.push({
                    'id': answer_id,
                    'title': answer_text,
                    'is_true': is_true
                });
            });
            data.push({
                'id': quest_id,
                'title' : quest_title,
                'answers' : answers
            })
        });
        return data;
    }
});