(function( $ ){

  $.fn.filemanager = function(type, options) {
    type = type || 'file';
    var th = $(this);
    this.on('click', function(e) {
      var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
      localStorage.setItem('target_input', $(this).data('input'));
      localStorage.setItem('target_preview', $(this).data('preview'));
      window.open(route_prefix + '?type=' + type, 'FileManager', 'width=1000,height=600');
      window.SetUrl = function (url, file_path) {
          //set the value of the desired input to image url
          var target_input = th.parent().find('.lfm_pick_input');
          target_input.val(file_path).trigger('change');
          th.parent().find('img').remove();
          th.parent().append('<img style="width: 30%; margin-top: 20px; height: auto;" src="'+file_path+'">');
          //set or change the preview image src
          var target_preview = $('#' + localStorage.getItem('target_preview'));
          target_preview.attr('src', url).trigger('change');
      };
      return false;
    });
  }

})(jQuery);
