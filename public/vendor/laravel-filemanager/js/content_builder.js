var UI = $('#postUI');
var editorN = 1;
$(document).on('click','#add_text',function () {
    var id = 'editor_num_'+editorN;
    var blank = UI.find('#blank_text').clone().html();
    $('#the_content').append(blank);
    $('#the_content .content_text:last-child').find('textarea').attr('id',id);
    CKEDITOR.replace( id );
    editorN++;
});

$(document).on('click','#add_image',function () {
    var blank = UI.find('#blank_image').clone().html();
    $('#the_content').append(blank);
    $('#the_content .content_image:last-child').find('.lfm_pick').filemanager('image');
});
$(document).on('click','#add_video',function () {
    var blank = UI.find('#blank_video').clone().html();
    $('#the_content').append(blank);
});
$(document).on('click','.remove_block',function () {
   $(this).closest('.content_item').remove();
});
$('#form-with-validation').on('submit',function () {
    var form = $(this);
    // var content = form.collectContent();
    // console.log(content);
    var input = $('<input type="hidden" name="content">');
    input.val(JSON.stringify(form.collectContent()));
    form.append(input);
    // return false;
});

$.fn.extend({
    collectContent: function () {
        var data = [],
            form = $(this);
            wrapper = form.find('#the_content');

        wrapper.find('.content_item').each(function () {
            var block = $(this);
            if (block.hasClass('content_text')) {
                var editor_id = block.find('textarea').attr('id');
                var editor_content = CKEDITOR.instances[editor_id].getData();
                var title = block.find('input[type="text"]').val();
                data.push({
                    'type':'text',
                    'content':editor_content,
                    'title': title
                });
            }
            if (block.hasClass('content_image')) {

                var nativePath = block.find('.lfm_pick_input').val();
                var caption = block.find('.caption').val();

                data.push({
                    'type':'image',
                    'path':nativePath,
                    'caption': caption
                });
            }
            if (block.hasClass('content_video')) {

                var embed = block.find('textarea').val();
                var caption = block.find('input').val();

                data.push({
                    'type':'video',
                    'embed':embed,
                    'caption': caption
                });
            }
        });

        return data;
    }
});
// var data = CKEDITOR.instances.editor1.getData();