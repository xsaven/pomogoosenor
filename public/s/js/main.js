var bower_path = '/public/bower_components/';

requirejs.config({
    baseUrl: '/s/js',  // base path with all js
    paths: {
        // ALIAS TO PLUGINS
        domReady: 'lib/domReady',


        // LIBS
        modernizr: 'lib/modernizr-custom',
        jquery: 'lib/jquery', // lib for js
        jquery_jscroll: 'lib/jquery.jscroll.min', //infinite scroll library
        slick: bower_path + 'slick-carousel/slick/slick.min',
        // PLUGINS
        // svg4everybody:               bower_path + 'svg4everybody/dist/svg4everybody.min',  // load svg
        // scrollbar:                   bower_path + 'perfect-scrollbar/js/perfect-scrollbar.jquery.min',
        bs_select: bower_path + 'bootstrap-select/dist/js/bootstrap-select',
        bs_dropdown: bower_path + 'bootstrap/js/dropdown',
        bs_collapse: bower_path + 'bootstrap/js/collapse',
        bs_transition: bower_path + 'bootstrap/js/transition',
        bs_tab: bower_path + 'bootstrap/js/tab',
        //air_datepicker: bower_path + 'air-datepicker/dist/js/datepicker.min',
        datepicker: bower_path + 'bootstrap-datapicker/js/bootstrap-datepicker',
        datepicker_locale: bower_path + 'bootstrap-datapicker/locales/bootstrap-datepicker.ru.min',
        popver: bower_path + 'Bootstrap-Popover/dist/jquery.webui-popover.min',
        validator: bower_path + 'jquery_validate/jquery.validate.min',
        jquery_mask: bower_path + 'jquery-mask-plugin/dist/jquery.mask.min',
        ionRange: bower_path + 'ion.rangeSlider/js/ion.rangeSlider.min',
        // scrollbar_comp:             bower_path + 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min',
        scrollbar: bower_path + 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar',
        perfect_scrollbar: bower_path + 'perfect-scrollbar/js/perfect-scrollbar.jquery',
        autosize: bower_path + 'autosize/dist/autosize.min',
        svg4everybody: bower_path + 'svg4everybody/dist/svg4everybody.min',
        googleMaps: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyD9s26MP3iCKPneRLD1MNzVrjPq_TpgSAY&language=ru&libraries=places',
        bs_modal: 'plugins/bs/modal', // fixed for multiple modal open, need modal fix
        stars: 'plugins/jquery.star-rating-svg.min', // fixed for multiple modal open, need modal fix

        // SEPARATE
        separate_global: 'separate/global', // detect width && height of window\

        // HELPERS
        resizer: 'helpers/resizer',
        object_fit: 'helpers/object-fit',
        bs_modal_fix: 'helpers/bs_modal_fix',
        // detectmobilebrowser:         'lib/detectmobilebrowser',
        // toggle_blocks:               'helpers/toggle-blocks',
        // update_columns:              'helpers/update-columns',
        // bs_modal_center:             'helpers/bsModalCenter',

        // COMPONENTS
        home: 'components/home',
        widgets: 'components/widgets',
        faq: 'components/faq',
        no_limit: 'components/no_limit',
        bid: 'components/bid',
        profile: 'components/profile',
        task_card: 'components/task_card',
        executors_filter: 'components/executors_filter',
        share: 'components/share',
        'js-header': 'components/js-header',
        madal_add_item: 'components/madal_add_item',
        'js-all-tasks-map': 'components/js-all-tasks-map',
        'ajax_form': 'components/ajax-form',
        'fast-register': 'components/fast-register',
        'phone-verification': 'components/phone-verification',
        'blockUI': 'lib/jquery.blockUI.min',
        'after_reg': 'components/finish-registration',
        'selectize': 'lib/selectize.min',
        'subscribe': 'components/subscribe',
        'notify': 'lib/notify',
        'testing_info': 'components/testing_info',
        'tester': 'components/tester',
        'photo_upload': 'components/photo_upload',
        'video_upload': 'components/video_upload',

        //PUSH Notifications
        push_notifications: 'components/push_notifications',
        infinite_scroll: 'components/infinite-scroll', //infinite scroll
        countdown: 'components/countdown', //time countdown script
        register_validation: 'components/validation/register-validation',
        tippy: 'lib/tippy.all.min'
    },
    shim: {
        'bs_modal': {
            deps: ['bs_modal_fix']
        },
        'object_fit': {
            deps: ['modernizr']
        }
    }
});
var main_js_components = [
    'object_fit',
    'separate_global',
    'modernizr',
    'resizer',
    'svg4everybody',
    'blockUI',
    'ajax_form',
    'selectize',
    'notify',
    'share',
    'push_notifications'
    // 'toggle_blocks',
];
requirejs(['domReady', 'jquery'], function (domReady, $) {
    requirejs(main_js_components, function (objectfit) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });
        setTimeout(function () {
            svg4everybody();
        });
        loadComponent({
            name: 'js-header',
            req: ['js-header']
        });
        loadComponent({
            name: 'js-all-tasks-map',
            req: ['js-all-tasks-map', 'googleMaps']
        });

        if ($('.scroll_block').length > 0) {
            requirejs(['scrollbar'], function () {
                var th = $('.scroll_block');
                th.mCustomScrollbar({
                    theme: "light",
                    scrollButtons: {enable: true},
                    advanced: {
                        updateOnContentResize: true,
                        autoScrollOnFocus: false
                    },
                    callbacks: {
                        onUpdate: function () {
                            $(this).mCustomScrollbar("scrollTo", "bottom")
                        }
                    },
                    scrollInertia: "last"
                });
            });
        }
        if ($('.scroll_block').length > 0) {
            requirejs(['autosize'], function (autosize) {
                // console.log(autosize);
                $('#predlogeniekzadaniju').on('shown.bs.modal', function () {
                    autosize($('.scroll_block textarea'));
                })
                // autosize(document.querySelectorAll('textarea'));
                if ($('.paste_textarea').length > 0) {
                    $('.paste_textarea').on('click', function () {
                        $(this).closest('form').find('.scroll_block textarea')["0"].value = $(this)["0"].innerText;
                        autosize.update($('.scroll_block textarea'));
                        $('.scroll_block').mCustomScrollbar('update');
                        $('.scroll_block').mCustomScrollbar("scrollTo", "bottom")
                    })
                }
            });
        }


        if ($('.modal-grey .icon-pensil').length > 0) {
            $('.modal-grey .icon-pensil').on('click', function () {
                $(this).closest('.modal-grey').find('input[readonly]').removeAttr('readonly').focus();
                $(this).closest('.modal-grey').find('input').on('focusout', function () {
                    $(this).attr('readonly', 'readonly');
                })
            });
        }


        if ($('footer').length > 0) {
            requirejs(['widgets']);
        }
        if ($('.selectpicker.change__butcon').length > 0) {
            $('.selectpicker.change__butcon').on('changed.bs.select', function () {
                $(this).closest('form').find("button[type='submit']").html('Предложить работу');
            });
        }

        // Прибитый футер
        function footerReCalc() {
            var vh = $('footer').outerHeight();
            $('.l-page-wrapper').css('padding-bottom', vh);
        }

        footerReCalc();
        $(window).on('resize', function () {
            footerReCalc();
        })

        // collapse
        if ($('.modal-select').length > 0) {
            requirejs(['madal_add_item']);
        }
        if ($('.collapse').length > 0) {
            requirejs(['bs_collapse', 'bs_dropdown', 'bs_transition']);
        }
        if ($('.l-faq').length > 0) {
            requirejs(['faq']);
        }

        // modal
        if ($('.modal').length > 0) {
            requirejs(['bs_modal']);
        }

        if ($('.main-page-wrapper').length > 0) {
            requirejs(['slick'], function () {
                requirejs(['home']);
            });
        }
        // Кнопка наверх
        $('.blog-insane__footer-btn').on('click', function () {
            $('html, body').animate({scrollTop: 0}, 200);
        });

        // selectpicker
        if ($('.selectpicker').length > 0) {
            requirejs(['bs_select', 'bs_dropdown'], function () {
                $('.selectpicker').selectpicker();
            });
        }
        ;

        if ($('.l-no-limit').length > 0) {
            requirejs(['no_limit']);
        }
        ;

        // Календарь
        if ($('.datepicker-here').length > 0) {
            requirejs(['datepicker'], function () {
                requirejs(['datepicker_locale'], function () {
                    $('.datepicker-here').datepicker({
                        format: 'dd.mm.yyyy',
                        startDate: '0d',
                        language: 'ru'
                    });
                });
            });
        }
        ;

        if ($('[data-toggle="popover"]').not('.share-popover').length > 0) {
            requirejs(['popver'], function () {
                $('[data-toggle="popover"]').webuiPopover('destroy').webuiPopover({trigger: 'hover'});
            });
        }


        //validator
        if ($('[data-toggle="validator"]').length > 0) {
            requirejs(['validator'], function () {
                $('[data-toggle="validator"]').validate({
                    submitHandler: function (form) {
                        form.submit();
                    },
                    //errorClass: "alert alert-error",
                    errorPlacement: function (error, element) {
                        if (element.parent().find('.errorMsg').length > 0) {
                            error.appendTo(element.parent().find('.errorMsg'));
                        } else {
                            error.appendTo(element.parents('.messContainer').find('.errorMsg'));
                        }
                        /*error.prependTo( element );
                        console.log(element);*/
                    }
                });
            });
        }


        /**
         * Perfect Scrollbar
         */
        if ($('#app').length > 0) {
            requirejs(['perfect_scrollbar']);
        }


        if ($('.l-bid').length > 0) {
            requirejs(['bid']);
        }
        ;

        if ($('.task-time').length > 0) {
            requirejs(['countdown']);
        }
        ;

        // Маска
        if ($('.mask').length > 0) {
            requirejs(['jquery_mask'], function () {
                $('.label-time .mask').mask('00:00');
                $('.label-card .mask').mask('0000');
                $('.label-cvv .mask').mask('000');
                $('.label-date .mask').mask('00');
                $('.tel-wrap .tel-input').mask('(000) 000-00-00');
                $('.phone_us').mask('(000) 000-00-00');
                $('.datepicker-here').mask('00.00.0000');
            });
        }
        ;

        // Слайдер цены
        if ($('.price-slider').length > 0) {
            requirejs(['ionRange'], function () {
                var objInp = $('#price_from_range');
                var dis = false;
                var timer = false;
                //var firstLoad = false;
                if ($('#myPriceInput').val() != '') {
                    dis = $('#myPriceInput').val();
                }
                var $priceRange = $("#budget-slider").ionRangeSlider({
                    type: 'single',
                    grid_snap: true,
                    grid: true,
                    from: $('[name="cost_save"]').val() != 'false' ? $('[name="cost_save"]').val() : 0,
                    min: 0,
                    max: 25000,
                    step: 5000,
                    prefix: '',
                    postfix: " рублей",
                    onStart: function (data) {
                        if (firstLoad) {
                            timer = setInterval(function () {
                                if (firstLoad) {
                                    $('#myPriceInput').val(firstLoad);
                                }
                            }, 100);
                        }
                    },
                    onFinish: function () {
                        if (timer) {
                            firstLoad = false;
                            clearInterval(timer);
                        }
                    },
                    prettify: function (num) {
                        $('#myPriceInput').val('');

                        if (num == 0) {
                            objInp.val("0");
                            $('[name="cost_save"]').val('0')
                            $('#myPriceInput').removeAttr("disabled");
                            return "0";
                        }
                        if (num == 5000) {
                            $('#myPriceInput').attr("disabled", "true");
                            objInp.val(ranges.formatted_data[0].value);
                            $('[name="cost_save"]').val('0');
                            return ranges.formatted_data[0].value;
                        }
                        if (num == 10000) {
                            $('#myPriceInput').attr("disabled", "true");
                            objInp.val(ranges.formatted_data[1].value);
                            $('[name="cost_save"]').val('10000');
                            return ranges.formatted_data[1].value;
                        }
                        if (num == 15000) {
                            $('#myPriceInput').attr("disabled", "true");
                            objInp.val(ranges.formatted_data[2].value);
                            $('[name="cost_save"]').val('15000');
                            return ranges.formatted_data[2].value;
                        }
                        if (num == 20000) {
                            $('#myPriceInput').attr("disabled", "true");
                            objInp.val(ranges.formatted_data[3].value);
                            $('[name="cost_save"]').val('20000');
                            return ranges.formatted_data[3].value;
                        }
                        if (num == 25000) {
                            $('#myPriceInput').attr("disabled", "true");
                            objInp.val(ranges.formatted_data[4].value);
                            $('[name="cost_save"]').val('25000');
                            return ranges.formatted_data[4].value;
                        }
                    }
                });

                var slider = $priceRange.data("ionRangeSlider");

                $('#myPriceInput').on('change', function () {
                    var val = $(this).val();
                    if (val != '') slider.update({disable: true, from: 0});
                    else {
                        slider.update({disable: false, from: 0});
                    }
                    $(this).val(val);
                }).on("click", function () {

                    $(this).removeAttr("disabled");
                });

                $('.l-bid-write__budget-slider').on('click', function () {
                    if ($('#myPriceInput').val() != '') {
                        slider.update({disable: false, from: 0});
                    }
                });

                if (dis) {
                    slider.update({disable: true, from: 0});
                    $('#myPriceInput').val(dis);
                }

            });
        }
        ;

        // Табы
        if ($('.nav-tabs').length > 0) {
            requirejs(['bs_transition', 'bs_tab']);
        }
        ;

        // Infinite scroll
        if ($('.my-tasks__item').length > 0) {
            requirejs(['infinite_scroll']);
        }

        if ($('.l-prof-set__subs-item').length > 0 || $('.favoritesForm').length > 0 || $('#settings').length > 0) {
            requirejs(['profile'], function () {

            });
        }
        ;

        if ($('.l-task-card-wrapper').length > 0) {
            requirejs(['tippy', 'task_card']);
        }
        ;

        if ($('#executors-city-modal').length > 0) {
            requirejs(['executors_filter']);
        }
        ;

        if ($('#fastregmail').length > 0) {
            requirejs(['fast-register']);
        }

        if ($('#sendPhoneVerification').length > 0) {
            requirejs(['phone-verification']);
        }

        if ($('#finishRegForm').length > 0) {
            requirejs(['after_reg']);
        }

        if ($('.subscribe_form').length > 0) {
            requirejs(['subscribe']);
        }

        if ($('.notify_warning').length > 0) {
            $('.notify_warning').each(function () {
                $.notify($(this).text(), "warn");
            })
        }
        if ($('.notify_danger').length > 0) {
            $('.notify_danger').each(function () {
                $.notify($(this).text(), "error");
            })
        }
        if ($('.notify_success').length > 0) {
            $('.notify_success').each(function () {
                $.notify($(this).text(), "success");
            })
        }


        if ($('.validation_error').length > 0) {
            $('.validation_error').each(function () {
                var input = $('input[name="' + $(this).data('name') + '"], select[name="' + $(this).data('name') + '"]');
                if (input.length > 0) {

                    input.addClass('error');
                }
                $.notify($(this).text(), "error");

            })
        }

        if ($('#testing_form_info').length > 0) {
            requirejs(['testing_info']);
        }

        if ($('#add_photo').length > 0) {
            requirejs(['photo_upload']);
        }

        if ($('.add_video').length > 0) {
            requirejs(['video_upload']);
        }

        /**
         * VALIDATION START
         */

        if ($('#fastRegForm').length > 0) {
            requirejs(['register_validation']);
        }

        /**
         * VALIDATION END
         */

        if ($('.steps-content .link_bondi').length > 0) {
            requirejs(['tester']);
        }


        if ($('#show-modal-need-confirm').length > 0) {
            $('#sendRegisterEmail').on('submit', function () {
                var form = $(this);
                $(this).ajaxCall('json', function (response) {
                    $('#email-sent-modal').modal('show');
                    $('#email-sent-modal-message').html(response.message);
                    $('#confirm').modal('hide');
                });
                return false;
            });
            setTimeout(function () {
                $('#confirm').modal('show');
            }, 200);
        }

        if ($('#show-modal-first-login').length > 0) {
            setTimeout(function () {
                $('#after_register').modal('show');
            }, 200);
        }

        $("a[data-rev-type='positive']").click(function () {
            $("input[name='positive'][value='1']").prop('checked', true);
        });
        $("a[data-rev-type='negative']").click(function () {
            $("input[name='positive'][value='0']").prop('checked', true);
        });
        // Звездочки для отзыва
        if ($('.estimate__stars').length > 0) {

            requirejs(['stars'], function () {
                $('.estimate__stars').starRating({
                    starSize: 40,
                    useFullStars: true,
                    totalStars: 5,
                    disableAfterRate: false,
                    callback: function (currentRating, el) {
                        var rate_type = $(el).attr('data-type');
                        $('input[name="' + rate_type + '"]').val(currentRating);
                    }
                });
            });
        }
        ;

    });

});