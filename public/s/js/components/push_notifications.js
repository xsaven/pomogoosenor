$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function onesignalInit() {
    window.OneSignal.push(function() {

        window.OneSignal.init({
            appId: window.OneSignalAppId,
        });
    });
}

function onesignalStoreUserId() {
    window.OneSignal.getUserId( function(userId) {
       if(userId != null && localStorage.getItem('one-signal-user-id') != userId) {
            localStorage.setItem('one-signal-user-id',userId);
               $.post( "/api/notifications/set-one-signal-user", {onesignal_id: userId}, function() {
                   alert( "success" );
               })
               .done(function() {
                   // alert( "second success" );
               })
               .fail(function() {
                   // alert( "error" );
               })
               .always(function() {
                   // alert( "finished" );
               });
        }
    });
}

onesignalInit();
onesignalStoreUserId();