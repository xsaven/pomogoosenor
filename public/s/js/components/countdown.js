$(function() {
    function leftPad(number) {
        var output = number + '';
        while (output.length < 2) {
            output = '0' + output;
        }
        return output;
    }

    setInterval(function () {
        $('.task-time').each(function () {
            var task_time = $(this).html().split(":");
            var TaskTime = {
                hours: parseInt(task_time[0]),
                minutes: parseInt(task_time[1]),
                seconds: parseInt(task_time[2])
            };
            var timeLeft = TaskTime.hours * 3600 + TaskTime.minutes * 60 + TaskTime.seconds;
            if (timeLeft != 0) {
                timeLeft--;
            }
            TaskTime.hours = leftPad(Math.floor(timeLeft / 3600));
            TaskTime.minutes = leftPad(Math.floor((timeLeft - (TaskTime.hours * 3600 )) / 60));
            TaskTime.seconds = leftPad(Math.floor((timeLeft - (TaskTime.hours * 3600) - (TaskTime.minutes * 60))));
            $(this).html(TaskTime.hours + ':' + TaskTime.minutes + ':' + TaskTime.seconds);
        });

    }, 1000);
});