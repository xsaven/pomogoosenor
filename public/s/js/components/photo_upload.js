$(document).on('change', '#add_photo', function () {
    var th = this;
    var block = $('.avatar_preview');
    block.closest('.steps-content__input-file').blockForm();
    if (th.files && th.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            block.attr('src', e.target.result);
            $.ajax({
                url: '/ajax-load-avatar',
                method: 'POST',
                dataType: 'json',
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    avatar: e.target.result
                }
            }).success(function (response) {
                block.unblockForm();
                $('.blockUI').remove();
                $('#avatar-loaded-modal').modal('show');
                $('#avatar-loaded-modal-message').html(response.message);
            });
        }
        reader.readAsDataURL(th.files[0]);
    }
});

$(document).on('click', '#delete_avatar', function () {
    var th = this;
    var block = $('.avatar_preview');
    block.closest('.steps-content__input-file').blockForm();
    block.attr('src', '/photos/defaults/avatar.png');
    $.ajax({
        url: '/ajax-delete-avatar',
        method: 'POST',
        dataType: 'json',
        data: {
            _token: $('meta[name="csrf-token"]').attr('content'),
            avatar: null
        }
    }).success(function (response) {
        block.unblockForm();
        $('.blockUI').remove();
        $('#avatar-loaded-modal').modal('show');
        $('#avatar-loaded-modal-message').html(response.message);
    });

});