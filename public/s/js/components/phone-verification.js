$('#sendPhoneVerification').on('submit', function (e) {
    var form = $(this);
    $(this).ajaxCall('json', function (response) {
        $('#modal-sms').modal('toggle');
    }, function (response) {
        form.find('#modal-mob-error').text(response.responseJSON.error.message);
    });
    return false;
});

$('#call-modal-mob-numb').on('click', function () {
    $('#modal-mob-numb').modal('toggle');
});

$('#checkPhoneVerification').on('submit', function () {
    var form = $(this);
    $(this).ajaxCall('json', function (response) {
        $('#modal-verify').modal('toggle');
        var p_code =   $('#modal-mob-numb').find('select[name="phone_code"]').val();
        var p_number = $('#modal-mob-numb').find('input[name="phone_number"]').val();
        $("#testing_form_info").find('input[name="phone_code"]').val(p_code);
        $("#testing_form_info").find('input[name="phone_number"]').val(p_number);
        $("#testing_form_info").find('input[name="phone_verified"]').val(1);
        $('button[data-target="#modal-mob-numb"]').hide();
    }, function (response) {
        form.find('#modal-sms-error').text(response.responseJSON.error.message);
    });
    return false;
});