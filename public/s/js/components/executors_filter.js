$("form.filter-form :input").change(function( event ) {
    event.preventDefault();
    var form_data = $(this).serialize();
    $.get('/executors/sort', form_data,function (data) {
        $('#executors-list').html(data);
    });
});

$('[name="subcategory"]').change(function () {
    $('.link-to-category').removeClass('active');
    $(this).parent().find('.link-to-category').addClass('active');
    var label = $("label[for='" + $(this).attr('id') + "']");
    $('#subcategory-label').html(label.html());
});

$('[name="city"]').change(function () {
    $('#city-name-link').html($(this).attr('data-city-name'));
});