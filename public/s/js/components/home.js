// Слайдер
$('.l-main-slider').slick({
	dots: false,
	infinite: true,
	speed: 500,
	fade: true,
	arrows: false,
	autoplay: true,
	autoplaySpeed: 5000
});

// Показать ещё	категории
$('.l-propose__item button').on('click', function(){
	var th = $(this);
	th.closest('.l-propose__item').find('.l-propose__item-links').removeClass('minimized');
	th.remove();
});

// Виджет фейсбук

