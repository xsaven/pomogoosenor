var shouldSubmit = false;

// $('#finishRegForm').on('submit', function () {
//     if (!shouldSubmit) {
//         $('input[name="name"]').trigger('change');
//         return false;
//     }
// });
var inputLabelWidth;

$('#finishRegForm :input').on('keyup', function () {
    var allFilled = true;

    $("#finishRegForm :input[type=text]").each(function () {
        allFilled = true;
        if ($(this).val() === "") {
            allFilled = false;
        }
    });
    var button = $('.but_bondi');
    if (allFilled)
        button.removeClass('disabled');
    else if (!button.hasClass('disabled'))
        button.addClass('disabled');
});

$('#finishRegForm').on('submit', function () {
    let target = $('#finishRegForm');
    requirejs(['validator'], function () {

        if (!shouldSubmit && target.valid()) {

            let form = target;
            _token = $('meta[name="csrf-token"]').attr('content');
            form.blockForm();

            $.ajax({
                url: '/finish-register/prevalidate',
                method: 'POST',
                dataType: 'json',
                data: form.serialize(),
                success: function (response) {

                    shouldSubmit = true;
                    $('input[name="name"]').closest('label').addClass('success');
                    $('input[name="phone"]').closest('label').addClass('success');
                    $('input[name="email"]').closest('label').addClass('success');
                    $('input[name="city"]').closest('label').addClass('success');
                    $('label.error').remove();
                    form.unblockForm();
                    window.form1 = form;
                    form.submit();

                },
                error: function (response) {
                    var data = response.responseJSON;
                    if (typeof data.name == 'undefined') {
                        $('input[name="name"]').closest('label').addClass('success');
                    }
                    if (typeof data.phone == 'undefined') {
                        $('input[name="phone"]').closest('label').addClass('success');
                    }
                    if (typeof data.city == 'undefined') {
                        $('input[name="city"]').closest('label').addClass('success');
                    }

                    $('label.error').remove();
                    form.showErrors(response);
                    form.unblockForm();
                },
            });
            return false;

        }
    });
    if (!shouldSubmit) return false;
});

$(document).on('change', '#avatar', function () {
    var th = this;
    var block = $('.avatar_preview');
    if (th.files && th.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            block.attr('src', e.target.result);
        };
        reader.readAsDataURL(th.files[0]);
    }
});

$(document).ready(function () {
    inputLabelWidth = $('input[name="name"]').width();
    var select_city = $('#select_city');
    select_city.selectize({
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        maxOptions: 10,
        render: {
            option: function (item, escape) {
                var title = escape(item.name);
                return '<div>' + title + '</div>';
            }
        },
        load: function (query, callback) {

            // console.log($(this))

            if (!query.length) return callback();
            jQuery.ajax({
                url: '/location/find-city',
                method: 'POST',
                dataType: 'json',
                data: {
                    query: query,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                error: function () {
                    callback();
                },
                success: function (res) {
                    console.log(res)
                    callback(res);
                }
            });
        },
        onChange: function (value) {
            select_city.trigger('change');
        }
    });
});