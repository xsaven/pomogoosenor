$('#fastRegForm').on('submit', function () {
    var prevent = false;
    var form = $(this);
    var fields = {
        email:form.find('input[name="email"]'),
        agree_checkbox:form.find('input[name="confirm_rules"]')
    };
    if(fields.email.val().length == 0){
        form.find('.steps-content__form-input').addClass('has-error');
        prevent = true;
    }
    if (!fields.agree_checkbox.prop('checked')){
        form.find('i.checkbox').attr('style','border: 2px solid #F3565D;');
        prevent = true;
    }
    if(prevent){
        event.preventDefault();
    }
});