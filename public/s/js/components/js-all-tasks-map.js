define(function (require, exports, module) {
        // Put traditional CommonJS module content here
        var init = function (elem) {
            initMap(locations);
        };
        return init;
    }
);

$('#tasks-sort').change(function () {
    window.location = '/task/all?status=' + $(this).val();
});

$('#sort-no-offers').change(function () {
    console.log();
    window.location = '/task/all?no_offers=' + $(this).prop('checked');
});

$('.city-filter').change(function () {
    $('#filter-city').val($(this).val());
    $('#city-name-link').html($(this).attr('data-city-name'));
    $('#task-sort-form').trigger('change').trigger('input');
});

$("#categories-filter :input").change(function () {
    setTimeout(function () {
        var form_data = $("#categories-filter").serializeArray();
        var categories = [];
        console.log(form_data);
        $.each(form_data, function (key, category) {
            categories.push(category.value)
        });
        console.log(categories.join());
        window.location = '/task/all?categories_filter=' + categories;
    }, 100);

});
//
// $('#task-search-query').on('input',function(e){
//     $('#task-sort-form').change();
// });

$('#task-sort-form').on('change', function (e) {
    var form = $(this);
    setTimeout(function () {
        var data = form.serialize();
        var url = form.prop('action');
        var subcategories = [];
        $.each($('.category-checkbox'), function (key, category) {
            if ($(category).prop('checked') == true) {
                subcategories.push($(category).val())
            }
        });
        data += '&subcategories=' + subcategories.join();

        $.get(url, data, function (response) {
            $('#tasks-list').html(response);
        });
    }, 500);
});

$('.all-tasks__filter-nav-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    $('.my_tasks_filter').toggleClass('hidden');
    if ($('.my_tasks_filter:first').hasClass('hidden')) {
        initMap(locationsCreators);
    } else {
        initMap(locations);
    }

});

$('#cancel-task-modal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var task_id = button.data('task-id');
    $('#cancel-task-form').attr('action', '/task/field_edit/' + task_id + '/status');
})