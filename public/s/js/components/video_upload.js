$(document).on('change', '.add_video', function () {
    var block = $('.video_preview');
    var url = $(this).attr('data-upload-url');
    var file_data = $(this).prop('files')[0];

    if(file_data.size >= 2097152) {
        alert('Превышен максимальный размер файла');
        return false;
    }

    var formData = new FormData();
    formData.append('video', file_data);

    block.closest('.l-prof-ab-me__add-video').blockForm();

    $.ajax({
        url: url,
        data: formData,
        method: 'POST',
        contentType: false, // The content type used when sending data to the server.
        cache: false, // To unable request pages to be cached
        processData: false
    }).done(function (response) {
        let errorBlock = block.closest('.l-prof-ab-me__add-video').find('.error');
        if (typeof response.error !== 'undefined') {
            errorBlock.html(response.error);
        } else {
            errorBlock.html('');
        }

        block.attr('src', response.path);
        block.css('display', 'block');
        block.unblockForm();
        $('.blockUI').remove();
    });
});