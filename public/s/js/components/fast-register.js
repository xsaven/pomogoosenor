$('#loginForm').on('submit', function (event) {
    let _this = this;
    let form = $(this);
    require(['validator'], function () {

        form.validate();

        if (!form.valid()) {
            event.preventDefault();
            return false;
        }

        $(_this).ajaxCall('json', function (response) {
            window.location.reload();
        }, function (response) {
            event.preventDefault();
            form.showErrors(response);
        });

        return false;
    });
    event.preventDefault();
    return false;

});

$('#already-register-button').on('click', function () {
    e.preventDefault();
    $("#fastregmail").modal('hide');
    setTimeout(function () {
        $('#loginModal').modal('show');
    }, 200);
});

$('#login-button').on('click', function (e) {
    e.preventDefault();
    $("#fastregmail").modal('hide');
    setTimeout(function () {
        $('#loginModal').modal('show');
    }, 200);
});

$('#register-button').on('click', function (e) {
    e.preventDefault();
    $("#loginModal").modal('hide');
    setTimeout(function () {
        $('#fastregmail').modal('show');
    }, 200);
});

$('#register-button-alt').on('click', function (e) {
    e.preventDefault();
    $("#loginModal").modal('hide');
    setTimeout(function () {
        $('#fastregmail').modal('show');
    }, 200);
});


