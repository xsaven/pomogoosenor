// Dropdown-ны с чекбоксами
$('.check-label.main-label input').on('change', function () {
    var th = $(this),
        subCheckbox = th.closest('.l-prof-set__subs-item').find('.l-prof-set__subs-item-cont input');

    if (th.is(':checked')) {
        subCheckbox.prop('checked', true);
    } else {
        subCheckbox.prop('checked', false);
    }
    ;
});

$('.l-prof-set__subs-item-cont input').on('change', function () {
    var th = $(this),
        main_checkbox = th.closest('.l-prof-set__subs-item').find('.check-label.main-label input'),
        subCheckbox = th.closest('.l-prof-set__subs-item').find('.l-prof-set__subs-item-cont input');

    main_checkbox.prop('checked', true);
    subCheckbox.each(function () {
        if (!$(this).prop('checked')) {
            main_checkbox.prop('checked', false);
        }
    });
});

// Добавить информацию
$('.l-prof-ab-me__head-top a').on('click', function (e) {
    e.preventDefault();
    let block = $(this).closest('.l-prof-ab-me__head');
    if (block.hasClass('active')) {
        $('.min-price-not-active').show();
        block.removeClass('active')
    } else {
        $('.min-price-not-active').hide();

        block.addClass('active');
    }
});

// Удалить загруженную фотку
$('.album-upload__item .delete').on('click', function () {
    $(this).toggleClass('back').closest('.album-upload__item').toggleClass('back');
});


var shouldSubmit = false;

// $('#finishRegForm').on('submit', function () {
//     if (!shouldSubmit) {
//         $('input[name="name"]').trigger('change');
//         return false;
//     }
// });
var inputLabelWidth;

//Update main  profile info

$('#profile-update').on('submit', function (e) {
    e.preventDefault();

    let form = $(this);
    requirejs(['validator'], function () {

        form.validate();

        if (!form.valid()) {
            return false;
        }

        let _token = $('meta[name="csrf-token"]').attr('content');
        form.blockForm();

        $.ajax({
            url: '/profile/update',
            method: 'PUT',
            dataType: 'json',
            data: form.serialize(),
            success: function (response) {
                shouldSubmit = true;
                $('input[name="name"]').closest('label').addClass('success');
                $('input[name="phone"]').closest('label').addClass('success');
                $('input[name="email"]').closest('label').addClass('success');
                $('input[name="surname"]').closest('label').addClass('success');
                $('input[name="birthday"]').closest('label').addClass('success');

                $('label.error').remove();
                form.unblockForm();
                $('#user-updated-message').html(response.message);
                $('#user-updated').modal();

            },
            error: function (response) {
                var data = response.responseJSON;
                if (typeof data.name == 'undefined') {
                    $('input[name="name"]').closest('label').addClass('success');
                }
                if (typeof data.phone == 'undefined') {
                    $('input[name="phone"]').closest('label').addClass('success');
                }
                if (typeof data.city == 'undefined') {
                    $('input[name="city"]').closest('label').addClass('success');
                }

                if (typeof data.surname == 'undefined') {
                    $('input[name="surname"]').closest('label').addClass('success');
                }

                if (typeof data.birthday == 'undefined') {
                    $('input[name="birthday"]').closest('label').addClass('success');
                }

                $('label.error').remove();
                form.showErrors(response);
                form.unblockForm();
            },
        });
    });
});

//update password
$('#password-update').on('submit', function (e) {
    e.preventDefault();

    let form = $(this);
    requirejs(['validator'], function () {

        form.validate({
            rules: {
                password_confirmation: {
                    equalTo: "#password"
                }
            },
            messages: {
                password_confirmation: {
                    equalTo: 'Пароли не совпадают'
                }
            }
        });

        if (!form.valid()) {
            return false;
        }


        let _token = $('meta[name="csrf-token"]').attr('content');

        form.blockForm();

        $.ajax({
            url: '/profile/password/update',
            method: 'PUT',
            dataType: 'json',
            data: form.serialize(),
            success: function (response) {
                shouldSubmit = true;
                $('input[name="password"]').closest('label').addClass('success');
                $('label.error').remove();
                form.unblockForm();
                $('#user-updated-message').html(response.message);
                $('#user-updated').modal();
                form.trigger("reset");
            },
            error: function (response) {
                var data = response.responseJSON;
                if (typeof data.password == 'undefined') {
                    $('input[name="password"]').closest('label').addClass('success');
                }
                $('label.error').remove();
                form.showErrors(response);
                form.unblockForm();
            },
        });
    });
});

//Change avatar (profile)
$(document).on('change', '#avatar', function () {
    var th = this;
    var block = $('.avatar_preview');
    if (th.files && th.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            block.attr('src', e.target.result);
        };
        reader.readAsDataURL(th.files[0]);
    }
});


//Create worker  profile info

$('#worker-create').on('submit', function (e) {
    e.preventDefault();

    var form = $(this),
        _token = $('meta[name="csrf-token"]').attr('content');
    form.blockForm();

    $.ajax({
        url: '/workers/store',
        method: 'POST',
        dataType: 'json',
        data: form.serialize(),
        success: function (response) {
            shouldSubmit = true;
            $('input[name="selected_cat"]').closest('label').addClass('success');
            $('textarea[name="about_me"]').closest('label').addClass('success');
            $('input[name="avatar"]').closest('label').addClass('success');
            $('input[name="min_price"]').closest('label').addClass('success');
            $('textarea[name="about_price"]').closest('label').addClass('success');

            $('label.error').remove();
            form.unblockForm();

            window.location.href = "/profile";


        },
        error: function (response) {
            var data = response.responseJSON;
            if (typeof data.about_me == 'undefined') {
                $('textarea[name="about_me"]').closest('label').addClass('success');
            }
            if (typeof data.avatar == 'undefined') {
                $('input[name="avatar"]').closest('label').addClass('success');
            }

            if (typeof data.min_price == 'undefined') {
                $('input[name="min_price"]').closest('label').addClass('success');
            }

            if (typeof data.about_price == 'undefined') {
                $('textarea[name="about_price"]').closest('label').addClass('success');
            }

            $('label.error').remove();
            form.showErrors(response);
            form.unblockForm();
        },
    });
});

$('.favoritesForm').on('submit', function (e) {
    e.preventDefault();
    var form = $(this);
    $(this).ajaxCall('json', function (response) {
        $('.favoritesForm').toggleClass("collapse")
    }, function (response) {
    });
    return false;
});

$('.updateAboutMeForm').on('submit', function (e) {
    e.preventDefault();
    var form = $(this),
        url = form.attr('action'),
        about_me = $('.about-me-textarea').val();
    var data = {
        _token: $('meta[name="csrf-token"]').attr('content'),
        about_me: about_me
    };
    let block = $(this).closest('.l-prof-ab-me__head');
    form.blockForm();
    form.find('label.error').remove();
    $.ajax({
        url: url,
        method: 'POST',
        data: data,
        success: function (response) {
            if (response.success == true) {
                block.removeClass('active');
                $('.about-me-text').html(about_me);
            }
            form.unblockForm();
        },
        error: function (response) {
            form.showErrors(response);
            form.unblockForm();
        }
    });
    return false;
});


$('.updateMinPrice').on('submit', function (e) {
    e.preventDefault();
    var form = $(this),
        url = form.attr('action'),
        min_price = $('#about-me-min-price').val();
    about_price = $('#about-me-about-price').val();
    var data = {
        _token: $('meta[name="csrf-token"]').attr('content'),
        min_price: min_price,
        about_price: about_price
    };
    form.blockForm();
    form.find('label.error').remove();
    $.ajax({
        url: url,
        method: 'POST',
        data: data,
        success: function (response) {
            if (response.success == true) {
                // $('.about-me-text').html(about_me);
            }

            $('.min-price-not-active').html(about_price);
            $('.min-price-not-active').show();
            $(form).closest('.l-prof-ab-me__head').removeClass('active');

            form.unblockForm();
        },
        error: function (response) {
            form.showErrors(response);
            form.unblockForm();
        }
    });
    return false;
});

var tab_name = $('.nav-tabs').attr('data-tab');
if (typeof tab_name !== 'undefined') {
    $('.nav-tabs a[href="#' + tab_name + '"]').tab('show');
}

