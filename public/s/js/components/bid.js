$('.l-bid-write__adress-line .but-del').on('click', function(){
	//$(this).closest('.l-bid-write__adress-line').remove();
});
if($('#bid-map').length > 0) {
	require(['googleMaps'], function(){
		/*function initMap() {
			window.map = new google.maps.Map(document.getElementById('bid-map'), {
				center: {lat: 49.834177, lng: 24.033391},
				disableDefaultUI: true,
				scrollwheel: false,
				zoom: 18,
			});

			var myLatlng = {lat: 49.834177, lng: 24.033391};
			var marker = new google.maps.Marker({
				position: myLatlng,
				map: map
			});
		}
		initMap();*/

        function initMap() {
            window.map = new google.maps.Map(document.getElementById('bid-map'), {
                zoom: 4,
                center: {lat: 49.834177, lng: 24.033391},
            });

            $('.pac-input').each(function( index ) {
                var autocomplete = new google.maps.places.Autocomplete(this);

                autocomplete.addListener('place_changed', function() {
                    window.reBuildPoints();
                });
            });

            window.directionsService = new google.maps.DirectionsService;
            window.directionsDisplay = new google.maps.DirectionsRenderer({
                draggable: true,
                map: map,
				//panel: document.getElementById('right-panel')
            });

            directionsDisplay.addListener('directions_changed', function() {
                computeTotalDistance(directionsDisplay.getDirections());
                console.log(directionsDisplay.getPosition());
            });

            /*displayRoute('Perth, WA', 'Sydney, NSW', directionsService,
                directionsDisplay);*/
        }

        window.displayRoute = function(origin, destination, points, addresses) {
            directionsService.route({
                origin: origin,
                destination: destination,
                //waypoints: [{location: 'Adelaide, SA'}, {location: 'Broken Hill, NSW'}],
                waypoints: points,
                travelMode: 'DRIVING',
                avoidTolls: true
            }, function(response, status) {
                if (status === 'OK') {
                var start_point = {
                    name: addresses[0],
                    lat: response.routes[0].overview_path[0].lat(),
                    lon: response.routes[0].overview_path[0].lng()
                };
                var points = [start_point];

                $(response.routes[0].legs).each(function(index) {
                    points.push({
                        name: addresses[index+1],
                        lat: this.start_location.lat(),
                        lon: this.start_location.lng()
                    });
                });

                    $("input[name='data[address]").val(JSON.stringify(points));
                    if (!first_rebuild) {
                        directionsDisplay.setDirections(response);
                    }else{
                        first_rebuild = false;
                    }
                } else {
                    document.getElementById('total_km').innerHTML = '';
                    document.getElementById('validAddress').value = '';
                    // alert('Could not display directions due to: ' + status);
                }
            });
        }

        function computeTotalDistance(result) {
            var total = 0;
            var myroute = result.routes[0];
            for (var i = 0; i < myroute.legs.length; i++) {
                total += myroute.legs[i].distance.value;
            }
            total = total / 1000;
            //$('[name="validAddress"]').val('valid');
            document.getElementById('validAddress').value = 'valid';
            document.getElementById('total_km').innerHTML = total + ' km';
        }

        initMap();
	});
};

$('.label-private-wrp.info').on('click', function(){
	$('.l-bid-write__addit').addClass('active');
});
$('.l-bid-write__addit-head').on('click', function(){
	$('.l-bid-write__addit').removeClass('active');
});

$('.label-private-wrp.photo').on('click', function(){
	$('.l-bid-write__file').addClass('active');
});