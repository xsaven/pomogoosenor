var select_city = $('#select_city');
select_city.selectize({
    valueField: 'id',
    labelField: 'name',
    searchField: ['name'],
    maxOptions: 10,
    render: {
        option: function (item, escape) {
            var title = escape(item.name);
            return '<div>' + title + '</div>';
        }
    },
    load: function(query, callback) {

        // console.log($(this))

        if (!query.length) return callback();
        jQuery.ajax({
            url: '/location/find-city',
            method: 'POST',
            dataType: 'json',
            data: {
                query: query,
                _token: $('meta[name="csrf-token"]').attr('content')
            },
            error: function() {
                callback();
            },
            success: function(res) {
                console.log(res)
                callback(res);
            }
        });
    },
    onChange: function(value) {
        select_city.trigger('change');
    }
});
$('.selectize-control').css('width','270px');
$('[class^="social__"').click(function(e){
    e.preventDefault();
    var url = $(this).attr('href')+'?'+$('#testing_form_info').serialize()
    var win = window.open(url, '_blank');
    var social = $(this);
    var timer = setInterval(function () {
        try {
            if (win.location.host == location.host) {
                win.close();
                social.parent().addClass('conected');
            }
            if(win.closed){
                clearInterval(timer);
            }
        } catch (err) {
        }
    }, 100);
})