//Карта
if ($('#bid-map').length > 0) {
    //map_elem = $('#bid-map');
    require(['googleMaps'], function () {
        function initMap() {
            window.map = new google.maps.Map(document.getElementById('bid-map'), {
                zoom: 4,
                center: {lat: 49.834177, lng: 24.033391},
            });

            window.directionsService = new google.maps.DirectionsService;
            window.directionsDisplay = new google.maps.DirectionsRenderer({
                draggable: true,
                map: map,
            });

            directionsDisplay.addListener('directions_changed', function () {
                computeTotalDistance(directionsDisplay.getDirections());
            });
        }

        window.displayRoute = function (origin, destination, points) {
            directionsService.route({
                origin: origin,
                destination: destination,
                waypoints: points,
                travelMode: 'DRIVING',
                avoidTolls: true
            }, function (response, status) {
                if (status === 'OK') {
                    directionsDisplay.setDirections(response);
                    var legs = response.routes[0].legs;
                    var distance = 0;
                    $.each(legs, function (key, val) {
                        if (val !== undefined)
                            distance += val.distance.value;
                    });
                    document.getElementById('total_km').innerHTML = (distance / 1000).toFixed(2) + ' км';
                } else {
                    document.getElementById('total_km').innerHTML = '';
                    alert('Could not display directions due to: ' + status);
                }
            });
        }

        function computeTotalDistance(result) {
            var total = 0;
            var myroute = result.routes[0];
            for (var i = 0; i < myroute.legs.length; i++) {
                total += myroute.legs[i].distance.value;
            }
            total = total / 1000;
            document.getElementById('total_km').innerHTML = total + ' км';
        }

        initMap();

        /*var map, myLatlng, marker, bounds, directionsService, directionsDisplay,
        labels = 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ',
        route_markers = [],
        locations = [
            {
                address:'Санкт-Петербург, Садовая 28/30'
            },
            {
                address:'Санкт-Петербург, Автовская 46, 33'
            }
        ];

        function initMap(locations) {
            if (map_elem.hasClass('map_single')) {
                map = new google.maps.Map(map_elem[0], {
                    center: {lat: 49.834177, lng: 24.033391},
                    disableDefaultUI: true,
                    scrollwheel: false,
                    zoom: 18
                });
                myLatlng = {lat: 49.834177, lng: 24.033391};
                marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map
                });
            };
                if (map_elem.hasClass('map_route')) {
                map = new google.maps.Map(map_elem[0], {
                    center: {
                        lat: 58.325,
                        lng: 18.070
                    },
                    disableDefaultUI: true,
                    scrollwheel: false,
                    zoom: 18
                });
                directionsService = new google.maps.DirectionsService;
                directionsDisplay = new google.maps.DirectionsRenderer({
                    map: map
                });
                function calculateAndDisplayRoute(directionsService, directionsDisplay) {
                    directionsService.route({
                        origin: locations[0].address,
                        destination: locations[1].address,
                        travelMode: 'DRIVING'
                    }, function(response, status) {
                        var myRoute = response.routes[0].legs[0];

                        for (var i = 0; i < myRoute.steps.length; i++) {
                            var marker;
                            var icon = {
                                url:'	/s/images/useful/markers/marker-empty.svg',
                                labelOrigin: {
                                    x:16,
                                    y:15
                                }
                            };
                            var label = {
                                fontSize:'16px',
                                color: 'white',
                            };
                            if (i == 0) {
                                label.text = labels[0];
                                marker = new google.maps.Marker({
                                    position: myRoute.steps[i].start_point,
                                    map: map,
                                    icon: icon,
                                    label: label
                                });
                                route_markers.push(marker);
                            };
                            if (i == myRoute.steps.length - 1) {
                                label.text = labels[1];
                                marker = new google.maps.Marker({
                                    position: myRoute.steps[i].start_point,
                                    map: map,
                                    icon: icon,
                                    label: label
                                });
                            };
                        }
                        if (status === 'OK') {
                            directionsDisplay.setDirections(response);
                        } else {
                            window.alert('Directions request failed due to ' + status);
                        }
                    });
                };
                calculateAndDisplayRoute(directionsService, directionsDisplay);
            };
        };
        initMap(locations);*/
    });
}
;

// Активная кнопка при наборе текста в textarea
$('.comments-answer__right textarea').on('input', function () {
    var th = $(this),
        btn = th.closest('.l-task-card__comments-answer').find('button');

    if (!th.val() == '') {
        btn.removeAttr('disabled');
    }
    else {
        btn.attr('disabled', 'disabled');
    }
});

// Написать новый комментарий
$('.l-task-card__comments-new').on('click', function (e) {
    e.preventDefault();
    $('.l-task-card__comments-answer.full').addClass('active');
});

// Ответить
$('.l-task-card__comments-item .comments-item__complaint').on('click', function (e) {
    e.preventDefault();
    $(this).closest('.question-answer-wrapper').find('.l-task-card__comments-answer').addClass('sub');
});

$('.offer-sort').on('click', function (e) {
    e.preventDefault();
    $('.offer-sort').removeClass("active");
    $(this).addClass('active');
    $.get($(this).attr('href'), function (response) {
        $('.task-offers-list').html(response);
    });
});

$('#template-collapse').click(function (e) {
    $.get('/templates/list', function (response) {
        $.each(response.templates, function (key, val) {
            $('#template-list-select').append("<option value='" + val.text + "'>" + val.title + "</option>");
        });
        $('#template-list-select').selectpicker('refresh');

    });
    $('#template-list').toggleClass("hide");
    $(this).toggleClass("hide");
});

$('#template-list-select').change(function () {
    if ($(this).val() != 'add_new_template') {
        $('#template-textarea').val($(this).val());
    } else {
        $('#template-modal').modal('show');
    }
});

$("#template-form").on("submit", function (event) {
    event.preventDefault();
    var data = $(this).serializeArray();
    $.post('/templates/store', data, function (response) {
        $('#template-list-select').append("<option value='" + data[1].value + "'>" + data[0].value + "</option>");
        $('#template-list-select').selectpicker('refresh');
        $('#template-modal').modal('hide');
        $('#task-offer-modal').modal('show');
    });
});

$('#card-list-select').on("change refreshed.bs.select", function () {
    if ($(this).val() == 'add_new_card') {
        $('#form-submit-btn').text('Привязать карту');
    } else {
        if ($(this).attr('data-type') == 'executor'){
            $('#form-submit-btn').text('Добавить предложение');
        }else {
            $('#form-submit-btn').text('Да');
        }
    }
});

function updateCards(cards_type) {
    var form = $("#offer-form");
    form.blockForm();
    var cards_list_type = cards_type + '_cards';
    $.get('/lists/cards', function (response) {
        $('#card-list-select').find('option').remove();
        $.each(response[cards_list_type], function (key, card) {
            $('#card-list-select').append("<option value='" + card.id + "' data-content='<img style=\"height: 1.5em\" src=\"https://www.echeep.com/bundles/virtualcertificate/img/payment/mastercard.svg\"> " + card.mask + "'></option>");
        });
        $('#card-list-select').append("<option class=\"add_new_option\" data-content=\"Добавить новую карту\" value=\"add_new_card\"></option>");
        $('#card-list-select').selectpicker('refresh');
    }).always(function() {
        form.unblockForm();
    });
}

function bindCard(cards_type) {
    $("#offer-form").blockForm();
    var url = '';
    if(cards_type == 'executor'){
        url = '/payment/sbr/bind-beneficiary-card';
    }else{
        url = '/payment/sbr/bind-payer-card';
    }
    var win = window.open(url, '_blank');
    var timer = setInterval(function () {
        try {

            if (win.location.host == location.host && win.location.href.indexOf("#closeWV") !== -1) {
                console.log('includes');
                win.close();
            }
            if(win.closed){
                console.log('closed');

                clearInterval(timer);
                updateCards(cards_type);
            }
        } catch (err) {
        }
    }, 100);
}


$("#offer-form").on("submit", function (event) {
    if ($('#agree_checkbox').length > 0 && !$('#agree_checkbox').prop('checked')){
        event.preventDefault();
    }
    if ($('#card-list-select').val() == 'add_new_card') {
        event.preventDefault();
        return bindCard($('#card-list-select').attr('data-type'));
    }
});

$('#task-offer-modal').on('shown.bs.modal', function (e) {
    $('#card-list-select').selectpicker('refresh');
});

$('#select-executor-modal').on('shown.bs.modal', function (e) {
    $('#card-list-select').selectpicker('refresh');
});

$('[data-target="#select-executor-modal"]').click(function () {
    var modal = $('#select-executor-modal');
    var link = modal.find('#user_link');
    var cost_input = modal.find('#cost_input');
    var user_id_input = modal.find('[name="user_id"]');
    link.html($(this).attr('data-user-name'));
    link.attr('href','/profile/'+$(this).attr('data-user-id'));
    cost_input.val($(this).attr('data-cost'));
    user_id_input.val($(this).attr('data-user-id'));
});