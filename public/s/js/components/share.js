var shareUrl = "";
var textToShare = "";
    $(document).on('click', '.init-share', function(event) {
        event.preventDefault();
        shareUrl = $(this).closest('.social-share-list').find('.share-data-info').attr("data-share-url");
        textToShare = $(this).closest('.social-share-list').find('.share-data-info').attr("data-text");
    });
    $(document).on('click', '.init-fb-share', function () {
        $(this).shareFb(shareUrl);
    });
    $(document).on('click', '.init-vk-share', function () {
        $(this).shareVk(shareUrl);
    });
    $(document).on('click', '.init-tw-share', function () {
        $(this).shareTwitter(shareUrl, textToShare);
    });
    $(document).on('click', '.init-go-share', function () {
        $(this).shareGoogle(shareUrl);
    });
    $(document).on('click', '.init-ok-share', function () {
        $(this).shareOK(shareUrl, textToShare);
    });
$.fn.extend({
    popup: function popup(url) {
        window.open(url, '', 'scrollbars=0, resizable=1, toolbar=0, status=0, width=626, height=436');
    },

    shareVk: function shareVk(shareUrl) {
        this.popup('https://vk.com/share.php?noparse=false&url=' + encodeURIComponent(shareUrl));
    },

    shareFb: function shareFb(shareUrl) {
        this.popup('http://www.facebook.com/sharer.php?s=100&u=' + encodeURIComponent(shareUrl));
    },

    shareTwitter: function shareTwitter(shareUrl, textToShare) {
        this.popup('https://twitter.com/share?url=' + encodeURIComponent(shareUrl) + '&text=' + encodeURIComponent(textToShare) + '&counturl=' + encodeURIComponent(shareUrl));
    },

    shareGoogle: function shareGoogle(shareUrl) {
        this.popup('https://plus.google.com/share?url='+encodeURIComponent(shareUrl));
    },

    shareOK: function shareOdnoklassniki(shareUrl, textToShare) {
        this.popup('http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=' + encodeURIComponent(shareUrl) + '&st.comments=' + encodeURIComponent(textToShare));
    }
});