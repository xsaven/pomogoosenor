$('ul.pagination').hide();

var init_pagination = function(){
    $('div[data-type="my-tasks-executor"] .c-pagination a').click(function(e){
        e.preventDefault();
        $.ajax({url : $(this).attr('href')}).done(function (data) {
            $('div[data-type="my-tasks-executor"]').html(data);
            init_pagination();
        });
    });

    $('div[data-type="my-tasks-creator"] .c-pagination a').click(function(e){
        e.preventDefault();
        $.ajax({url : $(this).attr('href')}).done(function (data) {
            $('div[data-type="my-tasks-creator"]').html(data);
            init_pagination();
        });
    });
};

$(function() {
    init_pagination();
});

function getTasks(url) {
    var result = '';
    $.ajax({
        url : url,
        async: false
    }).done(function (data) {
        result = data;
    });
    return result;
}