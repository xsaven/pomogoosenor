/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.language = 'ru';
	config.allowedContent = true;
    config.contentsCss = '/s/css/all.css';
    config.filebrowserBrowseUrl = '/laravel-filemanager?type=image';
    config.filebrowserUploadUrl = '/laravel-filemanager?type=image';
    config.enterMode = CKEDITOR.ENTER_BR;
    // config.dtd.a.div = 1;
    // config.dtd.a.p = 1;
	// config.uiColor = '#AADC6E';
};
