var bower_path = '/require/dist/bower_components/';

requirejs.config({
    baseUrl: '/require/dist/s/js',  // base path with all js
    paths: {
        // ALIAS TO PLUGINS
        domReady:                       'lib/domReady',
        
        
        // LIBS
        modernizr:                      'lib/modernizr-custom',
        jquery:                         'lib/jquery', // lib for js
        slick:                          bower_path +'slick-carousel/slick/slick.min',
        // PLUGINS
        // svg4everybody:               bower_path + 'svg4everybody/dist/svg4everybody.min',  // load svg
        // scrollbar:                   bower_path + 'perfect-scrollbar/js/perfect-scrollbar.jquery.min',
        bs_select:                      bower_path + 'bootstrap-select/dist/js/bootstrap-select',
        bs_dropdown:                    bower_path + 'bootstrap/js/dropdown',
        bs_collapse:                    bower_path + 'bootstrap/js/collapse',
        bs_transition:                  bower_path + 'bootstrap/js/transition',
        bs_tab:                         bower_path + 'bootstrap/js/tab',
        air_datepicker:                 bower_path + 'air-datepicker/dist/js/datepicker.min',
        jquery_mask:                    bower_path + 'jquery-mask-plugin/dist/jquery.mask.min',
        ionRange:                       bower_path + 'ion.rangeSlider/js/ion.rangeSlider.min',
        // scrollbar_comp:             bower_path + 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min',
        scrollbar:                  bower_path + 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar',
        autosize:                   bower_path + 'autosize/dist/autosize.min',
        svg4everybody:                  bower_path + 'svg4everybody/dist/svg4everybody.min',
        googleMaps:                     'https://maps.googleapis.com/maps/api/js?key=AIzaSyD9s26MP3iCKPneRLD1MNzVrjPq_TpgSAY&amp;amp;sensor=false',
        bs_modal:                       'plugins/bs/modal', // fixed for multiple modal open, need modal fix

        // SEPARATE
        separate_global:                'separate/global', // detect width && height of window\

        // HELPERS
        resizer:                        'helpers/resizer',
        object_fit:                     'helpers/object-fit',
        bs_modal_fix:                   'helpers/bs_modal_fix',
        // detectmobilebrowser:         'lib/detectmobilebrowser',
        // toggle_blocks:               'helpers/toggle-blocks',
        // update_columns:              'helpers/update-columns',
        // bs_modal_center:             'helpers/bsModalCenter',
        
        // COMPONENTS
        home:                           'components/home',
        widgets:                        'components/widgets',
        faq:                            'components/faq',
        no_limit:                       'components/no_limit',
        bid:                            'components/bid',
        profile:                        'components/profile',
        task_card:                      'components/task_card',
        'js-header':                    'components/js-header',
        madal_add_item:                'components/madal_add_item',
        'js-all-tasks-map':             'components/js-all-tasks-map',
        'ajax_form':                    'components/ajax-form',
        'fast-register':                'components/fast-register',
        'blockUI':                      'lib/jquery.blockUI.min',
        'after_reg':                    'components/finish-registration',
        'selectize':                    'lib/selectize.min',
        'subscribe':                    'components/subscribe',
        'notify':                       'lib/notify',
        'testing_info':                  'components/testing_info',
        'tester':                       'components/tester',
    },
    shim: {
        'bs_modal': {
            deps: ['bs_modal_fix']
        },
        'object_fit': {
            deps: ['modernizr']
        }
    }
});
var main_js_components = [
    'object_fit',
    'separate_global',
    'modernizr',
    'resizer',
    'svg4everybody',
    'blockUI',
    'ajax_form',
    'selectize',
    'notify',
    // 'toggle_blocks',
];
requirejs(['domReady','jquery'], function(domReady, $){
    requirejs(main_js_components, function(objectfit) {
        svg4everybody();
        loadComponent({
            name: 'js-header',
            req: ['js-header']
        });
        loadComponent({
            name: 'js-all-tasks-map',
            req: ['js-all-tasks-map', 'googleMaps']
        });

        if($('.scroll_block').length > 0){
            requirejs(['scrollbar'], function(){
                var th = $('.scroll_block');
                th.mCustomScrollbar({
                    theme:"light",
                    scrollButtons:{ enable: true },
                        advanced:{  
                            updateOnContentResize: true,
                            autoScrollOnFocus: false
                    },
                    callbacks:{
                        onUpdate: function(){
                            $(this).mCustomScrollbar("scrollTo","bottom")
                        }
                    },
                    scrollInertia: "last"
                });
            });
        }
        if($('.scroll_block').length > 0){
            requirejs(['autosize'], function(autosize){
                // console.log(autosize);
                $('#predlogeniekzadaniju').on('shown.bs.modal',function(){
                    autosize( $('.scroll_block textarea') );
                })
                // autosize(document.querySelectorAll('textarea'));
                if($('.paste_textarea').length > 0){
                    $('.paste_textarea').on('click',function(){
                        $(this).closest('form').find('.scroll_block textarea')["0"].value = $(this)["0"].innerText;
                        autosize.update( $('.scroll_block textarea') );
                        $('.scroll_block').mCustomScrollbar('update');
                        $('.scroll_block').mCustomScrollbar("scrollTo","bottom")
                    })
                }
            }); 
        }



        if($('.modal-grey .icon-pensil').length > 0){
            $('.modal-grey .icon-pensil').on('click', function(){
                $(this).closest('.modal-grey').find('input[readonly]').removeAttr('readonly');
                $(this).closest('.modal-grey').find('input').on('focusout', function(){
                    console.log($(this))
                    $(this).attr('readonly','readonly');
                })
            });
        }



        if($('footer').length > 0){
            requirejs(['widgets']); 
        }
        if($('.selectpicker.change__butcon').length > 0){
            $('.selectpicker.change__butcon').on('changed.bs.select', function(){
                $(this).closest('form').find("button[type='submit']").html('Предложить работу');
            });
        }
        // Прибитый футер
        function footerReCalc () {
            var vh = $('footer').outerHeight();
            $('.l-page-wrapper').css('padding-bottom', vh);
        }
        footerReCalc();
        $(window).on('resize', function() {
            footerReCalc();
        })

        // collapse
        if($('.modal-select').length > 0){
            requirejs(['madal_add_item']);
        }
        if($('.collapse').length > 0){
            requirejs(['bs_collapse', 'bs_dropdown', 'bs_transition']);
        }
        if($('.l-faq').length > 0){
            requirejs(['faq']);
        }

        // modal
        if($('.modal').length > 0){
            requirejs(['bs_modal']);
        }

        if($('.main-page-wrapper').length > 0){
            requirejs(['slick'], function(){
                requirejs(['home']);
            });
        }
        // Кнопка наверх
        $('.blog-insane__footer-btn').on('click', function(){
            $('html, body').animate({ scrollTop: 0 }, 200);
        });

        // selectpicker
        if($('.selectpicker').length > 0){
            requirejs(['bs_select', 'bs_dropdown'], function(){
                $('.selectpicker').selectpicker();
            });
        };

        if($('.l-no-limit').length > 0){
            requirejs(['no_limit']);
        };

        // Календарь
        if($('.datepicker-here').length > 0){
            requirejs(['air_datepicker']);
        };

        if($('.l-bid').length > 0){
            requirejs(['bid']);
        };

        // Маска
        if($('.mask').length > 0){
            requirejs(['jquery_mask'], function(){
                $('.label-time .mask').mask('00:00');
                $('.label-card .mask').mask('0000');
                $('.label-cvv .mask').mask('000');
                $('.label-date .mask').mask('00');
                $('.tel-wrap .tel-input').mask('(000) 000-00-00');
            });
        };

        // Слайдер цены
        if($('.price-slider').length > 0){
            requirejs(['ionRange'], function(){
                $("#budget-slider").ionRangeSlider({
                    type: 'single',
                    grid_snap: true,
                    grid: true,
                    min: 0,
                    max: 25000,
                    step: 5000,
                    prefix: "До ",
                    postfix: " рублей"
                });
            });
        };

        // Табы
        if($('.nav-tabs').length > 0){
            requirejs(['bs_transition', 'bs_tab']);
        };


        if($('.l-prof-set__subs-item').length > 0){
            requirejs(['profile']);
        };

        if($('.l-task-card-wrapper').length > 0){
            requirejs(['task_card']);
        };

        if ( $('#fastregmail').length > 0 ) {
            requirejs(['fast-register']);
        }

        if ( $('#finishRegForm').length > 0 ) {
            requirejs(['after_reg']);
        }
        
        if ( $('.subscribe_form').length > 0 ) {
            requirejs(['subscribe']);
        }

        if ($('.notify_warning').length > 0) {
            $(window).load(function(){
                $('.notify_warning').each(function(){
                    $.notify($(this).text(), "warn");
                })
            });
        }
        if ($('.notify_danger').length > 0) {
            $(window).load(function(){
                $('.notify_danger').each(function(){
                    $.notify($(this).text(), "error");
                })
            });
        }
        if ($('.notify_success').length > 0) {
            $(window).load(function(){
                $('.notify_success').each(function(){
                    $.notify($(this).text(), "success");
                })
            });
        }


        if ($('.validation_error').length > 0) {
            $(window).load(function(){
                $('.validation_error').each(function(){
                    var input = $('input[name="' + $(this).data('name') + '"], select[name="' + $(this).data('name') + '"]');
                    if (input.length > 0) {
                        
                        input.addClass('error');
                    }
                    $.notify($(this).text(), "error");
                    
                })
            });
        }

        if ($('#testing_form_info').length > 0) {
            requirejs(['testing_info']);
        }

        if ($('.steps-content .link_bondi').length > 0) {
            requirejs(['tester']);
        }

        
    });

});