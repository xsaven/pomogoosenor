var shouldSubmit = false;

// $('#finishRegForm').on('submit', function () {
//     if (!shouldSubmit) {
//         $('input[name="name"]').trigger('change');
//         return false;
//     }
// });
var inputLabelWidth;

$('#finishRegForm').on('submit',function () {
    if (!shouldSubmit){

        var form = $(this),
            _token = $('meta[name="csrf-token"]').attr('content'),
            data = {
                _token: _token
            };
        form.blockForm();
        form.find('.form-data').each(function () {
            var input = $(this);
            if (typeof input.val() != 'undefined') {
                data[input.attr('name')] = input.val();
            }
        });
        // console.log(data)
        $.ajax({
            url: 'profile/prevalidate',
            method: 'POST',
            dataType: 'json',
            data: data,
            success: function (response) {
                shouldSubmit = true;
                $('input[name="name"]').closest('label').addClass('success');
                $('input[name="tel"]').closest('label').addClass('success');

                $('label.error').remove();
                form.unblockForm();

                form.submit();
            },
            error: function (response) {
                var data = response.responseJSON;
                if (typeof data.name == 'undefined') {
                    $('input[name="name"]').closest('label').addClass('success');
                }
                if (typeof data.tel == 'undefined') {
                    $('input[name="tel"]').closest('label').addClass('success');
                }
                if (typeof data.city_id == 'undefined') {
                    $('.selectize-input').addClass('success');
                }

                $('label.error').remove();
                form.showErrors(response)
                form.unblockForm();
            },
        });
        return false;
    }
});

$(document).on('change','#avatar',function(){
    var th = this;
    var block = $('#avatar_preview');
    if (th.files && th.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            block.attr('src',e.target.result);
        }
        reader.readAsDataURL(th.files[0]);
    }
});

$(document).ready(function () {
    inputLabelWidth = $('input[name="name"]').width();
    var select_city = $('#select_city');
    select_city.selectize({
        valueField: 'id',
        labelField: 'name',
        searchField: ['name'],
        maxOptions: 10,
        render: {
            option: function (item, escape) {
                var title = escape(item.name);
                return '<div>' + title + '</div>';
            }
        },
        load: function(query, callback) {

            // console.log($(this))

            if (!query.length) return callback();
            jQuery.ajax({
                url: '/location/find-city',
                method: 'POST',
                dataType: 'json',
                data: {
                    query: query,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                error: function() {
                    callback();
                },
                success: function(res) {
                    console.log(res)
                    callback(res);
                }
            });
        },
        onChange: function(value) {
            select_city.trigger('change');
        }
    });
});