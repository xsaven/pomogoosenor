var select_city = $('#select_city');
select_city.selectize({
    valueField: 'id',
    labelField: 'name',
    searchField: ['name'],
    maxOptions: 10,
    render: {
        option: function (item, escape) {
            var title = escape(item.name);
            return '<div>' + title + '</div>';
        }
    },
    load: function(query, callback) {

        // console.log($(this))

        if (!query.length) return callback();
        jQuery.ajax({
            url: '/location/find-city',
            method: 'POST',
            dataType: 'json',
            data: {
                query: query,
                _token: $('meta[name="csrf-token"]').attr('content')
            },
            error: function() {
                callback();
            },
            success: function(res) {
                console.log(res)
                callback(res);
            }
        });
    },
    onChange: function(value) {
        select_city.trigger('change');
    }
});
$('.selectize-control').css('width','270px');

$(document).on('change','#add_photo',function(){
    var th = this;
    var block = $('#avatar_preview');
    block.closest('.steps-content__input-file').blockForm();
    if (th.files && th.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            block.attr('src',e.target.result);
            $.ajax({
                url: '/ajax-load-avatar',
                method: 'POST',
                dataType: 'json',
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    avatar: e.target.result
                }
            }).done(function () {
                block.unblockForm();
                $('.blockUI').remove();
            });
        }
        reader.readAsDataURL(th.files[0]);
    }
});