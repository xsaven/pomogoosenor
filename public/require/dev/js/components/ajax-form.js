$.fn.extend({
	ajaxCall: function (dataType, success_callback, error_callback) {

	    if (typeof dataType == 'undefined') {
            dataType = 'json';
        }

	    var form = $(this),
            method = form.attr('method'),
            url = form.attr('action'),
            _token = $('meta[name="csrf-token"]').attr('content'),
            data = {
                _token: _token
            };
	    form.find('.form-data').each(function () {
            var input = $(this);
            input.removeClass('error');

	        if (input.attr('type') == 'checkbox' && input.prop('checked') === true) {
                data[input.attr('name')] = input.val();
            } else {
                if (typeof input.val() != 'undefined') {
                    data[input.attr('name')] = input.val();
                }
            }

        });

        form.blockForm();
        form.find('label.error').remove();
        $.ajax({
            url: url,
            method: method,
            dataType: dataType,
            data: data,
            success: function (response) {
                success_callback(response);
                form.unblockForm();
            },
            error: function (response) {
                error_callback(response);
                form.unblockForm();
            }
        });

    },
    unblockForm: function () {
        $(this).unblock();
    },
    blockForm: function () {
        $(this).css('position','relative').block({
            message: false,
            overlayCSS: {
                background: "#fff",
                opacity: 0.6
            },
            css: {
                background: 'none',
                border: 'none',
            }
        });
    },
    showErrors: function (response) {

        var form = $(this);
        $.each(response.responseJSON, function (input_name, error_messages) {

            var inputDom = form.find('input[name="'+input_name+'"], textarea[name="'+input_name+'"], select[name="'+input_name+'"]');

            var messages = '';
            for (var i = 0; i < error_messages.length; i++) {
                messages += error_messages[i] + '<br>';
            }
            if (inputDom.hasClass('selectized')) {
                $('<label class="error">'+messages+'</label>').insertAfter(inputDom.next());
            } else {
                $('<label class="error">'+messages+'</label>').insertAfter(inputDom);
            }
        });
    }
})