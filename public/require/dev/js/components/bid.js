$('.l-bid-write__adress-line .but-del').on('click', function(){
	$(this).closest('.l-bid-write__adress-line').remove();
});
if($('#bid-map').length > 0) {
	require(['googleMaps'], function(){
		function initMap() {
			var map = new google.maps.Map(document.getElementById('bid-map'), {
				center: {lat: 49.834177, lng: 24.033391},
				disableDefaultUI: true,
				scrollwheel: false,
				zoom: 18
			});

			var myLatlng = {lat: 49.834177, lng: 24.033391};
			var marker = new google.maps.Marker({
				position: myLatlng,
				map: map
			});
		}
		initMap();
	});
};

$('.label-private-wrp.info').on('click', function(){
	$('.l-bid-write__addit').addClass('active');
});
$('.l-bid-write__addit-head').on('click', function(){
	$('.l-bid-write__addit').removeClass('active');
});

$('.label-private-wrp.photo').on('click', function(){
	$('.l-bid-write__file').addClass('active');
});