// Вызываем фиксированный футер
$('.l-no-lt__item a').on('click', function(e){

	var footer = $('.l-no-limit__footer'),
		fixedBlockHeight = footer.outerHeight(),
		th = $(this);

	e.preventDefault();

	if(th.hasClass('active')){
		th.removeClass('active').find('span').text('Выбрать');
	}
	else{
		th.addClass('active').find('span').text('Отменить');
	}

	// th.addClass('active').find('span').text('Отменить');
	footer.addClass('active');

	$('.l-no-limit').css({
		'padding-bottom': fixedBlockHeight + 10
	});

});

$('.modal-no-limit__item').on('click', function(e){
	e.preventDefault();
	var th = $(this);
	th.closest('.col-md-4').siblings().find('.modal-no-limit__item').removeClass('active');
	th.addClass('active');
});