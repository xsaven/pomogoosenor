//Карта
if($('#card-task-map').length > 0) {
	map_elem = $('#card-task-map');
	require(['googleMaps'], function(){
		var map, myLatlng, marker, bounds, directionsService, directionsDisplay,
		labels = 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ',
		route_markers = [],
		locations = [
			{
				address:'Санкт-Петербург, Садовая 28/30'
			},
			{
				address:'Санкт-Петербург, Автовская 46, 33'
			}
		];

		function initMap(locations) {
			if (map_elem.hasClass('map_single')) {
				map = new google.maps.Map(map_elem[0], {
					center: {lat: 49.834177, lng: 24.033391},
					disableDefaultUI: true,
					scrollwheel: false,
					zoom: 18
				});
				myLatlng = {lat: 49.834177, lng: 24.033391};
				marker = new google.maps.Marker({
					position: myLatlng,
					map: map
				});
			};
			if (map_elem.hasClass('map_route')) {
				map = new google.maps.Map(map_elem[0], {
					center: {
						lat: 58.325,
						lng: 18.070
					},
					disableDefaultUI: true,
					scrollwheel: false,
					zoom: 18
				});
				directionsService = new google.maps.DirectionsService;
				directionsDisplay = new google.maps.DirectionsRenderer({
					map: map
				});
				function calculateAndDisplayRoute(directionsService, directionsDisplay) {
					directionsService.route({
						origin: locations[0].address,
						destination: locations[1].address,
						travelMode: 'DRIVING'
					}, function(response, status) {
						var myRoute = response.routes[0].legs[0];

						for (var i = 0; i < myRoute.steps.length; i++) {
							var marker;
							var icon = {
								url:'s/images/useful/markers/marker-empty.svg',
								labelOrigin: {
									x:16,
									y:15
								}
							};
							var label = {
								fontSize:'16px',
								color: 'white',
							};
							if (i == 0) {
								label.text = labels[0];
								marker = new google.maps.Marker({
									position: myRoute.steps[i].start_point, 
									map: map,
									icon: icon,
									label: label
								});
								route_markers.push(marker);
							};
							if (i == myRoute.steps.length - 1) {
								label.text = labels[1];
								marker = new google.maps.Marker({
									position: myRoute.steps[i].start_point, 
									map: map,
									icon: icon,
									label: label
								});
							};
						}
						if (status === 'OK') {
							directionsDisplay.setDirections(response);
						} else {
							window.alert('Directions request failed due to ' + status);
						}
					});
				};
				calculateAndDisplayRoute(directionsService, directionsDisplay);
			};
		};
		initMap(locations);
	});
};

// Активная кнопка при наборе текста в textarea
$('.comments-answer__right textarea').on('input', function(){
	var th = $(this),
		btn = th.closest('.l-task-card__comments-answer').find('button');

	if(!th.val() == ''){
		btn.removeAttr('disabled');
	}
	else{
		btn.attr('disabled', 'disabled');
	}
});

// Написать новый комментарий
$('.l-task-card__comments-new').on('click', function(e){
	e.preventDefault();
	$('.l-task-card__comments-answer.full').addClass('active');
});

// Ответить
$('.l-task-card__comments-item .comments-item__complaint').on('click', function(e){
	e.preventDefault();
	$(this).closest('.question-answer-wrapper').find('.l-task-card__comments-answer').addClass('sub');
});
