define(function(require, exports, module) {
	// Put traditional CommonJS module content here
	var init = function(elem){
		$('.menu__create-link').on('click', function(e){
			e.preventDefault();
		});

		$('.menu__create').on('click', function(){
			$(this).toggleClass('active');
			$(document).mouseup(function (e) {
			    var container = $('.menu__create');
			    if (container.has(e.target).length === 0){
			        container.removeClass('active');
			    }
			});
		});

		$('.c-header__menu_small-trigger').on('click', function(){
			$(this).toggleClass('active');
			$('.c-header__menu_small').toggleClass('active');
		});

		$('.c-header__right-notice').on('click', function(){
			$(this).toggleClass('active');
			$('.c-header__right-notice-menu').toggleClass('active');
		});


		$('.c-header__right-user').on('click', function(){
			$(this).toggleClass('active');
			$('.c-header__right-user-menu').toggleClass('active');
		});
	};

	return init;
});