// Dropdown-ны с чекбоксами
$('.check-label.main-label input').on('change', function () {
	var	th = $(this),
		subCheckbox = th.closest('.l-prof-set__subs-item').find('.l-prof-set__subs-item-cont input');

	if(th.is(':checked')){
		subCheckbox.prop('checked', true);
	}
	else{
		subCheckbox.prop('checked', false);
	};
});

$('.l-prof-set__subs-item-cont input').on('change', function(){
	var	th = $(this),
		main_checkbox = th.closest('.l-prof-set__subs-item').find('.check-label.main-label input'),
		subCheckbox = th.closest('.l-prof-set__subs-item').find('.l-prof-set__subs-item-cont input');

	main_checkbox.prop('checked', true);
	subCheckbox.each(function(){
		if(!$(this).prop('checked')) {
			main_checkbox.prop('checked', false);
		};
	});
});

// Добавить информацию
$('.l-prof-ab-me__head-top a').on('click', function(e){
	e.preventDefault();
	$(this).closest('.l-prof-ab-me__head').addClass('active');
});

// Удалить загруженную фотку
$('.album-upload__item .delete').on('click', function(){
	$(this).toggleClass('back').closest('.album-upload__item').toggleClass('back');
});