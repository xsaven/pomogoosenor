$('#fastRegForm').on('submit', function (e) {
    e.preventDefault();
    var form = $(this);
    console.log(form);

    return false;
});

$('#loginForm').on('submit', function () {
    var form = $(this);
    $(this).ajaxCall('json', function (response) {
        window.location.reload();
    }, function (response) {
        form.showErrors(response);
    });
    return false;
});