define(function (require, exports, module) {
    // Put traditional CommonJS module content here
    var init = function (elem) {
        var locations = [
            {
                title: 'example title',
                coords: {
                    lat: 59.325,
                    lng: 18.070
                },
                icon_src: 's/images/useful/markers/balloons.svg',
                balloon: {
                    text: 'Нужен курьер на машине на месяц',
                    status: 'Открыто',
                    price: '5 000 руб.'
                }
            },
            {
                title: 'example title1',
                coords: {
                    lat: 59.335,
                    lng: 18.070
                },
                icon_src: 's/images/useful/markers/barbershop.svg',
                balloon: {
                    text: 'Нужен курьер на машине на месяц',
                    status: 'Открыто',
                    price: '5 000 руб.'
                }
            },
            {
                title: 'example title1',
                coords: {
                    lat: 59.325,
                    lng: 18.200
                },
                icon_src: 's/images/useful/markers/car-search.svg',
                balloon: {
                    text: 'Нужен курьер на машине на месяц',
                    status: 'Открыто',
                    price: '5 000 руб.'
                }
            },
            {
                title: 'example title1',
                coords: {
                    lat: 58.325,
                    lng: 18.070
                },
                icon_src: 's/images/useful/markers/cleaning.svg',
                balloon: {
                    text: 'Нужен курьер на машине на месяц',
                    status: 'Открыто',
                    price: '5 000 руб.'
                }
            },
            {
                title: 'example title1',
                coords: {
                    lat: 59.325,
                    lng: 17.070
                },
                icon_src: 's/images/useful/markers/courier.svg',
                balloon: {
                    text: 'Нужен курьер на машине на месяц',
                    status: 'Открыто',
                    price: '5 000 руб.'
                }
            },
            {
                title: 'example title1',
                coords: {
                    lat: 59.325,
                    lng: 19.070
                },
                icon_src: 's/images/useful/markers/desktop.svg',
                balloon: {
                    text: 'Нужен курьер на машине на месяц',
                    status: 'Открыто',
                    price: '5 000 руб.'
                }
            },
            {
                title: 'example title1',
                coords: {
                    lat: 58.325,
                    lng: 17.070
                },
                icon_src: 's/images/useful/markers/education.svg',
                balloon: {
                    text: 'Нужен курьер на машине на месяц',
                    status: 'Открыто',
                    price: '5 000 руб.'
                }
            },
            {
                title: 'example title1',
                coords: {
                    lat: 59.999,
                    lng: 16.070
                },
                icon_src: 's/images/useful/markers/electro.svg',
                balloon: {
                    text: 'Нужен курьер на машине на месяц',
                    status: 'Открыто',
                    price: '5 000 руб.'
                }
            },
            {
                title: 'example title1',
                coords: {
                    lat: 60.325,
                    lng: 18.070
                },
                icon_src: 's/images/useful/markers/fix.svg',
                balloon: {
                    text: 'Нужен курьер на машине на месяц',
                    status: 'Открыто',
                    price: '5 000 руб.'
                }
            },
            {
                title: 'example title1',
                coords: {
                    lat: 61.325,
                    lng: 18.070
                },
                icon_src: 's/images/useful/markers/mobile.svg',
                balloon: {
                    text: 'Нужен курьер на машине на месяц',
                    status: 'Открыто',
                    price: '5 000 руб.'
                }
            },
            {
                title: 'example title1',
                coords: {
                    lat: 60.325,
                    lng: 20.070
                },
                icon_src: 's/images/useful/markers/palette.svg',
                balloon: {
                    text: 'Нужен курьер на машине на месяц',
                    status: 'Открыто',
                    price: '5 000 руб.'
                }
            },
            {
                title: 'example title1',
                coords: {
                    lat: 61.325,
                    lng: 17.070
                },
                icon_src: 's/images/useful/markers/photo.svg',
                balloon: {
                    text: 'Нужен курьер на машине на месяц',
                    status: 'Открыто',
                    price: '5 000 руб.'
                }
            },
            {
                title: 'example title1',
                coords: {
                    lat: 61.325,
                    lng: 16.070
                },
                icon_src: 's/images/useful/markers/scales.svg',
                balloon: {
                    text: 'Нужен курьер на машине на месяц',
                    status: 'Открыто',
                    price: '5 000 руб.'
                }
            },
            {
                title: 'example title1',
                coords: {
                    lat: 60.325,
                    lng: 16.570
                },
                icon_src: 's/images/useful/markers/soft.svg',
                balloon: {
                    text: 'Нужен курьер на машине на месяц',
                    status: 'Открыто',
                    price: '5 000 руб.'
                }
            },
            {
                title: 'example title1',
                coords: {
                    lat: 57.325,
                    lng: 17.070
                },
                icon_src: 's/images/useful/markers/support.svg',
                balloon: {
                    text: 'Нужен курьер на машине на месяц',
                    status: 'Открыто',
                    price: '5 000 руб.'
                }
            },
            {
                title: 'example title1',
                coords: {
                    lat: 58.325,
                    lng: 16.070
                },
                icon_src: 's/images/useful/markers/truck.svg',
                balloon: {
                    text: 'Нужен курьер на машине на месяц',
                    status: 'Открыто',
                    price: '5 000 руб.'
                }
            }
        ];

        function build_balloon_content(data) {
            var balloon__inner = document.createElement('div');
            balloon__inner.className = 'balloon__inner';
            var balloon__text = document.createElement('div');
            balloon__text.className = 'balloon__text';
            balloon__text.innerHTML = data.text;
            balloon__inner.appendChild(balloon__text);
            var balloon__status = document.createElement('div');
            balloon__status.className = 'balloon__status';
            balloon__status.innerHTML = data.status;
            balloon__inner.appendChild(balloon__status);
            var balloon__price = document.createElement('div');
            balloon__price.className = 'balloon__price';
            balloon__price.innerHTML = data.price;
            balloon__inner.appendChild(balloon__price);
            return balloon__inner.outerHTML;
        };

        function initMap(locations) {
            if (locations.length === 0) {
                var map = new google.maps.Map(document.getElementById('all-tasks__map'), {
                    center: {lat: 55.753960, lng: 37.620393},
                    mapTypeId: 'roadmap',
                    disableDefaultUI: true,
                    zoom: 5
                });
                return false;
            }

            var map = new google.maps.Map(document.getElementById('all-tasks__map'), {
                center: locations[0].coords,
                mapTypeId: 'roadmap',
                disableDefaultUI: true,
                // scrollwheel: false,
                zoom: 18
            });

            window.map = map;

            var map_block = $('.all-tasks__map-inner');
            var bounds = new google.maps.LatLngBounds();
            var infoWindow = new google.maps.InfoWindow({
                content: ''
            });

            function update_map() {
                map.setZoom(18);
                bounds = new google.maps.LatLngBounds();
                for (i = 0; i < locations.length; i++) {
                    var position = new google.maps.LatLng(locations[i].coords.lat, locations[i].coords.lng);
                    bounds.extend(position);
                }
                ;
                if (locations.length > 1) {
                    map.fitBounds(bounds);
                } else {
                    var position = new google.maps.LatLng(locations[0].coords.lat, locations[0].coords.lng);
                    map.setCenter(position);
                }
                ;
            };

            for (i = 0; i < locations.length; i++) {
                var position = new google.maps.LatLng(locations[i].coords.lat, locations[i].coords.lng);
                bounds.extend(position);
                var marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    title: locations[i].title,
                    content: build_balloon_content(locations[i].balloon),
                    icon: locations[i].icon_src
                });

                // Allow each marker to have an info window
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infoWindow.setContent(marker.get('content'));
                        infoWindow.open(map, marker);
                    };
                })(marker, i));

                // Automatically center the map fitting all locations on the screen
                if (locations.length > 1) {
                    map.fitBounds(bounds);
                }
                ;
            }
            ;
            google.maps.event.addListener(map, "click", function (event) {
                infoWindow.close();
            });
            $(window).on('resize', function () {
                update_map();
            });
            map_block.on('map_replace', function () {
                google.maps.event.trigger(map, 'resize');
                update_map();
            });
            $('.all-tasks__map-trig').on('click', function (e) {
                var bth = $('.all-tasks__map-trig');
                if (map_block.closest('.all-tasks__sidebar').length > 0) {
                    bth.addClass('active');
                    $('.all-tasks__filter').after(map_block);
                } else {
                    bth.removeClass('active');
                    $('.all-tasks__map-wrap').prepend(map_block);
                }
                ;
                map_block.trigger('map_replace');
            });


        };
        initMap(locations);
    };

    return init;
});