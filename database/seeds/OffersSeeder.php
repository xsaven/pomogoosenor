<?php

use App\User;
use App\UserTasks;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class OffersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $users = User::whereHas('profile')->pluck('id')->toArray();
        $tasks = UserTasks::where('creator_id',71)
            ->where('status',1)
            ->pluck('id')->toArray();

        foreach (range(1, 50) as $index) {
            $offers = \App\TaskOffers::all()->pluck('id')->toArray();

            \App\TaskOffers::create([
                'user_id' => $faker->randomElement($users),
                'task_id' => $faker->randomElement($tasks),
                'other_notification' => $faker->boolean(4),
                'deactivate_date'=>$faker->dateTimeThisMonth,
                'cost' => $faker->randomFloat(2,1, 4),
                'message' => $faker->text
            ]);
        }
    }
}
