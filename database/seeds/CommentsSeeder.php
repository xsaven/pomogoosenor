<?php

use App\User;
use App\UserTasks;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class CommentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $users = User::all()->pluck('id')->toArray();
        $tasks = UserTasks::all()->pluck('id')->toArray();

        foreach (range(1, 30) as $index) {
            $comments = \App\TaskComments::all()->pluck('id')->toArray();
            \App\TaskComments::create([
                'user_id' => $faker->randomElement($users),
                'task_id' => $faker->randomElement($tasks),
                'answer_for_id' => ($index % 2 == 0)?null:$faker->randomElement($comments),
                'message' => $faker->text
            ]);
        }
    }
}
