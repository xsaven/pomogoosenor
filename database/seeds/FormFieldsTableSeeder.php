<?php

use Illuminate\Database\Seeder;

class FormFieldsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('form_fields')->delete();
        
        \DB::table('form_fields')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Параметры груза',
                'slug' => 'cargo-parameters',
                'shape' => 'parametry-gruza',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Дата и время',
                'slug' => 'date-and-time',
                'shape' => 'date-and-time',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Адрес',
                'slug' => 'address',
                'shape' => 'address',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Исполнитель должен иметь документы для оформления расписки.',
                'slug' => 'documents-for-registration',
                'shape' => 'documents-for-registration',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}