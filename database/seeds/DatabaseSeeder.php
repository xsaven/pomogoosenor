<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(FormFieldsTableSeeder::class);
    }
}

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        \App\User::create([
            'role_id' => '1',
            'name' => 'Andriy',
            'email' => 'admin@admin.admin',
            'password' => bcrypt('admin'),
        ]);
        $this->call(FormFieldsTableSeeder::class);
    }
}

