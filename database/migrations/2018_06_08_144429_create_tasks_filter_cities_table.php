<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateTasksFilterCitiesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Model::unguard();
        Schema::create('tasks_filter_cities',function(Blueprint $table){
            $table->increments("id");
            $table->string("name");
            $table->string("google_name");
            $table->tinyInteger("type")->default(1);
            $table->tinyInteger("active")->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tasks_filter_cities');
    }

}