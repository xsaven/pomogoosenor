<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkerProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worker_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->text('about_me')->nullable();
            $table->string('min_price')->nullable();
            $table->text('about_price')->nullable();
            /*
             * Relation fields
             */
            $table->integer('profile_id')->unsigned();
            /*
             * Foreign key
             */
            $table->foreign('profile_id')
                ->references('id')
                ->on('profiles')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('worker_profiles', function (Blueprint $table) {
            $table->dropForeign('worker_profiles_profile_id_foreign');
        });
        Schema::dropIfExists('worker_profiles');
    }
}
