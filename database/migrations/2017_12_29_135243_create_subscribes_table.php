<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscribesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscribes', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('system_notification')->nullabe()->default(0);
            $table->boolean('sms_new_message')->nullabe()->default(0);
            $table->boolean('news')->nullabe()->default(0);
            $table->boolean('email')->nullabe()->default(0);
            $table->boolean('push')->nullabe()->default(0);
            $table->integer('user_id')->unsigned();
            $table->timestamps();


            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscribes', function (Blueprint $table) {
            $table->dropForeign('subscribes_user_id_foreign');
        });
        Schema::dropIfExists('subscribes');
    }
}
