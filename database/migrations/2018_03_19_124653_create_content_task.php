<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_task',function(Blueprint $table){
            $table->increments("id");
            $table->unsignedInteger('task_id');
            $table->unsignedInteger('content_id');
            $table->foreign('task_id')
                ->references('id')
                ->on('user_tasks')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('content_id')
                ->references('id')
                ->on('content')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('content_task');
    }
}
