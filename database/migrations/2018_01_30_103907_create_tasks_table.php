<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Model::unguard();
        Schema::create('tasks',function(Blueprint $table){
            $table->increments("id");
            $table->string("name");
            $table->integer("price")->nullable();
            $table->tinyInteger('status')->default(0);
            $table->string('address')->nullable();
            $table->decimal('latitude',17,14)->nullable();
            $table->decimal('longitude',17,14)->nullable();
            $table->boolean('is_sbr')->default(0);
            $table->boolean('is_regular')->default(0);
            $table->unsignedInteger('price_range_id')->nullable();
            $table->unsignedInteger('subcategory_id');
            $table->unsignedInteger('creator_id');
            $table->unsignedInteger('executor_id')->nullable();
            $table->foreign('price_range_id')
                ->references('id')
                ->on('price_ranges');
            $table->foreign('subcategory_id')
                ->references('id')
                ->on('subcategories');
            $table->foreign('creator_id')
                ->references('id')
                ->on('users');
            $table->foreign('executor_id')
                ->references('id')
                ->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tasks');
    }
}
