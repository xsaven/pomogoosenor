<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->text('private_description');
            $table->string('photo');
            $table->longText('data');
            $table->string('cost');
            $table->integer('email_report')->default('0');
            $table->integer('executor_only')->default('0');
            $table->integer('commentaries')->default('0');
            $table->integer('subcategory_id');
            $table->integer('creator_id');
            $table->integer('executor_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_tasks');
    }
}
