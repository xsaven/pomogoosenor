<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_region')->unsigned();
            $table->integer('id_country');
            $table->string('name');
            $table->integer('priority')->nullable();
            $table->string('name_url')->nullable();
            $table->timestamps();
        });

        Schema::table('city', function (Blueprint $table) {
            $table->foreign('id_region')
                ->references('id')
                ->on('region')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('city', function (Blueprint $table) {
            $table->dropForeign('city_id_region_foreign');
        });
        Schema::dropIfExists('city');
    }
}
