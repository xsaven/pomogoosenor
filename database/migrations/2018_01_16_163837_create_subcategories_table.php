<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateSubcategoriesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Model::unguard();
        Schema::create('subcategories',function(Blueprint $table){
            $table->increments("id");
            $table->string("name");
            $table->string("slug");
            $table->string("question");
            $table->string("description");
            $table->string("example_title");
            $table->text("hint_description");
            $table->decimal("price_from", 15, 2);
            $table->decimal("price_min", 15, 2);
            $table->string("order");
            $table->tinyInteger("default")->default(0)->nullable();
            $table->integer("categories_id")->references("id")->on("categories");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subcategories');
    }

}