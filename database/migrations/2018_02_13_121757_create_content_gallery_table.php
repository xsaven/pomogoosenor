<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentGalleryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Model::unguard();
        Schema::create('content_gallery',function(Blueprint $table){
            $table->increments("id");
            $table->unsignedInteger('gallery_id');
            $table->unsignedInteger('content_id');
            $table->boolean('is_avatar')->default(false);
            $table->foreign('gallery_id')
                ->references('id')
                ->on('galleries')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('content_id')
                ->references('id')
                ->on('content')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('content_gallery');
    }
}
