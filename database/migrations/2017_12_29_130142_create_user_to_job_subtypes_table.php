<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserToJobSubtypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_to_job_subtypes', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('subtype_id')->unsigned();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('subtype_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_to_job_subtypes', function (Blueprint $table) {
            $table->dropForeign('user_to_job_subtypes_user_id_foreign');
            $table->dropForeign('user_to_job_subtypes_subtype_id_foreign');
        });
        Schema::dropIfExists('user_to_job_subtypes');
    }
}
