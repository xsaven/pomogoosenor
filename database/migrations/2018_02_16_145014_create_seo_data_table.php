<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateSeoDataTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Model::unguard();
        Schema::create('seodata',function(Blueprint $table){
            $table->increments("id");
            $table->string("url_mask");
            $table->text("title")->nullable();
            $table->text("description")->nullable();
            $table->string("og_title")->nullable();
            $table->text("og_description")->nullable();
            $table->string("og_url")->nullable();
            $table->string("og_image")->nullable();
            $table->enum("twitter_card", ["summary", "summary_large_image", "app"])->nullable();
            $table->string("twitter_title")->nullable();
            $table->string("twitter_description")->nullable();
            $table->string("twitter_image")->nullable();
            $table->string("twitter_url")->nullable();
            $table->string("image_src")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('seodata');
    }

}