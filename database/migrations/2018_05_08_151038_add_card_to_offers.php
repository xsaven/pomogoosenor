<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCardToOffers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers_of_performers', function (Blueprint $table) {
            $table->unsignedInteger('card_id')->nullable();
            $table->foreign('card_id')
                ->references('id')
                ->on('user_credit_cards')
                ->onUpdate('set null')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers_of_performers', function (Blueprint $table) {
            $table->removeColumn('card_id');
        });
    }
}
