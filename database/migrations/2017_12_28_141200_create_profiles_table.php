<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('surname')->nullable();
            $table->date('birthday')->nullable();
            $table->boolean('sex')->nullable();
            $table->string('phone_code')->nullable();
            $table->string('phone')->nullable();
            $table->string('avatar_thumb')->nullable();
            $table->string('avatar_full')->nullable();
            $table->boolean('is_worker')->nullable();
            $table->boolean('moderated_worker')->nullable();
            $table->integer('testing_step')->nullable()->defautl('0');
            $table->integer('quest_step')->nullable()->defautl('0');
            /*
             *  TODO Payment in another table (If need)
             */
            $table->integer('money')->nullable();
            /*
             * Need remove (dublicate users)
             */
            $table->string('name')->nullable();


            /*
             * Relation fields
             */
            $table->integer('user_id')->unsigned();
            $table->integer('city_id')->unsigned()->nullable();
            /*
             * Foreign key
             */
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('city_id')
                ->references('id')
                ->on('city')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->dropForeign('profiles_user_id_foreign');
            $table->dropForeign('profiles_city_id_foreign');
        });
        Schema::dropIfExists('profiles');
    }
}
