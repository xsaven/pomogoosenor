<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('region', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_country')->unsigned();
            $table->string('name');
            $table->timestamps();
        });
        Schema::table('region', function (Blueprint $table) {
            $table->foreign('id_country')
                ->references('id')
                ->on('country')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('region', function (Blueprint $table) {
            $table->dropForeign('region_id_country_foreign');
        });
        Schema::dropIfExists('region');
    }
}
