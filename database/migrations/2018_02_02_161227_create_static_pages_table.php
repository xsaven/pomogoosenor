<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateStaticPagesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Model::unguard();
        Schema::create('staticpages',function(Blueprint $table){
            $table->increments("id");
            $table->string("title");
            $table->string("slug");
            $table->text("description")->nullable();
            $table->text("keywords")->nullable();
            $table->text("content");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('staticpages');
    }

}