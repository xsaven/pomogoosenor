<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCardColumnsToUserTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_tasks', function (Blueprint $table) {
            $table->unsignedInteger('creator_card_id')->nullable();
            $table->unsignedInteger('executor_card_id')->nullable();
            $table->foreign('creator_card_id')
                ->references('id')
                ->on('user_credit_cards')
                ->onUpdate('set null')
                ->onDelete('set null');
            $table->foreign('executor_card_id')
                ->references('id')
                ->on('user_credit_cards')
                ->onUpdate('set null')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_tasks', function (Blueprint $table) {
            $table->removeColumn('creator_card_id');
            $table->removeColumn('executor_card_id');
        });
    }
}
