<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCityIdColumnToUserTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_tasks', function (Blueprint $table) {
            $table->dropForeign('user_tasks_city_id_foreign');
            $table->foreign('city_id')
                ->references('id')
                ->on('tasks_filter_cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_tasks', function (Blueprint $table) {
            $table->dropForeign('user_tasks_city_id_foreign');
            $table->foreign('city_id')
                ->references('id')
                ->on('city');
        });
    }
}
