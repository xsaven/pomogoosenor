<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceRangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Model::unguard();
        Schema::create('price_ranges',function(Blueprint $table){
            $table->increments("id");
            $table->integer('order');
            $table->integer('price_from');
            $table->integer('price_to');
            $table->string('description');
            $table->string('description_small');
            $table->unsignedInteger('price_range_group_id');

            $table->foreign('price_range_group_id')
                ->references('id')
                ->on('price_range_groups')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('price_ranges');
    }
}
