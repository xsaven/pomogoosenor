<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTaskDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_task_deals', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->text('deal_data');
            $table->unsignedSmallInteger('status');
            $table->unsignedInteger('task_id');
            $table->foreign('task_id')
                ->references('id')
                ->on('user_tasks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_task_deals');
    }
}
