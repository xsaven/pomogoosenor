<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Model::unguard();
        Schema::create('reviews',function(Blueprint $table){
            $table->increments("id");
            $table->boolean("positive"); // положительный или отрицательный
            $table->boolean("type"); // исполнитель или закажчик

            $table->string("text");
            $table->tinyInteger("politeness")->nullable(); // Вежливость
            $table->tinyInteger("punctuality")->nullable(); // Пунктуальность
            $table->tinyInteger("adequacy")->nullable(); // Адекватность
            $table->tinyInteger("price")->nullable(); // Адекватность
            $table->tinyInteger("quality")->nullable(); // Стоимость услуг
            $table->unsignedInteger('task_id');
            $table->foreign('task_id')
                ->references('id')
                ->on('tasks')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reviews');
    }
}
