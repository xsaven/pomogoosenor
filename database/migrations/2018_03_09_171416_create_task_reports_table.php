<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateTaskReportsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Model::unguard();
        Schema::create('taskreports',function(Blueprint $table){
            $table->increments("id");
            $table->enum("reason", ["inadequate", "illegal", "abuse", "terms", "spam", "other"]);
            $table->text("description")->nullable();
            $table->integer("user_id")->references("id")->on("user");
            $table->integer("categories_id")->references("id")->on("categories");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('taskreports');
    }

}